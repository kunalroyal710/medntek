<?php 
	use App\Models\Category;
	use App\Models\Product;
	use App\Models\User;

    function formatAmt($amount){
        list ($number, $decimal) = explode('.', sprintf('%.2f', floatval($amount)));
        $sign = $number < 0 ? '-' : '';
        $number = abs($number);
        for ($i = 3; $i < strlen($number); $i += 3){
            $number = substr_replace($number, ',', -$i, 0);
        }
        if($decimal==00){
            return  $sign . $number;
        }else{
            return  $sign . $number;
        }
    }
    
	function categories(){
    	$getcategories = Category::with(['subcategories'=>function($query){
    		$query->with('subcategories');
    	}])->where(['parent_id'=>NULL,'status'=>1])->orderby('sort','ASC')->get();
    	$getcategories = json_decode(json_encode($getcategories),true);
    	return $getcategories;
    }

	function products(){
		$products = Product::where('status',1)->select('id','product_name','product_code')->get();
		$products = json_decode(json_encode($products),true);
		return $products;
	}

	function users(){
		$users = User::select('id','email','name')->where('status',1)->get();
		return $users;
	}

	function brands($getproducts){
        $productArr = json_decode(json_encode($getproducts),true);
        $brandids = array_column($productArr['data'], 'brand_id');
		$brands = DB::table('brands')->select('id','name')->where('status',1)->wherein('id',$brandids)->orderby('name','ASC')->get();
		$brands = json_decode(json_encode($brands),true);
		return $brands;
	}

    function getbrands(){
        $brands = DB::table('brands')->select('id','name')->where('status',1)->orderby('name','ASC')->get();
        $brands = json_decode(json_encode($brands),true);
        return $brands;
    }

	function clean($string) {
       $string = str_replace(' ', '-', strtolower($string)); // Replaces all spaces with hyphens.
       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    function media_exists($folder,$imgname){
    	if(file_exists(public_path().$folder.$imgname)){
    		return true;
    	}else{
    		return false;
    	}
    }

    function exceptionError($message,$code=422){
        $error = [
            'status'       => false,
            'code'          => $code,
            'message'       => $message,
        ];
        return $error;
    }

    function exceptionMessage($e){
        $message = $e->getMessage() . " Line Number ". $e->getLine()." File :- ".$e->getFile();
        return exceptionError($message);
    }

    function fireTextSms($type,$data){
        if($type =="signup"){
            $smsdetails['mobile'] = $data['mobile'];
            $smsdetails['message'] = "Dear Customer , 
Thank you for signing up with www.imaecmedntek.com. Complete solution providers for Infection Control and Dialysis.
Imaec Medntek Ltd.";
            $smsdetails['dlttempid'] = 1007849340923245636;
        }elseif($type=="order"){
            $smsdetails['mobile'] = $data['mobile'];
            $smsdetails['message'] = "Dear Customer,
Thank you for shopping with us. Your order is confirmed and will be shipped shortly. 
Imaec Medntek Ltd.";
            $smsdetails['dlttempid'] = 1007958304933407433;   
        }elseif($type=="order-cancel"){
            $smsdetails['mobile'] = $data['mobile'];
            $smsdetails['message'] = "Dear Customer,
Your Order ".$data['order_id']." has been cancelled as per your request. 
You can connect to us on our customer service id info@imaecmedntek.com or contact us on 8956234040 for any queries.
Imaec Medntek Ltd.";
            $smsdetails['dlttempid'] = 1007233472030392904;   
        }elseif($type=="order-delivered"){
            $smsdetails['mobile'] = $data['mobile'];
            $smsdetails['message'] = "Dear Customer , Thank you for shopping with Imaec Medntek . We hope you have a lovely day. For more deals do visit our website again www.imaecmedntek.com . Also, follow us on Facebook , Linked In and twitter. 
Imaec Medntek Ltd.";
            $smsdetails['dlttempid'] = 1007356462993823912;   
        }elseif($type=="order-shipped"){
            $smsdetails['mobile'] = $data['mobile'];
            $smsdetails['message'] = "Dear Customer ,
Your package has now been shipped. View tracking details AWB No. ".$data['awb_number']." . You can track your package on the link below. https://www.imaecmedntek.com/track-order?order_id=".$data['order_id']."
Imaec Medntek Ltd.";
            $smsdetails['dlttempid'] = 1007141842920502384;   
        }
        sendSms($smsdetails);
    }

    function sendSms($smsdetails){
        $request ="";
        $param['username']="medntek";
        $param['password']="medntek123";
        $param['mob']=$smsdetails['mobile'];
        $param['msg']=$smsdetails['message'];
        $param['sender']="IMAECP";
        $param['templateid']=$smsdetails['dlttempid'];
        foreach($param as $key=>$val) {
            $request.= $key."=".urlencode($val);
            $request.= "&";
        }
        $request = substr($request, 0, strlen($request)-1);

        $url ="http://www.sms123.in/QuickSend.aspx?".$request;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
    }

    function convert_number_to_words($number) {
        $hyphen      = '-';
        $conjunction = ' and ';
        $separator   = ', ';
        $negative    = 'negative ';
        $decimal     = ' point ';
        $dictionary  = array(
            0                   => 'zero',
            1                   => 'one',
            2                   => 'two',
            3                   => 'three',
            4                   => 'four',
            5                   => 'five',
            6                   => 'six',
            7                   => 'seven',
            8                   => 'eight',
            9                   => 'nine',
            10                  => 'ten',
            11                  => 'eleven',
            12                  => 'twelve',
            13                  => 'thirteen',
            14                  => 'fourteen',
            15                  => 'fifteen',
            16                  => 'sixteen',
            17                  => 'seventeen',
            18                  => 'eighteen',
            19                  => 'nineteen',
            20                  => 'twenty',
            30                  => 'thirty',
            40                  => 'fourty',
            50                  => 'fifty',
            60                  => 'sixty',
            70                  => 'seventy',
            80                  => 'eighty',
            90                  => 'ninety',
            100                 => 'hundred',
            1000                => 'thousand',
            1000000             => 'million',
            1000000000          => 'billion',
            1000000000000       => 'trillion',
            1000000000000000    => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );
        
        if (!is_numeric($number)) {
            return false;
        }
        
        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }
        
        $string = $fraction = null;
        
        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }
    
        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= convert_number_to_words($remainder);
                }
                break;
        }
    
        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }
        return $string;
    }

    function banners($type){
        $banners = \App\Models\BannerImage::where('type',$type)->where('status',1)->orderby('sort','DESC')->whereDate('start_date','<=',date('Y-m-d'))->whereDate('end_date', '>=',date('Y-m-d'))->get();
        $banners = json_decode(json_encode($banners),true);
        return $banners;
    }

    function validationResp($validator){
        foreach($validator->errors()->toArray() as $v => $a){
            $validationError = [
                'status'       => false,
                'message'       => $a[0],
            ];
            return $validationError;
        }
    }

    function apiSuccessResponse($message,$data=NULL){
        $success = [
            'status'       => true,
            'code'          => 200,
            'message'       => $message,  
            'data'          => $data
        ];
        return $success;
    }

    function validationResponse($validator){
        foreach($validator->errors()->toArray() as $v => $a){

            $validationError = [
                'status'       => false,
                'code'          => 422,
                'message'       => $a[0],
            ];
            return $validationError;
        }
    }

    function apiErrorResponse($message,$code=422){
        $error = [
            'status'       => false,
            'code'          => $code,
            'message'       => $message,
        ];
        return $error;
    }

    function footerInformations(){
        $informationArr = array('about-us','infrastructure','quality','careers','contact');
        $default_orders = "'" . implode ( "', '", $informationArr ) . "'";
        $pages = DB::table('cms_pages')->wherein('slug',$informationArr)->orderByRaw("FIELD(slug, $default_orders)")->where('status',1)->get();
        $pages = json_decode(json_encode($pages),true);
        return $pages;
    }

    function footerCustomerCare(){
        $informationArr = array('shipping-delivery','returns-replacements','cancellation','payments-refunds','account-settings');
        $default_orders = "'" . implode ( "', '", $informationArr ) . "'";
        $pages = DB::table('cms_pages')->wherein('slug',$informationArr)->orderByRaw("FIELD(slug, $default_orders)")->where('status',1)->get();
        $pages = json_decode(json_encode($pages),true);
        return $pages;
    }

    function checkCmsActive($slug){
        $status = DB::table('cms_pages')->where('slug',$slug)->where('status',1)->count();
        if($status == 0){
            return 0;
        }else{
            return 1;
        }
    }

?>