<?php
namespace App\Repositories;

interface ProductRepositoryInterface
{
    /**
     * Get's a post by it's ID
     *
     * @param int
     */
    public function get($proid);

    /**
     * Get's all categories.
     *
     * @return mixed
     */
    public function get_cat_products($catid);

}