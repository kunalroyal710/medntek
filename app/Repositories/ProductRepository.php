<?php
namespace App\Repositories;
use App\Models\Product;
use App\Models\Category;
use DB;
use Illuminate\Support\Arr;
class ProductRepository implements ProductRepositoryInterface {

	public function get($productid){
        $getproductdetails = Product::withCount(['pro_attrs as total_stock'=>function($query){
                $query->select(DB::raw('sum(stock)'));
            }])->with(['productimages','pro_attrs','category'=>function($query){
            $query->with('subcat');
        },'product_reviews','relatedproducts'=>function($query) use($productid){
            $query->where('id','!=',$productid)->with('pro_attrs');
        }])->where('id',$productid)->where('status',1)->first();
        $getproductdetails = json_decode(json_encode($getproductdetails),true);
        $response = array('status'=>false);
        if(!empty($getproductdetails)){
            if($getproductdetails['category']['status']==1){
                $response = array('status'=>true,'productdetails'=>$getproductdetails);
            }
        }
        return $response;
    }

    public function get_cat_products($catid){
        $products = Product::select('id','product_name','category_id','brand_id','product_name','product_code','discount','price','final_price','stock','discount_type')->with(['product_image','pro_attrs'])->where('category_id',$catid)->where('status',1)->limit(5)->get();
        $products = json_decode(json_encode($products),true);
        return $products; 
    }

    public function fetchProducts($respdata,$filterdata){
        $products = Product::where('products.status',1);
        $products = $products->select('products.id','products.product_name','products.category_id','products.brand_id','products.product_code','products.discount','products.price','products.final_price','products.stock','products.discount_type');
        $products = $products->with(['product_image','pro_attrs']);
        $products = $this->category_filters($products,$respdata,$filterdata);
        $products = $this->price_filters($products,$filterdata);
        $products = $this->discount_filters($products,$filterdata);
        $products = $this->sort_filters($products,$filterdata);
        return $products;
    }

    public function fetchSearchProducts($respdata,$filterdata){
        $products = Product::where('products.status',1);
        $products = $products->select('products.id','products.product_name','products.category_id','products.brand_id','products.product_code','products.discount','products.price','products.final_price','products.stock','products.discount_type');
        $products = $products->with(['product_image','pro_attrs']);
        $Words[] = $filterdata['q'];
        $Words[] = explode(" ",$filterdata['q']);
        $Words = array_unique(Arr::flatten($Words));
        if(isset($Words[0])){
            $products->where(function($query) use ($Words) {
                $query->where('product_name','LIKE','%'.trim($Words[0]).'%');
                unset($Words[0]);
                foreach($Words as $searchword){ 
                    $query->orwhere('product_name','LIKE','%'.trim($searchword).'%');
                }
            });
        }
        if(isset($filterdata['category']) && !empty($filterdata['category'])){
            $products = $this->category_filters($products,$respdata,$filterdata);
        }
        $products = $this->price_filters($products,$filterdata);
        $products = $this->discount_filters($products,$filterdata);
        $products = $this->sort_filters($products,$filterdata);
        return $products;
    }


    public function category_filters($products,$respdata,$filterdata){
        $catids = $respdata['catids'];
        if(isset($filterdata['category']) && !empty($filterdata['category'])){
            $catNames = explode('~',$filterdata['category']);
            $catids = Category::wherein('name',$catNames)->pluck('id')->toArray();
        }
        $products->leftjoin('product_categories','product_categories.product_id','=','products.id')
        ->join('categories','categories.id','=','product_categories.category_id')->wherein('product_categories.category_id',$catids)->groupby('product_categories.product_id');
        return $products;
    }

    public function price_filters($products,$data){
        if(isset($data['price']) && !empty($data['price'])){
            $priceArr = explode('~',$data['price']);
            $products = $products->where(function($q) use($priceArr) {
                $price0Explode =  explode('-',$priceArr[0]);
                $q->whereBetween('products.final_price', [$price0Explode[0], $price0Explode[1]]);
                if(isset($priceArr[1])){
                  $price1Explode =  explode('-',$priceArr[1]);
                    $q->orwhereBetween('products.final_price', [$price1Explode[0], $price1Explode[1]]);  
                }
                if(isset($priceArr[2])){
                  $price2Explode =  explode('-',$priceArr[2]);
                    $q->orwhereBetween('products.final_price', [$price2Explode[0], $price2Explode[1]]);  
                }
                if(isset($priceArr[3])){
                  $price3Explode =  explode('-',$priceArr[3]);
                    $q->orwhereBetween('products.final_price', [$price3Explode[0], $price3Explode[1]]);  
                }
            });
        }
        return $products;
    }


    public function discount_filters($products,$data){
        if(isset($data['discount']) && !empty($data['discount'])){
            $discounts = explode('~',$data['discount']);
            $disValues = array();
            foreach ($discounts as $dkey => $discount) {
                if(is_numeric($discount)){
                    for($d=$discount; $d<= $discount+9; $d++){
                        $disValues[] = $d;
                    }
                }
            }
            $products->whereIn('products.discount',$disValues);
        }   
        return $products;
    }

    function sort_filters($products,$data){
        if(isset($data['sort']) && !empty($data['sort'])){
            $sortname =$data['sort'];
            if($sortname=="newest"){
                $products->where('products.new_arrival','yes');
            }else if($sortname=="asc"){
                $products->orderby('products.product_name','asc');
            }else if($sortname=="desc"){
                $products->orderby('products.product_name','Desc');
            }else if($sortname=="lth"){
                $products->orderby('products.final_price','ASC');
            }else if($sortname=="htl"){
                $products->orderby('products.final_price','DESC');
            }
        }else{
            $products->orderBy('products.product_sort', 'DESC');
        }
        return $products;
    }
}
?>