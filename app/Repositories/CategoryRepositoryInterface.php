<?php
namespace App\Repositories;

interface CategoryRepositoryInterface
{
    /**
     * Get's a post by it's ID
     *
     * @param int
     */
    public function get($catid);

    /**
     * Get's all categories.
     *
     * @return mixed
     */
    public function all();

    /**
     * Get's Category info.
     *
     * @return mixed
     */
    public function catinfo($seo);

}