<?php
namespace App\Repositories;
use App\Models\Category;

class CategoryRepository implements CategoryRepositoryInterface {

	public function get($category_id){
        return Category::find($category_id);
    }

    public function all(){
        $getcategories = Category::with(['subcategories'=>function($query){
    		$query->with('subcategories');
    	}])->where(['parent_id'=>NULL,'status'=>1])->orderby('sort','ASC')->get();
    	$getcategories = json_decode(json_encode($getcategories),true);
    	return $getcategories;
    }

    public function catinfo($catseo){
        $getCatdetail = Category::with(['subcategories'=>function($query){
                $query->with('subcategories');
            }])->where('seo_unique',$catseo)->where('status',1)->first();
        $getCatdetail = json_decode(json_encode($getCatdetail),true);
        if(empty($getCatdetail)){
            $resp = array('status'=>false);
            return $resp;
        }
        //echo "<pre>"; print_r($getCatdetail);
        $catids =array();
        foreach($getCatdetail['subcategories'] as $subcat){
            $catids[] = $subcat['id'];
            foreach($subcat['subcategories'] as $subsubcat){
                $catids[] = $subsubcat['id'];
            }
        }
        $catids[] = $getCatdetail['id'];
        //echo "<pre>"; print_r($catids); die;
        $resp = array('status'=>true,'catids'=>$catids,'catdetail'=>$getCatdetail);
        return $resp;
    }
}
?>