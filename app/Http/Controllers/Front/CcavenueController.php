<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\User;
use Session;
use Illuminate\Support\Facades\Mail;
use Auth;
use Redirect;
use Softon\Indipay\Facades\Indipay; 
use App\Models\OrderHistory;
class CcavenueController extends Controller
{
    //
    public function __construct(Order $order,OrderHistory $orderhistory){
        $this->order      = $order;
        $this->orderhistory = $orderhistory;
    }
    
    public function ccavenuePayment(Request $request){
        if($request->isMethod('get')){
            $data = $request->all();
             if(isset($data['id']) && !empty($data['id'])){
                $orderId =  decrypt($data['id']);
    			$orderdetails = Order::where(['id'=>$orderId])->first();
                $parameters = [
                    'tid' => $orderId,
                    'order_id' => $orderId,
                    'merchantTxnId' => $orderId,
                    'amount' => $orderdetails->grand_total,
                    'currency' => 'INR',
                    'shipName'=> $orderdetails->name,
                    'shipAddress' => $orderdetails->address_line_1,
                    'shipState' => $orderdetails->state,
                    'shipCountry' => 'India',
                    'shipCity' => $orderdetails->city,
                    'shipZip' => $orderdetails->postcode,
                    'shipTel' => $orderdetails->mobile,
                    'shipping_email' => $orderdetails->email
                ];
                $order = Indipay::prepare($parameters);
                return Indipay::process($order);
            }else{
                return redirect()->action('Front\CartController@cart');
            }
        }
    }

    public function ccavenueresponse(Request $request){
        $response = Indipay::response($request);
        $response = Indipay::gateway('CCAvenue')->response($request);
        //echo "<pre>"; print_r($response); die;
        if(isset($response) && $response['order_id']){
            $orderDetails = Order::where('id',$response['order_id'])->first();
            $orderDetails = json_decode(json_encode($orderDetails),true);
            $encorderid = encrypt($response['order_id']);
            if($orderDetails && $orderDetails['grand_total'] == $response['amount']){
                if($response['order_status'] =="Success"){
                	$this->orderhistory->where('order_id',$response['order_id'])->delete();
			        $requestdata['order_status'] = 'Payment Captured';
			        $requestdata['comments'] = 'Payment has been received';
			        $requestdata['order_id'] =  $response['order_id'];
			        $this->orderhistory->createOrderHistory($requestdata);
                    Order::where('id',$response['order_id'])->update(['order_status'=>'Payment Captured','payment_status'=>'captured','txn_id'=>$response['tracking_id']]);
                    //Send Order Email
                    $this->order->sendOrderEmail($orderDetails['id']);
                    return redirect::to('/thanks?id='.$encorderid);
                }else{
                    Order::where('id',$response['order_id'])->update(['order_status'=>'Aborted']);
                    return redirect::to('/cancel?id='.$encorderid);
                }
            }else{
                return redirect::to('/cancel?id='.$encorderid);
            }
        }else{
            return redirect()->action('Front\CartController@cart');
        }
    } 

    public function ccavenueCancel(Request $request){
        $response = Indipay::response($request);
        $response = Indipay::gateway('CCAvenue')->response($request);
        if(isset($response) && $response['order_id']){
            Order::where('id',$response['order_id'])->update(['order_status'=>'Aborted']);
            $orderid = encrypt($response['order_id']);
            return redirect::to('/cancel?id='.$orderid);
        }else{
            return redirect()->action('Front\CartController@cart');
        }
    }
}
