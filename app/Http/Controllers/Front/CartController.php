<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use App\Models\User;
use App\Models\ProductAttribute;
use App\Models\Product;
use App\Models\Cart;
use App\Models\CouponCode;
use App\Models\Wishlist;
use App\Models\Order;
use Validator;
use Illuminate\Support\Facades\View;
use DB;
use Redirect;   
class CartController extends Controller
{
    //
	public function __construct(ProductAttribute $proattr,Cart $cart,Order $order){
        $this->proattr    = $proattr;
        $this->cart       = $cart;
        $this->order      = $order;
    }

    public function info(){
    	if(Auth::check()){
        	$info['user_id'] = Auth::user()->id;
        }else{
        	if(!Session::has('cartsessionId')){
                $session_id = Session::getId();
                Session::put('cartsessionId',$session_id);
            }else{
            	$session_id = Session::get('cartsessionId');
            }
            $info['session_id'] = $session_id;
        }
        return $info;
    }

    public function addtoCart(Request $request){
    	try{
    		if($request->isMethod('post')){
	            $data =  $request->all();
	            $sizes = $this->proattr->prosizes($data['proid']);
	            $validator = Validator::make($request->all(), [
	                    'size' => 'bail|required|in:'.implode(',',$sizes),
	                ],
	                [
	                    'size.required' => 'Please select product size.',
	                    'size.in' => 'Size might not be available or out of stock',
	                ]
	            );
	            if($validator->passes()){
	                $info = $this->info();
	                Session::forget('couponinfo');
	                $response = $this->cart->add_to_cart($data,$info);
	                return response()->json($response);
	            }else{
	                return response()->json(validationResp($validator));
	            }
	        }
    	}catch(\Exception $e){
            return response()->json(exceptionMessage($e),423);
        }
    }

    public function updateCart(Request $request){
    	if($request->ajax()){
    		$data = $request->all();
    		$cartinfo = $this->cart->where('id',$data['cartid'])->first();
    		$reqData['size'] = $cartinfo->size;
    		$reqData['proid'] = $cartinfo->product_id;
    		$reqData['qty'] = $data['qty'];
    		$info = $this->info();
	        Session::forget('couponinfo');
    		$response = $this->cart->add_to_cart($reqData,$info);
    		if($response['status']){
    			$cartitems = $this->cart->cartitems($info);
        		$totalItems = Cart::totalitems($info);
        		$cartSummary = $this->cart->cartdetails($cartitems);
    			return response()->json([
    				'status' => true,
	                'view' => (String)View::make('front.cart.cart-details')->with(compact('cartitems','cartSummary','totalItems')),
	                'totalItems' => $totalItems,
	                'message'   => 'Cart item has been updated successfully'
	            ]);
    		}else{
    			return response()->json([
    				'status' => false,
	                'message' => $response['message']
	            ]);
    		}
    	}
    }

    public function cart(Request $request){
    	try{
	        $info = $this->info();
        	$cartitems = $this->cart->cartitems($info);
        	$totalItems = Cart::totalitems($info);
        	$cartSummary = $this->cart->cartdetails($cartitems);
        	/*echo "<pre>"; print_r($cartitems); 
        	echo "<pre>"; print_r($cartSummary); die;*/
        	$title = "Shopping Cart";
            return view('front.cart.cart')->with(compact('title','cartitems','totalItems','cartSummary'));
    	}catch(\Exception $e){
            return response()->json(exceptionMessage($e),423);
        }
    }

    public function cartItemsAjax(Request $request){
    	try{
	        if($request->ajax()){
	        	$info = $this->info();
	        	$cartitems = $this->cart->cartitems($info);
	        	$totalItems = Cart::totalitems($info);
	        	$cartSummary = $this->cart->cartdetails($cartitems);
	        	//echo "<pre>"; print_r($cartSummary); die;
	            return response()->json([
	                'view' => (String)View::make('front.cart.cart-popup')->with(compact('cartitems','cartSummary')),
	                'totalItems' => $totalItems
	            ]);
	        }
    	}catch(\Exception $e){
            return response()->json(exceptionMessage($e),423);
        }
    }

    public function removeCartItem(Request $request){
    	try{
	        if($request->ajax()){
	        	$data = $request->all();
	        	if(isset($data['type'])){
	        		if($data['type'] == "remove"){
			        	$this->cart->where('id',$data['cartid'])->delete();
			        	$info = $this->info();
			        	$cartitems = $this->cart->cartitems($info);
			        	$totalItems = $this->cart->totalitems($info);
			        	$cartSummary = $this->cart->cartdetails($cartitems);
	        			return response()->json([
			            	'status' => true,
			                'view' => (String)View::make('front.cart.cart-details')->with(compact('cartitems','cartSummary','totalItems')),
			                'totalItems' => $totalItems,
			                'message' => 'Cart Item has been deleted successfully'
			            ]);
	        		}else{
	        			if(Auth::check()){
	        				$cartInfo = $this->cart->where('id',$data['cartid'])->first();
				        	$this->cart->where('id',$data['cartid'])->delete();
				        	$info = $this->info();
				        	$cartitems = $this->cart->cartitems($info);
				        	$totalItems = $this->cart->totalitems($info);
				        	$cartSummary = $this->cart->cartdetails($cartitems);
		        			Wishlist::saveWishlist($cartInfo->product_id);
		        			return response()->json([
				            	'status' => true,
				                'view' => (String)View::make('front.cart.cart-details')->with(compact('cartitems','cartSummary','totalItems')),
				                'totalItems' => $totalItems,
				                'message' => 'Cart Item has been moved to wishlists'
				            ]);
	        			}else{
	        				return response()->json([
			                    'status'=>false,
			                    'message' =>'You need to login to move product to wishlist'
			                ]);
	        			}
	        		}
	        	}else{
		        	$this->cart->where('id',$data['cartid'])->delete();
		        	$info = $this->info();
		        	$cartitems = $this->cart->cartitems($info);
		        	$totalItems = $this->cart->totalitems($info);
		        	$cartSummary = $this->cart->cartdetails($cartitems);
	        		return response()->json([
		            	'status' => true,
		                'view' => (String)View::make('front.cart.cart-popup')->with(compact('cartitems','cartSummary')),
		                'totalItems' => $totalItems,
		                'message' => 'Cart Item has been deleted successfully'
		            ]);
	        	}
	        }
    	}catch(\Exception $e){
            return response()->json(exceptionMessage($e),423);
        }
    }

    public function applyCoupon(Request $request){
        if($request->ajax()){
            if(Auth::check()){
                $data = $request->all();
                $validator = Validator::make($request->all(), [
                        'code' => 'bail|required',
                    ]
                );
                if($validator->passes()){
	            	$info = $this->info();
		        	$cartitems = $this->cart->cartitems($info);
                    $response = CouponCode::applycouponcode($data['code'],$cartitems);
                    $cartitems = $this->cart->cartitems($info);
		        	$totalItems = $this->cart->totalitems($info);
		        	$cartSummary = $this->cart->cartdetails($cartitems);
                    return response()->json([
                        'status'=>$response['status'],
                        'message' =>$response['message'],
                        'view' => (String)View::make('front.cart.cart-details')->with(compact('cartitems','cartSummary','totalItems')),
                        'totalItems' => $totalItems
                    ]);
                }else{
                    Session::forget('couponinfo');
                    return response()->json([
                        'status'=>false,
                        'message' =>'Coupon Code is required'
                    ]);
                }
            }else{
                return response()->json([
                    'status'=>false,
                    'message' =>'You need to login to apply this coupon'
                ]);
            }
        }
    }

    public function checkout(){
        try{
            $info = $this->info();
            $cartitems = $this->cart->cartitems($info);
            $totalItems = $this->cart->totalitems($info);
            $cartSummary = $this->cart->cartdetails($cartitems);
            if(!$cartitems){
                return redirect()->action('Front\CartController@cart')->with('flash_message_error','Please add products in cart before checkout.');
            }
            //echo "<pre>"; print_r($cartSummary); die;
            $title ="Checkout";
            $metakeywords ="";
            $metadescription="";
            return view('front.cart.checkout')->with(compact('title','metakeywords','metadescription','cartSummary','cartitems'));
        }catch(\Exception $e){
            return response()->json(exceptionMessage($e),423);
        }
    }

    public function placeOrder(Request $request){
        try{
            if($request->isMethod('post')){
                $data = $request->all();
                $info = $this->info();
                $cartitems = $this->cart->cartitems($info);
                $cartSummary = $this->cart->cartdetails($cartitems);
                $cartinfo['items'] =  $cartitems;
                $cartinfo['summary'] =  $cartSummary;
                $resp = $this->order->order_checkpoints($data,$cartinfo);
                if($resp['status']){
                    DB::beginTransaction();
                    $order = $this->order->createOrder($cartinfo,$data);
                    Session::forget('couponinfo');
                    DB::commit();
                    if(isset($data['paymentMode']) && !empty($data['paymentMode']) && $data['paymentMode'] =="ccavenue" ){
                        return redirect::to('ccavenue/payment?id='.encrypt($order->id));
                    }else{
                        //Send Order Email
                        $this->order->sendOrderEmail($order->id);
                        return redirect::to('/thanks?id='.encrypt($order->id));
                    }
                }else{
                    return redirect::to($resp['redirect'])->with('flash_message_error',$resp['message']);
                }
            }
        }catch(\Exception $e){
            return response()->json(exceptionMessage($e),423);
        }
    }

    public function thanks(Request $request){
        if($request->isMethod('get')){
            $title ="Thanks";
            $data = $request->all();
            if(isset($data['id']) && !empty($data['id'])){
                $orderid  = decrypt($data['id']);
                if(!Auth::check()){
                    $orderinfo = $this->order->where('id',$orderid)->first();
                    Auth::loginUsingId($orderinfo->user_id);
                }
                $thanksorderid = $orderid;
                return view('front.cart.thanks')->with(compact('title','thanksorderid'));
            }else{
                return redirect::to('/');
            }
        }
    } 

    public function cancel(Request $request){
        $data =$request->all();
        if(isset($data['id']) && !empty($data['id'])){
            $orderid  =decrypt($data['id']);
            if(!Auth::check()){
                $orderinfo = $this->order->where('id',$orderid)->first();
                Auth::loginUsingId($orderinfo->user_id);
            }
            $title = "Order Cancelled";
            return view('front.cart.cancel')->with(compact('title'));
        }else{
            return redirect::to('/');
        }   
    }
}
