<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\ProductRepositoryInterface;
use Illuminate\Support\Facades\Route;
use Validator;
use App\Models\Contact;
use App\Models\Career;
use App\Models\CmsPage;
use App\Models\NewsletterSubscriber;
use App\Models\Wishlist;
use Illuminate\Support\Facades\Mail;
use Auth;
use Redirect;
class IndexController extends Controller
{
    //
    protected $categories;
    public function __construct(CategoryRepositoryInterface $categories,ProductRepositoryInterface $products, Wishlist $wishlist){
        $this->categories = $categories;
        $this->products   = $products;
        $this->wishlist   = $wishlist;
    }

    public function coming_soon(){
        try{
            return view('front.coming_soon');
        }catch(\Exception $e){
            return response()->json(['status'=>false,'message'=>$e->getMessage(),'errors'=>array('name'=>$e->getMessage())]);
        }
    }

    public function home(){
    	try{
            if(isset($_GET['phpinfo'])){
                echo phpinfo(); die;
            }
			$categories = $this->categories->all();
			$catProducts = array();
			foreach($categories as $ckey => $category){
				$products = $this->products->get_cat_products($category['id']);
				$catProducts[$ckey] = $category;
				$catProducts[$ckey]['products'] = $products;
			}
            if(Auth::check()){
                $wishlists = $this->wishlist->wishproids(Auth::user()->id);
            }else{
                $wishlists = array();
            }
			$title = "Home";
	    	return view('front.home')->with(compact('title','catProducts','wishlists'));
    	}catch(\Exception $e){
            return response()->json(['status'=>false,'message'=>$e->getMessage(),'errors'=>array('name'=>$e->getMessage())]);
        }
    }

    /*public function careers(){
        $title = "Careers";
        return view('front.careers')->with(compact('title'));
    }*/

    public function saveCareer(Request $request){
        if($request->ajax()){
            $data = $request->all();
            $validator = Validator::make($request->all(), [
                    'name' => "bail|required",
                    'email' => "bail|required",
                    'position' => "bail|required",
                    'work_experience' => "bail|required",
                    'relevant_experience' => "bail|required",
                    'current_organisation' => "bail|required",
                    'qualification' => "bail|required",
                    'message' => "bail|required",
                    'mobile' => "bail|required|numeric|digits:10",
                ]
            );
            if($validator->passes()) {
                $CVfilePath = "";$CoverfilePath="";
                if($request->hasFile('cv')) {
                    $file = $request->file('cv');
                    $filename = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME);
                    $extension = $file->getClientOriginalExtension();
                    $cvfileName = $filename.rand(10000,4555555)."-".time().".".$extension;
                    $destinationPath = 'CareerMedia'.'/';
                    $file->move($destinationPath, $cvfileName);
                    $CVfilePath = public_path('/CareerMedia/'.$cvfileName);
                }
                if($request->hasFile('cover_letter')) {
                    $file = $request->file('cover_letter');
                    $filename = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME);
                    $extension = $file->getClientOriginalExtension();
                    $coverfileName = $filename.rand(10000,4555555)."-".time().".".$extension;
                    $destinationPath = 'CareerMedia'.'/';
                    $file->move($destinationPath, $coverfileName);
                    $CoverfilePath = public_path('/CareerMedia/'.$coverfileName);
                }
                unset($data['_token']);
                unset($data['cv']);
                unset($data['cover_letter']);
                if(isset($coverfileName)){
                    $data['cover_letter'] = $coverfileName;
                }
                if(isset($cvfileName)){
                    $data['cv'] = $cvfileName;
                }
                Career::create($data);
                if(env('MAIL_MODE') =='live'){
                    $emails = array('careers@imaecmedntek.com');
                    $messageData = [
                        'data' => $data,
                        'heading' => 'Hi Admin! You have received an Career Request. Below are the details'
                    ];
                    Mail::send('emails.career_email', $messageData, function($message) use ($emails,$CVfilePath,$CoverfilePath){
                        $message->to($emails)->subject('Career Request Received');
                        if(!empty($CVfilePath)){
                            $message->attach($CVfilePath);
                        }
                        if(!empty($CoverfilePath)){
                            $message->attach($CoverfilePath);
                        }
                    });
                }
                return response()->json(['status'=>true,'message'=>'Your career request has been submitted successfully. We will get back to you soon.']);
            }else{
                return response()->json(['status'=>false,'errors'=>$validator->messages()]);
            }
        }
    }

    public function media(){
        $title = "Media";
        return view('front.media')->with(compact('title'));
    }

    public function cms(){
        $slug = Route::getFacadeRoot()->current()->uri();
        $details = CmsPage::where('slug',$slug)->first();
        if($details){
            $cmsInfo = json_decode(json_encode($details),true);
            $title = $cmsInfo['title'];
            $metadescription = $cmsInfo['meta_description'];
            $metakeywords = $cmsInfo['meta_keywords'];
            if($cmsInfo['slug'] =="about-us"){
                $view = "front.cms.about-us";
            }elseif($cmsInfo['slug'] =="contact"){
                $view = "front.cms.contact";
            }elseif($cmsInfo['slug'] =="careers"){
                $view = "front.careers";
            }else{
                $view = "front.cms.cms";
            }
            return view($view)->with(compact('title','slug','metadescription','metakeywords','cmsInfo')); 
        }else{
            return redirect::to('/');
        }
    }

    public function contact(){
        $title = "Contact";
        $metadescription = "";
        $metakeywords = "";
        return view('front.cms.contact')->with(compact('title','metadescription','metakeywords')); 
    }

    public function saveContact(Request $request){
        if($request->ajax()){
            $data = $request->all();
            if($request->isMethod('post')){
                $data = $request->all();
                $validator = Validator::make($request->all(), [
                        'name' => "bail|required",
                        'email' => "bail|required",
                        'address' => "bail|required",
                        'company_name' => "bail|required",
                        'contact_number' => "bail|required|numeric|digits:10",
                        'interested_in' => "bail|required",
                        'message' => "bail|required",
                    ]
                );
                if($validator->passes()) {
                    Contact::create($data);
                    if(env('MAIL_MODE') =='live'){
                        $emails = array('info@imaecmedntek.com');
                        $messageData = [
                            'data' => $data,
                            'heading' => 'Hi Admin! You have received an Inquiry. Below are the details'
                        ];
                        Mail::send('emails.contact-email', $messageData, function($message) use ($emails){
                            $message->to($emails)->subject('Contact Inquiry Received');
                        });
                    }
                    return response()->json(['status'=>true,'message'=>'Your information has been submitted successfully. We will get back to you soon.']);
                }else{
                    return response()->json(['status'=>false,'errors'=>$validator->messages()]);
                }
            }
        }
    }

    public function addSubscriber(Request $request){
        if($request->ajax()){
            $data = $request->all();
            $checksubscriberexists = NewsletterSubscriber::where('email',$data['email'])->count();
            if($checksubscriberexists == 0){
                $status = "ok";
                $subscriber = new NewsletterSubscriber;
                $subscriber->email = $data['email'];
                $subscriber->status = 1;
                $subscriber->save();
                $message= "Subscribed successfully!";
            }else{
                $status = "not ok";
                $message= "This email is already subscribed";
            }
            return response()->json([
                'status' => $status,
                'message' =>$message
            ]);
        }
    }
}
