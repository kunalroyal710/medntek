<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Socialite;
use Session;
use DB;
use Auth;
use App\Models\Cart;
class SocialAuthController extends Controller{
    
    //
    public function googleredirect() {
        return Socialite::driver('google')->stateless()->redirect();   
    }   

    public function googlecallback(\App\SocialAccountService $service){
        $user = $service->createOrGetGoogleUser(Socialite::driver('google')->stateless()->user());
        auth()->login($user);
        $this->updatingCartSessionToUser();
        return redirect()->to('/');
    }

    public function updatingCartSessionToUser(){
        if(Session::has('cartsessionId')){
            Cart::where('session_id',Session::get('cartsessionId'))->update(['user_id'=>Auth::user()->id,'session_id'=>'']);
            DB::select("DELETE FROM carts WHERE id NOT IN (SELECT * FROM (SELECT MAX(n.id) FROM carts n GROUP BY n.product_id,n.size) x) and user_id = ".Auth::user()->id."");
        }
        $mesage = "success";
        return $mesage;
    }
}
