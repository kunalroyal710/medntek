<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Category;
use App\Models\Product;
use App\Models\RecentViewProduct;
use App\Models\ProductAttribute;
use App\Models\Cart;
use App\Models\CouponCode;
use App\Models\Order;
use App\Models\Wishlist;
use App\Models\FrequentProduct;
use App\Models\ProductReview;
use DB;
use Illuminate\Support\Facades\View;
use Session;
use Validator;
use Auth;
use Redirect;
use Illuminate\Support\Arr;
use App\Repositories\ProductRepositoryInterface;
use App\Repositories\CategoryRepositoryInterface;
class ProductsController extends Controller
{
    //
    public function __construct(ProductRepositoryInterface $products,CategoryRepositoryInterface $category,Wishlist $wishlist){
        $this->products   = $products;
        $this->category = $category;
        $this->wishlist = $wishlist;
    }

    public function products(Request $request){
    	try{
    		$catseo = Route::getFacadeRoot()->current()->uri();
    		$catdata = $this->category->catinfo($catseo);
    		if($catdata['status']){
    			$filterdata = $request->all();;
    			$products = $this->products->fetchProducts($catdata,$filterdata);
    			$products = $products->paginate(100);
	            $products = $products->appends(request()->query());
	            $isProductsPage = true;
                if(Auth::check()){
                    $wishlists = $this->wishlist->wishproids(Auth::user()->id);
                }else{
                    $wishlists = array();
                }
	            if($request->ajax()){
		            $start = $products->firstItem();
		            $end = $products->lastItem();
		            $total = $products->total();
		            if($start ==0){
		                $countstring = "";
		            }else{
		                $countstring = "(Showing ".$start." to ".$end." products of ".$total." products)";
		            }
		            return response()->json([
		                'view' => (String)View::make('front.products.product-listing')->with(compact('products','isProductsPage','wishlists')),
		                'pagination_view' => (String)View::make('front.products.products-pagination')->with(compact('products')),
		                'countproducts' => $countstring,
                        'queryStringData' => $filterdata
		            ]);
		        }else{
                    //echo "<pre>"; print_r(json_decode(json_encode($products),true)); die;
		        	$title = $catdata['catdetail']['name'];
                    $closeUrl = url('/'.$catseo);
		            return view('front.products.products')->with(compact('catdata','products','title','isProductsPage','closeUrl','wishlists'));
		        }
    		}else{
    			abort(404);
    		}
    	}catch(\Exception $e){
    		if($e->getStatusCode() ==404){
    			abort(404);
    		}
            return response()->json(exceptionMessage($e),423);
        }
    }

    public function product($id,$slug){
        try{
            $productResp = $this->products->get($id);
            //echo "<pre>"; print_r($productResp); die;
            if($productResp['status']){
                $title = $productResp['productdetails']['product_name'];
                if(Auth::check()){
                    $wishlists = $this->wishlist->wishproids(Auth::user()->id);
                }else{
                    $wishlists = array();
                }
                return view('front.products.product')->with(compact('title','productResp','wishlists'));
            }else{
                abort(404);
            }
        }catch(\Exception $e){
            if($e->getStatusCode() ==404){
                abort(404);
            }
            return response()->json(exceptionMessage($e),423);
        }
    }

    public function getProductPricing(Request $request){
        try{
            if($request->ajax()){
                $data = $request->all();
                $productResp = $this->products->get($data['proid']);
                $Attrkey = array_search($data['size'], array_column($productResp['productdetails']['pro_attrs'], 'size'));
                $proAttrInfo = $productResp['productdetails']['pro_attrs'][$Attrkey];
                if(!empty($productResp['productdetails']['discount'])){
                    $price = round($proAttrInfo['price'] - ($proAttrInfo['price'] * $productResp['productdetails']['discount'] /100));
                    $strikePrice =  $proAttrInfo['price'];
                    $discount = $productResp['productdetails']['discount'];
                }else{
                    $price = $proAttrInfo['price'];
                    $strikePrice =  0;
                    $discount = 0;
                }
                $priceHtml = '<div class="price">
                                <h5>₹ '.$price;
                                if(!empty($strikePrice)){
                                    $priceHtml .= '<span><del>₹ '.$strikePrice.'</del></span>';  
                                }
                            $priceHtml .= '</h5>    
                            </div>';
                            if($discount >0){
                                $priceHtml .= '<div class="savings">
                                        <h4>You save &#x20B9;'.formatAmt($strikePrice - $price).'<span>('.$discount.'% OFF)</span></h4>
                                    </div>'; 
                            }
                return response()->json(['status'=>true,'pricing_html'=>$priceHtml]);
            } 
        }catch(\Exception $e){
            if($e->getStatusCode() ==404){
                abort(404);
            }
            return response()->json(exceptionMessage($e),423);
        }
    }


    public function searchProducts(Request $request){
        try{
            $data = $request->all();
            $checkCat = Category::where('name',$data['q'])->first();
            if($checkCat){
                return redirect::to($checkCat->seo_unique);
            }else{
                $filterdata = $request->all();
                $catdata['catids'] = array();
                $products = $this->products->fetchSearchProducts($catdata,$filterdata);
                $products = $products->paginate(100);
                $products = $products->appends(request()->query());
                $isProductsPage = true;
                if(Auth::check()){
                    $wishlists = $this->wishlist->wishproids(Auth::user()->id);
                }else{
                    $wishlists = array();
                }
                if($request->ajax()){
                    $start = $products->firstItem();
                    $end = $products->lastItem();
                    $total = $products->total();
                    if($start ==0){
                        $countstring = "";
                    }else{
                        $countstring = "(Showing ".$start." to ".$end." products of ".$total." products)";
                    }
                    return response()->json([
                        'view' => (String)View::make('front.products.product-listing')->with(compact('products','isProductsPage','wishlists')),
                        'pagination_view' => (String)View::make('front.products.products-pagination')->with(compact('products')),
                        'countproducts' => $countstring
                    ]);
                }else{
                    $title = $filterdata['q'];
                    $catdata['catdetail']['name'] = $filterdata['q'];
                    $closeUrl = url('/search-results?q='.$filterdata['q']);
                    return view('front.products.products')->with(compact('catdata','products','title','isProductsPage','closeUrl','wishlists'));
                }
            } 
        }catch(\Exception $e){
            if($e->getStatusCode() ==404){
                abort(404);
            }
            return response()->json(exceptionMessage($e),423);
        }
    }

    public function addtoWishlist(Request $request){
        if($request->ajax()){
            if(Auth::check()){
                $data = $request->all();
                $checkifExits = Wishlist::where([
                    'user_id'=>Auth::user()->id,
                    'product_id' => $data['proid']
                ])->count();
                if($checkifExits ==0){
                    $wishlist = new Wishlist;
                    $wishlist->user_id = Auth::user()->id;
                    $wishlist->product_id = $data['proid'];
                    $wishlist->save();
                    return response()->json(['status'=>true,'message'=>'set']);
                }else{
                    Wishlist::where([
                        'user_id'=>Auth::user()->id,
                        'product_id' => $data['proid'],
                    ])->delete();
                    return response()->json(['status'=>true,'message'=>'unset']);
                }
            }else{
                return response()->json(['status'=>false,'message'=>'Kindly Login to add products in wishlist']);
            }
        }
    }

    public function submitProductReview(Request $request){
        try{
            if($request->ajax()){
                $data =  $request->all();
                $validator = Validator::make($request->all(), [
                        'product_id' =>  'bail|required',
                        'star' =>  'bail|required',
                        'review' => 'bail|required'
                    ]
                );
                if($validator->passes()){
                   $checkProReviewExists = ProductReview::where('product_id',$data['product_id'])->where('user_id',Auth::user()->id)->count();
                    if($checkProReviewExists == 0){
                        $proReview = new ProductReview;
                        $proReview->product_id = $data['product_id'];
                        $proReview->user_id = Auth::user()->id;
                        $proReview->star = $data['star'];
                        $proReview->review = $data['review'];
                        $proReview->save();
                        return response()->json([
                            'status' => true,
                            'message' => 'Product review has been submitted successfully'
                        ]);
                    }else{
                        return response()->json([
                            'status' => false,
                            'message' => 'You have already submitted review for this product'
                        ]);
                    } 
                }else{
                    return response()->json(validationResp($validator));
                }
            }
        }catch(\Exception $e){
            return response()->json(exceptionMessage($e),423);
        }
    }
}
