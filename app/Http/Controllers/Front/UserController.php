<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\User;
use App\Models\Cart;
use App\Models\ShippingAddress;
use App\Models\Wishlist;
use App\Models\Order;
use Auth;
use Illuminate\Support\Facades\Mail;
use DB;
use Illuminate\Support\Facades\View;
use Hash;
use Image;
use Redirect;
use App\Models\OrderProduct;
use App\Models\ReturnRequest;
class UserController extends Controller
{
    //
	public function __construct(User $user,Cart $cart,ShippingAddress $shippingAddr){
		$this->user = $user;
		$this->cart = $cart;
		$this->shippingAddr = $shippingAddr;
	}

    public function signin(Request $request){
		try{
			if($request->ajax()){
	            $validator = Validator::make($request->all(), [
		                'email' => 'bail|required|regex:/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i|',
		                'password' => 'bail|required',
	            	],
	            	[
                        'email.regex'=>'This email is not a valid email address',
                        'email.required'=>'Please enter email',
                    ]
	        	);
	            if($validator->passes()) {
	                $data = $request->all();
	                if(Auth::check()){
	                    return response()->json(['status'=>false,'type'=>'normal','errors'=>array('password'=>"We are sorry! Multiple Login not allowed.")]);
	                }else{
                        $loginAttempt = ['email' => $request->get('email'), 'password'=>$request->get('password')];
	                    if(Auth::attempt($loginAttempt)){
	                        if(Auth::user()->status ==0){
	                            Auth::logout();
	                            return response()->json(['status'=>false,'type'=>'normal','errors'=>array('email'=>"Your account is deactivated by system administrator")]);
	                        }
	                        $this->updatingCartSessionToUser();
							$redirectTo = url('/cart');
	                        return response()->json(['status'=>true,'message'=>'ok','url'=>$redirectTo]);
	                    }else{
	                        return response()->json(['status'=>false,'type'=>'normal','errors'=>array('password'=>"You have entered wrong email or password!")]);
	                    }
	                }
	            }else{
	                return response()->json(['status'=>false,'type'=>'validation','errors'=>$validator->messages()]);
	            }
	        }else{
	        	if(Auth::check()){
	        		return redirect('cart');
	        	}
	            $title = "Sign In / Sign Up";
	            return view('front.account.signin')->with(compact('title'));
	        }
		}catch(\Exception $e){
            return response()->json(exceptionMessage($e),423);
        }
    }

    public function signup(Request $request){
    	try{
			if($request->ajax()){
	            $validator = Validator::make($request->all(), [
	                    'name' => 'bail|required|string|max:255',
	                    /*'country' => 'bail|required',*/
	                    'state' => 'bail|required',
                        'g-recaptcha-response' => 'bail|required',
	                    'city' => 'bail|required',
	                    'postcode' => 'bail|required',
	                    'mobile'=>'bail|required|numeric|digits:10',
	                    'email' => 'bail|required|string|regex:/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i|max:255|unique:users',
	                    'password' => 'required|string|min:6',
	                ],
	                [
	                    'email.regex'=>'This email is not a valid email address',
                        'g-recaptcha-response.required' => 'Captcha verification is required'
	                ]);
	            if($validator->passes()) {
	                $data = $request->all();
                    //Validate Captcha at server Level
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, [
                        'secret' => '6Ldw1PccAAAAAEal1a0r6Ez8gyzYc8EpTUuwLEII',
                        'response' => $data['g-recaptcha-response'],
                        'remoteip' => $_SERVER['REMOTE_ADDR']
                    ]);

                    $resp = json_decode(curl_exec($ch));
                    curl_close($ch);
                    if ($resp->success){
                        //Nothing to do
                    }else{
                        return response()->json(['status'=>false,'type'=>'validation','errors'=>array('g-recaptcha-response'=>'Seems to be spam request. Please try again later')]);
                    }
                    $data['country'] = 'India';
	                $data['status'] =1;
	                $data['password'] = bcrypt($data['password']);
	                $this->user->create($data);
	                if(Auth::guard('web')->attempt($request->only('email','password'))) { 
                        fireTextSms('signup',$data);
	                    $this->updatingCartSessionToUser();
	                    if(Session::has('previousurl')){
	                        $redirectTo = Session::get('previousurl');
	                        Session::forget('previousurl');
	                    }else{
	                        $redirectTo = '/cart';
	                        $redirectTo = url('/');
	                    }
	                }
	                Mail::to($data['email'])->bcc(['info@imaecmedntek.com'])->send(new \App\Mail\RegisterMail($data));
	                return response()->json(['status'=>true,'message'=>'ok','url'=>$redirectTo]);
	            }else{
	                return response()->json(['status'=>false,'type'=>'validation','errors'=>$validator->messages()]);
	            }
	        }else{
	            if(Auth::check()){
	        		return redirect('cart');
	        	}
	            $title = "Signin / Signup";
	            return view('front.account.signin')->with(compact('title'));
	        }
    	}catch(\Exception $e){
            return response()->json(exceptionMessage($e),423);
        }
    }

    public function updatingCartSessionToUser(){
        if(Session::has('cartsessionId')){
            $this->cart->where('session_id',Session::get('cartsessionId'))->update(['user_id'=>Auth::user()->id,'session_id'=>'']);
            DB::select("DELETE FROM carts WHERE id NOT IN (SELECT * FROM (SELECT MAX(n.id) FROM carts n GROUP BY n.product_id,n.size) x) and user_id = ".Auth::user()->id."");
        }
        $mesage = "success";
        return $mesage;
    }

    public function forgotPassword(Request $request){
    	try{
    		if($request->ajax()){
	            $data = $request->all();
	            $validator = Validator::make($request->all(), [
	                'email' => 'required|string|email|max:255',
	            ]);
	            if ($validator->passes()) {
	                $userDetails = User::where('email',$data['email'])->first();
	                if($userDetails){
	                    $user = User::find($userDetails->id);
	                    $password = mt_rand(100000,555555);
	                    $user->password  = bcrypt($password);
	                    $user->save();
	                    $mailData['user'] = $user;  
	                    $mailData['decrypt_password'] = $password;  
	                    Mail::to($data['email'])->send(new \App\Mail\ForgotUserPasswordMail($mailData));
	                    $messages = array('New password sent to your email.');
	                    return response()->json(['status'=>true,'message'=>$messages]);
	                }else{
	                    $errors = array('email'=>'This email not exists');
	                    return response()->json(['status'=>false,'type'=>'validation','errors'=>$errors]);
	                }
	            }else{
	                return response()->json(['status'=>false,'type'=>'validation','errors'=>$validator->messages()]);
	            }
	        }else{
	        	if(Auth::check()){
	        		return redirect('cart');
	        	}
	        	$title = "Forgot Password";
	        	return view('front.account.forgot-password')->with(compact('title'));
	        }
    	}catch(\Exception $e){
            return response()->json(exceptionMessage($e),423);
        }
    }

    public function logout(){
        Session::forget('previousurl');
        Session::forget('couponinfo');
        Auth::logout();
        return redirect('/');
    }

    public function saveAddress(Request $request){
        try{
            if($request->ajax()){
                $validator = Validator::make($request->all(), [
                    'first_name'=>'bail|required',
                    'last_name'=>'bail|required',
                    'mobile'=>'required|numeric|digits:10',
                    'address_line_1'=>'bail|required',
                    'city'=>'bail|required',
                    'state'=>'bail|required',
                    'postcode'=>'bail|required|numeric|digits:6'
                ],
                [
                    'email.regex' =>'This email is not a valid email address'
                ]);
                if($validator->passes()) {
                    $data = $request->all();
                    unset($data['_token']);
                    $data['country'] = 'India';
                    $data['user_id'] = Auth::user()->id;
                    $data['name'] = $data['first_name']. " ".$data['last_name'];
                    if($this->shippingAddr->addresscount(Auth::user()->id) ==0){
                        $data['default_address'] = 1;
                        //Update Address in Users Table
                        $updateAddr['city'] = $data['city'];
                        $updateAddr['state'] = $data['state'];
                        $updateAddr['postcode'] = $data['postcode'];
                        $updateAddr['address_line_1'] = $data['address_line_1'];
                        $updateAddr['address_line_2'] = $data['address_line_2'];
                        if(Auth::user()->mobile ==""){
                            $updateAddr['mobile'] = $data['mobile'];
                        }
                        User::where('id',Auth::user()->id)->update($updateAddr);
                    }
                    if(!empty($data['shipping_id'])){
                        $shippingid = $data['shipping_id'];
                        unset($data['shipping_id']);
                        ShippingAddress::where('id',$shippingid)->update($data);
                    }else{
                        ShippingAddress::create($data);
                    }
                    return response()->json([
                        'status' => true,
                        'view' => (String)View::make('front.address.delivery-address')
                    ]);
                }else{
                    return response()->json(['status'=>false,'type'=>'validation','errors'=>$validator->messages()]);
                }
            }
        }catch(\Exception $e){
            return response()->json(exceptionMessage($e),423);
        }
    }

    public function getDeliveryAddress(Request $request){
        try{
            if($request->ajax()){
                $validator = Validator::make($request->all(), [
                    'id'=>'bail|required|exists:shipping_addresses,id',
                ]);
                if($validator->passes()) {
                    $data = $request->all();
                    $address = $this->shippingAddr->where('id',$data['id'])->first()->toArray();
                    return response()->json([
                        'status' => true,
                        'address' => $address
                    ]);
                }else{
                    return response()->json([
                        'status' => false,
                        'view' => (String)View::make('front.address.delivery-address')
                    ]);
                }
            }
        }catch(\Exception $e){
            return response()->json(exceptionMessage($e),423);
        }
    }

    public function setDefaultAddress(Request $request){
        try{
            if($request->ajax()){
                $validator = Validator::make($request->all(), [
                    'addressid'=>'bail|required|exists:shipping_addresses,id',
                ]);
                if($validator->passes()) {
                    $data = $request->all();
                    DB::beginTransaction();
                    $this->shippingAddr->where('id',$data['addressid'])->update(['default_address'=>1]);
                    $this->shippingAddr->where('id','!=',$data['addressid'])->where('user_id',Auth::user()->id)->update(['default_address'=>0]);
                    DB::commit();
                }
                return response()->json([
                    'status' => true,
                    'view' => (String)View::make('front.address.delivery-address')
                ]);
            }
        }catch(\Exception $e){
            return response()->json(exceptionMessage($e),423);
        }
    }

    public function removeDeliveryAddress(Request $request){
        try{
            if($request->ajax()){
                $validator = Validator::make($request->all(), [
                    'id'=>'bail|required|exists:shipping_addresses,id',
                ]);
                if($validator->passes()) {
                    $data = $request->all();
                    $this->shippingAddr->where('id',$data['id'])->delete();
                }
                return response()->json([
                    'status' => true,
                    'view' => (String)View::make('front.address.delivery-address')
                ]);
            }
        }catch(\Exception $e){
            return response()->json(exceptionMessage($e),423);
        }
    }

    public function getStateCity(Request $request){
        if($request->ajax()){
            $data =$request->all();
            $getdata = DB::table('cities')->where('pincode',$data['pincode'])->first();
            if($getdata){
                $state = $getdata->state;
                $city  = $getdata->city;
            }else{
                $state = "";
                $city  = "";
            }
            return array('state'=>$state,'city'=>$city);
        }
    }

    public function account($slug){
        $accountSlugs = array('my-orders','settings','my-profile','wishlists');
        $orders = array();$orderDetails= array(); $wishlists =array();
        if(in_array($slug,$accountSlugs)){
            if($slug=="my-orders"){
                if(isset($_GET['order_id']) && !empty($_GET['order_id'])){
                    $title ="Order Details";
                    $orderDetails = Order::withCount(['order_products as total_items'=>function($query){
                        $query->select(DB::raw('sum(product_qty)'));
                    }])->with(['order_products'])->where(['user_id'=>Auth::user()->id,'id'=>$_GET['order_id']])->first();
                    $orderDetails = json_decode(json_encode($orderDetails),true);
                    //echo "<pre>"; print_r($orderDetails); die;
                    if(!$orderDetails){
                        return redirect('account/my-orders');
                    }
                }else{
                    $title ="My Orders";
                    $orders = Order::where('user_id',Auth::user()->id)->orderby('id','DESC')->get();
                    $orders = json_decode(json_encode($orders),true);
                    //echo "<pre>"; print_r($orders); die;
                }
            }elseif($slug=="settings"){
                $title="Settings";
            }
            elseif($slug=="my-profile"){
                $title="My Profile";
            }elseif($slug=="wishlists"){
                $wishlists =Wishlist::wishlists();
                $title="Wishlists";
            }elseif($slug=="address"){
                $title="Address";
            }
            return view('front.account.account')->with(compact('title','orders','slug','orderDetails','wishlists'));
        }else{
            abort(404);
        }
    }

    public function submitAcountDetails(Request $request){
        if($request->ajax()){
            $data = $request->all();
            //echo "<pre>"; print_r($data); die;
            $validator = Validator::make($request->all(), [
                    'name' => 'bail|required',
                    'mobile' => 'bail|required|numeric|digits:10',
                    'state' => 'bail|required',
                    'city' => 'bail|required',
                    'postcode' => 'bail|required',
                    'address_line_1' => 'bail|required',
                ]
            );
            if($validator->passes()) {
                //Update user info
                $user = User::find(Auth::user()->id);
                $user->update($data);
                return response()->json(['status'=>true,'message'=>array('Profile has has been updated succesfully!')]);
            }else{
                return response()->json(['status'=>false,'type'=>'validation','errors'=>$validator->messages()]);
            } 
        }
    }

    public function changePassword(Request $request){
        if($request->ajax()){
            $data = $request->all();
            $validator = Validator::make($request->all(), [
                    'current_password' => 'required',
                    'password' => 'required|min:6|confirmed',
                    'password_confirmation' => 'required|min:6',
                ]
            );
            if($validator->passes()) {
                $password = $data['current_password'];
                $check_password = DB::table('users')
                       ->where('id',Auth::user()->id)
                        ->select('id','password')
                       ->first();
                if(Hash::check($password, $check_password->password)) {
                    $user = User::find(Auth::user()->id);
                    $user->password = bcrypt($data['password']);
                    $user->save();
                    return response()->json(['status'=>true,'message'=>array('Password has been updated successfully')]);
                }else{
                    return response()->json(['status'=>false,'errors'=>array('current_password'=>'Your current password is incorrect')]);
                }
            }else{
                return response()->json(['status'=>false,'type'=>'validation','errors'=>$validator->messages()]);
            }
        } 
    }

    public function removeWishlist($wishlistid){
        Wishlist::where('id',$wishlistid)->delete();
        return redirect()->back()->with('flash_message_success','Wishlist has been removed successfully');
    }

    public function returnOrderItem(Request $request){
        if($request->isMethod('post')){
            $data =$request->all();
            $details = OrderProduct::where('id',$data['order_product_id'])->where('user_id',Auth::user()->id)->select('order_id')->first();
            if($details){
                $return = new ReturnRequest;
                if(isset($data['type'])){
                    $return->type = $data['type'];
                }
                $return->user_id = Auth::user()->id;
                $return->order_id = $details->order_id;
                $return->order_product_id = $data['order_product_id'];
                $return->reason = $data['reason'];
                $return->comments = $data['comments'];
                if($request->hasFile('image')){
                    $file = $request->file('image');
                    $img = Image::make($file);
                    $destination = public_path('/images/ReturnImages/');
                    $ext = $file->getClientOriginalExtension();
                    $mainFilename = "user-request-image-".uniqid().str_random(5).time().".".$ext;
                    $img->save($destination.$mainFilename);
                    $return->image = $mainFilename;
                }
                $return->save();
                if($data['type']=="cancel"){
                    $smsinfo['order_id'] = $details->order_id;
                    $smsinfo['mobile'] = Auth::user()->mobile;
                    fireTextSms('order-cancel',$smsinfo);
                }
                return redirect()->back()->with('flash_message_success','Your request submiited successfully. We will get back to you soon');
            }else{
                return redirect()->back()->with('flash_message_error','Something went wrong');
            }
        }
    }

    public function trackOrder(Request $request){
        if($request->isMethod('get')){
            $data = $request->all();
            $trackLink = "";
            if(isset($data['order_id']) && !empty($data['order_id'])){
                $orderDetails =  Order::where('id',$data['order_id'])->first();
                $orderDetails = json_decode(json_encode($orderDetails),true);
                if(!empty($orderDetails['awb_number'])){
                    $trackLink = "https://acplcargo.com/test.php?serch=".$orderDetails['awb_number'];
                }else{
                    return redirect::to('track-order')->with('flash_message_error','No tracking found for this order id');
                }
            }
        }
        $title = "Track Order";
        return view('front.track-order')->with(compact('title','trackLink'));
    }
}
