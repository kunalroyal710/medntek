<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use DB;
use Cookie;
use Session;
use Crypt;
use Illuminate\Support\Facades\Mail;
use Auth;
use App\Models\ReturnRequest;
use App\Models\OrderProduct;
use App\Models\ProductAttribute;
use App\Models\Order;
use App\Models\OrderAddress;
use App\Models\OrderHistory;
use App\Models\Product;
class ReturnController extends Controller
{
    //
    public function cancelRequests(Request $Request){
		Session::put('active','cancel');
        if($Request->ajax()){
            $conditions = array();
            $data = $Request->input();
            $querys = DB::table('return_requests')->where('return_requests.type','cancel')->join('users','users.id','=','return_requests.user_id')->join('order_products','order_products.id','=','return_requests.order_product_id')->select('return_requests.*','users.email','users.name','users.mobile','order_products.product_name','order_products.product_size','order_products.product_sku','order_products.product_qty','order_products.subtotal');
            if(!empty($data['orderid'])){
                $querys = $querys->where('return_requests.id','like','%'.$data['orderid'].'%');
            }
            if(!empty($data['name'])){
                $querys = $querys->where(function($qyery) use($data){
                	$qyery->where('users.name','like','%'.$data['name'].'%')->orwhere('users.email','like','%'.$data['name'].'%')->orwhere('users.mobile',$data['name']);
                });
            }
            if(!empty($data['from_date'])){
                    $querys = $querys->whereDate('return_requests.created_at', '>=',$data['from_date']);
            }
            if(!empty($data['to_date'])){
                $querys = $querys->whereDate('return_requests.created_at', '<=',$data['to_date']);
            }
            $iTotalRecords = $querys->where($conditions)->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayStart = intval($_REQUEST['start']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $querys =  $querys->where($conditions)
                        ->skip($iDisplayStart)->take($iDisplayLength)
                		->orderby('return_requests.id','DESC')
                		->get();
            $sEcho = intval($_REQUEST['draw']);
            $records = array();
            $records["data"] = array(); 
            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;
            $querys=json_decode( json_encode($querys), true);
            foreach($querys as $return){
                $actionValues='';
                $returnimage ='';
                if(!empty($return['image'])){
                    $returnimage = '<img width="150px" src="'.url('images/ReturnImages/'.$return['image']).'"/>';
                }
                $records["data"][] = array(      
                    '<a target="_blank" href="'.url('admin/order-view/'.$return['order_id']).'">'.$return['order_id'].'</a>',
                    $return['name'] ."<br>".$return['email']."<br>".$return['mobile'],
                    $return['product_name'],
                    $return['product_size'],
                    "Rs.". formatAmt($return['subtotal']),
                    $return['reason'],
                    $returnimage,
                    date('d M Y H:ia',strtotime($return['created_at'])),
                    $return['comments'],
                    $actionValues
                );
            }
            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            return response()->json($records);
        }
        $title = "Cancel Requests";
        return View::make('admin.orders.cancel-requests')->with(compact('title'));
    }

    public function returnRequests(Request $Request){
		Session::put('active','returns');
        if($Request->ajax()){
            $conditions = array();
            $data = $Request->input();
            $querys = DB::table('return_requests')->wherein('return_requests.type',['return','exchange'])->join('users','users.id','=','return_requests.user_id')->join('order_products','order_products.id','=','return_requests.order_product_id')->select('return_requests.*','users.email','users.name','users.mobile','order_products.product_name','order_products.product_size','order_products.product_sku','order_products.product_qty','order_products.subtotal');
            if(!empty($data['orderid'])){
                $querys = $querys->where('return_requests.id','like','%'.$data['orderid'].'%');
            }
            if(!empty($data['name'])){
                $querys = $querys->where(function($qyery) use($data){
                	$qyery->where('users.name','like','%'.$data['name'].'%')->orwhere('users.email','like','%'.$data['name'].'%')->orwhere('users.mobile',$data['name']);
                });
            }
            if(!empty($data['from_date'])){
                    $querys = $querys->whereDate('return_requests.created_at', '>=',$data['from_date']);
            }
            if(!empty($data['to_date'])){
                $querys = $querys->whereDate('return_requests.created_at', '<=',$data['to_date']);
            }
            $iTotalRecords = $querys->where($conditions)->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayStart = intval($_REQUEST['start']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $querys =  $querys->where($conditions)
                        ->skip($iDisplayStart)->take($iDisplayLength)
                		->orderby('return_requests.id','DESC')
                		->get();
            $sEcho = intval($_REQUEST['draw']);
            $records = array();
            $records["data"] = array(); 
            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;
            $querys=json_decode( json_encode($querys), true);
            foreach($querys as $return){
                $actionValues='<a title="Update Status" data-returnid="'.$return['id'].'" class="btn btn-sm green updateReturnStatus" href="javascript:;">Update</a>';
                $returnimage ='';
                if(!empty($return['image'])){
                    $returnimage = '<img width="150px" src="'.url('images/ReturnImages/'.$return['image']).'"/>';
                }
                $records["data"][] = array(      
                    '<a target="_blank" href="'.url('admin/order-view/'.$return['order_id']).'">'.$return['order_id'].'</a>',
                    $return['name'] ."<br>".$return['email']."<br>".$return['mobile'],
                    $return['product_name'],
                    $return['product_size'],
                    "Rs.". formatAmt($return['subtotal']),
                    $return['reason'],
                    $returnimage,
                    date('d M Y H:ia',strtotime($return['created_at'])),
                    $return['status'],
                    $return['comments'],
                    $actionValues
                );
            }
            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            return response()->json($records);
        }
        $title = "Return Requests";
        return View::make('admin.orders.return-requests')->with(compact('title'));
    }

    public function updateReturnRequest(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>"; print_r($data); die;
            DB::beginTransaction();
            if($data['status'] =='Exchange Approved'){
                ReturnRequest::where('id',$data['return_id'])->update(['status'=>$data['status'],'comments'=>$data['comments'],'type'=>'exchange']);
                DB::commit();
                return redirect()->back()->with('flash_message_success','Status has been updated successfully and return request transferred to exchange request list!');
            }else{
                ReturnRequest::where('id',$data['return_id'])->update(['status'=>$data['status'],'comments'=>$data['comments']]);
                $userinfo = ReturnRequest::where('return_requests.id',$data['return_id'])->join('order_products','order_products.id','=','return_requests.order_product_id')->join('users','users.id','=','return_requests.user_id')->select('return_requests.*','users.email','users.name','order_products.product_name','order_products.product_size')->first();
                $subject = "Status Update Against your Return Request #".$userinfo->order_id;
                $email = $userinfo->email;
                $messageData = [
                    'userinfo' =>$userinfo,
                ];
                Mail::send('emails.return-request-email', $messageData, function($message) use ($email,$subject){
                    $message->to($email)->subject($subject);
                });
                DB::commit();
                return redirect()->back()->with('flash_message_success','Record has been update successfully!');
            }
        }
    }
}
