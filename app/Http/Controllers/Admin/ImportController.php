<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Redirect;
use App\Models\ProductAttribute;
use App\Models\ProductImage;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Illuminate\Support\Facades\Schema;
class ImportController extends Controller
{
    //
    public function importData(Request $request){
    	Session::put('active','imports');
    	if($request->isMethod('post')){
    		$data = $request->all();
    		$filename = $_FILES['file']['name'];
		    $ext = pathinfo($filename, PATHINFO_EXTENSION);
		    if($ext == 'xls'){

			}else{
				return redirect::to('/admin/import-data')->with('flash_message_error','Please import xls file format only');
			}
    		$fileResp = $this->uploadFile($request,$data['type']);
    		if($fileResp['status']){
    			$response = $this->validateProductFile($fileResp);
    			if($response['status']){
    				return redirect::to('/admin/import-data?&type=products&filename='.$fileResp['filename'])->with('flash_message_success','File has been validated successfully. Please click below button to import');
    			}else{
    				return redirect()->back()->with('flash_message_error',$response['message']);
    			}
    		}
    	}
    	$title="Import Data";
    	return view('admin.imports.import-data')->with(compact('title'));
    }

    public function uploadFile($request,$type){
		if($request->hasFile('file')){
			if ($request->file('file')->isValid()) {
			    $file = $request->file('file');
			    $destination = public_path('imports/'.$type);
			    $ext= $file->getClientOriginalExtension();
			    $mainFilename = $type."-".rand(12,5554).time().date('i').".".$ext;
			    $file->move($destination, $mainFilename);
			    return array('status'=>true,'filename'=>$mainFilename,'filepath'=>public_path('imports/products/'.$mainFilename));
			}
		}
	}

	public function validateProductFile($fileResp){
		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileResp['filepath']);
    	foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
		    $worksheets[$worksheet->getTitle()] = $worksheet->toArray();
		}
		$sheetsdata = array_values($worksheets);
		if(isset($sheetsdata[0]) && !empty($sheetsdata[0])){
			$sheetsdata = $sheetsdata[0];
			if(isset($sheetsdata[0]) && !empty($sheetsdata[0])){
				$excelCols = $sheetsdata[0];
				$excelColumns = array();
				foreach($excelCols as $ckey=> $col){
					if(empty($col)){
						$previousKey = $this->getPrevKey($ckey,$excelCols);
						$errorcolumn =  $excelCols[$previousKey];
						unlink($fileResp['filepath']);
						if(empty($errorcolumn)){
							$message ="Please remove empty columns from sheet and try again";
						}else{
							$message = 'Please remove empty column after '.$errorcolumn." column and try again.";
						}
	            		return array('status'=>false,'message'=>$message);
					}else{
						$excelColumns[] =  strtolower($col);
					}
				}
				$headings = array_shift($sheetsdata);
				array_walk(
				    $sheetsdata,
				    function (&$row) use ($headings) {
				        $row = array_combine($headings, $row);
				    }
				);
				$table = "products";
	    		$columns = DB::getSchemaBuilder()->getColumnListing($table);
	    		$columns = array_diff( $columns, ['id','created_at','updated_at','final_price','discount_type','weight','more_detail_image','stock']);
	    		$otherColumns = array('other_cat_ids','attr_sku_0','attr_size_0','attr_stock_0','attr_price_0','image_0');
	    		$allcolumns = array_merge($columns,$otherColumns);
	    		$response =true;
	    		$columnarr = array();
				foreach($allcolumns as $key => $column){
	            	if(!in_array($column,$excelColumns)){
	            		$response=false;
	            		$columnarr[] = $column;
	            	}
	            }
	            if(!$response){
	            	$columnnames = implode(',',$columnarr);
	            	unlink($fileResp['filepath']);
	            	return array('status'=>false,'message'=> 'Your CSV files having unmatched Columns to our database...Missing columns are:- '.$columnnames);
	            }else{
	            	//Check for Data Sets
	            	$countNoOfRows = count($sheetsdata);
	            	if($countNoOfRows>200){
	            		unlink($fileResp['filepath']);
	            		return array('status'=>false,'message'=> 'Please import minimum 100 products at a time');
	            	}else{
	            		$lineNumber = 1;
	            		foreach($sheetsdata as $pkey => $prodata){
	            			$lineNumber = $lineNumber +1;
	            			$resp = $this->checkProductRowValid($prodata,$lineNumber);
	            			if(!$resp['status']){
	            				unlink($fileResp['filepath']);
	            				return array('status'=>false,'message'=> $resp['message']);
	            			}else{
	            				$products[] = $resp['products'];
	            			}
	            		}
	            		// Final Check Points
	            		$proCodes = array_column($products, 'product_code');
						if($proCodes != array_unique($proCodes)){
							unlink($fileResp['filepath']);
							return array('status'=>false,'message'=> 'There are duplicates product_code in sheet. Please fix and upload it again');
						}
						return array('status'=>true);
	            	}
	            }
			}else{
				unlink($fileResp['filepath']);
				return array('status'=>false,'message'=>'No data for import');
			}
		}else{
			unlink($fileResp['filepath']);
			return array('status'=>false,'message'=>'No data for import');
		}
	}

	public function checkProductRowValid($prodata,$lineNumber){
		foreach($prodata as $proKey=> $prod){
			$proRow[trim(strtolower($proKey))] = $prod;
		}
		//Check Brand is Valid
		$columnChecks = array(
					'brand'=>'brand_id',
					'category'=>'category_id',
					'product_code'=>'product_code',
					'gender'=>'gender',
					'price' => 'price',
					'discount' => 'discount',
					'other_cat_ids' => 'other_cat_ids',
					'best_seller' => 'best_seller',
					'new_arrival' => 'new_arrival',
					'image_0' => 'image_0',
					'attr_sku_0' => 'attr_sku_0',
					'status' => 'status',
				);
		foreach($columnChecks as $ckey => $colcheck){
			$resp = checkProFile($proRow,$ckey,$proRow[$colcheck]);
			if(!$resp['status']){
				return array('status'=>false,'message'=> $resp['message']. ' at line number '.$lineNumber);
			}
		}
		return array('status'=>true,'products'=>$proRow);
	}

	public function getPrevKey($key, $hash = array()) {
	    $keys = array_keys($hash);
	    $found_index = array_search($key, $keys);
	    if ($found_index === false || $found_index === 0)
	        return false;
	    return $keys[$found_index-1];
	}

	public function importFileData(Request $request){
		if($request->isMethod('post')){
			$data = $request->all();
			if($data['type']=="products"){
				$filename = public_path('imports/products/'.$data['filename']);
				$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($filename);
		    	foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
				    $worksheets[$worksheet->getTitle()] = $worksheet->toArray();
				}
				$sheetsdata = array_values($worksheets);
				if(isset($sheetsdata[0]) && !empty($sheetsdata[0])){
					$sheetsdata = $sheetsdata[0];
					if(isset($sheetsdata[0]) && !empty($sheetsdata[0])){
						$headings = array_shift($sheetsdata);
						array_walk(
						    $sheetsdata,
						    function (&$row) use ($headings) {
						        $row = array_combine($headings, $row);
						    }
						);
						foreach ($sheetsdata as $key => $proRow) {
							foreach($proRow as $proKey=> $prod){
								$productinfo[trim(strtolower($proKey))] = $prod;
							}
							//Create Product
							$product = new Product;
							$product->brand_id = $productinfo['brand_id'];
							$product->category_id = $productinfo['category_id'];
							$product->product_name = $productinfo['product_name'];
							$product->product_code = $productinfo['product_code'];
							$product->gender = (!empty($productinfo['gender'])?$productinfo['gender']:'');
							$product->concern = (!empty($productinfo['concern'])?$productinfo['concern']:'');
							/*$product->weight = (!empty($productinfo['weight'])?$productinfo['weight']:'');*/
							$product->product_description = (!empty($productinfo['product_description'])?$productinfo['product_description']:'');
							$product->ingredients = (!empty($productinfo['ingredients'])?$productinfo['ingredients']:'');
							$product->short_description = (!empty($productinfo['short_description'])?$productinfo['short_description']:'');
							$product->benefits = (!empty($productinfo['benefits'])?$productinfo['benefits']:'');
							$product->how_to_use = (!empty($productinfo['how_to_use'])?$productinfo['how_to_use']:'');
							$product->group_code = (!empty($productinfo['group_code'])?$productinfo['group_code']:'');
							$product->best_seller = $productinfo['best_seller'];
							$product->new_arrival = $productinfo['new_arrival'];
							$product->status = $productinfo['status'];
							$product->price = $productinfo['price'];
							if($productinfo['discount'] >0){
								$product->discount_type = "product";
								$product->discount = $productinfo['discount'];
								$product->final_price = $productinfo['price'] - ($productinfo['price'] * $productinfo['discount'])/100;
							}else{
								$product->discount = 0;
								$product->final_price =$productinfo['price'];
							}
							$product->save();
							$cats = explode(',',$productinfo['other_cat_ids']);
							$product->product_categories()->attach($cats);
							$sort = 1;
							for($img = 0; $img <=9; $img ++){
    							$imgKey = "image_".$img;
    							if(isset($productinfo[$imgKey]) && !empty($productinfo[$imgKey])){
    								$productImage =  new ProductImage;
    								$productImage->product_id =  $product->id;
    								$productImage->image =  $productinfo[$imgKey];
    								$productImage->sort = $sort;
    								$productImage->save();
    								$sort ++;
    							}
    						}
    						//Adding Product Attributes
    						$attrSort = 1;$totalStock=0;
    						for($attr = 0; $attr <=9; $attr ++){
    							if(isset($productinfo['attr_sku_'.$attr]) && isset($productinfo['attr_size_'.$attr]) && !empty($productinfo['attr_sku_'.$attr]) &&   !empty($productinfo['attr_size_'.$attr])){
    								$totalStock += $productinfo['attr_stock_'.$attr];
    								$proAttr =  new ProductAttribute;
    								$proAttr->product_id  = $product->id;
    								$proAttr->sku  = $productinfo['attr_sku_'.$attr];
    								$proAttr->size  = $productinfo['attr_size_'.$attr];
    								$proAttr->stock  = $productinfo['attr_stock_'.$attr];
    								$proAttr->price  = $productinfo['attr_price_'.$attr];
    								$proAttr->sort = $attrSort;
    								$proAttr->status  = 1;
    								$proAttr->save();
    								$attrSort++;
    							}
    						}
    						Product::where('id',$product->id)->update(['stock'=> $totalStock]);
						}
						return redirect::to('/admin/import-data')->with('flash_message_success','Product has been imported successfully');
					}
				}else{
					return redirect::to('/admin/import-data')->with('flash_message_error','Import failed. Please try after sometime');
				}
			}
		}
	}
}
