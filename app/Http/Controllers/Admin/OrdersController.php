<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Session;
use DB;
use App\Models\Order;
use App\Models\OrderHistory;
use App\Models\AwbNumber;
use Auth;
use Illuminate\Support\Facades\Mail;
class OrdersController extends Controller
{
    //
    public function orders(Request $Request){
		Session::put('active','orders'); 
        $getorderstatus = DB::table('order_statuses')->where('status',1)->get();
        $getorderstatus = json_decode(json_encode($getorderstatus),true);
        if($Request->ajax()){
            $conditions = array();
            $data = $Request->input();
            $querys = DB::table('orders')->join('users','users.id','=','orders.user_id')->select('orders.*','users.email','users.name','users.mobile');
            if(!empty($data['orderid'])){
                $querys = $querys->where('orders.id','like','%'.$data['orderid'].'%');
            }
            if(!empty($data['name'])){
                $name = $data['name'];
                $querys = $querys->where(function($query) use($name){
                    $query->where('users.name','like','%'.$name.'%')->orwhere('users.mobile',$name)->orwhere('users.email',$name);
                });
            }
            if(!empty($data['order_status'])){
                $querys = $querys->where('orders.order_status','like','%'.$data['order_status'].'%');
            }
            if(!empty($data['from_date'])){
                    $querys = $querys->whereDate('orders.created_at', '>=',$data['from_date']);
            }
            if(!empty($data['to_date'])){
                $querys = $querys->whereDate('orders.created_at', '<=',$data['to_date']);
            }
            $iTotalRecords = $querys->where($conditions)->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayStart = intval($_REQUEST['start']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $querys =  $querys->where($conditions)
                        ->skip($iDisplayStart)->take($iDisplayLength)
                		->orderby('orders.id','DESC')
                		->get();
            $sEcho = intval($_REQUEST['draw']);
            $records = array();
            $records["data"] = array(); 
            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;
            $querys=json_decode( json_encode($querys), true);
            foreach($querys as $order){
            	if($order['order_status'] =="Cancelled" || $order['order_status'] =="Payment Refunded" || $order['order_status'] =="Aborted"){
            		$order['order_status'] = '<span class="label label-danger">'.$order['order_status'].'</span>';
            	}else{
            		$order['order_status'] = '<span class="label label-success">'.$order['order_status'].'</span>';
            	}
                $invoice = '
                <a target="_blank" title="View Invoice" class="btn btn-sm green" href="'.url('/admin/invoice/'.$order['id']).'"> <i class="fa fa-print"></i>
                </a>';
                $actionValues='
                	<a target="_blank" title="View Order Details" class="btn btn-sm blue" href="'.url('/admin/order-view/'.$order['id']).'"> <i class="fa fa-file"></i>
                    </a>'.$invoice;
                $records["data"][] = array(      
                    $order['id'],
                    $order['email']."<br>".$order['mobile']."<br>".$order['name'],
                    $order['currency_symbol']." ".formatAmt($order['grand_total']), 
                    $order['payment_method'],
                    date('d F Y h:i:a',strtotime($order['created_at'])), 
                    $order['order_status'],
                    $actionValues
                );
            }
            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            return response()->json($records);
        }
        $title = "Orders";
        return View::make('admin.orders.orders')->with(compact('title','getorderstatus'));
    }

    public function orderview($id){
        Session::put('active','orders'); 
        $orderDetails = Order::with(['getuser','order_products','histories'])->where('id',$id)->first();
        $orderDetails = json_decode(json_encode($orderDetails),true);
        $getorderstatus = DB::table('order_statuses')->where('type','yes')->where('status',1)->orderby('sort','ASC')->get();
        $getorderstatus = json_decode(json_encode($getorderstatus),true);
        //echo "<pre>"; print_r($orderDetails); die;
        $title="Order View";
        return view('admin.orders.order-view')->with(compact('title','orderDetails','getorderstatus'));
    }

    public function updateShippingAddress(Request $request,$id){
        if($request->isMethod('post')){
            $data = $request->all();
            unset($data['_token']);
            Order::where('id',$id)->update($data);
            return redirect()->back()->with('flash_message_success','Shipping address has been updated successfully');
        }
    }

    public function vieworderInvoice(Request $request,$id){
        $orderDetails = Order::withCount(['order_products as total_items'=>function($query){
                $query->select(DB::raw('sum(product_qty)'));
            }])->with(['getuser','order_products'])->where('id',$id)->first();
        $orderDetails = json_decode(json_encode($orderDetails),true);
        $title="Invoice";
        $numberWords =  convert_number_to_words($orderDetails['grand_total']);
        return view('admin.orders.order-invoice')->with(compact('title','orderDetails','numberWords'));
    }

    public function updateOrderStatus(Request $request,$orderid){
        if($request->isMethod('post')){
            $data = $request->all();
            $orderDetails = Order::with(['order_products','getuser'])->where('id',$orderid)->first();
            $orderDetails = json_decode(json_encode($orderDetails),true);
            DB::beginTransaction();
            $history = array('order_status'=>$data['status'],'comments'=>$data['comments'],'updated_by'=>Auth::guard('admin')->user()->id,'order_id'=>$orderid);
            OrderHistory::create($history);
            Order::where('id',$orderid)->update(['order_status'=>$data['status']]);
            if($orderDetails['invoice_no'] ==""){
                $lastInvoice = Order::select('invoice_no')->orderby('invoice_no','DESC')->where('invoice_no','!=','')->first();
                $lastInvoice = json_decode(json_encode($lastInvoice),true);
                if(!empty($lastInvoice)){
                    $newInvoiceNo = $lastInvoice['invoice_no']+1;
                }else{
                    $newInvoiceNo = 100;
                }
                $invID = sprintf('%05d',$newInvoiceNo);
                $invNoStr = 'IMAEC/SL-EC/FR/'.$invID;
                Order::where('id',$orderid)->update(['invoice_no'=>$newInvoiceNo,'invoice_no_str'=>$invNoStr,'invoice_date'=>date('Y-m-d'),'awb_number'=>$data['awb_number']]);
            }
            DB::commit();
            if($data['status'] == "Delivered"){
                $smsinfo['mobile'] = $orderDetails['mobile'];
                fireTextSms('order-delivered',$smsinfo);
            }elseif($data['status'] == "Shipped"){
                $smsinfo['order_id'] = $orderDetails['id'];
                $smsinfo['awb_number'] = $data['awb_number'];
                $smsinfo['mobile'] = $orderDetails['mobile'];
                fireTextSms('order-shipped',$smsinfo);
            }
            $email = $orderDetails['getuser']['email'];
            $messageData = [
                'message_info' => $data,
                'orderDetails' => $orderDetails
            ];
            Mail::send('emails.update-order-status', $messageData, function($message) use ($email,$data){
                $message->to($email)->subject($data['subject']);
            });
            return redirect()->back()->with('flash_message_success','Order Status has been updated successfully!');
        }
    }

    public function invoice($orderid){
        $orderDetails = Order::withCount(['order_products as total_items'=>function($query){
                $query->select(DB::raw('sum(product_qty)'));
            }])->with(['getuser','order_products'])->where('id',$orderid)->first();
        $orderDetails = json_decode(json_encode($orderDetails),true);
        //echo "<pre>"; print_r($orderDetails); die;
        $title = "Invoice";
        $numberWords =  convert_number_to_words($orderDetails['grand_total']);
        return view('admin.orders.invoice')->with(compact('title','orderDetails','numberWords'));
    }
}
