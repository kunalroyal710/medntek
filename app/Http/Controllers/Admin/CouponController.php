<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Models\AdminRole;
use DB;
use Cookie;
use Session;
use Crypt;
use Illuminate\Support\Facades\Mail;
use App\Models\CouponCode;
use App\Models\Category;
use App\Models\Products;
use Auth;
use Image;
use Validator;
use Illuminate\Support\Str;
class CouponController extends Controller
{
    //
    public function coupons(Request $Request){
		Session::put('active','coupons'); 
        $checkEditAccess = AdminRole::checkupdateAccess('admin/add-edit-coupon/{id?}');
        if($Request->ajax()){
            $conditions = array();
            $data = $Request->input();
            $querys = DB::table('coupon_codes');
            if(!empty($data['code'])){
                $querys = $querys->where('code','like','%'.$data['code'].'%');
            }
            $iTotalRecords = $querys->where($conditions)->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayStart = intval($_REQUEST['start']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $querys =  $querys->where($conditions)
                		->skip($iDisplayStart)->take($iDisplayLength)
                		->OrderBy('id','DESC')
                		->get();
            $sEcho = intval($_REQUEST['draw']);
            $records = array();
            $records["data"] = array(); 
            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;
            $i=$iDisplayStart;
            $querys=json_decode( json_encode($querys), true);
            foreach($querys as $coupon){
                $checked='';
                if($coupon['status']==1){
                    $checked='on';
                }
                else{
                    $checked='off';
                }
                $deletcoupon ='<a  title="Delete"  class="btn btn-sm red margin-top-10 delete"  onclick=" return ConfirmDelete()" href="'.url('admin/delete-coupon/'.$coupon['id']).'"> <i class="fa fa-times"></i>
                    </a>'; 
                $actionValues='
                    <a title="Edit" class="btn btn-sm green margin-top-10" href="'.url('/admin/add-edit-coupon/'.$coupon['id']).'"> <i class="fa fa-edit"></i>
                    </a>'.$deletcoupon;

                if($coupon['amount_type'] =="Percentage"){
                	$amount = $coupon['amount'] ." %";
                }else{
                	$amount = $coupon['amount'] ." Rs.";
                }
                $num = ++$i;
                if($checkEditAccess){
                    $status = '<div  id="'.$coupon['id'].'" rel="coupon_codes" class="bootstrap-switch  bootstrap-switch-'.$checked.'  bootstrap-switch-wrapper bootstrap-switch-animate toogle_switch">
                    <div class="bootstrap-switch-container" ><span class="bootstrap-switch-handle-on bootstrap-switch-primary">&nbsp;Active&nbsp;&nbsp;</span><label class="bootstrap-switch-label">&nbsp;</label><span class="bootstrap-switch-handle-off bootstrap-switch-default">&nbsp;Inactive&nbsp;</span></div></div>';
                }else{
                    $status ='No Access';
                }
                $records["data"][] = array(      
                    $num,
                    $coupon['code'],
                    $coupon['coupon_type'],
                    $amount,
                    date('d-F-Y',strtotime($coupon['expiry_date'])),
                    $status,  
                    $actionValues
                );
            }
            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            return response()->json($records);
        }
        $title = "Coupons";
        return View::make('admin.coupons.coupons')->with(compact('title'));
    }

    public function addEditCoupon(Request $request,$couponid=NULL){
    	$Selcats = array();
    	$SelProds = array();
    	if(!empty($couponid)){
    		$couponData = CouponCode::where('id',$couponid)->first();
    		$couponData = json_decode(json_encode($couponData),true);
            $Selcats = explode(',',$couponData['categories']);
            $SelProds = explode(',',$couponData['products']);
    		$title ="Edit Coupon";
    	}else{
    		$title ="Add Coupon";
	    	$couponData =array();
    	}
    	return view('admin.coupons.add-edit-coupon')->with(compact('title','couponData','Selcats','SelProds'));
    }


    public function saveCoupon(Request $request){
    	try{
            if($request->ajax()){
                $data = $request->all();
                if($data['couponid']==""){
                	if($data['codeoption'] =="Manual"){
	                    $validator = Validator::make($request->all(), [
                       		'code'=>'bail|required|unique:coupon_codes,code',
	                    ]);
	                    if(!$validator->passes()) {
	                        return response()->json(['status'=>false,'type'=>'validation','errors'=>$validator->messages()]);
	                    }
	                }
	                if($data['coupon_type'] =="Limited Times"){
	                    $validator = Validator::make($request->all(), [
	                        "limited_time"    => "bail|required|numeric|lt:1000",
	                    ]);
	                    if(!$validator->passes()) {
	                        return response()->json(['status'=>false,'type'=>'validation','errors'=>$validator->messages()]);
	                    }
	                }
                	$type = "add";
                    $validator = Validator::make($request->all(), [
			                'coupon_type'=>'bail|required',
			                'coupon_applicable_on'=>'bail|required',
			                'amount_type'=>'bail|required',
			                'amount'=>'bail|required|numeric',
			                'min_qty'=>'bail|required|numeric',
			                'min_amount'=>'bail|required|numeric|gt:1',
			                'start_date'=>'bail|required|date_format:Y-m-d',
			                'expiry_date'=>'bail|required|date_format:Y-m-d',
	                    ]
	                );
                }else{ 
                	$type ="update";
	                $validator = Validator::make($request->all(), [
			                'coupon_type'=>'bail|required',
			                'coupon_applicable_on'=>'bail|required',
			                'amount_type'=>'bail|required',
			                'amount'=>'bail|required|numeric',
			                'min_qty'=>'bail|required|numeric',
			                'min_amount'=>'bail|required|numeric|gt:1',
			                'start_date'=>'bail|required|date_format:Y-m-d',
			                'expiry_date'=>'bail|required|date_format:Y-m-d',
	                    ]
	                );
                }
                if($validator->passes()) {
                    $data = $request->all();
                    if($type =="add"){
                        $coupon = new CouponCode; 
                    }else{
                        $coupon = CouponCode::find($data['couponid']); 
                    }
                    if($type=="add"){
                    	if($data['codeoption'] =="Manual"){
				            $coupon->code = strtolower($data['code']);
				        }else{
				            $code = Str::random(6);
				            $checkCode = DB::table('coupon_codes')->where(['code'=>$code])->count();
				            while($checkCode>0)
				            {
				                $code = Str::random(6);
				                $checkCode = DB::table('coupon_codes')->where(['code'=>$code])->count();
				            }
				            $coupon->code = strtolower($code);
				        }
                    	$coupon->codeoption = $data['codeoption'];
                    }
                    if(isset($data['user_emails'])){
			            $coupon->user_emails = implode(',',$data['user_emails']);
			        }
                    if($data['coupon_behaviour'] =="category"){
                        if(isset($data['categories']) && !empty($data['categories'])){
    			            $coupon->categories = implode(',',$data['categories']);
    			        }else{
                            $coupon->categories = "";
                        }
                    }else{
                        $coupon->categories = "";
                    }
                    if($data['coupon_behaviour'] =="product"){
    			        if(isset($data['products']) && !empty($data['products'])){
    			            $coupon->products = implode(',',$data['products']);
    			        }else{
                            $coupon->products = "";
                        }
                    }else{
                         $coupon->products = "";
                    }
			        if($data['coupon_type'] =="Limited Times"){
						$coupon->limited_time = $data['limited_time'];
			        }else{
			        	$coupon->limited_time = 0;
			        }
			        $coupon->coupon_behaviour = $data['coupon_behaviour'];
			        $coupon->coupon_type = $data['coupon_type'];
                    $coupon->amount_type = $data['amount_type'];
			        $coupon->min_qty = $data['min_qty'];
			        $coupon->max_qty = $data['max_qty'];
			        $coupon->min_amount = $data['min_amount'];
			        $coupon->max_amount = $data['max_amount'];
			        $coupon->start_date = $data['start_date'];
			        $coupon->expiry_date = $data['expiry_date'];
			        $coupon->amount = $data['amount'];
			        $coupon->coupon_applicable_on = $data['coupon_applicable_on'];
                    if(isset($data['status'])){
			            $coupon->status = 1;
			        }else{
			            $coupon->status = 0;
			        }
			        if(isset($data['visible'])){
			            $coupon->visible = 1;
			        }else{
			            $coupon->visible = 0;
			        }
                    $coupon->save();
                    $redirectTo = url('/admin/coupons?s');
                    return response()->json(['status'=>true,'message'=>'ok','url'=>$redirectTo]);
                }else{
                    return response()->json(['status'=>false,'errors'=>$validator->messages()]);
                }
            }
        }catch(\Exception $e){
            return response()->json(['status'=>false,'message'=>$e->getMessage(),'errors'=>array('amount'=>$e->getMessage())]);
        }
    }

    public function deleteCoupon($id){
    	CouponCode::where('id',$id)->delete();
    	return redirect()->back()->with('flash_message_success','Coupon has been deleted succesfully');
    }
}
