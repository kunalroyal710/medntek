<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use DB;
use Cookie;
use Session;
use Crypt;
use Illuminate\Support\Facades\Mail;
use App\Models\Brand;
use Auth;
use Image;
use Validator;
class BrandController extends Controller
{
    //
    public function brands(Request $Request){
        Session::put('active','brands'); 
        if($Request->ajax()){
            $conditions = array();
            $data = $Request->input();
            $querys = Brand::query();
            if(!empty($data['name'])){
                $querys = $querys->where('name','like','%'.$data['name'].'%');
            }
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayStart = intval($_REQUEST['start']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iTotalRecords = $querys->where($conditions)->count();
            $querys =  $querys->where($conditions)
                		->skip($iDisplayStart)->take($iDisplayLength)
                		->OrderBy('id','DESC')
                		->get();
            $sEcho = intval($_REQUEST['draw']);
            $records = array();
            $records["data"] = array(); 
            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;
            $i=$iDisplayStart;
            $querys=json_decode( json_encode($querys), true);
            foreach($querys as $brand){
                $checked='';
                if($brand['status']==1){
                    $checked='on';
                }
                else{
                    $checked='off';
                }
                $actionValues='
                    <a title="Edit" class="btn btn-sm green margin-top-10" href="'.url('/admin/add-edit-brand/'.$brand['id']).'"> <i class="fa fa-edit"></i>
                    </a>';
                $foldername = '/images/BrandImages/';
                if(!empty($brand['brand_logo']) && media_exists($foldername,$brand['brand_logo']) ){
                    $brand_logo = '<img width="100px" src='.url($foldername.$brand['brand_logo']).'>';
                }else{
                    $brand_logo = '<img width="100px" src='.url('images/default.png').'>';
                }
                $num = ++$i;
                $records["data"][] = array(      
                    $brand['id'],
                    $brand['name'],
                    $brand_logo,
                    $brand['description'],
                    $brand['sort'],
                    '<div  id="'.$brand['id'].'" rel="brands" class="bootstrap-switch  bootstrap-switch-'.$checked.'  bootstrap-switch-wrapper bootstrap-switch-animate toogle_switch">
                    <div class="bootstrap-switch-container" ><span class="bootstrap-switch-handle-on bootstrap-switch-primary">&nbsp;Active&nbsp;&nbsp;</span><label class="bootstrap-switch-label">&nbsp;</label><span class="bootstrap-switch-handle-off bootstrap-switch-default">&nbsp;Inactive&nbsp;</span></div></div>',   
                    $actionValues
                );
            }
            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            return response()->json($records);
        }
        $title = "Brands";
        return View::make('admin.brands.brands')->with(compact('title'));
    }

    public function addEditBrand(Request $request,$brandid=NULL){
    	if(!empty($brandid)){
    		$branddata = Brand::where('id',$brandid)->first();
    		$title ="Edit Brand";
    	}else{
    		$title ="Add Brand";
	    	$branddata =array();
    	}
    	return view('admin.brands.add-edit-brand')->with(compact('title','branddata'));
    }

    public function saveBrand(Request $request){
    	try{
            if($request->ajax()){
                $data = $request->all();
                if($data['brandid']==""){
                    $type ="add";
                    $slugunique = "unique:brands";
                }else{ 
                    $type ="update";
                    $slugunique = "unique:brands,slug,".$data['brandid'];
                }
                $validator = Validator::make($request->all(), [
                        'slug' => 'bail|required|'.$slugunique,
                        'name' => 'bail|required',
                        'sort' => 'bail|required|integer|min:1',
                        'status' => 'bail|required'
                    ]
                );
                if($validator->passes()) {
                    $data = $request->all();
                    if($type =="add"){
                        $brand = new Brand; 
                    }else{
                        $brand = Brand::find($data['brandid']); 
                    }
                    $brand->name = $data['name'];
                    $brand->slug = $data['slug'];
                    $brand->sort = $data['sort'];
                    $brand->description = $data['description'];
                    $brand->meta_title = $data['meta_title'];
                    $brand->meta_description = $data['meta_description'];
                    $brand->meta_keyword = $data['meta_keyword'];
                    $brand->status = $data['status'];

                    if($request->hasFile('brand_logo')){
                        $file = $request->file('brand_logo');
                        $img = Image::make($file);
                        $destination = public_path('/images/BrandImages/');
                        $ext = $file->getClientOriginalExtension();
                        $mainFilename = str_random(5).time().".".$ext;
                        $img->save($destination.$mainFilename);
                        $brand->brand_logo = $mainFilename;
                    }
                    $brand->save();
                    $redirectTo = url('/admin/brands?s');
                    return response()->json(['status'=>true,'message'=>'ok','url'=>$redirectTo]);
                }else{
                    return response()->json(['status'=>false,'errors'=>$validator->messages()]);
                }
            }
        }catch(\Exception $e){
            return response()->json(['status'=>false,'message'=>$e->getMessage(),'errors'=>array('password'=>$e->getMessage())]);
        }
    }
}
