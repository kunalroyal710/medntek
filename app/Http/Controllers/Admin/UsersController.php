<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use DB;
use Cookie;
use Session;
use Crypt;
use Illuminate\Support\Facades\Mail;
use App\Models\AdminRole;
use Auth;
class UsersController extends Controller
{
    //
    public function users(Request $Request){
        Session::put('active','users'); 
        $checkEditAccess = AdminRole::checkupdateAccess('admin/add-edit-user/{id?}');
        if($Request->ajax()){
            $conditions = array();
            $data = $Request->input();
            $querys = DB::table('users');
            if(!empty($data['name'])){
                $querys = $querys->where('name','like','%'.$data['name'].'%');
            }
            if(!empty($data['email'])){
                $querys = $querys->where('email','like','%'.$data['email'].'%');
            }
            if(!empty($data['mobile'])){
                $querys = $querys->where('mobile','like','%'.$data['mobile'].'%');
            }
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayStart = intval($_REQUEST['start']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iTotalRecords = $querys->where($conditions)->count();
            $querys =  $querys->where($conditions)
                	->skip($iDisplayStart)->take($iDisplayLength)
                	->OrderBy('id','Desc')
                	->get();
            $sEcho = intval($_REQUEST['draw']);
            $records = array();
            $records["data"] = array(); 
            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;
            $i=$iDisplayStart;
            $querys=json_decode( json_encode($querys), true);
            foreach($querys as $user){
                $id= base64_encode(convert_uuencode($user['id'])); 
                $checked='';
                if($user['status']==1){
                    $checked='on';
                }else{
                    $checked='off';
                }
                $actionValues='
                    <a title="Edit User" class="btn btn-sm green margin-top-10" href="'.url('admin/add-edit-user/'.$user['id']).'"> <i class="fa fa-edit"></i>
                    </a>';
                $actionValues ='';
                $num = ++$i;
                if($checkEditAccess){
                    $status = '<div  id='.$user['id'].' rel=users class="bootstrap-switch  bootstrap-switch-'.$checked.'  bootstrap-switch-wrapper bootstrap-switch-animate toogle_switch">
                    <div class="bootstrap-switch-container" ><span class="bootstrap-switch-handle-on bootstrap-switch-primary">&nbsp;Active&nbsp;&nbsp;</span><label class="bootstrap-switch-label">&nbsp;</label><span class="bootstrap-switch-handle-off bootstrap-switch-default">&nbsp;Inactive&nbsp;</span></div></div>';
                }else{
                    $status ='No Access';
                }
                $records["data"][] = array(     
                    $num,
                    $user['name'],
                    $user['email'],
                    $user['mobile'],
                    "<b>Address:</b>&nbsp;&nbsp;".$user['address_line_1'].'<br><b>State:</b>&nbsp;&nbsp;'.$user['state']."<br><b>City:</b>&nbsp;&nbsp;".$user['city']."<br><b>Postcode:</b> &nbsp;&nbsp;".$user['postcode'],
                    $status,   
                    $actionValues
                );
            }
            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            return response()->json($records);
        }
        $title = "Users";
        return View::make('admin.users.users')->with(compact('title'));
    }
}
