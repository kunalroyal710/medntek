<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Models\Admin;
use Auth;
use DB;
use Cookie;
use Session;
use Crypt;
use Illuminate\Support\Facades\Mail;
use PDF;
use App\Models\Module;
use App\Models\AdminRole;
use App\Models\CmsPage;
use Hash;
use Illuminate\Support\Arr;
class AdminController extends Controller
{
    //
    public function status(Request $request){
        if(Auth::guard('admin')->check()){
        	if($request->ajax()){
                $data = $request->input();
                if(DB::table($data['table'])->where('id', $data['id'])->update(['status' => $data['status'] ]) ){
                    echo "1";die;
                } else {
                    echo "0";die; 
                }
            }
        }
    }

    public function login(Request $request){
    	if(Auth::guard('admin')->check()){
    		return redirect('admin/dashboard');
    	}
    	if($request->isMethod('post')){
    		$data = $request->all();
    		$rules = [
                'email' => 'bail|required|email|regex:/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i|max:255',
                'password' => 'bail|required',
            ];
            $customMessages = [
            	//Add custom Messages here
            ];
            $this->validate($request, $rules, $customMessages);
    		if(Auth::guard('admin')->attempt($request->only('email','password'))) {
    			if(Auth::guard('admin')->user()->status ==0){
    				Auth::guard('admin')->logout();
		    		return redirect()->back()->with('flash_message_error','Account deactivated');
    			}else{
			        return redirect('admin/dashboard')->with('flash_message_success','Logged in successfully');
    			}
		    }else{
		    	return redirect()->back()->with('flash_message_error','Invalid email or password');
		    }
    	}
    	return view('admin.admin_login');
    }

    public function checkAdminEmail(Request $request) {
        $data = $request->all();
        $email = $data['email'];
        $count = DB::table('admins')
                       ->where('email', $email)
                       ->count();
        if($count == 1) {
            echo '{"valid":true}';die;
        } else {
            echo '{"valid":false}';die;;
        }
    }

    public function dashboard(){
        Session::put('active',1);
        if(Auth::guard('admin')->user()->type =="admin"){
            $getModules = DB::table('modules')->where('status',1)->where('table_name','!=','')->select('id','name','view_route','table_name','icon')->orderBy('sortorder','ASc')->get();
        }else{
            $getsubadminmodules = DB::table('admin_roles')->where(['admin_id'=>Auth::guard('admin')->user()->id,'view_access'=>'1'])->select('module_id')->get();
            $getsubadminmodules = Arr::flatten(json_decode(json_encode($getsubadminmodules),true));
            $getModules = DB::table('modules')->where('status',1)->whereIn('id',$getsubadminmodules)->where('table_name','!=','')->select('id','name','view_route','table_name','icon')->orderBy('sortorder','ASc')->get();
        }
        $getModules = json_decode(json_encode($getModules),true);
        foreach ($getModules as $key => $module) {
            if($module['table_name']=="admins"){
                $getModules[$key]['table_count'] = DB::table('admins')->where('type','!=','admin')->count();
            }else{
                $getModules[$key]['table_count'] = DB::table($module['table_name'])->count();
            }
        }
        $title = "Dashboard";
        return view('admin.admin_dashboard')->with(compact('title','getModules'));
    }

    public function profile(Request $request){
        Session::put('active',3);
        $admindata = DB::table('admins')->where('id', Auth::guard('admin')->user()->id)->first();
        $admindata=json_decode( json_encode($admindata), true);
        $title = "Profile";
        return view('admin.profile', ['admindata'=>$admindata,'title'=>$title]);
    }

    public function logout(){
        Auth::guard('admin')->logout();
        return redirect()->action('Admin\AdminController@login')->with('flash_message_success', 'Logged out successfully.');
       
    }

    public function settings(Request $request){
        Session::put('active',4);
        if($request->isMethod('post')){
            $data = $request->all();
            $this->validate($request, [
                'name'=>'required',
                'email'=>'required|email',
                'mobile'=>'required'
        ]);
        $update_data = DB::table('admins')
            ->where('id', Auth::guard('admin')->user()->id)
            ->update([
                'name'=>$data['name'],
                'email'=>$data['email'],
                'mobile'=>$data['mobile']]); 
            return redirect()->back()->with('flash_message_success','Profile has been updated successfully');
        } else{
            $admindata = DB::table('admins')->where('id', Auth::guard('admin')->user()->id)->first();
            $admindata =json_decode( json_encode($admindata), true);
            $title = "Account Settings";
            return view('admin.admin_accountSettings', ['admindata'=>$admindata,'title'=>$title]); 
        }
    }

    public function changeAdminLogo(Request $request){
    	if($request->isMethod('post')){
    		$image=$_FILES;
	        if($image['image']['error']==0){
	            $imgName = pathinfo($_FILES['image']['name']);
	            $ext = $imgName['extension'];
	            $NewImageName = rand(4,10000);
	            $destination = base_path() . '/public/images/AdminImages/';
	            if(move_uploaded_file($image['image']['tmp_name'],$destination.$NewImageName.".".$ext)){
	                if(file_exists($destination.Auth::guard('admin')->user()->image) && !empty(Auth::guard('admin')->user()->image)){
	                    unlink($destination.Auth::guard('admin')->user()->image);
	                }  
	                $image =DB::table('admins')
	                ->where('id', Auth::guard('admin')->user()->id)
	                ->update(['image' => $NewImageName.".".$ext]);
	                if(!empty($image)){
	                   return redirect()->action('Admin\AdminController@profile')->with('flash_message_success', 'Image has been uploaded successfully');         
	                } else {
	                   return redirect('admin/settings/#tab_1_2')->with('flash_message_error', 'You have not Select any image'); 
	                }
	            }
	        }
	        else {
	            return redirect('admin/settings/#tab_1_2')->with('flash_message_error', 'You have not Select any image'); 
	        }
    	}
    }

    public function changeAdminPassword(Request $request){
        if($request->isMethod('post')){
            $data = $request->input();
            if(!empty($data)){
                if (Hash::check($data['password'], Auth::guard('admin')->user()->password)){
                    if($data['new_password'] == $data['re_password']){
                            DB::table('admins')
                            ->where('id', Auth::guard('admin')->user()->id)
                            ->update(['password' => bcrypt($data['new_password'])]);  
                        return redirect('admin/settings/')->with('flash_message_success', 'Password has been updated successfully');   
                    }else{
                        return redirect('admin/settings/#tab_1_3')->with('flash_message_error', 'New password and Retype password not match'); 
                    }
                } else {
                    return redirect('admin/settings/#tab_1_3')->with('flash_message_error', 'Your current password is incorrect'); 
                }
            }
        }
    }

    public function subadmins(Request $Request){
        Session::put('active','subadmins'); 
        if($Request->ajax()){
            $conditions = array();
            $data = $Request->input();
            $querys = DB::table('admins')->where('type','!=','admin');
            if(!empty($data['name'])){
                $querys = $querys->where('name','like','%'.$data['name'].'%');
            }
            if(!empty($data['email'])){
                $querys = $querys->where('email','like','%'.$data['email'].'%');
            }
            $querys = $querys->OrderBy('id','DESC');
            $iTotalRecords = $querys->where($conditions)->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayStart = intval($_REQUEST['start']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $querys =  $querys->where($conditions)
                    ->skip($iDisplayStart)->take($iDisplayLength)
                    ->get();
            $sEcho = intval($_REQUEST['draw']);
            $records = array();
            $records["data"] = array(); 
            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;
            $i=$iDisplayStart;
            $querys=json_decode( json_encode($querys), true);
            foreach($querys as $subadmin){
                $id= base64_encode(convert_uuencode($subadmin['id'])); 
                $checked='';
                if($subadmin['status']==1){
                    $checked='on';
                }else{
                    $checked='off';
                }
                $adminrole ='<a title="Update Subadmin Role" class="btn btn-sm yellow margin-top-10" href="'.url('admin/update-role/'.$subadmin['id']).'"> <i class="fa fa-unlock-alt"></i>
                    </a>';
                $actionValues='<a title="Edit Subadmin" class="btn btn-sm green margin-top-10" href="'.url('admin/add-edit-subadmin/'.$subadmin['id']).'"> <i class="fa fa-edit"></i>
                    </a>'.$adminrole.'<a title="Change Password" data-userid="'.$subadmin['id'].'" class="btn btn-sm blue margin-top-10 editPassword" href="javascript:;"> <i class="fa fa-clock-o"></i>
                    </a>';
                $num = ++$i;
                $records["data"][] = array(     
                    $num,
                    $subadmin['name'],
                    $subadmin['email'],
                    $subadmin['mobile'],
                    '<div  id="'.$subadmin['id'].'" rel="admins" class="bootstrap-switch  bootstrap-switch-'.$checked.'  bootstrap-switch-wrapper bootstrap-switch-animate toogle_switch">
                    <div class="bootstrap-switch-container" ><span class="bootstrap-switch-handle-on bootstrap-switch-primary">&nbsp;Active&nbsp;&nbsp;</span><label class="bootstrap-switch-label">&nbsp;</label><span class="bootstrap-switch-handle-off bootstrap-switch-default">&nbsp;Inactive&nbsp;</span></div></div>',   
                    $actionValues
                );
            }
            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            return response()->json($records);
        }
        $title = "SubAdmins";
        return View::make('admin.subadmins.subadmins')->with(compact('title'));
    }

    public function addeditSubadmin(Request $request,$id=null){
        Session::put('active','subadmins'); 
        if($id ==""){
            $message ='SubAdmin has been added successfully!';
            $title = "Add SubAdmin"; 
            $admin = new Admin;
            $admindata = array();
            $getunitAccessids = array();
        }else{
            $message = 'SubAdmin has been updated successfully!';
            $title = "Edit SubAdmin"; 
            $admin = Admin::find($id);
            $admindata = json_decode(json_encode($admin),true);
        }
        if($request->isMethod('post')){
            $data = $request->all();
            unset($data['_token']);
            foreach ($data as $key => $value) {
                if($key != "password"){
                    $admin->$key = $value;
                }
            }
            if(empty($admindata)){
                $admin->password = bcrypt($data['password']);
            }
            $admin->type="subadmin";
            $admin->save();
            return redirect::to('admin/subadmins')->with('flash_message_success',$message);
        }
        return view('admin.subadmins.add-edit-subadmin')->with(compact('title','admindata'));
    }

    public function changeSudadminPassword(Request $request){
        if($request->ajax()){
            $data = $request->all();
            $userdata = Admin::where('id',$data['id'])->select('id','password')->first();
            $userdata =json_decode(json_encode($userdata),true);
            echo json_encode($userdata); die;
        }
        if($request->isMethod('post')){
            $data = $request->all();
            $user = Admin::find($data['id']);
            $user->password = bcrypt($data['password']);
            $user->save();
            return redirect()->back()->with('flash_message_success', 'Password has been Updated Successfully!');
        }
    } 

    public function updateRole(Request $request,$id){
        $getModules = Module::where('shown_in_roles','1')->get();
        $getModules = json_decode(json_encode($getModules),true);
        $adminid = $id;
        $getRoleDetails = AdminRole::where('admin_id',$adminid)->get();
        $getRoleDetails = json_decode(json_encode($getRoleDetails),true);
        if($request->isMethod('post')){
            $data = $request->all();
            foreach ($data['module_id'] as $mkey => $module) {
                $checkIfExists = AdminRole::where(['admin_id'=>$id,'module_id'=>$mkey])->first();
                if(!empty($checkIfExists)){
                    $adminrole = AdminRole::find($checkIfExists->id);
                }else{
                    $adminrole = new AdminRole;
                    $adminrole->admin_id = $id;
                    $adminrole->module_id = $mkey;
                }
                if(is_array($data['module_id'][$mkey])){
                    foreach ($data['module_id'][$mkey] as $akey => $value) {
                        if(isset($data['module_id'][$mkey]['view_access'])){
                            $adminrole->$akey = 1;
                        }else{
                            $adminrole->view_access = 0;
                        }
                        if(isset($data['module_id'][$mkey]['edit_access'])){
                            $adminrole->$akey = 1;
                        }else{
                            $adminrole->edit_access = 0;
                        }
                        if(isset($data['module_id'][$mkey]['delete_access'])){
                            $adminrole->$akey = 1;
                        }else{
                            $adminrole->delete_access = 0;
                        }
                    }
                }else{
                    $adminrole->view_access = 0;
                    $adminrole->edit_access = 0;
                    $adminrole->delete_access = 0;
                }
                $adminrole->save();
            }
            return redirect()->back()->with('flash_message_success','Subadmins Roles Updated Successfully!');
        }
        $title = "Update SubAdmin Role";
        return view('admin.subadmins.update-roles')->with(compact('title','adminid','getRoleDetails','getModules'));
    }

    public function subscribers(Request $Request){
        Session::put('active','subscribers'); 
        if($Request->ajax()){
            $conditions = array();
            $data = $Request->input();
            $querys = DB::table('subscribers');
            if(!empty($data['email'])){
                $querys = $querys->where('email','like','%'.$data['email'].'%');
            }
            $iTotalRecords = $querys->where($conditions)->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayStart = intval($_REQUEST['start']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $querys =  $querys->where($conditions)
                ->skip($iDisplayStart)->take($iDisplayLength)
                ->OrderBy('subscribers.id','DESC')
                ->get();  
            $sEcho = intval($_REQUEST['draw']);
            $records = array();
            $records["data"] = array(); 
            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;
            $i=$iDisplayStart;
            $querys=json_decode( json_encode($querys), true);
            foreach($querys as $websetting){
                $actionValues='';
                $records["data"][] = array(  
                    $websetting['email'],  
                    $websetting['mobile'], 
                    $actionValues
                );
            }
            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            return response()->json($records);
        }
        $title = "Subscribers";
        return View::make('admin.users.subscribers')->with(compact('title'));
    }

    public function cmspages(Request $Request){
        Session::put('active','cmspages'); 
        if($Request->ajax()){
            $conditions = array();
            $data = $Request->input();
            $querys = DB::table('cms_pages');
            if(!empty($data['title'])){
                $querys = $querys->where('title','like','%'.$data['title'].'%');
            }
            $iTotalRecords = $querys->where($conditions)->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayStart = intval($_REQUEST['start']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $querys =  $querys->where($conditions)
                ->skip($iDisplayStart)->take($iDisplayLength)
                ->OrderBy('id','DESC')
                ->get();  
            $sEcho = intval($_REQUEST['draw']);
            $records = array();
            $records["data"] = array(); 
            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;
            $i=$iDisplayStart;
            $querys=json_decode( json_encode($querys), true);
            foreach($querys as $cms){
                $checked='';
                if($cms['status']==1){
                    $checked='on';
                }else{
                    $checked='off';
                } 
                $status = '<div  id='.$cms['id'].' rel=cms_pages class="bootstrap-switch  bootstrap-switch-'.$checked.'  bootstrap-switch-wrapper bootstrap-switch-animate toogle_switch">
                    <div class="bootstrap-switch-container" ><span class="bootstrap-switch-handle-on bootstrap-switch-primary">&nbsp;Active&nbsp;&nbsp;</span><label class="bootstrap-switch-label">&nbsp;</label><span class="bootstrap-switch-handle-off bootstrap-switch-default">&nbsp;Inactive&nbsp;</span></div></div>';
                $actionValues='<a title="Edit" class="btn btn-sm green margin-top-10" href="'.url('admin/edit-cms-page/'.$cms['id']).'"> <i class="fa fa-edit"></i>
                    </a>';
                $records["data"][] = array(  
                    $cms['title'], 
                    $status,
                    $actionValues
                );
            }
            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            return response()->json($records);
        }
        $title = "CMS Pages";
        return View::make('admin.footer.cms-pages')->with(compact('title'));
    }

    public function editCmsPage(Request $request, $id){
        if($request->isMethod('post')){
            $input=$request->all();
            if(!empty($input)){ 
                CmsPage::where('id',$id)->update(array(
                    'description' =>  $input['description']
                ));
               return redirect()->action('Admin\AdminController@cmspages')->with('flash_message_success', 'Record Updated successfully.');
            }
        }
        $details=DB::table('cms_pages')->where('id',$id)->first();
        $details=json_decode(json_encode($details),true);
        $title = "Edit CMS Page";
        return View::make('admin.footer.edit-cms-page')->with(compact(array('details','title')));
    }
}
