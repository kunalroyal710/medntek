<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use DB;
use Cookie;
use Session;
use Crypt;
use Illuminate\Support\Facades\Mail;
use App\Models\Category;
use Auth;
use Image;
use Validator;
class CategoryController extends Controller
{
    //
    public function categories(Request $Request){
        Session::put('active','categories'); 
        if($Request->ajax()){
            $conditions = array();
            $data = $Request->input();
            $querys = Category::query();
            if(!empty($data['name'])){
                $querys = $querys->where('name','like','%'.$data['name'].'%');
            }
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayStart = intval($_REQUEST['start']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iTotalRecords = $querys->where($conditions)->count();
            $querys =  $querys->where($conditions)
                		->skip($iDisplayStart)->take($iDisplayLength)
                		->OrderBy('id','DESC')
                		->get();
            $sEcho = intval($_REQUEST['draw']);
            $records = array();
            $records["data"] = array(); 
            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;
            $i=$iDisplayStart;
            $querys=json_decode( json_encode($querys), true);
            foreach($querys as $category){
                $checked='';
                if($category['status']==1){
                    $checked='on';
                }else{
                    $checked='off';
                }
                if($category['parent_id'] == NULL){
                    $parentcategory = "ROOT";
                }else{
                    $parent_category = DB::table('categories')->where('id',$category['parent_id'])->select('name')->first();
                    $parentcategory = $parent_category->name;
                }
                $actionValues='
                    <a title="Edit" class="btn btn-sm green margin-top-10" href="'.url('/admin/add-edit-category/'.$category['id']).'"> <i class="fa fa-edit"></i>
                    </a>';
                $num = ++$i;
                $records["data"][] = array(      
                    $category['id'],
                    $category['name'],
                    $parentcategory,
                    $category['description'],
                    $category['sort'],
                    '<div  id="'.$category['id'].'" rel="categories" class="bootstrap-switch  bootstrap-switch-'.$checked.'  bootstrap-switch-wrapper bootstrap-switch-animate toogle_switch">
                    <div class="bootstrap-switch-container" ><span class="bootstrap-switch-handle-on bootstrap-switch-primary">&nbsp;Active&nbsp;&nbsp;</span><label class="bootstrap-switch-label">&nbsp;</label><span class="bootstrap-switch-handle-off bootstrap-switch-default">&nbsp;Inactive&nbsp;</span></div></div>',   
                    $actionValues
                );
            }
            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            return response()->json($records);
        }
        $title = "Categories";
        return View::make('admin.categories.categories')->with(compact('title'));
    }

    public function addEditCategory(Request $request,$categoryid=NULL){
    	if(!empty($categoryid)){
    		$categorydata = Category::where('id',$categoryid)->first();
    		$title ="Edit Category";
    	}else{
    		$title ="Add Category";
	    	$categorydata =array();
    	}
    	return view('admin.categories.add-edit-category')->with(compact('title','categorydata'));
    }

    public function saveCategory(Request $request){
    	try{
            if($request->ajax()){
                $data = $request->all();
                if($data['categoryid']==""){
                    $type ="add";
                    $seounique = "unique:categories";
                }else{ 
                    $type ="update";
                    $seounique = "unique:categories,seo_unique,".$data['categoryid'];
                }
                $validator = Validator::make($request->all(), [
                        'seo_unique' => 'bail|required|'.$seounique,
                        'category' => 'bail|required',
                        'name' => 'bail|required',
                        'sort' => 'bail|required|integer|min:1',
                        'status' => 'bail|required'
                    ]
                );
                if($validator->passes()) {
                    $data = $request->all();
                    if($type =="add"){
                        $category = new Category; 
                    }else{
                        $category = Category::find($data['categoryid']); 
                    }
                    $category->name = $data['name'];
                    $category->seo_unique = $data['seo_unique'];
                    if($data['category']=="ROOT"){
                        $category->parent_id = NULL;
                    }else{
                        $category->parent_id = $data['category'];
                    }
                    $category->sort = $data['sort'];
                    $category->description = $data['description'];
                    $category->category_discount = $data['category_discount'];
                    $category->meta_title = $data['meta_title'];
                    $category->meta_description = $data['meta_description'];
                    $category->meta_keyword = $data['meta_keyword'];
                    $category->status = $data['status'];

                    if($request->hasFile('image')){
                        $file = $request->file('image');
                        $img = Image::make($file);
                        $destination = public_path('/images/CategoryImages/');
                        $ext = $file->getClientOriginalExtension();
                        $mainFilename = str_random(5).time().".".$ext;
                        $img->save($destination.$mainFilename);
                        $category->image = $mainFilename;
                    }
                    if($request->hasFile('normal_image')){
                        $file = $request->file('normal_image');
                        $img = Image::make($file);
                        $destination = public_path('/images/CategoryImages/');
                        $ext = $file->getClientOriginalExtension();
                        $mainFilename = "normal-".uniqid().str_random(5).time().".".$ext;
                        $img->save($destination.$mainFilename);
                        $category->normal_image = $mainFilename;
                    }
                    if($request->hasFile('hover_image')){
                        $file = $request->file('hover_image');
                        $img = Image::make($file);
                        $destination = public_path('/images/CategoryImages/');
                        $ext = $file->getClientOriginalExtension();
                        $mainFilename = "hover-".uniqid().str_random(5).time().".".$ext;
                        $img->save($destination.$mainFilename);
                        $category->hover_image = $mainFilename;
                    }
                    if($data['categoryid'] != ""){
                        if(!empty($data['category_discount'])){
                            $catpercentage = $data['category_discount'];
                            $currentDis = 'category';
                        }else{
                            $currentDis = '';
                            $catpercentage = 0;
                        }
                        $catid = $data['categoryid'];
                        \App\Models\Product::wherein('discount_type',['','category'])->where('category_id',$catid)
                            ->update(array(
                                'discount_type' =>$currentDis,
                                'discount'     => $catpercentage,
                                'final_price' => DB::raw('price - (price * '.$catpercentage.' / 100.0)')
                            ));
                    }
                    $category->save();
                    $redirectTo = url('/admin/categories?s');
                    return response()->json(['status'=>true,'message'=>'ok','url'=>$redirectTo]);
                }else{
                    return response()->json(['status'=>false,'errors'=>$validator->messages()]);
                }
            }
        }catch(\Exception $e){
            return response()->json(['status'=>false,'message'=>$e->getMessage(),'errors'=>array('name'=>$e->getMessage())]);
        }
    }
}
