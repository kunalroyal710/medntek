<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Models\FrequentProduct;
use DB;
use Cookie;
use Session;
use Crypt;
use Illuminate\Support\Facades\Mail;
use App\Models\Category;
use App\Models\Product;
use App\Models\AdminRole;
use App\Models\ProductCategory;
use App\Models\ProductAttribute;
use App\Models\ProductImage;
use App\Models\ProductSection;
use App\Models\ProductReview;
use Auth;
use Image;
use Validator;
class ProductController extends Controller
{
    //
    public function products(Request $Request){
        Session::put('active','products'); 
        $checkEditAccess = AdminRole::checkupdateAccess('admin/add-edit-product/{id?}');
        if($Request->ajax()){
            $conditions = array();
            $data = $Request->input();
            $querys = Product::with('product_image')->join('categories','categories.id','=','products.category_id')->select('categories.name as cat_name','products.product_name','products.id','products.product_code','products.status','products.category_id');
            if(!empty($data['cat_name'])){
                $querys = $querys->where('categories.name','like','%'.$data['cat_name'].'%');
            }
            if(!empty($data['p_name'])){
                $querys = $querys->where('products.product_name','like','%'.$data['p_name'].'%');
            }
            if(!empty($data['sku'])){
                $querys = $querys->where('products.product_code','like','%'.$data['sku'].'%');
            }
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayStart = intval($_REQUEST['start']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iTotalRecords = $querys->where($conditions)->count();
            $querys =  $querys->where($conditions)
                		->skip($iDisplayStart)->take($iDisplayLength)
                		->OrderBy('products.id','DESC')
                		->get();
            $sEcho = intval($_REQUEST['draw']);
            $records = array();
            $records["data"] = array(); 
            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;
            $i=$iDisplayStart;
            $querys=json_decode( json_encode($querys), true);
            foreach($querys as $product){
                $checked='';
                if($product['status']==1){
                    $checked='on';
                }else{
                    $checked='off';
                }
                $actionValues='
                    <a target="_blank" title="Edit" class="btn btn-sm green margin-top-10" href="'.url('/admin/add-edit-product/'.$product['id']).'"> <i class="fa fa-edit"></i>
                    <a target="_blank" title="Upload Product Images" class="btn btn-sm blue margin-top-10" href="'.url('/admin/product-images/'.$product['id']).'"> <i class="fa fa-image"></i>
                    </a>';
                $num = ++$i;
                $foldername = '/images/ProductImages/medium/';
                if(!empty($product['product_image']['image']) && media_exists($foldername,$product['product_image']['image'])) {
                    $productImage = '<img width="100px" src="'.asset($foldername.$product['product_image']['image']).'"/>';
                }else{
                    $productImage ='<img width="100px" src='.url('images/default.png').'>';
                }
                if($checkEditAccess){
                    $status = '<div  id="'.$product['id'].'" rel="products" class="bootstrap-switch  bootstrap-switch-'.$checked.'  bootstrap-switch-wrapper bootstrap-switch-animate toogle_switch">
                    <div class="bootstrap-switch-container" ><span class="bootstrap-switch-handle-on bootstrap-switch-primary">&nbsp;Active&nbsp;&nbsp;</span><label class="bootstrap-switch-label">&nbsp;</label><span class="bootstrap-switch-handle-off bootstrap-switch-default">&nbsp;Inactive&nbsp;</span></div></div>';
                }else{
                    $status ='No Access';
                }
                $records["data"][] = array(      
                    $num,
                    $productImage,
                    '<a target="_blank" href="'.url('/product/'.$product['id'].'/'.str_slug($product['product_name'])).'">'.$product['product_name'].'</a>',
                    $product['product_code'],
                    $product['cat_name'],
                    $status,   
                    $actionValues
                );
            }
            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            return response()->json($records);
        }
        $title = "Products";
        return View::make('admin.products.products')->with(compact('title'));
    }

    public function addEditProduct($productid=null){
    	$productCats = array();
        $frequentProducts =array();
    	if(!empty($productid)){
    		$title ="Edit Product";
    		$productdata = Product::with('attributes')->where('id',$productid)->first();
    		$productdata = json_decode(json_encode($productdata),true);
    		$productCats = ProductCategory::where('product_id',$productid)->select('category_id')->pluck('category_id')->toArray();
            $frequentProducts = FrequentProduct::where('product_id',$productid)->pluck('frequent_product_id')->toArray();
    	}else{
    		$title ="Add Product";
	    	$productdata =array();
    	}
        $allproducts = Product::where('status',1)->select('id','product_name')->get();
        $allproducts = json_decode(json_encode($allproducts),true); 
    	return view('admin.products.add-edit-product')->with(compact('title','productdata','productCats','allproducts','frequentProducts'));
    }

    public function saveProduct(Request $request,$productid=null){
        try{
            if($request->ajax()){
                $data = $request->all();
                if($data['productid']==""){
                    $type ="add";
                }else{ 
                    $type ="update";
                }
                $validator = Validator::make($request->all(), [
                        'product_name' => 'bail|required|',
                        'product_gst' => 'bail|required|',
                        'category_id' => 'bail|required',
                        'product_code' => 'bail|required',
                        'brand_id' => 'bail|required',
                        'product_sort' => 'bail|integer|gte:1',
                        "cats"    => "bail|required|array|min:1",
                        "cats.*"  => "bail|required|distinct|min:1",
                        "price"  => "bail|required|gte:1|regex:/^\d+(\.\d{1,2})?$/",
                        "discount"  => "bail|regex:/^\d+(\.\d{1,2})?$/",
                    ]
                );
                if($validator->passes()) {
                    $data = $request->all();
                    if($type =="add"){
                        $product = new Product; 
                    }else{
                        $product = Product::find($data['productid']); 
                    }
                    $product->product_name = $data['product_name'];
                    $product->product_code = $data['product_code'];
                    $product->brand_id = $data['brand_id'];
                    $product->category_id = $data['category_id'];
                    $product->weight = $data['weight'];
                    $product->salient_features = $data['salient_features'];
                    $product->contact_time = $data['contact_time'];
                    $product->direction_of_use = $data['direction_of_use'];
                    $product->area_of_application = $data['area_of_application'];
                    $product->microbial_efficacy = $data['microbial_efficacy'];
                    $product->product_description = $data['product_description'];
                    $product->short_description = $data['short_description'];
                    $product->technical_details = $data['technical_details'];
                    $product->price = $data['price'];
                    $product->discount = $data['discount'];
                    $product->group_code = $data['group_code'];
                    $product->best_seller = $data['best_seller'];
                    $product->new_arrival = $data['new_arrival'];
                    $product->is_featured = $data['is_featured'];
                    $product->status = $data['status'];
                    $product->product_gst = $data['product_gst'];
                    $product->product_sort = $data['product_sort'];
                    $product->case_count = $data['case_count'];
                    if(!empty($data['discount'])){
                        $product->discount_type = "product";
                        $product->final_price = $data['price'] - ($data['price'] * $data['discount'])/100;
                    }else{
                        $getcatdetails = Category::where('id',$data['category_id'])->select('category_discount')->first();
                        if($getcatdetails->category_discount == 0){
                            $product->discount_type = "";
                            $product->final_price = $data['price'];
                        }else{
                            $product->discount_type = "category";
                            $product->discount = $getcatdetails->category_discount;
                            $product->final_price = $data['price'] - ($data['price'] * $getcatdetails->category_discount )/100;
                        } 
                    }
                    if($request->hasFile('more_detail_image')){
                        $file = $request->file('more_detail_image');
                        $img = Image::make($file);
                        $destination = public_path('/images/ProductImages/DetailImages/');
                        $ext = $file->getClientOriginalExtension();
                        $mainFilename = str_random(5).uniqid().time().".".$ext;
                        $img->save($destination.$mainFilename);
                        $product->more_detail_image = $mainFilename;
                    }
                    //echo "<pre>"; print_r($data); die;
                    $product->save();
                    if($type =="add"){
                        $product->product_categories()->attach($data['cats']);
                    }else{
                        $product->product_categories()->sync($data['cats']);
                    }
                    DB::table('frequent_products')->where('product_id',$product->id)->delete();
                    if(isset($data['fbt']) && !empty($data['fbt'])){
                        foreach ($data['fbt'] as $key => $fbt) {
                            DB::table('frequent_products')->insert(['product_id'=>$product->id,'frequent_product_id'=>$fbt]);
                        }
                    }
                    //Update Stock and Price on Edit Product
                    if(isset($data['attr_id'])){
                        foreach($data['attr_id'] as $attrKeyId => $attrIdDetails){
                            $proAttrUpdate = ProductAttribute::find($attrIdDetails);  
                            $proAttrUpdate->price = $data['attr_price'][$attrKeyId];  
                            $proAttrUpdate->moq = $data['attr_moq'][$attrKeyId];  
                            //$proAttrUpdate->bar_code = $data['attr_bar_code'][$attrKeyId];  
                            $proAttrUpdate->stock = $data['attr_stock'][$attrKeyId];  
                            $proAttrUpdate->save();
                        }
                    }
                    //Add Product Attributes
                    foreach($data['pro_sku'] as $attrKey => $attrDetails){
                        if(!empty($attrDetails)){
                            $proAttr = new ProductAttribute;
                            $proAttr->product_id = $product->id;  
                            $proAttr->sku = $attrDetails;  
                            $proAttr->size = $data['pro_size'][$attrKey];  
                            $proAttr->price = $data['pro_price'][$attrKey];  
                            $proAttr->moq = $data['pro_moq'][$attrKey];  
                            //$proAttr->bar_code = $data['pro_bar_code'][$attrKey];  
                            $proAttr->stock = $data['pro_stock'][$attrKey];  
                            $proAttr->status = 1; 
                            $proAttr->save();
                        }
                    }
                    $redirectTo = url('/admin/products?s');
                    return response()->json(['status'=>true,'message'=>'ok','url'=>$redirectTo]);
                }else{
                    return response()->json(['status'=>false,'errors'=>$validator->messages()]);
                }
            }
        }catch(\Exception $e){
            return response()->json(['status'=>false,'message'=>$e->getMessage(),'errors'=>array('name'=>$e->getMessage())]);
        }    
    }

    public function ChangeAttrStatus(Request $request){
        if($request->ajax()){
            $data = $request->all();
            if($data['status'] =="yes"){
                ProductAttribute::where('id',$data['attrid'])->update(['status'=>1]);
            }else{
                ProductAttribute::where('id',$data['attrid'])->update(['status'=>0]);
            }
            return 'ok';
        }
    }

    public function removeAttribute($attrid,$proid){
        DB::table('carts')->where('product_id',$proid)->delete();
        DB::table('product_attributes')->where('id',$attrid)->delete();
        return redirect()->back()->with('flash_message_success','Product Attribute has been deleted successfully');
    }

    public function productImages(Request $request,$productid){
        if($request->isMethod('post')){
            $data = $request->all();
            if($request->hasFile('images')){
                $files = $request->file('images');
                foreach($files as $fkey => $file){
                    $productImage = new ProductImage;
                    $img = Image::make($file);
                    $originalname = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME);
                    $extension = $file->getClientOriginalExtension();
                    $fileName = uniqid()."-".time().".".$extension;
                    $sizeArray = array('large'=>array('0'=>'1500','1'=>'1900'),'medium'=>array('0'=>' 217','1'=>'275'));
                    foreach($sizeArray as $key=> $size){
                        if($key !="large"){
                            $img->resize($size[0],$size[1]);
                        }
                        $destinationPath = 'images/ProductImages/'.$key."/";
                        $img->save($destinationPath.$fileName);
                    }
                    $productImage->sort = (!empty($data['sort'][$fkey])?$data['sort'][$fkey]:'1');
                    $productImage->product_id = $productid;
                    $productImage->image = $fileName;
                    $productImage->save();
                }
                return redirect::to('/admin/products?s');
            }else{
                return redirect()->back()->with('flash_message_error','Please select atleast one image to upload');
            }
        }
        $title = "Product Images";
        $productImages = DB::table('product_images')->where('product_id',$productid)->get();
        $productImages = json_decode(json_encode($productImages),true);
        return view('admin.products.product-images')->with(compact('title','productImages','productid'));
    }

    public function updateImageSort(Request $request){
        if($request->ajax()){
            $data  = $request->all();
            ProductImage::where(['id'=>$data['imageid']])->update(['sort'=>$data['sort']]);
            return 'ok';
        }
    }

    public function deleteProductImage(Request $request){
        if($request->ajax()){
            $data = $request->all();
            $getdetails = ProductImage::find($data['id']);
            if($getdetails->image!=""){
                if(media_exists('/images/ProductImages/large/',$getdetails->image)){
                    unlink('images/ProductImages/large/'.$getdetails->image);
                }
                if(media_exists('/images/ProductImages/medium/',$getdetails->image)){
                    unlink('images/ProductImages/medium/'.$getdetails->image);
                }
                //unlink('images/ProductImages/small/'.$getdetails->image);
            }
            $getdetails->delete();
            echo "success";
        }
    }

    public function productReviews(Request $Request){
        Session::put('active','productreviews'); 
        if($Request->ajax()){
            $conditions = array();
            $data = $Request->input();
            $querys = ProductReview::join('products','products.id','=','product_reviews.product_id')->join('users','users.id','=','product_reviews.user_id')->select('product_reviews.*','products.product_name','users.name');
            /*if(!empty($data['cat_name'])){
                $querys = $querys->where('categories.name','like','%'.$data['cat_name'].'%');
            }
            if(!empty($data['p_name'])){
                $querys = $querys->where('products.product_name','like','%'.$data['p_name'].'%');
            }
            if(!empty($data['sku'])){
                $querys = $querys->where('products.product_code','like','%'.$data['sku'].'%');
            }*/
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayStart = intval($_REQUEST['start']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iTotalRecords = $querys->where($conditions)->count();
            $querys =  $querys->where($conditions)
                        ->skip($iDisplayStart)->take($iDisplayLength)
                        ->OrderBy('product_reviews.id','DESC')
                        ->get();
            $sEcho = intval($_REQUEST['draw']);
            $records = array();
            $records["data"] = array(); 
            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;
            $i=$iDisplayStart;
            $querys=json_decode( json_encode($querys), true);
            foreach($querys as $productreview){
                $checked='';
                if($productreview['status']==1){
                    $checked='on';
                }else{
                    $checked='off';
                }
                $actionValues='';
                $num = ++$i;
                $status = '<div  id="'.$productreview['id'].'" rel="product_reviews" class="bootstrap-switch  bootstrap-switch-'.$checked.'  bootstrap-switch-wrapper bootstrap-switch-animate toogle_switch">
                <div class="bootstrap-switch-container" ><span class="bootstrap-switch-handle-on bootstrap-switch-primary">&nbsp;Active&nbsp;&nbsp;</span><label class="bootstrap-switch-label">&nbsp;</label><span class="bootstrap-switch-handle-off bootstrap-switch-default">&nbsp;Inactive&nbsp;</span></div></div>';
                $records["data"][] = array(      
                    $num,
                    '<a target="_blank" href="'.url('/product/'.$productreview['product_id'].'/'.str_slug($productreview['product_name'])).'">'.$productreview['product_name'].'</a>',
                    $productreview['name'],
                    $productreview['star'],
                    $productreview['title'],
                    $productreview['review'],
                    $status,   
                    $actionValues
                );
            }
            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            return response()->json($records);
        }
        $title = "Product Reviews";
        return View::make('admin.products.product-reviews')->with(compact('title'));
    }
}
