<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\StreamedResponse;
use App\Models\Subscriber;
use App\Models\OrderProduct;
use App\Models\Order;
use App\Models\User;
use DB;

class ReportsController extends Controller
{
    //
    public function exportUsers(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $headers = array(
                'Content-Type'        => 'text/csv',
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
                'Content-Disposition' => 'attachment; filename=users.csv',
                'Expires'             => '0',
                'Pragma'              => 'public',
            );
            $response = new StreamedResponse(function() use($data) {
                // Open output stream
                $handle = fopen('php://output', 'w');
                // Add CSV headers
                fputcsv($handle, ["UserId","Name","Email","Mobile","State","City","Postcode","Address","Status","Created At"]);
                $exportUsers  = User::orderby('id','DESC');
                if(!empty($data['from_date'])){
                    $exportUsers = $exportUsers->whereDate('users.created_at','>=',$data['from_date']);
                }
                if(!empty($data['to_date'])){
                    $exportUsers = $exportUsers->whereDate('users.created_at','<=',$data['to_date']);
                }
                $exportUsers = $exportUsers->chunk(500, function($users) use($handle) {
                    foreach ($users as $user){
                        fputcsv($handle, [
                            $user->id,
                            $user->name,
                            $user->email,
                            $user->mobile,
                            $user->state,
                            $user->city,
                            $user->postcode,
                            $user->address_line_1,
                            $user->status,
                            date('d M Y',strtotime($user->created_at))
                        ]);
                    }
                });
                // Close the output stream
                fclose($handle);
            }, 200, $headers);

            return $response->send();
        }
        $title = "Export Users";
        return view('admin.users.export-users')->with(compact('title'));
    }

    public function exportorders(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $headers = array(
                'Content-Type'        => 'text/csv',
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
                'Content-Disposition' => 'attachment; filename=orders.csv',
                'Expires'             => '0',
                'Pragma'              => 'public',
            );
            $response = new StreamedResponse(function() use($data){
                // Open output stream
                $handle = fopen('php://output', 'w');
                // Add CSV headers
                fputcsv($handle, ["OrderId","UserId","Email","Name","Address","City","State","Postcode","Mobile","Shipping Name","Shipping Address","Shipping City","Shipping State","Shipping Postcode","Shipping Mobile",'Coupon','Order Status','Payment Method','Order Date','Product Name','Code','SKU','Product Qty','MRP','Product Discount','Product Price','Product Subtotal','Coupon Discount','Grand Total','Coupon Used','Coupon Discount Rate (%)','Line Discount','Line Shipping Charges','Line Prepaid Discount','Unit Price','Line Amount']);
                    if($data['type'] =="Orders"){
                        $exportOrders  = OrderProduct::join('orders','orders.id','=','order_products.order_id')->join('users','users.id','=','orders.user_id')->join('products','products.id','=','order_products.product_id')->select('order_products.order_id','order_products.product_id','orders.name as shipping_name','orders.address_line_1 as shipping_address','orders.city as shipping_city','orders.state as shipping_state','orders.postcode as shipping_postcode','orders.mobile as shipping_mobile','orders.user_id','orders.comments','orders.shipping_charges','orders.coupon_code','orders.coupon_discount','orders.order_status','orders.payment_method','orders.created_at','orders.subtotal as order_subtotal','orders.grand_total','order_products.product_name','order_products.product_code','order_products.product_sku','order_products.product_qty','order_products.product_price','order_products.discount as prodiscount','order_products.mrp','order_products.discount_type','order_products.subtotal as pro_subtotal','users.email','users.name as user_name','users.mobile as user_mobile','users.city as user_city','users.state as user_state','users.postcode as user_postcode','users.address_line_1 as user_address','order_products.discount_rate','order_products.line_discount','order_products.line_shipping_charges','order_products.line_prepaid_discount','order_products.unit_price','order_products.coupon_used','order_products.line_amount')->orderBy('order_products.id','DESC');
                    if(isset($data['status']) && !empty($data['status'])){
                        $exportOrders = $exportOrders->wherein('orders.order_status',$data['status']);
                    }
                    if(!empty($data['from_date'])){
                        $exportOrders = $exportOrders->whereDate('orders.created_at','>=',$data['from_date']);
                    }
                    if(!empty($data['to_date'])){
                        $exportOrders = $exportOrders->whereDate('orders.created_at','<=',$data['to_date']);
                    }
                    $exportOrders = $exportOrders->chunk(500, function($orderPro) use($handle) {
                        foreach ($orderPro as $order) {
                            $disRate = $order->discount_rate;
                            $lineDis = $order->line_discount;
                            $unitPrice = $order->unit_price;
                            $LineAmount = $order->line_amount;
                            // Add a new row with data
                            fputcsv($handle, [
                                $order->order_id,
                                $order->user_id,
                                $order->email,
                                $order->user_name,
                                substr($order->user_address, 0, 100),
                                $order->user_city,
                                $order->user_state,
                                $order->user_postcode,
                                $order->shipping_mobile,
                                $order->shipping_name,
                                substr($order->shipping_address, 0, 100),
                                $order->shipping_city,
                                $order->shipping_state,
                                $order->shipping_postcode,
                                $order->shipping_mobile,
                                $order->coupon_code,
                                $order->order_status,
                                $order->payment_method,
                                date('d/m/Y h:ia',strtotime($order->created_at)),
                                $order->product_name,
                                $order->product_code,
                                $order->product_sku,
                                $order->product_qty,
                                $order->mrp,
                                $order->prodiscount."%",
                                $order->product_price,
                                $order->pro_subtotal,
                                $order->coupon_discount,
                                $order->grand_total,
                                $order->coupon_used,
                                $disRate,
                                $lineDis,
                                $order->line_shipping_charges,
                                $order->line_prepaid_discount,
                                $unitPrice,
                                $LineAmount
                            ]);
                        }
                    });
                    // Close the output stream
                    fclose($handle);
                }else{}
                
            }, 200, $headers);
            return $response->send();
        }else{
            $orderstatuses =  DB::table('order_statuses')->get();
            $orderstatuses = json_decode(json_encode($orderstatuses),true); 
            $title = "Export Orders";
            return view('admin.orders.export-orders')->with(compact('title','orderstatuses'));
        }
    }

    public function exportSubscribers(Request $request){
        $headers = array(
            'Content-Type'        => 'text/csv',
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-Disposition' => 'attachment; filename=subscribers.csv',
            'Expires'             => '0',
            'Pragma'              => 'public',
        );
        $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');
            // Add CSV headers
            fputcsv($handle, ["Email","Mobile"]);
            $exportSubscribers  = Subscriber::select('email','mobile');
            $exportSubscribers = $exportSubscribers->chunk(500, function($subscribers) use($handle) {
                foreach ($subscribers as $subscriber){
                    fputcsv($handle, [
                        $subscriber->email,
                        $subscriber->mobile
                    ]);
                }
            });
            // Close the output stream
            fclose($handle);
        }, 200, $headers);

        return $response->send();
    }
}
