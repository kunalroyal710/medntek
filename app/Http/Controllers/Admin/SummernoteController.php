<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
class SummernoteController extends Controller
{
    //
    public function upload(Request $request){
        if($request->hasFile('file')) {
            $originName = $request->file('file')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('file')->move(public_path('images/editorMedia'), $fileName);
            $url = asset('images/editorMedia/'.$fileName); 
            echo $url;
        }
    }
}
