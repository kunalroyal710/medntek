<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeDetail;
use App\Models\Product;
use DB;
use Validator;
class ApiController extends Controller
{
    //
    public function __construct(Product $product, ProductAttribute $proattr){
    	$this->product = $product;
    	$this->proattr = $proattr;
    }

    public function checkAuthorizationToken($token){
    	if($token =="Bearer eyJhbGciOiJIUzI1NiJ9.eyJSb2xlIjoiQWRtaW4iLCJJc3N1ZXIiOiJJbWFlYyBNZWRudGVrIiwiVXNlcm5hbWUiOiJJbWFlYyIsImV4cCI6MTYzMTYyMDMxMSwiaWF0IjoxNjMxNjIwMzExfQ.1JOq6QurMTMhviFfOl8lLLUmdezxIL2F_0dMkYU8fo8"){
    		return true;
    	}else{
    		return false;
    	}
    }

    public function syncStock(Request $request){
    	try{
    		if($request->isMethod('post')){
    			$data = $request->all();
    			$headerAuthorization = $request->header('Authorization');
    			$headerTokenResp = $this->checkAuthorizationToken($headerAuthorization);
    			if($headerTokenResp){
    				$rules = [
	    				"items"    => "required|array|min:1|max:100",
	    				"items.*"  => "required|distinct|min:1",
	    				"items.*.stn_inv_no"  => "required",
	    				"items.*.stn_inv_date"  => "required",
	    				"items.*.material_name"  => "required",
	    				"items.*.material_code"  => "required",
	    				"items.*.batch_no"  => "required",
	    				"items.*.qty"  => "required|numeric|gte:0",
	    				"items.*.free_qty"  => "required|numeric|gte:0",
	    				"items.*.rate_per_unit"  => "required|regex:/^\d+(\.\d{1,2})?$/",
	    				"items.*.amt_before_tax"  => "required|regex:/^\d+(\.\d{1,2})?$/",
	    				"items.*.gst"  => "required|regex:/^\d+(\.\d{1,2})?$/",
	    				"items.*.total_inv_amt"  => "required|regex:/^\d+(\.\d{1,2})?$/"
				    ];
				    $validator = Validator::make($request->all(), $rules);
				    if ($validator->fails()){
				     	return response()->json(validationResp($validator),422); 
				    }
				    DB::beginTransaction();
				    $cases = [];
		            $ids = [];
		            $params = [];
		            $successSkus = array();
		            $failedSkus = array();
		            foreach ($data['items'] as $value) {
		            	//Check SKU Exists in database
		            	$checkSku = $this->proattr->where('sku',$value['material_code'])->first();
		            	if($checkSku){
		            		$successSkus[] = $value['material_code'];
		            		$this->proattr->where('sku',$value['material_code'])->update(['stock'=>$value['qty']]);
		            		$value['product_attribute_id'] = $checkSku->id;
		            		ProductAttributeDetail::create($value);
		            	}else{
		            		$failedSkus[] = $value['material_code'];
		            	}
		            	$allSkus[] = $value['material_code'];
		            }
		            $getProids = $this->proattr->wherein('sku',$allSkus)->distinct('product_id')->pluck('product_id')->toArray();
		            if(!empty($getProids)){
		            	DB::table('products')->wherein('id',$getProids)->update([
	                		"stock" => \DB::raw('(select sum(stock) from product_attributes
	                		where product_attributes.product_id = products.id and status=1)')
	           		 	]);
		            }
		            DB::commit();
		            $response = array();
		            if(!empty($successSkus)){
		            	$response['success_skus'] = $successSkus;
		            }
		            if(!empty($failedSkus)){
		            	$response['failed_skus']  = $failedSkus;
		            }
		            $message ="Stock has been synced successfully";
	    			return response()->json(apiSuccessResponse($message,$response),200);
    			}else{
    				$message = "Token Mismatch Exception";
                	return response()->json(apiErrorResponse($message),422);
    			}
    		}
    	}catch(\Exception $e){
	      	return response()->json(exceptionMessage($e),423);
		}
    }
}
