<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    use HasFactory;

    public static function prosizes($productid){
        $sizes = ProductAttribute::select('size')->where('status',1)->where('product_id',$productid)->groupby('size')->pluck('size')->toArray();
        return $sizes;
    }
}
