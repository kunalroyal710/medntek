<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin;
use App\Models\Module;
use Auth;
use DB;
use Illuminate\Support\Arr;
class Module extends Model
{
    use HasFactory;

    protected $fillable = [
        'id','name', 'parent_id', 'view_route','edit_route','delete_route','icon','session_value','status','sortorder','shown_in_roles','table_name','created_at','updated_at'
    ];

    public static function getModules(){
    	if(Auth::guard('admin')->user()->type=="admin"){
    		$allModules = Module::with('undermodules')->where('status',1)->orderby('sortorder','ASC')->where('parent_id','ROOT')->get();
	    	$allModules = json_decode(json_encode($allModules),true);
	    	return $allModules;
    	}else{
    		$getEmpModules = DB::table('admin_roles')->where(['admin_id'=>Auth::guard('admin')->user()->id,'view_access'=>'1'])->select('module_id')->get();
    		$getEmpModules = Arr::flatten(json_decode(json_encode($getEmpModules),true));
    		$allModules = Module::with(['undermodules'=>function($query) use($getEmpModules){
                $query->wherein('id',$getEmpModules);
            }])->where('status',1)->where('parent_id','ROOT')->orderby('sortorder','ASC')->get();
	    	$allModules = json_decode(json_encode($allModules),true);
	    	return $allModules;
    	}
    }

    public function undermodules(){
    	return $this->hasMany('App\Models\Module','parent_id')->orderby('sortorder','asc');
    }
}
