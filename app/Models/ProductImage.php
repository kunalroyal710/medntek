<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    use HasFactory;
    protected $appends = ['medium_image_url','large_image_url'];

    public function getMediumImageUrlAttribute(){
        return url('/images/ProductImages/medium').'/'.$this->image;
    }
    public function getLargeImageUrlAttribute(){
        return url('/images/ProductImages/large').'/'.$this->image;
    }
}
