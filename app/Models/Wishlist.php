<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;
class Wishlist extends Model
{
    use HasFactory;
    public function product(){
    	return $this->belongsTo('App\Models\Product','product_id')->with('product_image');
    }

    public static function wishlists(){
        $wishlists= Wishlist::with('product')->where('user_id',Auth::user()->id)->get()->toArray();
        return $wishlists;
    }

    public static function wishproids($userid){
        $wishproids = Wishlist::where('user_id',$userid)->pluck('product_id')->toArray();
        return $wishproids;
    }

    public static function saveWishlist($productid){
        $wishlist = new Wishlist;
        $wishlist->user_id = Auth::user()->id;
        $wishlist->product_id = $productid;
        $wishlist->save();
    }
}
