<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BannerImage extends Model
{
    use HasFactory;
    public static function sectionBanners($section){
    	$banners = BannerImage::where('type',$section)->where('status',1)->orderby('sort','DESC')->whereDate('start_date','<=',date('Y-m-d'))->whereDate('end_date', '>=',date('Y-m-d'))->get();
    	$banners = json_decode(json_encode($banners),true);
    	return $banners;
    }
}
