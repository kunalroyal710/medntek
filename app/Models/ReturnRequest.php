<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class ReturnRequest extends Model
{
    use HasFactory;

    public static function checkReturn($id){
    	$check = ReturnRequest::where('order_product_id',$id)->count();
    	if($check==0){
    		return 1;
    	}else{
    		return 0;
    	}
    }
}
