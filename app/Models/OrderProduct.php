<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Cart;
use Auth;	
class OrderProduct extends Model
{
    use HasFactory;

    public function productdetail(){
        return $this->belongsTo('App\Models\Product','product_id')->with('product_image');
    }

    public function createOrderProducts($orderitem){
    	$cartitem = $orderitem['item'];
    	$couponitems = $orderitem['couponitems'];
    	$couponItemsSubtotal = array_sum(array_column($couponitems, 'subtotal'));
    	$priceDetails = Cart::calProPricing($cartitem);
    	$couponUsed = "no";$disRate =0;$lineDis =0; 
    	$prepaid_dis_per =$orderitem['summary']['prepaid_dis_per'];
    	$perProShipping =$orderitem['summary']['perProShipping'];
        $unitPrice = $priceDetails['price'];
        $LineAmount = $priceDetails['prosubtotal'];
        if(!empty($couponitems)){
            foreach ($couponitems as $ckey => $coupCartItem) {
                if($coupCartItem['cart_id'] == $cartitem['id']){
                    $couponUsed = "yes";
                    $couponproitem = $coupCartItem;
                    $totalDisRate = (($orderitem['summary']['discount']/$couponItemsSubtotal)*100);
                    $lineDis = round(($couponproitem['subtotal']/100) * $totalDisRate,2);
                    $disRate = round(($lineDis/$couponproitem['subtotal'])*100,2);
                    $LineAmount =  round($couponproitem['subtotal']  - $lineDis,2);
                    $unitPrice = round($LineAmount/$couponproitem['qty'],2);
                    break;
                }
            }
        }
        $lineShippingCharges = $perProShipping * $cartitem['qty'];
        $linePrePaidDis = ($unitPrice * $prepaid_dis_per)/100;
        $unitPrice = $unitPrice - $linePrePaidDis + $perProShipping;
        $LineAmount = $LineAmount - ($linePrePaidDis*$cartitem['qty']) + $lineShippingCharges;
    	$this->user_id           = Auth::user()->id;
		$this->order_id          = $orderitem['order_id'];
		$this->product_id        = $cartitem['product_id'];
		$this->product_code      = $cartitem['product']['product_code'];
		$this->product_sku       = $cartitem['sku'];
		$this->product_name      = $cartitem['product']['product_name'];
		$this->product_size      = $cartitem['size'];
		$this->discount_type     = $cartitem['product']['discount_type'];
		$this->discount          = $priceDetails['discount'];
		$this->mrp               = $cartitem['price'];
        $this->product_gst       = $cartitem['product']['product_gst'];
		$this->product_price     = $priceDetails['price'];
		$this->subtotal          = $priceDetails['prosubtotal'];
		$this->product_qty       = $cartitem['qty'];
		$this->discount_rate     = $disRate;
		$this->line_discount     = $lineDis;
		$this->line_shipping_charges= $lineShippingCharges;
		$this->line_prepaid_discount= $linePrePaidDis*$cartitem['qty'];
		$this->unit_price        = $unitPrice;
		$this->line_amount       = $LineAmount;
		$this->coupon_used       = $couponUsed;
		$this->save();
    }
}
