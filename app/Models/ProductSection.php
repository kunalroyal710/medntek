<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class ProductSection extends Model
{
    use HasFactory;

    public static function prosections($productid,$section){
    	$sections = DB::table('product_sections')->where(['product_id'=>$productid,'type'=>$section])->orderby('sort','DESC')->get();
    	$sections = json_decode(json_encode($sections),true);
    	return $sections;
    }
}
