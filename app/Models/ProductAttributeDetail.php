<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductAttributeDetail extends Model
{
    use HasFactory;
     protected $fillable = [
        'product_attribute_id','stn_inv_no', 'stn_inv_date','material_code','material_name','batch_no','qty','free_qty','rate_per_unit','amt_before_tax','gst','total_inv_amt'
    ];
}
