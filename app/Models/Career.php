<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','email','mobile','position','work_experience','relevant_experience','current_organisation','qualification','cv','cover_letter','message'
    ];
}
