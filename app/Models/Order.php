<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ShippingAddress;
use App\Models\OrderHistory;
use App\Models\OrderProduct;
use App\Models\ProductAttribute;
use App\Models\Product;
use App\Models\Cart;
use Auth;
use Illuminate\Support\Facades\Mail;
use DB;
class Order extends Model
{
    use HasFactory;

    public function getuser(){
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function order_products(){
        return $this->hasMany('App\Models\OrderProduct','order_id')->with('productdetail');
    }

    public function histories(){
        return $this->hasMany('App\Models\OrderHistory')->orderby('id','DESC');
    }

    public static function order_checkpoints($data,$cartinfo){
    	//echo "<pre>"; print_r($cartinfo); die;
    	//Check Number of items in cart
    	$totalItems = count($cartinfo['items']);
    	if($totalItems > 0){
    		//Check Delivery Address
    		$response = Order::delivery_address();
			$status = $response['status'];
			$message = $response['message'];
			return array('status'=>$status,'message'=>$message,'redirect'=>$response['redirect']);
    	}else{
    		return array('status'=>false,'message'=>'Please add items in cart.','redirect'=>'cart');
    	}
    }

    public static function delivery_address(){
    	$shipInfo = ShippingAddress::where('user_id',Auth::user()->id)->where('default_address',1)->first();
    	if(!$shipInfo){
    		return array('status'=>false,'message'=>'Please add delivery address before placing an order','redirect'=>'checkout');
    	}else{
            $checkServiceAble = DB::table('cities')->where('pincode',$shipInfo->postcode)->count();
            if($checkServiceAble >0){
                return array('status'=>true,'message'=>'ok','redirect'=>'thanks');
            }else{
               return array('status'=>false,'message'=>'Delivery pincode is not serviceable.','redirect'=>'checkout'); 
            }

    	}
    }

    public static function checkCouponUsed($couponcode){
        $checkCouponUsed = DB::table('orders')->where('user_id',Auth::user()->id)->where('coupon_code',$couponcode)->wherein('payment_status',['cod','captured'])->count();
        return $checkCouponUsed;
    }

    public function createOrder($cartdata,$requestdata){
        //echo "<pre>"; print_r($cartdata); die;
    	$address = ShippingAddress::where('user_id',Auth::user()->id)->where('default_address',1)->first();
        $prepaid_discount = 0;$prepaid_dis_per=0;
        if($requestdata['paymentMode'] =='ccavenue'){
            $prePaidDisResp = Cart::calculatePrepaidDis($cartdata['summary']);
            $prepaid_discount = $prePaidDisResp['prepaid_discount'];
            $prepaid_dis_per = $prePaidDisResp['prepaid_dis_per'];
            $cartdata['summary']['grandtotal'] = $prePaidDisResp['grand_total'];
        }
    	$this->user_id     		  =   Auth::user()->id;
    	$this->name        		  =   $address->name;
        $this->first_name         =   $address->first_name;
        $this->last_name          =   $address->last_name;
    	$this->mobile             =   $address->mobile;
    	$this->country            =   $address->country;
    	$this->state              =   $address->state;
    	$this->city               =   $address->city;
    	$this->postcode           =   $address->postcode;
    	$this->address_line_1     =   $address->address_line_1;
    	$this->address_line_2     =   $address->address_line_2;
    	$this->payment_method     =   $requestdata['paymentMode'];
        $this->comments           =   $requestdata['comments'];
    	$this->country_code       =   'IN';
        $this->currency           =   'INR';
        $this->currency_symbol    =   'Rs.';
    	$this->currency_rate      =   1;
        $this->prepaid_discount   =   $prepaid_discount;
        $this->prepaid_discount_per = $prepaid_dis_per;
    	$this->subtotal           =   $cartdata['summary']['subtotal'];
    	$this->grand_total        =   $cartdata['summary']['grandtotal'];
    	$this->payment_status     =   ($requestdata['paymentMode']=="cod" ? "cod" : "cancelled");
    	$this->order_status       =   ($requestdata['paymentMode']=="cod" ? "COD Confirmed" : "Cancelled");
        $couponitems = array();
        if(isset($cartdata['summary']['discount'])){
            $this->coupon_discount =   $cartdata['summary']['discount'];
            $this->coupon_code     =   $cartdata['summary']['couponcode'];
            $couponitems           =   $cartdata['summary']['couponCartItems'];
        }
        $this->shipping_charges    = $cartdata['summary']['shipping'];
        $this->save();
        $perProShipping =0;
        if($cartdata['summary']['shipping'] >0){
            $totalCartitems = array_sum(array_column($cartdata['items'],'qty'));
            $perProShipping = $cartdata['summary']['shipping']/$totalCartitems;
        }
        $cartdata['summary']['prepaid_dis_per'] = $prepaid_dis_per;
        $cartdata['summary']['perProShipping'] = $perProShipping;
        //Create Order Products
        foreach($cartdata['items'] as $cartitem){
            $orderitem['item'] = $cartitem;
            $orderitem['order_id'] = $this->id;
            $orderitem['summary'] = $cartdata['summary'];
            $orderitem['couponitems'] = $couponitems;
            $this->orderproduct = new OrderProduct; 
            $this->orderproduct->createOrderProducts($orderitem);
        }
        //Creating Order History
        $this->orderhistory = new OrderHistory; 
        $requestdata['order_status'] = ($requestdata['paymentMode']=="cod" ? "COD Confirmed" : "Cancelled");
        $requestdata['comments'] =  ($requestdata['paymentMode']=="cod" ? "COD order created" : "Payment has been cancelled");
        $requestdata['order_id'] =  $this->id;
        $this->orderhistory->createOrderHistory($requestdata);
        //echo $this->id; die;
    	return $this;
    }

    public function sendOrderEmail($orderid){
        $orderDetails = Order::with(['order_products','getuser'])->where('id',$orderid)->first();
        $orderDetails = json_decode(json_encode($orderDetails),true);
        Order::reduceStock($orderDetails);
        Cart::where('user_id',$orderDetails['user_id'])->delete();
        fireTextSms('order',$orderDetails);
        $email = $orderDetails['getuser']['email'];
        $messageData = [
            'orderDetails' => $orderDetails
        ];
        Mail::send('emails.order', $messageData, function($message) use ($email){
            $message->to($email)->subject('Order Placed with '.config('constants.project_name'))->cc(['orders@imaecmedntek.com']);
        });
    }

    public static function reduceStock($orderDetails){
        $testEmails = array('kunal@rtpltech.com','isha@rtpltech.com','anil909@gmail.com');
        if(!in_array($orderDetails['getuser']['email'], $testEmails)){
            $userEmail = $orderDetails['getuser']['email'];
            if(strpos($userEmail, '@yopmail.com') !== false) {
                //nothing to do here
            }else{
                foreach($orderDetails['order_products'] as $orderpro){
                    ProductAttribute::where('product_id',$orderpro['product_id'])->where('size',$orderpro['product_size'])->where('status',1)->decrement('stock',$orderpro['product_qty']);
                    Product::where('id',$orderpro['product_id'])->decrement('stock',$orderpro['product_qty']);
                }
            }
        }
    }
}
