<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
use App\Models\ProductAttribute;
use Carbon\Carbon;
class Cart extends Model
{
    use HasFactory;

    public function product(){
        return $this->belongsTo('App\Models\Product','product_id')->select('id','category_id','product_name','product_code','discount_type','discount','status','price','final_price','product_gst')->with('product_image')->with('category')->where('status',1);
    }

    public static function items($type,$info){
        $cartitems = Cart::with('product')->join('products','products.id','=','carts.product_id')->join('categories','categories.id','=','products.category_id')->select('carts.*','products.product_name','product_attributes.size','product_attributes.stock','product_attributes.sku','product_attributes.price','product_attributes.moq')->join('product_attributes','product_attributes.product_id','=','products.id')->whereColumn('product_attributes.size','carts.size')->whereColumn('carts.qty','<=','product_attributes.stock')->orderby('carts.id','desc')->where('product_attributes.stock','>',0)->where('categories.status',1)->where('products.status',1)->where('product_attributes.status',1);
        if(isset($info['user_id'])) {
            $cartitems->where('carts.user_id',$info['user_id']);  
        }else{
            $cartitems->where('carts.session_id',$info['session_id']);
        }
        if($type=="listing"){
            $cartitems = $cartitems->get();  
            $cartitems = json_decode(json_encode($cartitems),true);
        }else{
            $cartitems = $cartitems->sum('qty'); 
        }
        return $cartitems;
    }

    public static function totalitems($info){
        $cartcount = Cart::items('count',$info);
        return $cartcount;
    }

    public static function cartitems($info){
        $getcartitems = Cart::items('listing',$info);
        return $getcartitems;
    }

    public static function add_to_cart($data,$info){
        if(isset($data['qty']) && !empty($data['qty'])){
            $reqQty = $data['qty'];
        }else{
            $reqQty = 1;
        }
        $checkPro = Product::where('id',$data['proid'])->where('status',1)->count();
        if($checkPro){
            $checkcart = Cart::where(['product_id'=>$data['proid'],'size'=>$data['size']]);
            if(isset($info['user_id'])){
                $checkcart = $checkcart->where('user_id',$info['user_id']);
            }else{
                $checkcart = $checkcart->where('session_id',$info['session_id']);
            }
            $checkcart = $checkcart->first();
            $checkcart = json_decode(json_encode($checkcart),true);
            //Check Stock in Logic
            $productStockResp = Cart::check_product_stock($data['proid'],$data['size'],$reqQty);
            if(!$productStockResp['status']){
                return $productStockResp;
            }
            $todayDate = date('Y-m-d');
            $expiry_date = date('Y-m-d', strtotime("+7 days", strtotime($todayDate)));
            if(!empty($checkcart)){
                $cart = Cart::find($checkcart['id']);
            }else{
                $cart = new Cart;
            }
            if(isset($info['session_id'])){
                $cart->session_id = $info['session_id'];
            }
            $cart->product_id = $data['proid'];
            $cart->size = $data['size'];
            $cart->qty = $reqQty;
            $cart->expiry_date = $expiry_date;
            if(isset($info['user_id'])){
                $cart->user_id = $info['user_id'];
            }
            $cart->first_reminder_date = Carbon::now()->addDays(3)->format('Y-m-d');
            $cart->second_reminder_date = Carbon::now()->addDays(9)->format('Y-m-d');
            $cart->third_reminder_date = Carbon::now()->addDays(16)->format('Y-m-d');
            $cart->expiry_date =  Carbon::now();
            $cart->save();
            $totalItems = Cart::totalitems($info);
            return array('status'=>true,'message'=>'Product has been added successfully in shopping bag','totalItems'=>$totalItems);
        }else{
            return array('status'=>false,'message'=>'We are sorry this product is not available for buy at the moment');
        }
    }

    public static function check_product_stock($productid,$size,$qty){
        $sku = ProductAttribute::where(['product_id'=>$productid,'size'=>$size])->select('sku','stock')->first();
        if($sku){
            /*Check Stock from database*/
            if($qty<=$sku->stock){
                return array('status'=>true,'message'=>'ok');
            }else{
                return array('status'=>false,'message'=>'We are sorry requested quantity not available at the moment');
            }
        }else{
            return array('status'=>false,'message'=>'Size might not be available or out of stock');
        }
    }

    public static function calProPricing($cartitem){
        //echo "<pre>"; print_r($cartitem); die;
        if(!empty($cartitem['product']['discount'])){
            $price = $cartitem['price'] - ($cartitem['price'] * $cartitem['product']['discount'] /100);
            $strikePrice =  $cartitem['price'];
            $discount = $cartitem['product']['discount'];
            $prosubtotal = $price * $cartitem['qty'];
        }else{
            $price = $cartitem['price'];
            $strikePrice =  0;
            $discount = 0;
            $prosubtotal = $cartitem['price'] * $cartitem['qty'];
        }
        /*$proGst = $cartitem['product']['product_gst']; 
        $gstTax = round($prosubtotal*$proGst/100,2);*/
        //echo $strikePrice; die;
        return array('price'=> $price,'strikeprice'=>$strikePrice,'discount'=>$discount,'prosubtotal'=> $prosubtotal);
    }

    public static function CalculateShipping($subtotal) {
        return 0;
    }

    public static function cartdetails($cartitems){
        $priceArr = array(); $couponPriceArr =array(); $couponCartItems =array();
        //echo "<pre>"; print_r($cartitems); die;
        $countTotalItems = array_sum(array_column($cartitems,'qty'));
        $totalGst = 0;
        foreach($cartitems as $ckey=> $cart){
            $priceDetails = Cart::calProPricing($cart);
            $priceArr[] =  $priceDetails['prosubtotal'];
            /*$totalGst  += $priceDetails['gst_tax'];*/
            if(Session::has('couponinfo')){
                if(Session::get('couponinfo')['coupon_behaviour'] =="all"){
                    if(Session::get('couponinfo')['coupon_applicable_on'] =="all"){
                        $couponCartItems[$ckey]['cart_id']   =  $cart['id'];
                        $couponCartItems[$ckey]['qty']       =  $cart['qty'];
                        $couponCartItems[$ckey]['subtotal'] =  $priceDetails['prosubtotal'];
                        $couponCartItems[$ckey]['unit_price'] =  $priceDetails['price'];
                        $couponPriceArr[] =  $priceDetails['prosubtotal'];
                    }elseif(Session::get('couponinfo')['coupon_applicable_on'] =="discounted" && $priceDetails['discount'] >0){ 
                        $couponCartItems[$ckey]['cart_id']   =  $cart['id'];
                        $couponCartItems[$ckey]['qty']       =  $cart['qty'];
                        $couponCartItems[$ckey]['subtotal'] =  $priceDetails['prosubtotal'];
                        $couponCartItems[$ckey]['unit_price'] =  $priceDetails['price'];
                        $couponPriceArr[] =  $priceDetails['prosubtotal'];
                    }elseif(Session::get('couponinfo')['coupon_applicable_on'] =="non-discounted" && $priceDetails['discount'] == 0){
                        $couponCartItems[$ckey]['cart_id']   =  $cart['id'];
                        $couponCartItems[$ckey]['qty']       =  $cart['qty'];
                        $couponCartItems[$ckey]['subtotal'] =  $priceDetails['prosubtotal'];
                        $couponCartItems[$ckey]['unit_price'] =  $priceDetails['price'];
                        $couponPriceArr[] =  $priceDetails['prosubtotal'];
                    }
                }elseif(Session::get('couponinfo')['coupon_behaviour'] =="product"){
                    $explodeProds = array_filter(explode(',',Session::get('couponinfo')['products']));
                    if(in_array($cart['product_id'], $explodeProds)){
                        if(Session::get('couponinfo')['coupon_applicable_on'] =="all"){
                            $couponCartItems[$ckey]['cart_id']   =  $cart['id'];
                            $couponCartItems[$ckey]['qty']       =  $cart['qty'];
                            $couponCartItems[$ckey]['subtotal'] =  $priceDetails['prosubtotal'];
                            $couponCartItems[$ckey]['unit_price'] =  $priceDetails['price'];
                            $couponPriceArr[] =  $priceDetails['prosubtotal'];
                        }elseif(Session::get('couponinfo')['coupon_applicable_on'] =="discounted" && $priceDetails['discount'] >0){ 
                            $couponCartItems[$ckey]['cart_id']   =  $cart['id'];
                            $couponCartItems[$ckey]['qty']       =  $cart['qty'];
                            $couponCartItems[$ckey]['subtotal'] =  $priceDetails['prosubtotal'];
                            $couponCartItems[$ckey]['unit_price'] =  $priceDetails['price'];
                            $couponPriceArr[] =  $priceDetails['prosubtotal'];
                        }elseif(Session::get('couponinfo')['coupon_applicable_on'] =="non-discounted" && $priceDetails['discount'] == 0){
                            $couponCartItems[$ckey]['cart_id']   =  $cart['id'];
                            $couponCartItems[$ckey]['qty']       =  $cart['qty'];
                            $couponCartItems[$ckey]['subtotal'] =  $priceDetails['prosubtotal'];
                            $couponCartItems[$ckey]['unit_price'] =  $priceDetails['price'];
                            $couponPriceArr[] =  $priceDetails['prosubtotal'];
                        }
                    }
                }elseif(Session::get('couponinfo')['coupon_behaviour'] =="category"){
                    $explodeCatids = array_filter(explode(',',Session::get('couponinfo')['categories']));
                    if(in_array($cart['product']['category_id'], $explodeCatids)){
                        if(Session::get('couponinfo')['coupon_applicable_on'] =="all"){
                            $couponCartItems[$ckey]['cart_id']   =  $cart['id'];
                            $couponCartItems[$ckey]['qty']       =  $cart['qty'];
                            $couponCartItems[$ckey]['subtotal'] =  $priceDetails['prosubtotal'];
                            $couponCartItems[$ckey]['unit_price'] =  $priceDetails['price'];
                            $couponPriceArr[] =  $priceDetails['prosubtotal'];
                        }elseif(Session::get('couponinfo')['coupon_applicable_on'] =="discounted" && $priceDetails['discount'] >0){ 
                            $couponCartItems[$ckey]['cart_id']   =  $cart['id'];
                            $couponCartItems[$ckey]['qty']       =  $cart['qty'];
                            $couponCartItems[$ckey]['subtotal'] =  $priceDetails['prosubtotal'];
                            $couponCartItems[$ckey]['unit_price'] =  $priceDetails['price'];
                            $couponPriceArr[] =  $priceDetails['prosubtotal'];
                        }elseif(Session::get('couponinfo')['coupon_applicable_on'] =="non-discounted" && $priceDetails['discount'] == 0){
                            $couponCartItems[$ckey]['cart_id']   =  $cart['id'];
                            $couponCartItems[$ckey]['qty']       =  $cart['qty'];
                            $couponCartItems[$ckey]['subtotal'] =  $priceDetails['prosubtotal'];
                            $couponCartItems[$ckey]['unit_price'] =  $priceDetails['price'];
                            $couponPriceArr[] =  $priceDetails['prosubtotal'];
                        }
                    }
                }
            }
        }
        //echo $totalGst; die;
        $subtotal = round(array_sum($priceArr));
        $couponPriceSubtotal = round(array_sum($couponPriceArr));
        $discount =0;
        $grandtotal = round($subtotal);
        $couponcode = '';
        if(Session::has('couponinfo')){
            $couponcode = Session::get('couponinfo')->code;
            $discount = CouponCode::getCouponAmount(Session::get('couponinfo'),$couponPriceSubtotal);
        }
        $totalAfterDis = round($subtotal - $discount);
        $shipping = Cart::CalculateShipping($totalAfterDis);
        $grandtotal = round($subtotal - $discount + $shipping);
        return array('subtotal'=>$subtotal,'discount'=>$discount,'total_after_discount'=>$totalAfterDis,'grandtotal'=>$grandtotal,'couponcode'=>$couponcode,'shipping'=>$shipping,'couponCartItems'=>$couponCartItems,'countTotalItems'=>$countTotalItems);
    }

    public static function calculatePrepaidDis($cartDetails){
        $prepaidDisPer = 0;
        $totalafterDis = $cartDetails['total_after_discount'];
        $PrepaidDis = round(($totalafterDis * $prepaidDisPer)/100);
        $total = $totalafterDis - $PrepaidDis;
        $grandTotal = round($total + $cartDetails['shipping']);
        return array('grand_total'=>$grandTotal,'prepaid_discount'=>$PrepaidDis,'prepaid_dis_per'=>$prepaidDisPer);
    }
}
