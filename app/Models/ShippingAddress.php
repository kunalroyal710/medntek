<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;
class ShippingAddress extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id','name','first_name','last_name','mobile','country','state','city','postcode','address_line_1','address_line_2','default_address'
    ];
    
    public static function addresses(){
    	$addresses = ShippingAddress::where('user_id',Auth::user()->id)->get()->toArray();
    	return $addresses;
    }

    public static function addresscount($userid){
    	$count = ShippingAddress::where('user_id',$userid)->count();
    	return $count;
    }
}
