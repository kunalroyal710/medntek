<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    use HasFactory;
    public function user(){
    	return $this->belongsto('App\Models\User','user_id')->select('id','name');
    }

    public static function reviews($productid){
    	$proReviews = ProductReview::with('user')->where('product_id',$productid)->where('status',1)->get()->toArray();
        $averageRatings = 0;
        if(!empty($proReviews)){
            $averageRatings = array_sum(array_column($proReviews,'star'))/count($proReviews);
        }
    	/*echo "<pre>"; print_r($proReviews); die;*/
    	return array('reviews'=>$proReviews,'averageRatings'=>$averageRatings);
    }
}
