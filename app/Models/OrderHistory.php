<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderHistory extends Model
{
    use HasFactory;
    protected $fillable = [
        'id','order_id','order_status','comments','updated_by','created_at','updated_at','awb_number','invoice_no','invoice_date'
    ];

    public function createOrderHistory($requestdata){
    	$this->order_id        = $requestdata['order_id'];
    	$this->order_status    = $requestdata['order_status'];
    	$this->comments        = $requestdata['comments'];
    	$this->awb_number      = (isset($requestdata['awb_number'])?$requestdata['awb_number']:'');
    	$this->invoice_no      = (isset($requestdata['invoice_no'])?$requestdata['invoice_no']:'');
    	$this->invoice_date      = (isset($requestdata['invoice_date'])?$requestdata['invoice_date']:'');
    	$this->shipped_by      = (isset($requestdata['shipped_by'])?$requestdata['shipped_by']:'');
    	$this->updated_by      = (isset($requestdata['updated_by'])?$requestdata['updated_by']:'');
    	$this->save();
    }
}
