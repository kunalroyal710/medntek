<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function product_image(){
        return $this->hasOne('App\Models\ProductImage','product_id')->orderby('sort','ASC');
    }

    public function product_categories(){
        return $this->belongsToMany('App\Models\ProductCategory','product_categories','product_id','category_id');
    }

    public function category(){
    	return $this->belongsTo('App\Models\Category','category_id')->select('id','parent_id','name','status','seo_unique','category_discount');
    }

    public function attributes(){
        return $this->hasMany('App\Models\ProductAttribute','product_id');
    }

    public function pro_attrs(){
        return $this->hasMany('App\Models\ProductAttribute','product_id')->where('status',1);
    }

    public function relatedproducts(){
        return $this->hasMany('App\Models\Product','category_id','category_id')->select('id','category_id','status','product_name','discount','discount_type','final_price','price')->where('status',1)->with(['productimages','category','product_image']);
    }

    public function productimages(){
        return $this->hasMany('App\Models\ProductImage','product_id')->orderby('sort','ASC');
    }

    public function product_reviews(){
        return $this->hasMany('App\Models\ProductReview','product_id')->where('status',1)->orderby('id','ASC')->with('user');
    }

}
