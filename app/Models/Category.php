<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class Category extends Model
{
    use HasFactory;
    protected $appends = ['category_image_url','category_normal_image_url','category_hover_image_url'];

    public function getCategoryImageUrlAttribute(){
        return url('/images/CategoryImages').'/'.$this->image;
    }

    public function getCategoryNormalImageUrlAttribute(){
        return url('/images/CategoryImages').'/'.$this->normal_image;
    }

    public function getCategoryHoverImageUrlAttribute(){
        return url('/images/CategoryImages').'/'.$this->hover_image;
    }
    
    public function subcategories(){
    	return $this->hasMany('App\Models\Category','parent_id')->select('id','name','parent_id','seo_unique')->orderby('id','ASC')->where('status',1);
    }

    public function products(){
        return $this->hasMany('App\Models\Product','category_id')->where('status',1);
    }
    
    public static function catproducts(){
        $categories = Category::with('products')->where('status',1)->get();
        $categories = json_decode(json_encode($categories),true);
        return $categories;
    }

     public function subcat(){
        return $this->belongsTo('App\Models\Category','parent_id');
    }
}
