<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class AppSetting extends Model
{
    use HasFactory;
    public static function shippinginfo(){
    	$getShippingDetails = DB::table('app_settings')->where('type','shipping')->first();
        if($getShippingDetails){
            $shipNotApply = $getShippingDetails->shipping_not_apply_above;
        }else{
            $shipNotApply = 0;
        }
        return $shipNotApply;
    }

    public static function shippinginstructions($cartpricing){
        //echo "<pre>"; print_r($cartpricing); die;
    	$shipNotApply = AppSetting::shippinginfo();
    	$free_ship_instruction ="";
    	$add_product_instruction = "";
    	if($cartpricing['total_after_discount'] < $shipNotApply){
    		$add_more_pro_val =  $shipNotApply - $cartpricing['total_after_discount'];
    		$free_ship_instruction = "(Free Shipping on orders above Rs. $shipNotApply)";
    		$add_product_instruction = "(Shop for Rs. ".$add_more_pro_val." more and  get free shipping)";
    	}elseif($cartpricing['total_after_discount']>=0){
            $free_ship_instruction = "Free Shipping";
        }

    	return array('free_ship_instruction'=>$free_ship_instruction,'add_product_instruction'=>$add_product_instruction);
    }
}
