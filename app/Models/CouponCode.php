<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Cart;
use App\Models\Product;
use App\Models\Category;
use Session;
use DB;
use Auth;
class CouponCode extends Model
{
    use HasFactory;
    public static function applycouponcode($code,$cartitems){
        $response = array('status'=> false,'message'=>'Invalid coupon code. Please try with some vaild coupon code.'); 
        $checkcoupon =CouponCode::where(['code'=>$code,'status'=>1])
                    ->where('start_date','<=',date('Y-m-d'))
                    ->where('expiry_date','>=',date('Y-m-d'))
                    ->where(function ($q) {
                        $q->whereRaw('FIND_IN_SET("'.Auth::user()->email.'",user_emails)')->orwhere('user_emails','=','');
                    })
                    /*->where('max_qty','>=',$totalitems)
                    ->where('max_amount','>=',$producttotal)*/
                    ->first();
        //echo "<pre>"; print_r($cartitems); die;
        if($checkcoupon){
            $priceArr = array();$totalitems =0;$catids =array();
            if($checkcoupon->coupon_behaviour =="all"){
                foreach($cartitems as $ckey=> $cart){
                    $catids[] = $cart['product']['category_id'];
                    $totalitems += $cart['qty'];
                    $priceDetails = Cart::calProPricing($cart);
                    if($checkcoupon->coupon_applicable_on =="all"){
                        $priceArr[] =  $priceDetails['prosubtotal'];
                    }elseif($checkcoupon->coupon_applicable_on =="discounted" && $priceDetails['discount'] >0){ 
                        $priceArr[] =  $priceDetails['prosubtotal'];
                    }elseif($checkcoupon->coupon_applicable_on =="non-discounted" && $priceDetails['discount'] == 0){
                        $priceArr[] =  $priceDetails['prosubtotal'];
                    }
                }
            }else if($checkcoupon->coupon_behaviour =="product"){
                $explodeProds = array_filter(explode(',',$checkcoupon->products));
                foreach($cartitems as $ckey=> $cart){
                    if(in_array($cart['product_id'], $explodeProds)){
                        $prosFound[] = true;
                        $catids[] = $cart['product']['category_id'];
                        $totalitems += $cart['qty'];
                        $priceDetails = Cart::calProPricing($cart);
                        if($checkcoupon->coupon_applicable_on =="all"){
                            $priceArr[] =  $priceDetails['prosubtotal'];
                        }elseif($checkcoupon->coupon_applicable_on =="discounted" && $priceDetails['discount'] >0){ 
                            $priceArr[] =  $priceDetails['prosubtotal'];
                        }elseif($checkcoupon->coupon_applicable_on =="non-discounted" && $priceDetails['discount'] == 0){ 
                            $priceArr[] =  $priceDetails['prosubtotal'];
                        }
                    }else{
                        $prosFound[] = false;
                    }
                }
                if(!in_array(true,$prosFound)){
                    Session::forget('couponinfo');
                    $response = array('status'=> false,'message'=>'This coupon is not valid for added products in cart');
                    return $response;
                }
            }else if($checkcoupon->coupon_behaviour =="category"){
                $explodeCats = array_filter(explode(',',$checkcoupon->categories));
                //echo "<pre>"; print_r($explodeCats); die;
                foreach($cartitems as $ckey=> $cart){
                    if(in_array($cart['product']['category_id'], $explodeCats)){
                        $catsFound[] = true;
                        $catids[] = $cart['product']['category_id'];
                        $totalitems += $cart['qty'];
                        $priceDetails = Cart::calProPricing($cart);
                        if($checkcoupon->coupon_applicable_on =="all"){
                            $priceArr[] =  $priceDetails['prosubtotal'];
                        }elseif($checkcoupon->coupon_applicable_on =="discounted" && $priceDetails['discount'] >0){ 
                            $priceArr[] =  $priceDetails['prosubtotal'];
                        }elseif($checkcoupon->coupon_applicable_on =="non-discounted" && $priceDetails['discount'] == 0){ 
                            $priceArr[] =  $priceDetails['prosubtotal'];
                        }
                    }else{
                        $catsFound[] = false;
                    }
                }
                if(!in_array(true,$catsFound)){
                    Session::forget('couponinfo');
                    $response = array('status'=> false,'message'=>'This coupon is not valid for added products in cart');
                    return $response;
                }
            }
            $producttotal = array_sum($priceArr);
            if($checkcoupon->min_qty <= $totalitems){
                if($checkcoupon->min_amount <= $producttotal){
                    $minusCouponAmount = CouponCode::getCouponAmount($checkcoupon,$producttotal);
                    if($producttotal > $minusCouponAmount ){
                        if($checkcoupon->coupon_type =="Single Time"){
                            $checkCouponUsed=0;
                            //Later will used for orders
                            $checkCouponUsed = DB::table('orders')->where('user_id',Auth::user()->id)->where('coupon_code',$checkcoupon->code)->wherein('payment_status',['cod','captured'])->count();
                            if($checkCouponUsed==0){
                                Session::put('couponinfo',$checkcoupon);
                                $response = array('status'=> true,'message' =>'Coupon Applied successfully!');
                            }else{
                                Session::forget('couponinfo');
                            }
                        }else if($checkcoupon->coupon_type =="Limited Times"){
                            //Later will used for orders
                            $checkCouponUsed = DB::table('orders')->where('user_id',Auth::user()->id)->where('coupon_code',$checkcoupon->code)->wherein('payment_status',['cod','captured'])->count();
                            if($checkcoupon->limited_time > $checkCouponUsed){
                                Session::put('couponinfo',$checkcoupon);
                                $response = array('status'=> true,'message' =>'Coupon Applied successfully!');
                            }else{
                                Session::forget('couponinfo');
                            }
                        }else{
                            Session::put('couponinfo',$checkcoupon);
                            $response = array('status'=> true,'message'=>' Coupon Applied successfully!');
                        }
                    }else{
                        Session::forget('couponinfo');
                        $response = array('status'=> false,'message'=>' Please add more products in cart to avail this coupon');
                    }
                }else{
                    Session::forget('couponinfo');
                    if($checkcoupon->coupon_applicable_on =="all"){
                        $response = array('status'=> false,'message'=>' Shop for Rs. '.$checkcoupon->min_amount.' or above to avail this coupon.');
                    }elseif($checkcoupon->coupon_applicable_on =="non-discounted"){
                        $response = array('status'=> false,'message'=>'Avail this coupon for Non discounted products of Rs '.$checkcoupon->min_amount ." or above");   
                    }else{
                        $response = array('status'=> false,'message'=>'Avail this coupon for discounted products of Rs '.$checkcoupon->min_amount ." or above");   
                    }
                }
            }else{
                Session::forget('couponinfo');
                if($checkcoupon->coupon_applicable_on =="all"){
                    $response = array('status'=> false,'message'=>'Shop for '.$checkcoupon->min_qty ." or more products to avail this coupon");
                }elseif($checkcoupon->coupon_applicable_on =="non-discounted"){
                    $response = array('status'=> false,'message'=>'Shop  Non discounted roducts for '.$checkcoupon->min_qty ." or more products to avail this coupon");  
                }else{
                    $response = array('status'=> false,'message'=>'Shop for '.$checkcoupon->min_qty ." or more products to avail this coupon");
                }
            }
        }else{
            Session::forget('couponinfo');
        }
        return $response;
    }

    public static function getCouponAmount($checkcoupon,$producttotal){
        if($checkcoupon->amount_type == "Rupees") {
            $minusCouponAmount = $checkcoupon->amount;
        }else{
            $minusCouponAmount = ($producttotal * $checkcoupon->amount)/100;
        }
        return round($minusCouponAmount);
    }

    public static function checkCouponStatus(){
        if(Session::has('couponinfo')){
            $checkstatus = CouponCode::where('code',Session::get('couponinfo')['code'])->first();
            if(!empty($checkstatus) &&  $checkstatus->status == 0){
                Session::forget('couponinfo');
            }
            if(!empty($checkstatus) &&  $checkstatus->expiry_date < date('Y-m-d')){
                Session::forget('couponinfo');
            }
        }
        return true;
    }

    public static function availableCoupons($cartitems){
        $priceArr = array();$totalitems =0;$catids =array();
        foreach($cartitems as $ckey=> $cart){
            $catids[] = $cart['product']['category_id'];
            $totalitems += $cart['qty'];
            $priceDetails = Cart::calProPricing($cart);
            $priceArr[] =  $priceDetails['prosubtotal'];
        }
        $catids = array_unique($catids);
        $producttotal = array_sum($priceArr);
        $coupons = CouponCode::where('expiry_date','>=',date('Y-m-d'))
                            ->where('start_date','<=',date('Y-m-d'))
                            ->where('min_qty','<=',$totalitems)
                            ->where('max_qty','>=',$totalitems)
                            ->where('min_amount','<=',$producttotal)
                            ->where('max_amount','>=',$producttotal)
                            ->where('status',1)
                            ->where('visible',1)
                            ->where(function ($q) {
                                $q->whereRaw('FIND_IN_SET("'.Auth::user()->email.'",user_emails)')->orwhere('user_emails','=','');
                            })
                            ->get()
                            ->toArray();
        $availableCoupons = array();
        //this below code is not as much good we can do it in more better way later
        foreach($coupons as $ckey=> $coupon){
            //Check coupon used in orders
            if($coupon['coupon_type'] =='Single Time'){
                $checkCouponUsed = Order::checkCouponUsed($coupon['code']);
                if($checkCouponUsed == 0){
                    $availableCoupons[$ckey] = $coupon;
                }
            }else if($coupon['coupon_type'] =='Limited Times'){
                $checkCouponUsed = DB::table('orders')->where('user_id',Auth::user()->id)->where('coupon_code',$coupon['code'])->wherein('payment_status',['cod','captured'])->count();
                if($coupon['limited_time'] > $checkCouponUsed){
                    $availableCoupons[$ckey] = $coupon;
                }
            }else{
                $availableCoupons[$ckey] = $coupon;
            }
        }
        //echo "<pre>"; print_r($availableCoupons); die;
        return $availableCoupons;
    }
}
