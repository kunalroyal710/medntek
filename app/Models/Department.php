<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    public function openings(){
    	return $this->hasMany('App\Models\CurrentOpening')->where('status',1);
    }

    public static function depts(){
    	$depts = Department::with('openings')->where('status',1)->get();
    	$depts = json_decode(json_encode($depts),true);
    	return $depts;
    }
}
