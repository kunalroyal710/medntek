<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
class AdminRole extends Model
{
    use HasFactory;

    public static function checkupdateAccess($route){
    	if(Auth::guard('admin')->user()->type=="subadmin"){
    		$module = DB::table('modules')->where('edit_route',$route)->select('id')->first();
    		if($module){
    			$checkAccess = DB::table('admin_roles')->where('admin_id',Auth::guard('admin')->user()->id)->where('module_id',$module->id)->select('edit_access')->first();
    			if($checkAccess && $checkAccess->edit_access ==1){
    				$access =true;
    			}else{
    				$access = false;
    			}
    		}else{
    			$access = false;
    		}
    	}else{
    		$access = true;
    	}
    	return $access;
    }
}
