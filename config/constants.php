<?php

return [
	'project_name' => 'Imaec Medntek',
	'contact_email' => 'info@imaecmedntek.com',
	'email_footer' => '',
	'email_mobile' => '',
	'project_email' => 'info@imaecmedntek.com',
	'project_mobile' => '+91 8956234040',
	'gst' => 'GST_HERE',
	'pan' => 'PAN_NO_HERE',
	'cin' => 'CIN_NO_HERE',
	'pincode'=> '411045',
	'return_address' => 'Ware hose Address - Sr. No. 157/2A, Godown Ground floor, Jivan Nagar, Near Sai Petrol pump, Mumbai Pune Highway, Tathawade, PCMC, pune 411033.',
];
?>