<html>
    <head>
        <style type='text/css'>
            .style2 {
            font-size: 11px;
            font-weight: bold;
            text-decoration: none;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            color:#666666;
            }
            .style3 {
            text-decoration: none;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            color:#666666;
            }
        </style>
    </head>
    <body>
        <table width='700' border='0' cellpadding='0' cellspacing='0'  style='border:#EFEFEF 5px solid; padding:5px;'>
            <tr>
                <td colspan='3'></td>
            </tr>
            <tr>
                <td  align='left' valign='middle'><img border='0' src="{{ asset('images/medntek.png') }}" alt='logo' /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td height='70' align='right' valign='top'>
                    <table width='93%' border='0' align='right' cellpadding='3' cellspacing='0' >
                        <tr>
                            <td align='left' valign='top' class='style3'><span class='style2'>Date :</span> <?php echo date('Y-m-d'); ?></td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>Dear {{ $orderDetails['getuser']['name'] }},</td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>Thank you for shopping at {{config('constants.project_name')}}</td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>Your order {{ $orderDetails['id'] }} has been confirmed and will be delivered within 3-5 working days.</td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>Payment Method: {{$orderDetails['payment_method']}}</td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>Order Status: {{$orderDetails['order_status']}}</td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>Your order details are: </td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>
                                <table width='95%' border='0' align='left' cellpadding='3' cellspacing='1' bgcolor='ACA899'>
                                    <tr>
                                        <td width='20%' align='center' valign='top' class='style2' bgcolor='#cccccc'>Product Name</td>
                                        <td width='15%' align='center' valign='top' class='style2' bgcolor='#cccccc'>Product Code</td>
                                        <td width='15%' align='center' valign='top' class='style2' bgcolor='#cccccc'>Quantity</td>
                                        <td width='20%' align='right' valign='top' class='style2' bgcolor='#cccccc'>Price</td>
                                        <td width='20%' align='right' valign='top' class='style2' bgcolor='#cccccc'>Subtotal</td>
                                    </tr>
                                    @foreach($orderDetails['order_products'] as $pro)
                                    <tr>
                                        <td align='center' valign='top' class='style3' bgcolor='#F7F7F7'>{{ $pro['product_name'] }}</td>
                                        <td align='center' valign='top' class='style3' bgcolor='#cccccc'>{{ $pro['product_sku'] }}</td>
                                        <td align='center' valign='top' class='style3' bgcolor='#F7F7F7'>{{ $pro['product_qty'] }}</td>
                                        <td align='center' valign='top' class='style3' bgcolor='#F7F7F7'>{{ $orderDetails['currency_symbol'] }} {{ $pro['product_price'] }}</td>
                                        <td align='center' valign='top' class='style3' bgcolor='#F7F7F7'>{{ $orderDetails['currency_symbol'] }} {{ $pro['subtotal'] }}</td>
                                    </tr>
                                    @endforeach
                                    @if($orderDetails['coupon_discount']>0)
                                         <tr>
                                            <td colspan='4' align='right' valign='top' class='style3' bgcolor='#F7F7F7'>Coupon Code</td>
                                            <td align='center' valign='top' class='style3' bgcolor='#F7F7F7'> <p style="color: red;">{{ $orderDetails['coupon_code'] }} </p></td>
                                        </tr>
                                    @endif
                                    @if($orderDetails['coupon_discount']>0)
                                    <tr>
                                        <td colspan='4' align='right' valign='top' class='style3' bgcolor='#F7F7F7'>Coupon Discount</td>
                                        <td align='center' valign='top' class='style3' bgcolor='#F7F7F7'>{{ $orderDetails['currency_symbol'] }} {{ round($orderDetails['coupon_discount']) }}</td>
                                    </tr>
                                    @endif
                                    @if($orderDetails['payment_method'] == "COD")
                                        <tr>
                                            <td colspan='4' align='right' valign='top' class='style3' bgcolor='#F7F7F7'>COD Charges (If Any)</td>
                                            <td align='center' valign='top' class='style3' bgcolor='#F7F7F7'>{{ $orderDetails['currency_symbol'] }} {{ round($orderDetails['cod_charges']) }}</td>
                                        </tr>
                                    @endif
                                    @if($orderDetails['shipping_charges']>0)
                                        <tr>
                                            <td colspan='4' align='right' valign='top' class='style3' bgcolor='#F7F7F7'>Shipping Charges</td>
                                            <td align='center' valign='top' class='style3' bgcolor='#F7F7F7'>{{ $orderDetails['currency_symbol'] }} {{ round($orderDetails['shipping_charges']) }}</td>
                                        </tr>
                                    @endif
                                    @if($orderDetails['prepaid_discount'] >0)
                                        <tr>
                                            <td colspan='4' align='right' valign='top' class='style3' bgcolor='#F7F7F7'>Prepaid Discount</td>
                                            <td align='center' valign='top' class='style3' bgcolor='#F7F7F7'>{{ $orderDetails['currency_symbol'] }} {{ $orderDetails['prepaid_discount'] }}</td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <td colspan='4' align='right' valign='top' class='style3' bgcolor='#F7F7F7'>Grand Total</td>
                                        <td align='center' valign='top' class='style3' bgcolor='#F7F7F7'><strong>{{ $orderDetails['currency_symbol'] }} {{ $orderDetails['grand_total'] }} </strong></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width='100%'>
                                    <tr>
                                        <td width='50%'>
                                            <table width='100%' border='0' align='left' cellpadding='3' cellspacing='0'>
                                                <tr class='shop'>
                                                    <td colspan='2' align='left' valign='middle' class='style3' ><span class='top_text1'><strong>Bill To: -</strong></span></td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2' align='left' valign='middle' class='style3'>{{ $orderDetails['getuser']['name'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2' align='left' valign='middle' class='style3'>{{ $orderDetails['getuser']['address_line_1'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2' align='left' valign='middle' class='style3'>{{ $orderDetails['getuser']['city'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2' align='left' valign='middle' class='style3'>{{ $orderDetails['getuser']['state'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2' align='left' valign='middle' class='style3'>{{ $orderDetails['getuser']['postcode'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td align='left' valign='middle' class='style3'>{{ $orderDetails['getuser']['country'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td align='left' valign='middle' class='style3'>{{ $orderDetails['getuser']['mobile'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td align='left' valign='middle' class='top_text1'></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width='39%'>
                                            <table width='100%' border='0' align='left' cellpadding='3' cellspacing='0'>
                                                <tr class='shop'>
                                                    <td colspan='2' align='left' valign='middle' class='style3' ><span class='style2'><strong>Ship To: -</strong></span></td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2' align='left' valign='middle' class='style3'>{{ $orderDetails['name'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2' align='left' valign='middle' class='style3'>{{ $orderDetails['address_line_1'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2' align='left' valign='middle' class='style3'>{{ $orderDetails['city'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2' align='left' valign='middle' class='style3'>{{ $orderDetails['state'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2' align='left' valign='middle' class='style3'>{{ $orderDetails['postcode'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td align='left' valign='middle' class='style3'>{{ $orderDetails['country'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td align='left' valign='middle' class='style3'>{{ $orderDetails['mobile'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td height='5' align='left' valign='middle' class='top_text1'></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>
                                <table width='90%' border='0' align='left' cellpadding='3' cellspacing='0'>
                                    <tr>
                                    </tr>
                                    <tr>
                                        <td colspan='3' class='style3'>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan='3' class='style3'>
                                            For any other queries, mail us at <a href="mailto:{{config('constants.contact_email')}}"> {{config('constants.contact_email')}}</a> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height='16' colspan='3' class='style3'>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height='16' colspan='3' class='style3'>
                                            <div align='left'>   
                                                Team {{config('constants.project_name')}}!!
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>