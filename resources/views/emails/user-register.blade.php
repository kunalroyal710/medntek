<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <head>
        <style type='text/css'>
            <!--
                .style2 {
                    font-size: 11px;
                    font-weight: bold;
                    text-decoration: none;
                    font-family: Verdana, Arial, Helvetica, sans-serif;
                    color:#666666;
                }
                .style3 {
                    text-decoration: none;
                    font-family: Verdana, Arial, Helvetica, sans-serif;
                    font-size: 11px;
                    color:#666666;
                }
                -->

                @import url('https://fonts.googleapis.com/css2?family=Covered+By+Your+Grace&display=swap');

                .main-table {
                    width: 700px;
                }
                .inner-table tbody .email-bg {
                    background-color: rgba(255,255,255,0.7);
                }
                .purple-bg {
                    background-color: #f7f7f7;
                    /*color: #fff;*/
                }

                .purple-bg .btm-table h3 {
                    font-family: 'Covered By Your Grace', cursive;
                    margin-bottom: 0px;
                    font-size: 11px;
                }

                .purple-bg .btm-table td {
                   /* color: #fff;*/
                    width: 16.6%;
                }
                .purple-bg .btm-table tr.experts img {
                    width: 70%;
                }

                .purple-bg .btm-table tr.experts p {
                    margin-top: 0px;
                    margin-bottom: 10px;
                    font-size: 10px;
                }
               /* .footer-btm {
                    background-color: #43435d;
                    width: 100%;
                    padding: 10px;
                }*/

                /*.footer-btm .footer-btn a {
                    background-color: #ffb8af;
                    color: #43435d;
                    padding: 5px;
                    text-decoration: none;
                }*/
                @media only screen and (max-width: 767px) {
                    .main-table {
                        width: 100%;
                    }
                }
        </style>
    </head>
    <body>
        <table class="main-table" border='0' cellpadding='0' cellspacing='0'  style='border:#ddd 3px dashed; padding:5px;'>
            <tr>
                <td colspan='3'></td>
            </tr>
            <tr>
                <td  align='center' valign='middle'><img border='0' width="100px" src="{{ asset('images/medntek.png') }}"  /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align='center' valign='top'>
                    <table class="inner-table" width='100%'>
                        <tr>
                            <td align='left' valign='top' class='style3'>&nbsp;</td>
                        </tr>
                        <tr class="email-bg" style="width: 70%; padding-top: 30px;">
                            <td align='center' valign='top' class='style3'>
                                <br />
                            Dear {{ $data['name'] }},</td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>&nbsp;</td>
                        </tr>
                        
                        <tr class="email-bg" style="width: 70%; padding-top: 30px;">
                            <td align='center' valign='top' class='style3'><h3 style="color: #9797C0; margin-bottom: 0px;">A warm welcome to {{config('constants.project_name')}} Family!</h3></td>
                        </tr>
                        <tr class="email-bg" style="text-align: center; margin: 0 auto; width: 70%; display: block; padding-bottom: 30px;">
                            <td align='center' valign='top' class='style3'> <br />
                                Now that you’ve registered with us, the first one to know all about Fresh Arrivals, Exclusive Offers and Discounts, will be YOU!<br /><br />
                            </td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td height='16' colspan='3' class='style3'>
                                <div align='left'>   
                                    Regards<br />
                                    Team {{config('constants.project_name')}}
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
