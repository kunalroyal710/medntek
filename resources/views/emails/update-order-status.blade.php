<html>
    <head>
        <style type='text/css'>
            <!--
                .style2 {
                font-size: 11px;
                font-weight: bold;
                text-decoration: none;
                font-family: Verdana, Arial, Helvetica, sans-serif;
                color:#666666;
                
                }
                .style3 {
                text-decoration: none;
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 11px;
                color:#666666;
                }
                -->
        </style>
    </head>
    <body>
        <table width='700' border='0' cellpadding='0' cellspacing='0'  style='border:#EFEFEF 5px solid; padding:5px;'>
            <tr>
                <td colspan='3'></td>
            </tr>
            <tr>
                <td  align='left' valign='middle'><img border='0' width="75px" src="{{asset('images/medntek.png')}}" alt='logo' /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td height='70' align='right' valign='top'>
                    <table width='93%' border='0' align='right' cellpadding='3' cellspacing='0' >
                        <tr>
                            <td align='left' valign='top' class='style3'><span class='style2'>Date :</span> <?php echo date('d F Y',strtotime(date('Y-m-d'))); ?></td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>Hey {{ $orderDetails['getuser']['name'] }}, Your Order No. {{ $orderDetails['id'] }} has been successfully {{$message_info['status']}}.  </td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'><span class='style2'>Remarks</span> : {{ $message_info['comments'] }} </td>
                        </tr>
                        <tr>
                            <td align='left' valign='top' class='style3'>
                                <table width='90%' border='0' align='left' cellpadding='3' cellspacing='0'>
                                    <tr>
                                        <td colspan='3' class='style3'>
                                            <p>Contact Us-</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height='16' colspan='3' class='style3'>
                                            <p><a href='mailto: {{config('constants.project_email')}} '> {{config('constants.project_name')}} </a></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height='16' colspan='3' class='style3'>
                                            <p>+91-70877-99000</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height='16' colspan='3' class='style3'>
                                            <div align='left'>   
                                                Regards<br />
                                                {{config('constants.project_name')}} 
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>