@extends('layouts.frontLayout.front-layout')
@section('content')
<section class="breacrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-text product-more">
                    <a href="{{url('/')}}">Home</a>
                    <a href="{{url('/contact')}}">Customer Care</a>
                    <span>Track Order</span>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="quality-policy right-blue-corner-shape bottom-purpl-corner-shape">
    <div class="container">
        <div class="row">
            @if(!empty($trackLink))
                <iframe width="100%" height="350px" allowfullscreen="true"  src="{{$trackLink}}" title="W3Schools Free Online Web Tutorials">
                </iframe>
            @endif
            <div class="col-lg-12 col-md-12 col-sm-12 sd_txt t_order">
                @if(Session::has('flash_message_error'))
                    <div role="alert" class="alert alert-danger alert-dismissible"> <button aria-label="Close" data-dismiss="alert" style="text-indent: 0;" class="close" type="button"><span aria-hidden="true">x</span></button> <strong>Error!</strong> {!! session('flash_message_error') !!} </div>
                @endif
                <div class="track_inner">
                    <h2 class="hd-38">Track Order</h2>
                    <form method="get" action="{{url('/track-order')}}">
                        <div class="order_container">
                            <input type="text" id="order_id" class="inputwithicon" name="order_id" placeholder="Order Id">
                        </div>
                        <button class="button_1 track_btn" type="submit"><span>Track</span></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@stop