@if(isset($_GET['order_id']) && !empty($_GET['order_id']))
    <div class="col-12">
        @if(Session::has('flash_message_error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Error! </strong>  {!! session('flash_message_error') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if(Session::has('flash_message_success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Success! </strong>  {!! session('flash_message_success') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="container mt-4 cart checkout">
            <div class="heading-order-details"><h4 class="hd-24">Order Details #{{$_GET['order_id']}}</h4></div>
           <div class="row">
                <div class="col-lg-7 col-12">
                    @foreach($orderDetails['order_products'] as $orderProduct)
                       
                        <div class="row cart-details checkout-cart-details checkout-cart-details-clone">
                            <div class="col-lg-3 col-md-3 col-12 col-sm-12 cart-pic text-center">
                                @if(isset($orderProduct['productdetail']['product_image']['medium_image_url']))
                                    <img src="{{$orderProduct['productdetail']['product_image']['medium_image_url']}}" class="img-fluid" alt="{{$orderProduct['product_name']}}">
                                @endif
                            </div>
                            <div class="col-lg-9 col-md-9 col-12 col-sm-12 checkout-cart-details-clone-inner">
                                <div class="row">
                                    <div class="col-md-5 col-12 product-inner-cart-details">
                                        <h5 class="cart-prod-name">{{$orderProduct['product_name']}}</h5>
                                        <div class="product-inner-cart-details-content">
                                            <p>SKU. : {{$orderProduct['product_sku']}}</p>
                                            <p>Size : {{$orderProduct['product_size']}}</p>
                                            <p>Sub Total : <span class="orange">₹ {{$orderProduct['subtotal']}}</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-6 cart-qty-style">
                                        <div class="quantity-main">
                                            <p>{{$orderProduct['product_qty']}}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-6 cart-prod-price">
                                        <p>
                                        <span class="orange hd-24">₹ {{$orderProduct['product_price']}}
                                        </span>
                                        </p>
                                        <?php $isreturn = \App\Models\ReturnRequest::checkReturn($orderProduct['id']);

                                            $date1 = $orderDetails['updated_at'];
                                            $date2 = date('Y-m-d');
                                            $diff = strtotime($date2) - strtotime($date1); 
                                            $diff =  abs(round($diff / 86400));
                                        ?>
                                        @if($isreturn)
                                            @if($orderDetails['order_status'] =="Payment Captured" || $orderDetails['order_status'] =="Successful"|| $orderDetails['order_status'] =="COD Confirmed")
                                                <p class="account-return"><a class="returnItem text-right" href="javascript:;" data-type="cancel" data-orderproid="{{$orderProduct['id']}}">Cancel Item</a></p>
                                            @elseif($orderDetails['order_status']=="Delivered" && $diff<=7)
                                                <p class="account-return"><a class="returnItem text-right" href="javascript:;" data-type="return-exachange" data-orderproid="{{$orderProduct['id']}}">Return/Exchange</a></p>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                    @endforeach
              </div>
              <div class="offset-lg-1 col-lg-4 col-12">
                 <div class="ro">
                    <div class="col-sm-12 col-12 cart-total cart-total-chekout">
                       <h5 class="orange">Order Details</h5>
                       <br>
                       <p>Order Id <span>{{$orderDetails['id']}}</span></p>
                       <p>Order Status <span>{{$orderDetails['order_status']}}</span></p>
                       <p>Items in Order<span>{{$orderDetails['total_items']}} Items</span></p>
                       <p>Order Date<span>{{date('d M Y h:iA',strtotime($orderDetails['created_at']))}}</span></p>
                       <p>Amount Paid<span>Rs. {{$orderDetails['grand_total']}}</span></p>
                       <div id="PrepaidDis"></div>
                       <hr>
                    </div>
                    <div class="col-sm-12 col-12 cart-total cart-total-chekout">
                       <h5 class="orange">Shipping Address</h5>
                       <br>
                       <p>Name: <span>{{$orderDetails['name']}}</span></p>
                       <p>Postcode:<span>{{$orderDetails['postcode']}}</span></p>
                       <p>Phone<span>{{$orderDetails['mobile']}}</span></p>
                       <p>City:<span>{{$orderDetails['city']}}</span></p>
                       <p>State:<span>{{$orderDetails['state']}}</span></p>
                    </div>
                    <div class="col-sm-12 col-12 cart-total cart-total-chekout">
                       <h5 class="orange">Order Summary</h5>
                       <br>
                       <p>Subtotal<span>Rs. {{$orderDetails['subtotal']}}</span></p>
                       <p>Coupon Discount (-)<span>Rs. {{$orderDetails['coupon_discount']}}</span></p>
                       <p>Shipping Charges (+)<span>Rs. {{$orderDetails['shipping_charges']}}</span></p>
                       <p>Grand Total<span>Rs. {{$orderDetails['grand_total']}}</span></p>
                    </div>
                 </div>
                
                
              </div>
           </div>
        </div>
    </div>
@else
    <div class="col-12">
        <div class="row accTabsInfo">
            <div class="col-12 profile-head">
                <h4 class="hd-24">My Orders</h4>
            </div>
            <table class="table table-hover tbl_res">
                <thead>
                    <tr class="">
                        <td>Order Id.</td>
                        <td>Payment Method</td>
                        <td>Order Date</td>
                        <td>Amount</td>
                        <td>Status</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $orderinfo)
                        <tr>
                            <td>{{$orderinfo['id']}}</td>
                            <td>
                                @if($orderinfo['payment_method'] == "ccavenue")
                                    UPI/Net Banking
                                @else
                                    {{ucwords($orderinfo['payment_method'])}}
                                @endif
                                </td>
                            <td>{{date('d M Y h:iA',strtotime($orderinfo['created_at']))}}</td>
                            <td>Rs. {{$orderinfo['grand_total']}}</td>
                            <td>{{$orderinfo['order_status']}}</td>
                            <td><a href="{{url('/account/my-orders?order_id='.$orderinfo['id'])}}">View Detail</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endif
<div class="modal cencel-item-popup" id="returnItem">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="modalPopupTitle"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form method="post" action="{{url('/return-order-item')}}" enctype="multipart/form-data">@csrf
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label for="type" class="col-form-label">Type:</label>
                        <select name="type" class="form-control" required>
                            <!-- <option value="">Please Select</option>
                            <option value="return">I want to Return</option>
                            <option value="exchange">I want to Exchange</option> -->
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="reason" class="col-form-label">Reason:</label>
                        <select name="reason"  class="form-control" required>
                            <option value="">Please Select</option>
                            <option value="Incorrect product received">Incorrect product received</option>
                            <option value="Defective product received">Defective product received</option>
                            <option value="Quality issues">Quality issues</option>
                            <option value="Not required anymore">Not required anymore</option>
                            <option value="Others">Others</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Comments:</label>
                        <input type="hidden" name="order_product_id">
                        <textarea name="comments" placeholder="Comments..." class="form-control" id="message-text" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Choose file (if any):</label>
                        <input type="file" name="image" accept="image/gif, image/jpeg, image/png" class="form-control">
                        <p style="color: red;" class="text-center"> Note:- Max file size is 2MB</p>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@section('javascript')
@parent
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click','.returnItem',function(){
            var orderproid = $(this).data('orderproid');
            var type = $(this).data('type');
            if(type=="cancel"){
                $('[name=type]').html("<option value='cancel'>I want to Cancel this item</option>"); 
                $('#modalPopupTitle').text('Cancel Item');
            }else{
                $('[name=type]').html("<option value=>Please Select</option><option value=return>I want to Return</option><option value=exchange>I want to Exchange</option>"); 
                $('#modalPopupTitle').text('Return/Exchange Item');
            }
            $('[name=order_product_id]').val(orderproid);
            $('#returnItem').modal('show');
        })
    })
</script>
@stop