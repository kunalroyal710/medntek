<div class="col-12">
    <div class="row accTabsInfo">
        @if(isset($_GET['r']) && $_GET['r'] =="success")
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> Profile has been updated successfully! 
            </div>
        @endif
        <div class="col-12 profile-head">
            <h4 class="hd-24">My Profile</h4>
        </div>
        <div class="col-sm-8 col-12 billing">
            <form id="MyAccountform" autocomplete="off" action="javascript:;" method="post">@csrf
                <div class="row profile-form">
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group form_name">
                            <input type="text" name="name" class="input-style form-control" placeholder="Enter First Name" value="{{Auth::user()->name}}"/>
                            <p class="err text-center" id="MyAccount-name" style="display: none;"></p>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group form_email form-country">
                            <select id="country" name="country">
                                <option value="India" selected>India</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group form_email">
                            <input type="text" name="postcode" class="input-style form-control" id="ship_pincode" placeholder="Postcode" value="{{Auth::user()->postcode}}"/>
                            <p class="err text-center" id="MyAccount-postcode" style="display: none;"></p>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group form_email form-city">
                            <input type="text" name="state" class="input-style form-control" placeholder="State" id="ship_state" value="{{Auth::user()->state}}"/>
                            <p class="err text-center" id="MyAccount-state" style="display: none;"></p>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group form_email form-city">
                            <input type="text" name="city" class="input-style form-control" placeholder="City" id="ship_city" value="{{Auth::user()->city}}"/>
                            <p class="err text-center" id="MyAccount-city" style="display: none;"></p>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group form_email form-city">
                            <textarea id="subject" name="address_line_1" placeholder="Address *" style="height:40px">{{Auth::user()->address_line_1}}</textarea>
                            <p class="err text-center" id="MyAccount-address_line_1" style="display: none;"></p>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group form_email form-gst">
                            <input type="text" name="gst" class="input-style form-control" placeholder="GST" value="{{Auth::user()->gst}}" />
                            <p class="err text-center" id="MyAccount-gst" style="display: none;"></p>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group form_email form-gst">
                            <input type="text" name="dci" class="input-style form-control" placeholder="DCI" value="{{Auth::user()->dci}}"/>
                            <p class="err text-center" id="MyAccount-dci" style="display: none;"></p>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group form_email register-icon">
                            <input type="text" name="register_name" class="input-style form-control" placeholder="Register Name" value="{{Auth::user()->register_name}}" />
                            <p class="err text-center" id="MyAccount-register_name" style="display: none;"></p>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group form_email">
                            <input type="text"  class="input-style form-control" placeholder="Email" value="{{Auth::user()->email}}" readonly /> 
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group mobile-form">
                            <input type="number" name="mobile" class="input-style form-control" placeholder="Enter Mobile" value="{{Auth::user()->mobile}}" />
                            <p class="err text-center" id="MyAccount-mobile" style="display: none;"></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-12 text-right">
                        <div class="text-right" style="display:none">
                        </div>
                        <button type="submit" class="button_1 disinfectants_btn"><span>SAVE</span></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@section('javascript')
@parent
<script>
    var accounturl = '/account/my-profile';
    window.history.pushState({path:accounturl},'',accounturl);
    //Register 
    $("#MyAccountform").submit(function(e){
        e.preventDefault();
        $('.PleaseWaitDiv').show();
        var formdata = $("#MyAccountform").serialize();
        $.ajax({
            url: '/submit-account-details',
            type:'POST',
            data: formdata,
            success: function(data) {
                $('.PleaseWaitDiv').hide();
                if(!data.status){
                    $.each(data.errors, function (i, error) {
                        $('#MyAccount-'+i).attr('style', '');
                        $('#MyAccount-'+i).html(error);
                        setTimeout(function () {
                            $('#MyAccount-'+i).css({
                                'display': 'none'
                            });
                        }, 3000);
                    });
                }else{
                    window.location.href = '/account/my-profile?r=success';
                }
            }
        });
    });
</script>
@stop