@extends('layouts.frontLayout.front-layout')
@section('content')
<div class="main-section-container">
    <section class="login-register-wrap right-blue-corner-shape bottom-purpl-corner-shape">
        <div class="container">
            <div class="lr-form-wrap">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 login-form">
                        <div class="login-form-inner">
                            <h2 class="hd-38">Sign In </h2>
                            <p>Enter your Email and Password to Sign In</p>
                            <form id="SignInForm" autocomplete="off" method="post">@csrf
                                <span class="forminput-icons">
                                    <input class="form-control" name="email" type="email" placeholder="Email address" required>
                                    <div class="alert alert-danger" id="Login-email" style="display: none;"></div>
                                </span>

                                <span class="forminput-icons pwicon lowpadd">

                                    <input id="pass_log_id" name="password" class="password-style form-control" type="password" placeholder="Password" required>
                                    <span toggle="#password-field" class="fa fa-fw field_icon toggle-password icon-eye fa-eye"></span>
                                    <div class="alert alert-danger" id="Login-password" style="display: none;"></div>

                                </span>
                                <p>
                                    <a href="{{url('/forgot-password')}}" id="forgotArcus" class="forgotPW">Forgot Password ?</a>
                                </p>
                                <button type="submit" class="button_1">
                                    <span>Submit</span>
                                </button>
                                <!-- <span class="login-withgoogle">Or Sign in With <a href="#"> <img src="{{asset('assets/images/google-logo.png')}}"></a>
                                </span> -->
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 colsm-12 register-form">
                        <div class="register-form-inner">
                            <h2 class="hd-38">Create Account</h2>
                            <p>Get Started with us</p>
                            <form id="RegisterForm" action="javascript:;" autocomplete="off" method="post">@csrf
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <span class="forminput-icons unicon"><input type="text" id="fname" class="form-control" name="name" placeholder="Name" required></span>
                                        <div class="alert alert-danger" id="Register-name" style="display: none;"></div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <span class="forminput-icons"><input type="email"  class="form-control" name="email" placeholder="Email" required></span>
                                        <div class="alert alert-danger" id="Register-email" style="display: none;"></div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 mobile-code-area">
                                        <span class="forminput-icons phicon"><input type="number"  class="form-control custom-number-code " name="mobile" placeholder="Mobile" required></span>
                                        <span class="mobile-code">+91</span>
                                        <div class="alert alert-danger" id="Register-mobile" style="display: none;"></div>
                                    </div>
                                     <div class="col-lg-6 col-md-6 col-sm-12">
                                        <span class="forminput-icons contyicon">
                                            <input type="text"  class="form-control" name="postcode" placeholder="India" readonly>
                                        </span>
                                        <div class="alert alert-danger" id="Register-postcode" style="display: none;"></div>
                                    </div>
                                    
                                    <!-- <div class="col-lg-6 col-md-6 col-sm-12">
                                        <span class="forminput-icons ">
                                            <select name="country" id="country" class="form-control" name="country" required>
                                                <option value="India"></option> 
                                                <option><span class="contyicon">India</span></option>
                                               <option value="USA">USA</option> 
                                            </select>
                                        </span>
                                        <div class="alert alert-danger" id="Register-country" style="display: none;"></div>
                                    </div> -->
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <span class="forminput-icons contyicon">
                                            <input type="text" id="ship_pincode"  class="form-control" name="postcode" placeholder="Postcode" required>
                                        </span>
                                        <div class="alert alert-danger" id="Register-postcode" style="display: none;"></div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <span class="forminput-icons contyicon"><input type="text"  class="form-control" name="state" id="ship_state" placeholder="State" required></span>
                                        <div class="alert alert-danger" id="Register-state" style="display: none;"></div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <span class="forminput-icons contyicon"><input type="text" class="form-control" name="city" id="ship_city" placeholder="City" required></span>
                                        <div class="alert alert-danger" id="Register-city" style="display: none;"></div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 password-account">
                                        <span class="forminput-icons pwicon"><input type="password"  class="form-control" id="account-password" name="password" placeholder="Password" required></span>
                                         <span toggle="#password-field" class="fa fa-fw field_icon toggle-password1 icon-eye fa-eye"></span>
                                        <div class="alert alert-danger" id="Register-password" style="display: none;"></div>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                        <div class="form-group">
                                            <div id="html_element" class="g-recaptcha"></div>
                                        </label>
                                        <div class="alert alert-danger" id="Register-g-recaptcha-response" style="display: none;"></div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <button type="submit" class="button_1"><span>Submit</span></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop
@section('javascript')
@parent
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
<script>
var onloadCallback = function() {
    grecaptcha.render('html_element', {
    'sitekey' : '6Ldw1PccAAAAALx24CqoVPa5cpyBNvt4s0leDx-b',
    'callback' : correctCaptcha
  });
};
var correctCaptcha = function(response) {
};
</script>
@stop