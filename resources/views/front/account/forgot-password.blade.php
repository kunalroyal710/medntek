@extends('layouts.frontLayout.front-layout')
@section('content')
<!-- Main Container Section -->
<section class="breacrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-text product-more">
                    <a href="{{url('/')}}">Home</a>
                    <span>Forgot Password </span>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="quality-policy right-blue-corner-shape bottom-purpl-corner-shape">
    <div class="container">
        <form id="ForgotPwdForm" autocomplete="off" method="post" action="javascript:;">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 sd_txt t_order">
                    <div class="track_inner">
                        <h2 class="hd-38">Forgot Password </h2>
                        <div class="order_container">
                            <input type="text" id="forgot_password_id" class="inputwithicon" name="email" placeholder="Your email address">
                        </div>
                        <p class="err text-center" id="forgot-email" style="display: none;"></p>
                        <div class="alert alert-success print-success-msg" style="display:none">
                            <ul></ul>
                        </div>
                        <button class="button_1 track_btn">
                            <span>Submit</span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- Main Container Section End-->
@stop