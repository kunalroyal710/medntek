@extends('layouts.frontLayout.front-layout')
@section('content')
<div class="main-section-container">
    <div class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text product-more">
                        <a href="{{url('/')}}">Home</a>
                        @if($slug=="my-profile")
                            <span>My Profile</span>
                        @elseif($slug=="settings")
                            <span>Settings</span>
                        @elseif($slug=="my-orders")
                            <span>My Orders</span>
                        @elseif($slug=="wishlists")
                            <span>Wishlists</span>
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="profile-wrapper  right-blue-corner-shape bottom-purpl-corner-shape">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 tab-listing">
                    <div class="accTabs">
                        <ul>
                            <?php
                                $dashboradArr = array('account/my-profile'=>'My Profile','account/my-orders'=>'My Orders','account/settings'=>'Settings','account/wishlists'=>'Wishlists');
                                $dashslug = "account/".$slug;
                            ?>
                            @foreach($dashboradArr as $dkey=> $dashboardInfo)
                                <li class="nav-item @if($dashslug==$dkey) active-profile @endif">
                                    <a href="{{url($dkey)}}">{{$dashboardInfo}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @if($slug=="my-profile")
                    @include('front.account.my-profile')
                @elseif($slug=="settings")
                    @include('front.account.change-password')
                @elseif($slug=="address")
                    @include('front.address.my-address')
                @elseif($slug=="my-orders")
                    @include('front.account.orders')
                @elseif($slug=="wishlists")
                    @include('front.account.wishlists')
                @endif
            </div>
        </div>
    </div>
</div>
<style type="text/css">
        .accTabs{ width: 100%; display: inline-block;}
        .accTabs ul{ margin: 0;padding: 0;list-style: none;display:flex;}
        .accTabs ul li{display: inline-block;border-right: 1px solid #fff;padding: 7px 40px;margin: 15px 0;}
        .accTabs ul li a{ color: #747678; display: block;font-size: 15px; line-height: 40px; text-decoration: none;}
        .accTabs ul li:last-child {border-right: none;}
        .accTabs ul li a.active{ background: #979AC6; color: #fff; }
        .accTabs ul li a.active:hover{ color: #fff; }
        .accTabs ul li a:hover{ color: #333; }
        .accTabsInfo{ background:#F6F6F6; padding: 50px 44px 92px 54px; border-radius: 0.25rem;}
        /*.accTabsInfo form{ margin-top: 2em; }*/
        .accTabsInfo p{ margin-bottom: 0;}
        .accTabsInfo p.wishlist-btn { float: right; }
        .accTabsInfo a{ color: #666; text-decoration: underline; }
        /*.accTabsInfo h5{ margin-top: 20px;}*/
        .account-logout {
        color: #979ac6;
        }
        .account-logout:hover {
        color: #979ac6;
        }
        /* Billing Shipping Style Starts Here */
        .billing .form-group {
        /*width: 50%;*/
        float: left;
        /*padding-right: 15px;*/
        /*padding-left: 15px;*/
        }
        .billing .form-group.address-form textarea {
        height: 38px;
        max-height: 70px;
        }
        .billing .form-group {
        width: 100%;
        }
        /* Billing Shipping Style Ends Here */
        .home-popup .form-group {
        margin-bottom: 0px;
        }
        .form-group:not(:last-of-type){margin-bottom: 1rem;}
        .billing .form-group.address-form textarea {
        height: 38px;
        max-height: 70px;
        }
        .home-popup .form-group {
        margin-left: 10px;
        }
        .form-group:not(:last-of-type) {
        margin-bottom: 0px;
        }
        .detail-right .input-style {
        width: 60%;
        height: 40px;
        font-size: 12px;
        color: #666;
        padding: 0px 15px;
        display: inline-block;
        border: 1px solid rgba(0,0,0,0.1);
        }
        .form-control,.form-control:focus{box-shadow: 0 0 0 rgba(0,0,0,0); -moz-box-shadow: 0 0 0 rgba(0,0,0,0);-webkit-box-shadow: 0 0 0 rgba(0,0,0,0);}
        .form-control{border: 1px solid #ced4da; /*border-radius: 0;*/}
        .form-control:focus{border: 1px solid #ced4da;}
        .popup-options .input-group > .custom-select:not(:last-child), .popup-options .input-group > .form-control:not(:last-child) {
        border-top-right-radius: 0.25rem;
        border-bottom-right-radius: 0.25rem;
        }
        .form-control:focus {
        box-shadow:none !important;
        border-color:#ccc;
        }
        .password-style.form-control:focus {
        background-color: transparent !important;
        }
        p.err {
        width: 100%;
        font-size: 90%;
        color: #d4340b;
        margin-bottom: 0px;
        }
        .popup-options .input-group p.err {
        width: 100%;
        font-size: 90%;
        color: #d4340b;
        margin-bottom: 0px;
        }
        .detail-right .input-style {
        width: 60%;
        height: 40px;
        font-size: 12px;
        color: #666;
        padding: 0px 15px;
        display: inline-block;
        border: 1px solid rgba(0,0,0,0.1);
        }
        .profile-wrapper {
        padding: 50px 0;
        }
        .form-control {
        padding: 13px 45px !important;
        }
</style>
@stop