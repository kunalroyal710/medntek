<div class="col-12">
    <div class="row accTabsInfo">
        <div class="col-12 profile-head">
            <h4 class="hd-24">Change Password</h4>
        </div>
        <div class="col-sm-8 col-12 billing">
            <form id="MySettingsForm" autocomplete="off" action="javascript:;" method="post">@csrf
                <div class="row profile-form">
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group form_old_password passwordicon">
                            <input id="pass-3" type="text" name="current_password" class="input-style form-control" placeholder="Current Password" />
                            <span toggle="#password-field" class="fa fa-fw field_icon toggle-password2 icon-eye fa-eye"></span>
                            <p class="err text-center" id="ChangePwd-current_password" style="display: none;"></p>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group form_new_password passwordicon">
                            <input id="pass-4" type="text" name="password" class="input-style form-control" placeholder="New Password" />
                            <span toggle="#password-field" class="fa fa-fw field_icon toggle-password3 icon-eye fa-eye"></span>
                            <p class="err text-center" id="ChangePwd-password" style="display: none;"></p>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group form_confirm_password passwordicon">
                            <input id="pass-5" type="text" name="password_confirmation" class="input-style form-control" placeholder="Confirm Password" />
                            <span toggle="#password-field" class="fa fa-fw field_icon toggle-password4 icon-eye fa-eye"></span>
                            <p class="err text-center" id="ChangePwd-password_confirmation" style="display: none;"></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="alert alert-success print-success-msg" style="display:none">
                        <ul></ul>
                    </div>
                    <div class="col-12 text-right">
                        <div class="text-right" style="display:none">
                        </div>
                        <button type="submit" class="button_1 disinfectants_btn"><span>SAVE</span></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@section('javascript')
@parent
<script>
   //Register 
       $("#MySettingsForm").submit(function(e){
        e.preventDefault();
        $('.PleaseWaitDiv').show();
        var formdata = $("#MySettingsForm").serialize();
        $.ajax({
            url: '/change-password',
            type:'POST',
            data: formdata,
            success: function(data) {
                $('.PleaseWaitDiv').hide();
                if(!data.status){
               $.each(data.errors, function (i, error) {
                  $('#ChangePwd-'+i).attr('style', '');
                  $('#ChangePwd-'+i).html(error);
                  setTimeout(function () {
                     $('#ChangePwd-'+i).css({
                        'display': 'none'
                     });
                  }, 3000);
                  });
               }else{
                  printSuccessMsg(data.message);
                  $('.print-success-msg').delay(2000).fadeOut('slow');
                  $("#MySettingsForm").trigger("reset");
                  $('#MySettingsForm').find('.processing').removeClass('processing');
                  $('#my_settings').find('.updating').removeClass('updating');
               }
            }
        });
    });
</script>
@stop