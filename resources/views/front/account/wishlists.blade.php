<div class="col-12">
    <div class="row accTabsInfo">
        @if(Session::has('flash_message_success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> {!! session('flash_message_success') !!}
            </div>
        @endif
        <div class="col-12 profile-head">
            <h4 class="hd-24">Wishlists</h4>
        </div>
        <div class="col-sm-12 col-12 billing">
            <div class="wishlist-table">
                <table>
                    <tbody>
                        @foreach($wishlists as $wishlist)
                            <tr class="shopping-cart-item-row">
                                <td class="cart-pic wish-cart-list">
                                    @if(isset($wishlist['product']['product_image']['large_image_url']))
                                        <img src="{{$wishlist['product']['product_image']['medium_image_url']}}">
                                    @endif
                                </td>
                                <td class="cart-title">
                                    <div class="row cart-title1">
                                        <div class="shopping-cart-item-cols col-lg-4 col-md-6 col-sm-12">
                                            <h4 class="hd-20"><a href="{{url('/product/'.$wishlist['product_id'].'/'.str_slug($wishlist['product']['product_name']))}}">{{$wishlist['product']['product_name']}}</a></h4>
                                        </div>
                                        <div class="shopping-cart-item-cols col-lg-4 col-md-6 col-sm-12">
                                            <?php $productInfo = $wishlist['product'];  ?>
                                            @if(!empty($productInfo['discount_type']))
                                                <div class="shopping-cart-item-subhead price-cart">
                                                    <h5 class="hd-24">₹ {{formatAmt($productInfo['final_price'])}}</h5>
                                                    <h5 class="hd-24"><del>₹ {{formatAmt($productInfo['price'])}}</del></h5>
                                                </div>
                                                <div class="shopping-cart-item-subhead quantity-cart">
                                                    <h5 class="hd-13">You save
                                                        ₹ {{formatAmt($productInfo['price'] - $productInfo['final_price'])}} <span class="discount-percentage">({{$productInfo['discount']}}% OFF)</span>
                                                    </h5>
                                                </div>
                                            @else
                                                <div class="shopping-cart-item-subhead price-cart">
                                                    <h5 class="hd-24">₹ {{formatAmt($productInfo['final_price'])}}</h5>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="cartbtn col-lg-4 col-md-12 col-sm-12"><a href="{{url('/product/'.$wishlist['product_id'].'/'.str_slug($wishlist['product']['product_name']))}}" class="button_1"><span>Add to Cart</span></a></div>
                                    </div>
                                    <div class="row cart-title2">
                                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 shopping-cart-item-cols2">
                                            <ul class="shopping-quantity-list hd-13">
                                                <li class="shopping-quantity-list-inner"><a href="{{url('/remove-wishlist/'.$wishlist['id'])}}"  onclick="if (confirm('Are you sure you want to delete?')){return true;}else{event.stopPropagation(); event.preventDefault();};">REMOVE</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>