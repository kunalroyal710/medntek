@extends('layouts.frontLayout.front-layout')
@section('content') 
<div class="main-section-container">
    <section class="home-slider hero-section">
        <div class="hero-items owl-carousel home-banner-div">
            @foreach(banners('home1') as $bannerInfo)
                <div class="single-hero-items set-bg">
                    <img class="hero-items_img" src="{{asset('images/BannerImages/'.$bannerInfo['image'])}}" />
                    <div class="container">
                        <div class="row_wrap">
                            <div class="row">
                                <div class="col-lg-7">
                                    <h3 class="banner-title-0 hd-24">{{$bannerInfo['title']}}</h3>
                                    <h2 class="banner-title"><?php echo nl2br($bannerInfo['description']); ?></h2>
                                    @if(!empty($bannerInfo['title_two']))
                                        <h3 class="banner-title-0 hd-24">{{$bannerInfo['title_two']}}</h3>
                                    @endif
                                    @if(!empty($bannerInfo['link']))
                                        <a href="{{$bannerInfo['link']}}" class="button_1 banner_button"><span>Explore</span></a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <section class="disinfectants">
        <div class="disinfectants_div">
            <div class="dis1_inner disinfectants_carousel">
                @foreach(categories() as $catinfo)
                    <div class="dis1_inner_ctn row">
                        <div class="dis1_inner_ctn_div col-lg-8 col-md-8">
                            <h2 class="dis1_inner_head hd-38">{{$catinfo['name']}}</h2>
                            <p class="disinfectants_para"><?php echo nl2br($catinfo['description']); ?></p>
                            <a href="{{url($catinfo['seo_unique'])}}" class="button_1 disinfectants_btn"><span>Shop Now</span></a>
                        </div>
                        <div class="disinfectants_images_inner disinfectants_images_inner1 col-lg-4 col-md-4">
                            <img src="{{$catinfo['category_image_url']}}" alt="disinfectants_img1">
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="disinfectants_list">
                <div class="disinfectants_list_inner">
                    <div class="dis_list disinfectants_navigation" id="slick-1">
                        @foreach(categories() as $catinfo)
                            <div class="dis_list_inner">
                                <div class="dis_list_inner_img dis_list_inner_img1">
                                    <img class="normal_img" src="{{$catinfo['category_normal_image_url']}}" />
                                    <img class="hover_img" src="{{$catinfo['category_hover_image_url']}}" /></div>
                                <div class="dis_list_inner_txt">
                                    {{$catinfo['name']}}
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="progressbarline">
                        <div class="slider-progress">
                            <div class="progress" id="progresslinedis">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container-fluid">
        <div class="products">
            <h2 class="hd-38">Products</h2>
            <ul class="product-list">
                @foreach($catProducts as $catkey=> $categoryInfo)
                    <li class="links-product @if($catkey ==0) active-category @endif" data-title="product-tab-{{$catkey}}"><a>{{$categoryInfo['name']}}</a></li>
                @endforeach
            </ul>
            @foreach($catProducts as $catkey=> $categoryInfo)
                <div class="links-product product_tablink @if($catkey ==0) active-category @endif"  data-title="product-tab-{{$catkey}}"><a>{{$categoryInfo['name']}}</a></div>
                <div class="product-items product-tab-{{$catkey}} @if($catkey ==0) active-product-item @endif">
                    @if(empty($categoryInfo['products']))
                        <h2>Coming Soon</h2>
                    @else
                        @php $products = $categoryInfo['products'] @endphp
                        @include('front.products.product-listing')
                    @endif
                </div>
            @endforeach
            <!-- <a href="category.php" class="button_1 disinfectants_btn"><span>View All</span></a> -->
        </div>
    </section>
    <section class="offer_container">
        <div class="offer_main_container">
            <div class="container">
                <div class="offer_rw">
                    <div class="row">
                        @foreach(banners('home2') as $bannerInfo)
                            <div class="col-lg-3 col-md-3 offer_box_content">
                                <div class="offer_context">
                                    <div class="offer_img_div">
                                        <img src="{{asset('images/BannerImages/'.$bannerInfo['image'])}}" alt="offer_imgs">
                                    </div>
                                    <div class="offer_context_div">
                                        <h3 class="hd-24">{{$bannerInfo['title']}}</h3>
                                        <a @if(!empty($bannerInfo['link']))  href="{{$bannerInfo['link']}}" @else href="javascript:;" @endif class="hd-14">VIEW ALL</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- <section class="consignee_container">
        <div class="consignee_main_container">
            <div class="container">
                <div class="consignee_header">
                    <h2 class="hd-38">Consignee</h2>
                </div>
                <div class="consignee_context">
                    <div class="consignee_tab_2">
                        <select name="state" id="state">
                            <option value="state">Select State</option>
                            <option value="value1">Value1</option>
                            <option value="value2">Value2</option>
                        </select>
                    </div>
                    <div class="consignee_tab_2">
                        <select name="city" id="city">
                            <option value="city">Select City</option>
                            <option value="value1">Value1</option>
                            <option value="value2">Value2</option>
                        </select>
                    </div>
                    <div class="consignee_tab_3">
                        <input type="button" class="white-btn consignee_btn" value="Submit" />
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <section class="about_tech_container">
        <div class="about_main_container">
            <div class="container">
                <div class="about_context_1">
                    <div class="about_all">
                        <div class="about_content">
                            <h2 class="hd-38">About IMAEC MEDNTEK</h2>
                            <p class="abt_content_p">Mahavir Coal Washeries, our parent company was established with the sole purpose of washing high ash indigenous coal. Since its inception, Mahavir Group has reached insurmountable heights in the Coal Washeries Industry...</p>
                            <a href="{{url('/about-us')}}" class="abt_link">READ MORE</a>
                        </div>
                        <div class="about_image">
                            <img src="{{asset('assets/images/about_industry_img.jpg')}}" alt="about_home_image">
                        </div>
                    </div>
                    <div class="abt_tech_1">
                        <div class="about_technology">
                            <h4 class="hd-24">Roadmap/Way Ahead</h4>
                            <div class="inv-pad">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <ul class="return_list">
                                            <li>Launch products in the Indian market in the field of Dialysis and Infection Control for the first time on 1st August 2021.</li>
                                            <li>Expecting to enter Southern, Western and Central India in the near future.</li>
                                            <li>Foray into the Northern and Eastern parts of India by 2023.</li>
                                            <li>Initiate project Greenfield at Chakan</li>
                                          <!--   <li>Bilaspur Project Completion by 2022.</li> -->
                                        </ul>
                                    </div>
                                    <div class="col-lg-6">
                                        <ul class="return_list">
                                            <li>Start commercial production by 1st April 2023.</li>
                                            <li>Obtain essential regulatory approvals and permissions by 2023 including ISO, CE, 510 k etc.</li>
                                            <li>Start exporting to the semi-regulatory market by 2023.</li>
                                            <li>Participate in the Government Tender business by 2023.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="purple_container"></div>
        </div>
    </section>
    <section class="infrastructure-wrapper">
        <div class="container">
            <div class="infrastructure">
                <img class="infrastructure_img" src="{{asset('assets/images/infrastructure.jpg')}}" />
                <h2 class="hd-38">Infrastructure</h2>
                <p>With top of the line infrastructure, we at IMAEC MEDNTEK aspire to be market leaders in our industry. With top of the line infrastructure, we at IMAEC MEDNTEK...</p>
            </div>
            <div class="row infra-points">
                
                <div class="col-lg-4 col-md-4 col-sm-12 infra-inner">
                    <div class="infra-icon">
                        <img src="{{asset('assets/images/infra-icon-2.png')}}" alt="Infrastructure-Icon-1">
                    </div>
                    <h4>Operational Excellence</h4>
                    <p>IMAEC MEDNTEK Ltd has a state of the art manufacturing facility in 20,000 m²...</p>
                    <a href="{{url('/quality#opexcellence')}}">READ MORE</a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 infra-inner">
                    <div class="infra-icon">
                        <img src="{{asset('assets/images/infra-icon-4.png')}}" alt="Infrastructure-Icon-1">
                    </div>
                    <h4>Manufacturing Capabilities</h4>
                    <p>IMAEC MEDNTEK manufactures its wide range of products with maximum process automation...</p>
                    <a href="{{url('/quality#mfcapabilities')}}">READ MORE</a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 infra-inner">
                    <div class="infra-icon">
                        <img src="{{asset('assets/images/infra-icon-3.png')}}" alt="Infrastructure-Icon-1">
                    </div>
                    <h4>Quality</h4>
                    <p>At IMAEC MEDNTEK, quality is an integral part of our day-to-day operations, business...</p>
                    <a href="{{url('/quality#qualitypolicy')}}">READ MORE</a>
                </div>
            </div>
        </div>
    </section>
    <section class="homepage_bottom_wrapper">
        <div class="bottom_wrap_txt">
            <h2 class="hd-28">Join us on an exciting  journey of growth!</h2>
            <div class="homepg_btn_div"><button onclick="window.location='{{ url("careers") }}'" class="button_1 homepg_bottom_btn"><span>Apply Now</span></button></div>
        </div>
    </section>
 <!--    <section class="carrers">
        <div class="container">
            <div class="carrers_mainheading">
                <h2 class="hd-38">News</h2>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 carrers_inner_div">
                    <div class="carrers_img_section">
                        <img src="{{asset('assets/images/carrer-img1.jpg')}}" alt="carrer-img1">
                        <span class="carrers_subhead">BLOG</span>
                    </div>
                    <h3 class="carrers_heading">This is Photoshop's version of Lorem Ipsum Dummy</h3>
                    <p class="carrers_date">29 MAY, 2021</p>
                    <p class="carrers_para">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>
                    <a href="" class="carrers_readmorebtn">Read More</a>
                </div>
                <div class="col-lg-4 col-md-4 carrers_inner_div">
                    <div class="carrers_img_section">
                        <img src="{{asset('assets/images/carrer-img2.jpg')}}" alt="carrer-img2">
                        <span class="carrers_subhead">NEWS</span>
                    </div>
                    <h3 class="carrers_heading">This is Photoshop's version of Lorem Ipsum Dummy</h3>
                    <p class="carrers_date">29 MAY, 2021</p>
                    <p class="carrers_para">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>
                    <a href="" class="carrers_readmorebtn">Read More</a>
                </div>
                <div class="col-lg-4 col-md-4 carrers_inner_div">
                    <div class="carrers_img_section">
                        <img src="{{asset('assets/images/carrer-img3.jpg')}}" alt="carrer-img3">
                        <span class="carrers_subhead">EVENTS</span>
                    </div>
                    <h3 class="carrers_heading">This is Photoshop's version of Lorem Ipsum Dummy</h3>
                    <p class="carrers_date">29 MAY, 2021</p>
                    <p class="carrers_para">This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>
                    <a href="" class="carrers_readmorebtn">Read More</a>
                </div>
            </div>
            <div class="time-bar">
                <div class="carrers-findoutbtn">
                    <a href="" class="button_1"><span>Find Out</span></a>
                </div>
            </div>
        </div>
    </section> -->
</div>

@stop