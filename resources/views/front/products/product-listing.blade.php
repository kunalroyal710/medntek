@foreach($products as $productInfo)
    @if(isset($isProductsPage) && $isProductsPage)
        <div class="col-lg-3 col-sm-6 items-outter">
    @endif
        <div class="items-inner">
            <a href="{{url('/product/'.$productInfo['id'].'/'.str_slug($productInfo['product_name']))}}" class="product-details-anchor">
                @if(!empty($productInfo['product_image']))
                    <img class="home-product-img" src="{{$productInfo['product_image']['medium_image_url']}}" alt="product-img">
                @else
                    <img class="home-product-img" src="{{asset('images/no-image.jpg')}}" alt="product-img">
                @endif
                <div class="item-details">
                    <ul class="specs">
                        @if(!empty($productInfo['product_image']))
                            <li title="Zoom Image">
                                <a title="Zoom Image" data-fancybox="demo" data-src="{{$productInfo['product_image']['large_image_url']}}"><img src="{{asset('assets/images/zoom.png')}}" alt="zoom-icon"></a>
                            </li>
                        @else
                            <li title="Zoom Image">
                                <a title="Zoom Image" data-fancybox="demo" data-src="{{asset('images/no-image.jpg')}}"><img src="{{asset('assets/images/zoom.png')}}" alt="zoom-icon"></a>
                            </li>
                        @endif
                        @if(count($productInfo['pro_attrs']) ==1)
                            <li title="Add to Cart">
                                <a title="Add to Cart" href="javascript:;" data-proid="{{$productInfo['id']}}" data-size="{{$productInfo['pro_attrs'][0]['size']}}" class="addTocartListing">
                                    <img src="{{asset('assets/images/cart-black.png')}}" alt="Bag-icon">
                                </a>
                            </li>
                        @else
                            <li title="Add to Cart">
                                <a title="Add to Cart" href="{{url('/product/'.$productInfo['id'].'/'.str_slug($productInfo['product_name']))}}">
                                    <img src="{{asset('assets/images/bag.png')}}" alt="Bag-icon">
                                </a>
                            </li>
                        @endif
                        <li title="Add to wishlist">
                            <a href="javascript:;" title="Add to wishlist" class="addWishList" data-productid="{{$productInfo['id']}}">
                                @if(in_array($productInfo['id'],$wishlists))
                                    <img id="WishPro-{{$productInfo['id']}}" src="{{asset('assets/images/wishlist-fill.jpg')}}" alt="Wishlist-icon">
                                @else
                                    <img id="WishPro-{{$productInfo['id']}}" src="{{asset('assets/images/wishlist.png')}}" alt="Wishlist-icon">
                                @endif
                            </a>
                        </li>
                        <!-- <li>
                            <a href="#"><img src="{{asset('assets/images/compare.png')}}" alt="Compare-icon"></a>
                        </li> -->
                    </ul>
                    <h4>{{$productInfo['product_name']}}</h4>
                    
                        @if(!empty($productInfo['discount_type']))
                            <h5>&#x20B9; {{formatAmt($productInfo['final_price'])}} <span><del>&#x20B9; {{formatAmt($productInfo['price'])}}</del></span></h5>
                            <p>You save &#x20B9; {{formatAmt($productInfo['price'] - $productInfo['final_price'])}} <span class="value-off">({{$productInfo['discount']}}% OFF)</span></p>
                        @else
                            <h5>&#x20B9; {{formatAmt($productInfo['final_price'])}}</h5>
                        @endif
                    
                </div>
            </a>
        </div>
    @if(isset($isProductsPage) && $isProductsPage)
        </div>
    @endif
@endforeach
@if(isset($isProductsPage) && $isProductsPage && count($products) ==0)
    <h3>Coming Soon</h3>
@endif