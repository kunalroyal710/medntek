<div class="col-lg-2 col-md-12 col-sm-12 order-2 order-lg-1 produts-sidebar-filter">
	<div class="filter_mobile_header">
		<div class="filter_close" onclick="filterclose()">
			<img src="{{asset('assets/images/close-white.svg')}}" alt="close-white">
		</div>
	</div>
	<div class="filter-widget search-bar">
		<div class="searchform">
			<form action="{{url('/search-results')}}" method="get" autocomplete="off">
				<input class="search-input" name="q" type="text" placeholder="Search" required value="@if(isset($_GET['q'])){{$_GET['q']}}@endif" >
				<button class="search-btn-product" type="submit">
				<img src="{{asset('assets/images/search-product-icon.svg')}}" alt="search-product-icon">
				</button>
			</form>
		</div>
		<div class="accordion accordion-product" id="accordionExample">
			<div class="apply-clear"><a href="#" class="applylink" onclick="filterclose()">Apply</a> <span class="filterdivider">|</span> <a href="{{$closeUrl}}" class="clearlink">Clear</a></div>
			<div class="filter-widget">
				<div class="card-header sidebar-head-product" id="headingOne">
					<div class="product-sidebar-head fw-title" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Categories</div>
				</div>
				<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
					<div class="card-body product-list-sidebar">
						<div class="fw-brand-check">
							@foreach(categories() as $category)
								<div class="bc-item">
									<label for="bc-{{$category['id']}}">{{$category['name']}}
										<input name="category" type="checkbox" id="bc-{{$category['id']}}" value="{{$category['name']}}" class="filterAjax"><span class="checkmark"></span>
									</label>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
			<div class="filter-widget">
				<div class="card-header sidebar-head-product" id="headingTwo">
					<div class="product-sidebar-head fw-title collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Price</div>
				</div>
				<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
					<div class="card-body">
						<div class="fw-brand-check">
							<?php $priceRangArr = array('1-500'=>'Below ₹ 500','500-1000'=>'₹ 500 - 1000','1000-1500'=>'₹ 1000 - 1500','1500-2000'=>'₹ 1500 - 2000'); 
							?>
							@foreach($priceRangArr as $pkey => $priceRange)
								<div class="bc-item">
									<label for="bc-{{$pkey}}"> {{$priceRange}}
										<input name="price" type="checkbox" id="bc-{{$pkey}}" value="{{$pkey}}" class="filterAjax"><span class="checkmark"></span>
									</label>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
			<div class="filter-widget">
				<div class="card-header sidebar-head-product" id="headingThree">
					<div class="product-sidebar-head fw-title collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Discount</div>
				</div>
				<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
					<div class="card-body">
						<div class="fw-brand-check">
							<?php $disArr = array(10,20,30,40); ?>
							@foreach($disArr as $dis)
								<div class="bc-item">
									<label for="bc-per-{{$dis}}">{{$dis}}%
										<input name="discount" type="checkbox" id="bc-per-{{$dis}}" value="{{$dis}}" class="filterAjax"> <span class="checkmark"></span>
									</label>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>