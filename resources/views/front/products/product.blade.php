@extends('layouts.frontLayout.front-layout')
@section('content')
<section class="breacrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-text product-more">
                    <a href="{{url('/')}}">Home</a>
                    <a href="{{url('/'.$productResp['productdetails']['category']['seo_unique'])}}">{{$productResp['productdetails']['category']['name']}}</a>
                    <span>{{$productResp['productdetails']['product_name']}}</span>
                </div>
            </div>
        </div>
    </div>
</section>
<div class=hongo-layout>
    <section class="top-wrapper-sec-1  hongo-single-product-main-wrap hongo-main-content-wrap top-space right-blue-corner-shape">
        <div class="container-fluid single-product-sticky">
            <div class=row>
                <div class="col-sm-12 col-xs-12 hongo-full-width-layout hongo-full-width-no-padding hongo-content-full-part">
                    <div id=primary class=content-area>
                        <div id=main class=site-main role=main>
                            <div class=woocommerce-notices-wrapper></div>
                            <div id=product-36119 class="hongo-alternate-image-wrap product type-product post-36119 status-publish first instock product_cat-featured-products product_cat-shoes-collection product_cat-winter-jackets product_cat-speakers product_cat-study-set product_cat-summer-collection product_tag-shorts has-post-thumbnail sale featured taxable shipping-taxable purchasable product-type-simple">
                                <div class="hongo-sticky-content-images-wrap bottom-purpl-corner-shape"  data-sticky_parent>
                                    <div class=hongo-single-product-sticky-thumb-wrap data-sticky_column>
                                        <ul>
                                            @foreach($productResp['productdetails']['productimages'] as $pkey => $proimage)
                                                <li>
                                                    <a href=javascript:void(0) data-image-attribute={{$pkey}}>
                                                    <img src="{{$proimage['medium_image_url']}}" />
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns=4>
                                        <div class=hongo-single-product-image-wrap>
                                            <!-- <div class=sale-new-wrap> <span class="onsale alt-font">-40%</span></div> -->
                                            <figure class="woocommerce-product-gallery__wrapper photoswipe-lightbox">
                                                @foreach($productResp['productdetails']['productimages'] as $proimage)
                                                    <div data-thumb="{{$proimage['medium_image_url']}}" class=woocommerce-product-gallery__image>
                                                        <a href="javascript:;">
                                                        <img src="{{$proimage['large_image_url']}}" data-zoom-image="{{$proimage['large_image_url']}}" class="imggallery"  />
                                                        </a>	
                                                </div>
                                                @endforeach
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="summary entry-summary product-detail-medntek" data-sticky_column>
                                        <div class=summary-main-title>
                                            <div class=summary-main-title-left>
                                                <h1 class="product_title entry-title alt-font">{{$productResp['productdetails']['product_name']}}</h1>
                                                <div class=woocommerce-product-details__short-description>
                                                    <p>{{$productResp['productdetails']['short_description']}}</p>
                                                    <table class="p-detail-table">
                                                        <tr>
                                                            <th>Product Category:</th>
                                                            <td  colspan="2"><span>{{$productResp['productdetails']['category']['name']}}</span></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Contact Time :</th>
                                                            <td  colspan="2"><span>{{$productResp['productdetails']['contact_time']}}</span></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Availability:</th>
                                                            <td colspan="2"><span class="green-color">@if($productResp['productdetails']['total_stock'] >0)In stock @else 
                                                                <span class="red-color">Out of Stock
                                                                </span>@endif</span></td>
                                                        </tr>
                                                        @if($productResp['productdetails']['case_count'] >0)

                                                            <tr>
                                                            <th>Case Count:</th>
                                                            <td><span>{{$productResp['productdetails']['case_count']}}</span></td>
                                                            @if(isset($productResp['productdetails']['pro_attrs'][0]))
                                                            <td rowspan="2"><span>{{$productResp['productdetails']['case_count']}} Case = {{$productResp['productdetails']['pro_attrs'][0]['moq']}} units</span></td>
                                                            @endif
                                                            </tr>
                                                        @endif
                                                        @if(isset($productResp['productdetails']['pro_attrs'][0]))
                                                            <tr>
                                                                <th>Min. order quantity (in case) :</th>
                                                                <td>
                                                                    <span>{{$productResp['productdetails']['pro_attrs'][0]['moq']}}</span>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    </table>
                                                <!-- New  -->
                                                <div class="costing-details">
                                                    <span id="ProDetailPricings">
                                                    <div class="price">
                                                        <h5>₹ {{$productResp['productdetails']['final_price']}}
                                                        @if(!empty($productResp['productdetails']['discount_type']))
                                                            <span><del>₹ {{$productResp['productdetails']['price']}}</del></span></h5>
                                                        @endif
                                                    </div> 
                                                    @if(!empty($productResp['productdetails']['discount_type']))
                                                        <div class="savings">
                                                            <h4>You save &#x20B9; {{formatAmt($productResp['productdetails']['price'] - $productResp['productdetails']['final_price'])}}<span>({{$productResp['productdetails']['discount']}}% OFF)</span></h4>
                                                        </div>
                                                    @endif
                                                    </span>
                                                    <div class="review">
                                                        @if(\Auth::check())
                                                            <span><img src="{{asset('assets/images/review.png')}}" alt="Review-icon"> <a href="#reviews">Write A Review</a></span>
                                                        @else
                                                            <span> <a href="{{url('/signin')}}">Login to add Review</a></span>
                                                        @endif
                                                    </div>
                                                </div>
                                                    <div class="details-product">
                                                        <h4 class="hd-18">PRODUCT DETAILS</h4>
                                                        <p><?php echo nl2br($productResp['productdetails']['product_description']); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <form class="cart" action="javascript:;">
                                            <div class="qnty">
                                                <div class="quantity-main">
                                                    <h5 class="hd-18">Case Qty</h5>
                                                    <div class="quantity alt-font">
                                                        <label class=screen-reader-text for="quantity_60de65b2aafa4">Quantity</label>
                                                        <input type=button value=- class="hongo-qtyminus" data-field="quantity_60de65b2aafa4">
                                                        <input type=number id="quantity_60de65b2aafa4" class="input-text qty text" step="1" min="1" name="qty" value=1 title=Qty>
                                                        <input type=button value=+ class="hongo-qtyplus" data-field="quantity_60de65b2aafa4">
                                                    </div>
                                                </div>
                                                <div class="size-product">
                                                    <h5 class="hd-18">SIZE</h5>
                                                    <select name="size" id="size-item">
                                                        @foreach($productResp['productdetails']['pro_attrs'] as $proAttr)
                                                            <option value="{{$proAttr['size']}}">{{$proAttr['size']}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="add_cart_section">
                                                    <div class="product-cartbtn">
                                                        <a href="javascript:;" class="button_1 disinfectants_btn addtoCart" tabindex="-1"><span><img class="cart-img" src="{{asset('assets/images/cart.png')}}" alt="cart-icon">ADD TO CART</span></a>
                                                    </div>
                                                    <div class="wishlist">
                                                        <a href="javascript:;" class="addWishList" data-productid="{{$productResp['productdetails']['id']}}">
                                                            @if(in_array($productResp['productdetails']['id'],$wishlists))
                                                                <img id="WishPro-{{$productResp['productdetails']['id']}}" src="{{asset('assets/images/wishlist-fill.jpg')}}" alt="Wishlist-icon">
                                                            @else
                                                                <img id="WishPro-{{$productResp['productdetails']['id']}}" src="{{asset('assets/images/wishlist.png')}}" alt="Wishlist-icon">
                                                            @endif</a>
                                                    </div>
                                                    <div class="share-to">
                                                        <h5>SHARE</h5>
                                                        <div class="product-social">
                                                            <div onclick="shareIcon()">
                                                                <a href="javascript:;"><img src="{{asset('assets/images/share-new.png')}}" alt="Share-icon"></a>
                                                                @php
                                                                    $prourl = "https://imaecmedntek.com/product/".$productResp['productdetails']['id'].'/'.str_slug($productResp['productdetails']['product_name']);
                                                                @endphp
                                                                <ul id="sharebtn" style="display:none;">
                                                                    <li><a href="https://api.whatsapp.com/send?text={{$prourl}}" data-action="share/whatsapp/share" target="_blank"><img src="{{asset('assets/images/social-7.png')}}" alt=""></a></li>
                                                                    <li><a href="https://www.facebook.com/sharer/sharer.php?u={{$prourl}}" target="_blank"><img src="{{asset('assets/images/social-2.png')}}" alt=""></a></li>
                                                                    <!-- <li><a href="https://www.instagram.com/" target="_blank"><img src="{{asset('assets/images/social-5.png')}}" alt=""></a></li> -->
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="product-availability">
                                            {!!$productResp['productdetails']['technical_details']!!}
                                        </div>
                                        <div id=hongo-accordion class=hongo-accordion>
                                            <ul class=hongo-accordion-section role=tablist>
                                                @if(!empty($productResp['productdetails']['salient_features']))
                                                <li class=description_tab id=tab-title-description role=tab aria-controls=tab-description>
                                                    <a class="hongo-accordion-section-title alt-font active" href=#tab-description>Salient Features<span></span></a>
                                                    <div class="hongo-accordion-section-content woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content open" style="display: block;" id=tab-description role=tabpanel aria-labelledby=tab-title-description>
                                                        {!!$productResp['productdetails']['salient_features']!!}
                                                    </div>
                                                </li>
                                                @endif
                                                @if(!empty($productResp['productdetails']['direction_of_use']))
                                                <li class=additional_information_tab id=tab-title-additional_information role=tab aria-controls=tab-additional_information>
                                                    <a class="hongo-accordion-section-title alt-font" href=#tab-additional_information>Direction of Use<span></span></a>
                                                    <div class="hongo-accordion-section-content woocommerce-Tabs-panel woocommerce-Tabs-panel--additional_information panel entry-content" id=tab-additional_information role=tabpanel aria-labelledby=tab-title-additional_information>
                                                        {!!$productResp['productdetails']['direction_of_use']!!}
                                                    </div>
                                                </li>
                                                @endif
                                                @if(!empty($productResp['productdetails']['area_of_application']))
                                                <li class=reviews_tab id=tab-title-reviews role=tab aria-controls=tab-reviews>
                                                    <a class="hongo-accordion-section-title alt-font" href=#tab-reviews>Area of Application<span></span></a>
                                                    <div class="hongo-accordion-section-content woocommerce-Tabs-panel woocommerce-Tabs-panel--reviews panel entry-content" id=tab-reviews role=tabpanel aria-labelledby=tab-title-reviews>
                                                        {!!$productResp['productdetails']['area_of_application']!!}
                                                    </div>
                                                </li>
                                                @endif
                                                @if(!empty($productResp['productdetails']['microbial_efficacy']))
                                                <li class=microbial_tab id=tab-title-microbial role=tab aria-controls=tab-microbial>
                                                    <a class="hongo-accordion-section-title alt-font" href=#tab-microbial>Microbial Efficacy<span></span></a>
                                                    <div class="hongo-accordion-section-content woocommerce-Tabs-panel woocommerce-Tabs-panel--microbial panel entry-content" id=tab-microbial role=tabpanel aria-labelledby=tab-title-microbial>
                                                        {!!$productResp['productdetails']['microbial_efficacy']!!}
                                                    </div>
                                                </li>
                                                @endif
                                            </ul>
                                        </div>
                                        @if(Auth::check())
                                            @include('front.products.product-review')
                                        @endif
                                    </div>
                                </div>
                                <div class="product_related_list">
                                    <div class="titlediv">Related Products</div>
                                    <div class="product-items active-product-item relatedPro">
                                        @php $products = $productResp['productdetails']['relatedproducts'] @endphp
                                        @include('front.products.product-listing')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div id=hongo_compare_popup class="woocommerce hongo-popup-content hongo-compare-popup hongo-white-popup"></div>
<div id=hongo_quick_view_popup class="woocommerce hongo-popup-content hongo-quick-view-popup hongo-white-popup"></div>
@stop
@section('javascript')
@parent
    <script src="{{asset('assets/js/unveilhooks.js')}}"></script>
    <script src="{{asset('assets/js/cookies.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap_1.js')}}"></script>
    <script>
        var wc_single_product_params = {};
        var hongoAddons = {
            "wishlist_added_message": "Product was added to wishlist successfully",
            "wishlist_remove_message": "Product was removed from wishlist successfully",
        };
        var hongoMain = {};
        
        jQuery(document).ready(function(){
            $(document).on('change','[name=size]',function(){
                $('.PleaseWaitDiv').show();
                var proid = "<?php echo $productResp['productdetails']['id'] ?>"
                var size = $(this).val();
                $.ajax({
                    type : 'POST',
                    url : '/get-product-pricing',
                    data : {size:size,proid:proid},
                    success:function(resp){
                        $('#ProDetailPricings').html(resp.pricing_html);
                        $('.PleaseWaitDiv').hide();
                    },
                    error:function(){

                    }
                })
            })

            $(document).on('click','.addtoCart',function(){
                var proid = "<?php echo $productResp['productdetails']['id'] ?>"
                var qty = $('[name=qty]').val();
                var size = $('[name=size]').val();
                if(qty >=1){
                    $('.PleaseWaitDiv').show();
                    $.ajax({
                        url: "{{url('/add-to-cart')}}",
                        type:'POST',
                        data: {qty:qty,size:size,proid:proid},
                        success: function(data) {
                            $('.PleaseWaitDiv').hide();
                            if(!data.status){
                                alert(data.message);
                            }else{
                                $(".cartItems").trigger("click");
                                window.scrollTo(0, 0);
                            }
                        }
                    });
                }else{
                    alert('We are sorry! requested quantity is not available at the moment');
                }
            });

            jQuery('.relatedPro').slick({
              dots: false,
              array: false,
              infinite: false,
              speed: 300,
              slidesToShow: 5,
              slidesToScroll: 1,
              responsive: [
                {
                  breakpoint: 1500,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: false
                  }
                },
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: false,
                  }
                },
                {
                  breakpoint: 780,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
              ]
            });
        });
    </script>
    <script>
        function shareIcon() {
          var x = document.getElementById("sharebtn");  
          if (x.style.display === "none") {
            x.style.display = "block";
          } else {
            x.style.display = "none";
          }
        }
    </script>
    <script src="{{asset('assets/js/author.js')}}"></script>
    <script>
        jQuery('.imggallery').elevateZoom({
            zoomType: "inner",
            cursor: "crosshair",
            zoomWindowFadeIn: 500,
            zoomWindowFadeOut: 750
        }); 
    </script>
<style type="text/css">
    .hongo-accordion ul li span,.hongo-accordion ul li 
    {
    font-size: 16px !important;
    color: #686868 !important;
    }
</style>
@stop