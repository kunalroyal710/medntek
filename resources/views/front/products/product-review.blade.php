<?php $reviewResp = \App\Models\ProductReview::reviews($productResp['productdetails']['id']);?>
<div class="detail-review">
    <div class="row rating">
        <div class="col-md-12 lf-rating">
            <h3>Rating & Reviews</h3>
            <span>{{$reviewResp['averageRatings']}}<i class="fa fa-star" aria-hidden="true"></i></span>
            <h5>{{count($reviewResp['reviews'])}} reviews</h5>
        </div>
    </div>
    @foreach($reviewResp['reviews'] as $review)
        <div class="row review-des">
            <div class="col-md-12 inner-review">
                <p>{{$review['review']}}</p>
            </div>
            <div class="col-md-6 review-star">
                <h4>{{$review['user']['name']}}</h4>
                <div class="inner-stars">
                    @for($i=1; $i<=$review['star']; $i++)
                        <img src="{{asset('assets/images/rating-star.png')}}" alt="Rating-star-icon">
                    @endfor
                </div>
            </div>
        </div>
    @endforeach
    @if(Auth::check())
        <!-- Paste Here -->
        <div id="reviews" class="woocommerce-Reviews">
            <h3 class="review-heading hd-18">Write A Review</h3>
            <div id="review_form_wrapper">
                <div id="review_form">
                    <div id="respond" class="comment-respond">
                        <form action="javascript:;" method="post" id="proReview" class="comment-form" autocomplete="off">
                            <input type="hidden" name="product_id" value="{{$productResp['productdetails']['id']}}">
                           <!--  <p class="comment-notes"><span id="email-notes">Your email address will not be published.</span> Required fields are marked <span class="required">*</span></p>
                            <p class="comment-form-author">
                                <label for="author">Name&nbsp;<span class="required">*</span></label>
                                <input id="author" name="name" type="text"  size="30" placeholder="Enter Name">
                            </p>
                            <p class="comment-form-email">
                                <label for="email">Email&nbsp;<span class="required">*</span></label>
                                <input id="email" name="email" type="email"  size="30" placeholder="Enter Email">
                            </p> -->
                            <div class="comment-form-rating">
                                <label for="rating">Your rating<span class="required">*</span></label>
                                <select name="star" id="rating" style="display: none;">
                                    <option value="">Rate…</option>
                                    <option value="5">Perfect</option>
                                    <option value="4">Good</option>
                                    <option value="3">Average</option>
                                    <option value="2">Not that bad</option>
                                    <option value="1">Very poor</option>
                                </select>
                            </div>
                            <p class="comment-form-comment">
                                <label for="comment">Your review<span class="required">*</span></label>
                                <textarea id="comment" class="comment-field" name="review" cols="45" rows="8" aria-required="true" placeholder="Enter your review..."></textarea>
                            </p>
                            <p class="form-submit">
                                <button type="submit" class="button_1"><span>Submit</span></button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <!-- Paste here end  -->
    @else
        <a href="javascript:;" class="btn">Login to Review</a>
    @endif
</div>