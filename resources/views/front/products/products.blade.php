@extends('layouts.frontLayout.front-layout')
@section('content')
<!-- Main Container Section -->
<div class="main-section-container">
	<div class="breacrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="breadcrumb-text"> <a href="{{url('/')}}"> Home</a> <span>{{$catdata['catdetail']['name']}}</span> </div>
				</div>
			</div>
		</div>
	</div>
	<section class="product-shop spad product_landing right-blue-corner-shape">
		<div class="container">
			<div class="row">
				@include('front.products.product-filters')
				<div class="col-lg-10 order-1 order-lg-2 product-landing-1">
					<div class="product-show-option-outter">
						<div class="product-show-option">
							<div class="row">
								<div class="col-lg-8 col-md-8 col-sm-12">
									<div class="head-products">
										<h2 class="hd-30" id="titleDetails">{{$catdata['catdetail']['name']}}</h2>
										<span  id="productCount"><small>
											(Showing @if(!empty($products->firstItem() )) {{ $products->firstItem() }} @else 0 @endif to {{ $products->lastItem() }} <span class="d-none d-sm-inline-block">products of</span> <span class="d-inline-block d-sm-none">/</span> {{$products->total()}} <span class="d-none d-sm-inline-block">products)</span></small></span>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12 text-center">
									<div class="sort-div-main">
										<div class="sort-div">
											<label for="sort-item-lable" class="sort-item-class">Sort By :&nbsp;&nbsp;
											</label>
											<select name="sort" id="sort-item" class="sort-item-select getsort">
												<option value="">Select</option>
												<option value="newest">New Arrival</option>
												<option value="htl">High to Low </option>
												<option value="lth">Low to High </option>
											</select>
										</div>
									</div>
									<!-- <div class="item-nums-div">
										<label for="item-nums">View Items :</label>
										<select name="item-nums" id="item-nums">
											<option value="12">12</option>
											<option value="11">11</option>
											<option value="10">10</option>
											<option value="09">09</option>
										</select>
									</div> -->
									<div class="filter-options">
										<span class="filter_on" onclick="filteron()">
											Product Filter <i class="fa fa-filter" aria-hidden="true"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="product-list products-landing-list">
							<div class="row" id="appnedProductListing">
								@include('front.products.product-listing')
							</div>
						</div>
						<div class="product-page-numbering" id="appendpagination"> 
							@include('front.products.products-pagination')
						</div>
				    </div>
				</div>
			</div>
		</div>
	</section>
</div>
<!-- Main Container Section End-->
@stop
@section('javascript')
@parent
<script type="text/javascript">
	$(document).ready(function(){
		//Filter Products
		var queryStringObject = {};
	    if($('.filtertrue').length > 0) {
	        $(".filterAjax").each( function () {
	            var name = $(this).attr('name');
	            queryStringObject[name] = [];
	            $.each($("input[name='"+$(this).attr('name')+"']:checked"), function(){
	                queryStringObject[name].push($(this).val());
	            });
	            if(queryStringObject[name].length == 0){
	                delete queryStringObject[name];
	            }
	        });
	        var value = $('.getsort option:selected').val();
	        var name= $('.getsort').attr('name');
	        queryStringObject[name] = [value];
	        if(value==""){
	            delete queryStringObject[name];
	        }
	    }
	    $(".filterAjax").click(function(){
	        var name = $(this).attr('name');
	        queryStringObject[name] = [];
	        $.each($("input[name='"+$(this).attr('name')+"']:checked"), function(){
	            queryStringObject[name].push($(this).val());
	        });
	        if(queryStringObject[name].length == 0){
	            delete queryStringObject[name];
	        }
	        filterproducts(queryStringObject);
	    });
	    
	    $(document).on('change','.getsort',function(){
	        var value = $(this).val();
	        var name= $(this).attr('name');
	        queryStringObject[name] = [value];
	        if(value==""){
	            delete queryStringObject[name];
	        }
	        filterproducts(queryStringObject);
	    });
	})
	function filterproducts(queryStringObject){
    	if( window.innerWidth < 575 ) {
    		//nothing to do
    	}else{
    		$(".PleaseWaitDiv").show();
    	}
        let searchParams = new URLSearchParams(window.location.search);
	    if(searchParams.has('q')){
	        let parameterQuery = searchParams.get('q');
	        var queryString = "?q="+parameterQuery;
	    }else{
	        var queryString = "";
	    }
        for (var key in queryStringObject) {
            if(queryString==''){
                queryString +="?"+key+"=";
            }else{
                queryString +="&"+key+"=";
            }
            var queryValue = "";
            for (var i in queryStringObject[key]) {
                if(queryValue==''){
                    queryValue += queryStringObject[key][i];
                } else {
                    queryValue += "~"+queryStringObject[key][i];
                }
            }
            queryString += queryValue;
        }
        if (history.pushState) {
            var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + queryString;
            window.history.pushState({path:newurl},'',newurl);
        }
        if (newurl.indexOf("?") >= 0) {
            newurl = newurl+"&json=";
        }else{
            newurl = newurl+"?json=";
        }
        $.ajax({
            url : newurl,
            type : 'get',
            dataType:'json',
            success:function(resp){
            	$('#productCount').text(resp.countproducts);
                $("#appnedProductListing").html(resp.view);
                $("#appendpagination").html(resp.pagination_view);
                $(".PleaseWaitDiv").hide();
                var title = resp.queryStringData.category.replace(/~/g, ', ');
                $('#titleDetails').html(title);
            },
            error:function(){}
        });
    }
</script>
@stop