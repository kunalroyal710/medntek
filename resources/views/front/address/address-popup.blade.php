<style type="text/css">
    .err{
        color: red;
    }
</style>
<div class="modal fade searchpop leadershippop ship-add-popup" id="shipAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <form id="addressModalForm" action="javascript:;" method="post">@csrf
    <div class="modal-dialog leadership_modal_dialog " role="document">
        <div class="modal-content">
            <div class="modal-body search-modal">
                <div class="search-modal-inner leadership_modal_inner shiping-modal">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="close-popup" aria-hidden="true"><img src="{{asset('assets/images/cross_close.svg')}}" class="image_cross_close"></span>
                    </button>
                    <div class="container">
                        <div class="ls_card_modal  address-card-modal">

                            <div class="ls_card_context_modal ship-address  ship-modal-address">

                                <div class="modal-body ship-main-address">
                                    <h4>Shipping Address</h4>
                                    <hr />
                                    <div class="col-sm-12 col-12">
                                        <div class="row">
                                            <div class="col-sm-6 col-12 form-group">
                                                <input type="text" class="input-style form-control" name="first_name" id="first_name"  placeholder="First Name" />
                                                <p class="err text-center" id="DeliveryAddress-first_name" style="display: none;"></p>
                                            </div>
                                            <input type="hidden" name="shipping_id">
                                            <div class="col-sm-6 col-12 form-group">
                                                <input type="text" class="input-style form-control" name="last_name" id="last_name"  placeholder="Last Name" />
                                                <p class="err text-center" id="DeliveryAddress-last_name" style="display: none;"></p>
                                            </div>
                                            <div class="col-sm-6 col-12 form-group">
                                                <input type="text" class="input-style form-control" name="mobile" id="mobile" placeholder="Mobile"  />
                                                <p class="err text-center" id="DeliveryAddress-mobile" style="display: none;"></p>
                                            </div>
                                            <div class="col-sm-6 col-12 form-group">
                                                <input type="text" class="input-style form-control" name="postcode" id="ship_pincode" placeholder="Postcode"  />
                                                <p class="err text-center" id="DeliveryAddress-postcode" style="display: none;"></p>
                                            </div>
                                            <div class="col-sm-6 col-12 form-group">
                                                <input type="text" class="input-style form-control" name="state" id="ship_state" placeholder="State"  />
                                                <p class="err text-center" id="DeliveryAddress-state" style="display: none;"></p>
                                                <?php /*<select  class="form-control" name="state">
                                                    <option value="">Please Select</option>
                                                    @foreach($states as $state)
                                                    <option value="{{$state}}">{{$state}}</option>
                                                    @endforeach
                                                </select>*/?>
                                                <p class="err text-center" id="DeliveryAddress-state" style="display: none;"></p>
                                            </div>
                                            <div class="col-sm-6 col-12 form-group">
                                                <input type="text" class="input-style form-control" name="city" id="ship_city" placeholder="City"  />
                                                <p class="err text-center" id="DeliveryAddress-city" style="display: none;"></p>
                                            </div>
                                            
                                            <div class="col-sm-6 col-12 form-group">
                                                <textarea class="input-style form-control" name="address_line_1" id="ship_address" placeholder="Address Line 1" ></textarea>
                                                <p class="err text-center" id="DeliveryAddress-address_line_1" style="display: none;"></p>
                                            </div>
                                            <div class="col-sm-6 col-12 form-group">
                                                <textarea class="input-style form-control" name="address_line_2" id="ship_address" placeholder="Address Line 2" ></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer m-footer-btn">
                                        <button type="submit" class="button_1 disinfectants_btn"><span>Save</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</div>