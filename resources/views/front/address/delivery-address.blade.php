<div class="col-12">
    <h5 class="orange">Delivery Address</h5>
    <br>
</div>
<?php $addresses = \App\Models\ShippingAddress::addresses(); ?> 
<div class="col-12 addresses">
    <div class="row">
        @foreach($addresses as $address)
            <div class="col-sm-6 col-12">
                <ul class="filter listeeTarget radio">
                    <li>
                        <input type="radio"  name="defaultAddress" id="Shipping{{$address['id']}}" @if($address['default_address'] ==1) checked @endif value="{{$address['id']}}">
                        <label for="Shipping{{$address['id']}}"><p>Name : {{$address['name']}} <br />
                            Mobile : {{$address['mobile']}} <br/>
                            {{$address['address_line_1']}}<br>
                            {{$address['city']}}, {{$address['state']}}, {{$address['country']}}, {{$address['postcode']}} </p><br />
                        <a href="javascript:;" data-addressid="{{$address['id']}}" class="address-edit editAddress" data-toggle="modal">Edit</a>
                        @if($address['default_address'] !=1)
                            <a href="javascript:;" data-addressid="{{ $address['id'] }}" class="address-edit removeAddress">Remove</a></label>
                        @endif
                    </li>
                </ul>
            </div>
        @endforeach
    </div>
</div>
<div class="col-12">
    <button class="add-btn-style" data-toggle="modal" data-target="#shipAdd">
    <svg class="svg-inline--fa fa-plus fa-w-14" aria-hidden="true" data-fa-processed="" data-prefix="fa" data-icon="plus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
    <path fill="currentColor" d="M448 294.2v-76.4c0-13.3-10.7-24-24-24H286.2V56c0-13.3-10.7-24-24-24h-76.4c-13.3 0-24 10.7-24 24v137.8H24c-13.3 0-24 10.7-24 24v76.4c0 13.3 10.7 24 24 24h137.8V456c0 13.3 10.7 24 24 24h76.4c13.3 0 24-10.7 24-24V318.2H424c13.3 0 24-10.7 24-24z"></path>
</svg>
&nbsp; Add New Address
</button>
</div>

@section('javascript')
@parent
<script type="text/javascript">
        
    // Remove Delivery Address
    $(document).on('click', '.removeAddress', function(e) {
        if (confirm('Are you sure you want to remove this?')) {
            $('.PleaseWaitDiv').show();
            var addressid = $(this).data("addressid");
            $.ajax({
                url: '/remove-delivery-address',
                type:'GET',
                data : {"id":addressid}, 
                success:function(resp){   
                    $('#deliveryAddresses').html(resp.view);
                    $('.PleaseWaitDiv').hide();
                }
            });
        }
    });

    // Set Default Delivery Address
    $(document).on('change', '[name=defaultAddress]', function(){
        $('.PleaseWaitDiv').show();
        var addressid = $(this).val();
        $.ajax({
            type:'get',
            url:'/set-default-address',
            data:{addressid:addressid},
            success:function(resp){
                $('#deliveryAddresses').html(resp.view);  
                $('.PleaseWaitDiv').hide();
                /*$('html, body').animate({
                    scrollTop: $('#deliveryAddresses').offset().top -120
                }, 1250, 'easeInOutQuad', function() {});*/
            },
            error:function(){
                //nothing to do
            }
        });
    });

    $(document).on('click', '.editAddress', function(e) {
        $('.PleaseWaitDiv').show();
        var addressid = $(this).data("addressid");
        $.ajax({
            data : { id:addressid},
            url : "/get-delivery-address",
            type : 'get',
            success:function(resp){
                $('.PleaseWaitDiv').hide();
                if(resp.status){
                    $('[name=shipping_id]').val(resp.address['id']);
                    $('[name=first_name]').val(resp.address['first_name']);
                    $('[name=last_name]').val(resp.address['last_name']);
                    $('[name=state]').val(resp.address['state']);
                    $('[name=city]').val(resp.address['city']);
                    $('[name=mobile]').val(resp.address['mobile']);
                    $('[name=postcode]').val(resp.address['postcode']);
                    $('[name=address_line_1]').val(resp.address['address_line_1']);
                    $('[name=address_line_2]').val(resp.address['address_line_2']);
                    $('#shipAdd').modal('show');
                }else{
                    $('#deliveryAddresses').html(resp.view);
                }
            }
        });
    });

    $("#addressModalForm").submit(function(e){
        e.preventDefault();
        $('.PleaseWaitDiv').show();
        var formdata = $("#addressModalForm").serialize();
        $.ajax({
            url: '/save-address',
            type:'POST',
            data: formdata,
            success: function(data) {
                $('.PleaseWaitDiv').hide();
                if(!data.status){
                    $.each(data.errors, function (i, error) {
                        $('#DeliveryAddress-'+i).attr('style', '');
                        $('#DeliveryAddress-'+i).html(error);
                        $('#DeliveryAddress-'+i).addClass('error-triggered');
                        setTimeout(function () {
                            $('#DeliveryAddress-'+i).css({
                                'display': 'none'
                            });
                        }, 3000);
                        $('#DeliveryAddress-'+i).removeClass('error-triggered');
                    });
                    $('html,body').animate({
                        scrollTop: $('.error-triggered').first().stop().offset().top - 200
                    }, 1000);
                }else{
                    $('[name=shipping_id]').val('');
                    $('#shipAdd').modal('hide');
                    $('#addressModalForm').trigger("reset");
                    $('#deliveryAddresses').html(data.view);
                }
                
            }
        });
    });
    $('#shipAdd').on('hidden.bs.modal', function () {
        $('#addressModalForm').trigger("reset");
        $('[name=shipping_id]').val('');
    })
</script>
@stop