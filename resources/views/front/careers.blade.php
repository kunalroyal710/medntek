@extends('layouts.frontLayout.front-layout')
@section('content')
<!-- Main Container Section -->
<div class="main-section-container">
    <section class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text product-more">
                        <a href="{{url('/')}}">Home</a>
                        <span>Careers</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="career-section right-blue-corner-shape bottom-purpl-corner-shape">
        <div class="container">
            @foreach(banners('careers-banner') as $bannerInfo)
            <div class="career-main-img">
                <img src="{{asset('images/BannerImages/'.$bannerInfo['image'])}}" alt="Career-Banner-image">
            </div>
            @endforeach
        </div>
        <?php echo $cmsInfo['description'];?>
        <div class="current-openings">
            <div class="container">
                <h2 class="page_inner_heading">Current Openings</h2>
                <?php $deptOpenings = \App\Models\Department::depts(); 
                    //echo "<pre>"; print_r($deptOpenings); die;
                ?>
                @foreach($deptOpenings as $dept)
                    <h4>{{$dept['department']}}</h4><br>
                    <div class="modal-careerform">
                        <div class="faq-box">
                            <div class="faq-wrapper">
                                @foreach($dept['openings'] as $opening)
                                    <div class="item">
                                        <div class="faqtitle"><a href="javascript:void(0)" id="" class="">{{$opening['title']}} <span class="plus_min_button"></span></a></div>
                                        <div class="faqData">
                                            <ul class="return_list"><?php echo $opening['description']; ?>
                                            </ul>
                                            <div class="apply-now">
                                                <a href="javascript:;" data-deptid="{{$dept['department']}}" class="enquire_Now button_1 applyNowDept"><span>Apply Now</span></a>
                                            </div>
                                       </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="career-wrapper">
            <form  id="CareerForm" action="javascript:;" method="post" autocomplete="off">@csrf
                <div class="container">
                    <div class="career-form">
                        <h3 class="hd-24">Apply Now</h3>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-12">
                                <div class="apply-attribute">
                                    <input type="text" name="name"   placeholder="Name*" required>
                                    <span class="validation-error" id="Career-name"></span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12">
                                <div class="apply-attribute">
                                    <input type="email" name="email"   placeholder="Email-id*" required>
                                    <span class="validation-error" id="Career-email"></span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12">
                                <div class="apply-attribute">
                                    <input type="number" name="mobile"   placeholder="Contact No*" required>
                                    <span class="validation-error" id="Career-mobile"></span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12">
                                <div class="apply-attribute">
                                    <select name="position" class="dept-apply" required>
                                        <option value="">Department Applied For*</option>
                                        @foreach($deptOpenings as $dept)
                                            <option value="{{$dept['department']}}">{{$dept['department']}}</option>
                                        @endforeach
                                        <option value="Other">Other</option>
                                    </select>
                                    <span class="validation-error" id="Career-position"></span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12">
                                <div class="apply-attribute">
                                    <input type="text" name="work_experience"   placeholder="Total Work Experience*" required>
                                    <span class="validation-error" id="Career-work_experience"></span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12">
                                <div class="apply-attribute">
                                    <input type="text" name="current_organisation"   placeholder="Current Organization*" required>
                                    <span class="validation-error" id="Career-current_organisation"></span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12">
                                <div class="apply-attribute">
                                    <input type="text" name="qualification"   placeholder="Qualification*" required>
                                    <span class="validation-error" id="Career-qualification"></span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12">
                                <div class="apply-attribute uploadbutton">
                                    <label class="uploadlabel">Upload CV*</label>
                                    <input type="file" id="upload-file" name="cv" placeholder="Upload CV*" required  accept=
                                    "application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
                                    text/plain, application/pdf, image/*">
                                    <span class="validation-error" id="Career-cv"></span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12">
                                <div class="apply-attribute uploadbutton">
                                    <label class="uploadlabel">Cover Letter</label>
                                    <input type="file" id="upload-file" name="cover_letter" placeholder="Cover Letter" >
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12">
                                <div class="apply-attribute">
                                    <input type="text" name="relevant_experience"   placeholder="Relevant Experience*" required>
                                    <span class="validation-error" id="Career-relevant_experience"></span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="apply-attribute">
                                    <textarea name="message" class="message-apply" id="message_box_id" placeholder="Message*" required></textarea>
                                    <span class="validation-error" id="Career-message"></span>
                                </div>
                            </div>
                        </div>
                        <div id="CareerSuccess" class="alert alert-success" role="alert" style="display: none;">
                        </div>
                        <button type="submit" class="button_1 disinfectants_btn"><span>Apply</span></a>
                    </div>
                </div>
            </form>
        </div>
        <div class="container">
            <div class="usp career-slider">
                <h2 class="hd-38">Gallery</h2>
                <ul class="toptabs">
                    <li class="active toptabli" id="tab_panel_Gallery1"> Imaec Medntek Team </li>
                    <li class="toptabli" id="tab_panel_Gallery2"> Special Events </li>
                </ul>
                <div class="panelgallery_wrap">
                    <div class="panelgallery showslide" id="panel_Gallery1">
                        <div class="inner-usp personslide usp-slider wrapper-slider">
                            @foreach(banners('imaec-medntek-team') as $bannerInfo)
                            <div class="col-lg-4 col-md-4 col-sm-12 personslide_inner">
                                <div class="usp-slide career-slide">
                                    <a href="{{asset('images/BannerImages/'.$bannerInfo['image'])}}" data-fancybox="gallery">
                                        <img src="{{asset('images/BannerImages/'.$bannerInfo['image'])}}" alt="usp-image">
                                    </a>
                                    <p class="hd-24 profilename">{{$bannerInfo['title']}}</p>
                                    <span class="title_designation">{{$bannerInfo['title_two']}}</span>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="slider-progress usp-progress">
                            <div class="progress" id="progressline"></div>
                        </div>
                    </div>
                    <div class="panelgallery" id="panel_Gallery2">
                        <div class="inner-usp personslide usp-slider wrapper-slider">
                            @foreach(banners('special-events') as $bannerInfo)
                            <div class="col-lg-4 col-md-4 col-sm-12 personslide_inner">
                                <div class="usp-slide career-slide">
                                    <a href="{{asset('images/BannerImages/'.$bannerInfo['image'])}}" data-fancybox="gallery">
                                        <img src="{{asset('images/BannerImages/'.$bannerInfo['image'])}}" alt="usp-image">
                                    </a>
                                    <p class="hd-24 profilename">Sales Team Traning</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="slider-progress usp-progress">
                            <div class="progress" id="progressline"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Main Container Section End-->
@stop
@section('javascript')
@parent
<script type="text/javascript">
    $(document).on('click','.applyNowDept',function(){
        var deptid = $(this).data('deptid');
        $("[name=position]").val(deptid);
        $('html, body').animate({ scrollTop:$("#CareerForm").offset().top}, 500);
    });

    $("#CareerForm").submit(function(e){
        e.preventDefault();
        $('.PleaseWaitDiv').show();
        var formdata = new FormData(this);
        $.ajax({
            url: "{{url('save-career')}}",
            type:'POST',
            type:'POST',
            data: formdata,
            processData: false,
            contentType: false,
            success: function(data) {
                $('.PleaseWaitDiv').hide();
                if(!data.status){
                    $.each(data.errors, function (i, error) {
                    $('#Career-'+i).show();
                    $('#Career-'+i).html(error);
                    $('#Career-'+i).addClass('error-triggered');
                    setTimeout(function () {
                        $('#Career-'+i).css({
                        'display': 'none'
                    });
                    $('#Career-'+i).removeClass('error-triggered');
                        }, 5000);
                    });
                    $('html,body').animate({
                    scrollTop: $('.error-triggered').first().stop().offset().top - 200
                    }, 1000);
                }else{
                    $('#CareerSuccess').html(data.message);
                    $('#CareerSuccess').show();
                    $("#CareerForm").trigger("reset");
                    setTimeout(function () {
                        $('#CareerSuccess').css({
                        'display': 'none'
                        });
                    }, 5000);
                }
            }
        });
    });
</script>
@stop