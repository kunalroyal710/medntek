<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Welcome</title>
        <link rel="stylesheet" href="{{asset('coming_soon/style.css')}}">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Merriweather+Sans:wght@300&display=swap" rel="stylesheet">
    </head>
    <style>
    </style>
    <body>
        <div class="rxWorld">
            <section class="rnOuter ">
                <section class="aoTable">
                    <div class="aoTableCell">
                        <div class="ribbon-as">
                            Inauguration of Website
                            <i></i>
                            <i></i>
                            <i></i>
                            <i></i>
                        </div>
                        <div class="text-curtain">
                            <h2>You are dearly invited to visit us on IMAEC Medntek website!</h2>
                        </div>
                        <div class="thelaunchlook">Welcome to Inauguration of IMAEC Medntek Website (<a href="https://www.imaecmedntek.com/index" target="_blank">https://www.imaecmedntek.com</a>)</div>
                    </div>
                </section>
                <div class="curtains">
                    <div class='rnInner' id="left-curtain">
                        <div class='rnUnit'></div>
                        <div class='rnUnit'></div>
                        <div class='rnUnit'></div>
                        <div class='rnUnit'></div>
                        <div class='rnUnit'></div>
                        
                    </div>
                    <div class='rnInner rnInnerright' id="right-curtain">
                        <div class='rnUnit'></div>
                        <div class='rnUnit'></div>
                        <div class='rnUnit'></div>
                        <div class='rnUnit'></div>
                        <div class='rnUnit'></div>
                        
                    </div>
                    <div class="badge"></div>
                    <div class="ribbon color" id="ribbon-left"></div>
                    <div class="ribbon ribbon2 color" id="ribbon-right"></div>
                </div>
            </section>
        </div>
        <script src="{{asset('coming_soon/jquery.min.js')}}"></script>
        <script>
            $(document).ready(function(){
                $(".badge").click(function(){
                    $(".ribbon").css({"transform-origin": "-120% top", "transform": "scaleX(0)"});
                    $(".ribbon2").css({"transform-origin": "120% top", "transform": "scaleX(0)"});
                    $(".badge").css({"top": "-60%", "transition": "all 2.8s"});
                    $(".rnInner").css({"transform-origin": "-120% top", "transform": "scaleX(0)"});
                    $(".rnInnerright").css({"transform-origin": "120% top", "transform": "scaleX(0)"});
                    $(".aoTableCell").css({"color": "#ffffff"});
                });
            });
        </script>
    </body>
</html>