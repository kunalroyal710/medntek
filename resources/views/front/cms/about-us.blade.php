@extends('layouts.frontLayout.front-layout')
@section('content')
<div class="main-section-container">
    <section class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text product-more">
                        <a href="{{url('/')}}">Home</a>
                        <span>About Us</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php echo $cmsInfo['description'];?>
</div>
@stop
@section('javascript')
@parent
    <script>
    jQuery('.our-points li').click(function(){
            jQuery('.our-points li').removeClass('active-pointer');
            jQuery('.vision-content').removeClass('active-text');
            jQuery('.vision-img').removeClass('img-active');
            var getdatatitle = jQuery(this).attr('data-title'); 
            jQuery(this).addClass('active-pointer');
            // jQuery(this).addClass('active-pointer'); 
            jQuery('.vision-content.'+getdatatitle).addClass('active-text')
            jQuery('.vision-img.'+getdatatitle).addClass('img-active')
        });
</script>
<script>
        jQuery('.product-list li').click(function(){
            jQuery('.product-list li').removeClass('active-category');
            jQuery('.product-items').removeClass('active-product-item');
            var getdatatitle = jQuery(this).attr('data-title'); 
            jQuery(this).addClass('active-category'); 
            jQuery('.product-items.'+getdatatitle).addClass('active-product-item')
        });
</script>
<script>
   $('.count1').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});


// Company Slider

$(document).ready(function(){
  var time = 3;
  var $bar1,
      $slick1,
      isPause1,
      tick1,
      percentTime1;
  
  $slick1 = $('.inner-usp');
  $slick1.slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    fade: false,
    autoplay: false,
    slide: '.section-usp',
    dots: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }
    ],
    dotsClass: 'custom_paging',
    customPaging: function (slider, i) {
        console.log(slider);
        return (i + 1) + '/' + '<div>'+ slider.slideCount +'</div>';
    } 
    
  });

  $bar1 = $('.usp-wrapper .slider-progress .progress');
  
  function startProgressbar() {
    resetProgressbar();
    percentTime1 = 0;
    isPause1 = false;
    tick1 = setInterval(interval, 17);
  }
  
  function interval() {
    if(isPause1 === false) {
      percentTime1 += 1 / (time+0.1);
      $bar1.css({
        width: percentTime1+"%"
      });
      if(percentTime1 >= 100)
        {
          $slick1.slick('slickNext');
          startProgressbar();
        }
    }
  }
    
  function resetProgressbar() {
    $bar1.css({
     width: 0+'%' 
    });
    clearTimeout(tick1);
  }
  
  startProgressbar();
  
});

</script>
@stop