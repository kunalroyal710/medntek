@extends('layouts.frontLayout.front-layout')
@section('content')
<style type="text/css">
.validation-error{
    color: red;
}
</style>
<!-- Main Container Section -->
<div class="main-section-container">
    <!-- Contact US -->
    <section class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text product-more">
                        <a href="{{url('/')}}">Home</a>
                        <span>Contact</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="map-medntek">
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3782.48845731583!2d73.76839171537975!3d18.552004673052988!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2bf69a915e4a7%3A0x12572e091e6bbe61!2sIMAEC%20MEDNTEK%20LIMITED!5e0!3m2!1sen!2sin!4v1625216297248!5m2!1sen!2sin"
            width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </section>
    <section class="address-wrapper right-blue-corner-shape bottom-purpl-corner-shape contact-us">
        <div class="container">
            <div class="row">
                <?php echo $cmsInfo['description'];?>
                <div class="col-lg-5 col-md-5 col-sm-12 form-main">
                    <form id="ContactusForm" action="javascript:;">@csrf
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <h3 class="gd-24">Enquiry Form</h3>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 enquiry-input">
                                <input type="text" id="fname" name="name" placeholder="Name *">
                                <span class="validation-error" id="Contact-name"></span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 enquiry-input">
                                <input type="email" id="e-id" name="email" placeholder="Email id *">
                                <span class="validation-error" id="Contact-email"></span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 enquiry-input">
                                <input type="text" id="e-id" name="contact_number" placeholder="Contact Number *">
                                <span class="validation-error" id="Contact-contact_number"></span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 enquiry-input">
                                <input type="text" id="lname" name="company_name" placeholder="Company Name *">
                                <span class="validation-error" id="Contact-company_name"></span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 enquiry-input">
                                <textarea id="subject" name="address" placeholder="Address *" style="height:80px"></textarea>
                                <span class="validation-error" id="Contact-address"></span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 enquiry-input">
                                <select name="interested_in">
                                    <option value="">I am intrested in</option>
                                    <option value="Dealership">Dealership</option>
                                    <option value="Purchasing">Purchasing</option>
                                    <option value="Others">Others</option>
                                </select>
                                <span class="validation-error" id="Contact-interested_in"></span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 enquiry-input">
                                <textarea id="subject" name="message" placeholder="Message" style="height:80px"></textarea>
                                <span class="validation-error" id="Contact-message"></span>
                            </div>
                            <div id="ContactSuccess" class="alert alert-success" role="alert" style="display: none;">
                                </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <button type="submit" class="button_1 disinfectants_btn"><span>Submit</span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>
</section>
<section class="connect-wrapper connect">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 connect-inner">
                
                <div class="connect-icon">
                    <img src="{{asset('assets/images/email.png')}}" alt="customer-care-icon">
                </div>
                <h3 class="hd-28">Customer Care</h3>
                <p><a href="mailto:info@imaecmedntek.com" target="_blank">info@imaecmedntek.com</a> </p>
                
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 connect-inner">
                <div class="connect-icon">
                    <img src="{{asset('assets/images/customer-care.png')}}" alt="customer-care-icon">
                </div>
                <h3 class="hd-28">Contact us</h3>
                <p><a href="tel:+91 8956234040" target="_blank">+91 8956234040</a></p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 connect-inner">
                <div class="connect-icon">
                    <img src="{{asset('assets/images/work-time.png')}}" alt="Working-hrs-icon">
                </div>
                <h3 class="hd-28">Work Time</h3>
                <p>Monday to Friday, 9:00am-06:00pm</p>
            </div>
        </div>
    </div>
</section>
<!-- Contact Us ends -->
</div>
<!-- Main Container Section End-->
@stop
@section('javascript')
@parent
<script type="text/javascript">
    $("#ContactusForm").submit(function(e){
        e.preventDefault();
        $('.PleaseWaitDiv').show();
        $.ajax({
            url: "{{url('save-contact')}}",
            type:'POST',
            data: $('#ContactusForm').serialize(),
            success: function(data) {
                $('.PleaseWaitDiv').hide();
                if(!data.status){
                    $.each(data.errors, function (i, error) {
                        $('#Contact-'+i).show();
                        $('#Contact-'+i).html(error);
                        $('#Contact-'+i).addClass('error-triggered');
                        setTimeout(function () {
                            $('#Contact-'+i).css({
                                'display': 'none'
                            });
                            $('#Contact-'+i).removeClass('error-triggered');
                        }, 5000);
                    });
                    $('html,body').animate({
                        scrollTop: $('.error-triggered').first().stop().offset().top - 200
                    }, 1000);
                }else{
                    $('#ContactSuccess').html(data.message);
                    $('#ContactSuccess').show();
                    $("#ContactusForm").trigger("reset");
                    setTimeout(function () {
                        $('#ContactSuccess').css({
                            'display': 'none'
                        });
                    }, 5000);
                }
            }
        });
    });
</script>
@stop