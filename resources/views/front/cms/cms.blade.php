@extends('layouts.frontLayout.front-layout')
@section('content')
@if($cmsInfo['type'] =="INFORMATION")
<div class="main-section-container">
@endif
<section class="breacrumb-section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="breadcrumb-text product-more">
					<a href="{{url('/')}}">Home</a>
					@if($cmsInfo['type'] =="CUSTOMER_CARE")
                    	<a href="{{url('/contact')}}">Customer Care</a>
                    @endif
					<span>{{$cmsInfo['title']}}</span>
				</div>
			</div>
		</div>
	</div>
</section>
<?php echo $cmsInfo['description'];?>
@if($cmsInfo['type'] =="INFORMATION")
</div>
@endif
@stop