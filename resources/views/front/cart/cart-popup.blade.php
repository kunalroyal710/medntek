<div class="cart-hover">
    <a id="closeCart" class="text-right" href="javascript:;">×</a>
    <div class="select-items">
        <table>
            @if(!empty($cartitems))
                <tbody>
                    @foreach($cartitems as $cartitem)
                        <tr>
                            <td class="si-pic">
                                @if(!empty($cartitem['product']['product_image']))
                                    <a href="{{url('/product/'.$cartitem['product']['id'].'/'.str_slug($cartitem['product_name']))}}">
                                        <img src="{{$cartitem['product']['product_image']['medium_image_url']}}" alt="" width="70">
                                    </a>
                                @endif
                            </td>
                            <td class="si-text" width="50%">
                                <div class="product-selected">
                                    <p>₹ {{$cartitem['price']}} x {{$cartitem['qty']}}</p>
                                    <h6>{{$cartitem['product']['product_name']}}</h6>
                                    <p>Size : {{$cartitem['size']}}</p>
                                </div>
                            </td>
                            <td class="si-close">
                                <a href="javascript:;" onclick="if (confirm('Are you sure?')){return true;}else{event.stopPropagation(); event.preventDefault();};" data-cartid="{{$cartitem['id']}}" class="removeCart">
                                    <img src="{{asset('assets/images/cross_close.svg')}}" alt="close">
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            @else
                <tbody>
                    <tr>
                       <td valign="center" width="100%" class="no-item-wrap" colspan="3"><span> No items found</span></td>
                    </tr>
                </tbody>
            @endif
        </table>
    </div>
    <div class="select-total">
        <span>Total:</span>
        <h5>₹ {{$cartSummary['grandtotal']}}</h5>
    </div>
    <div class="select-button">
        <a href="{{url('/cart')}}" class="button_1"><span>Go to cart</span></a>
        <a href="{{url('/')}}" class="button_1"><span>Continue Shopping</span></a>
    </div>
</div>