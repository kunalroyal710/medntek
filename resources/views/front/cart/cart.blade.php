@extends('layouts.frontLayout.front-layout')
@section('content')
<div class="main-section-container">
    <div class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text product-more"> <a href="{{url('/')}}">Home</a>
                        <span>Shopping Cart</span> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="spad right-blue-corner-shape bottom-purpl-corner-shape">
        <div class="container shopping-cart-inner">
            <!-- <div class="eligible-bar"> <img src="{{asset('assets/images/truck-purple.svg')}}" alt="truck-purple">
                <h4 class="hd-16">Eligible
                    for FREE SHIPPING!</h4>
            </div> -->
            <div id="AppendCartDetails">
                @include('front.cart.cart-details')
            </div>
        </div>
    </section>
</div>
@stop
@section('javascript')
@parent
<script type="text/javascript">
    $(document).on('change','[name=coupon]',function(){
        var coupon = $(this).val();
        $('#couponInput').val(coupon);
        $( "#ApplyCoupon" ).trigger( "submit" );
    })
    $(document).on('click','.updateQty',function(){
        $('.PleaseWaitDiv').show();
        var cartid = $(this).data('cartid');
        var qty = $('#Cart-'+cartid).val();
        $.ajax({
            data : {
                "_token" : "{{csrf_token()}}",
                "cartid" : cartid,
                "qty"    : qty
            },
            url : '/update-cart',
            type : 'post',
            success:function(resp){
                if(!resp.status){
                    alertclasss ="danger";
                }else{
                    $('#AppendCartDetails').html(resp.view);
                    $('#couponInput').val('');
                    alertclasss ="success";
                }
                $('.totalItems').html(resp.totalItems);
                $('.PleaseWaitDiv').hide();
                $('#couponInput').val('');
                if(resp.message !=""){
                    $('#PrintMessages').html('<div class="alert alert-'+alertclasss+' alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><span>'+resp.message+'</span></div>');
                    $('html, body').animate({
                        'scrollTop' : $("#PrintMessages").position().top
                    });
                }
            },  
            error:function(){
                //nothing to do
            }
        })
    });

    $(document).on('click','.removeCartProduct',function(){
        if (confirm('Are you sure?')) {
            $('.PleaseWaitDiv').show();
            var cartid = $(this).data('cart');
            var type = $(this).data('type');
            $.ajax({
                type : 'post',
                data : {
                    "cartid" : cartid,
                    'type'   : type,
                    "_token" : "{{csrf_token()}}"
                },
                url :'/remove-cart-item',
                success:function(resp){
                    if(!resp.status){
                        $('#AppendCartDetails').html(resp.view);
                        alertclasss ="danger";
                    }else{
                        $('#AppendCartDetails').html(resp.view);
                        alertclasss ="success";
                    }
                    $('#couponInput').val('');
                    $('.totalItems').html(resp.totalItems);
                    $('#PrintMessages').html('<div class="alert alert-'+alertclasss+' alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><span>'+resp.message+'</span></div>');
                    $('.PleaseWaitDiv').hide();
                    $('html, body').animate({
                        'scrollTop' : $("#PrintMessages").position().top
                    });
                },
                error:function(){
                    //nothing to do
                }
            })
        }
    })

    $(document).on('submit','#ApplyCoupon',function(e){
            e.preventDefault();
            $('.PleaseWaitDiv').show();
            var alertclasss ="";
            var formdata = $("#ApplyCoupon").serialize()+"&_token={{csrf_token()}}";
            $.ajax({
                type : 'post',
                data : formdata,
                url :'/apply-coupon',
                success:function(resp){
                    if(!resp.status){
                        if(resp.type=="login"){
                            $('[name=redirect]').val('cart');
                            $('#h-login').trigger('click');
                        }
                        $('#AppendCartDetails').html(resp.view);
                        $('#couponInput').val('');
                        alertclasss ="danger";
                    }else{
                        $('#AppendCartDetails').html(resp.view);
                        alertclasss ="success";
                    }
                    $('#PrintMessages').html('<div class="alert alert-'+alertclasss+' alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><span>'+resp.message+'</span></div>');
                    $('.PleaseWaitDiv').hide();
                    $('html, body').animate({
                        'scrollTop' : $("#PrintMessages").position().top
                    });
                },
                error:function(){
                    //nothing to do
                }
            })
        });
</script>
@stop