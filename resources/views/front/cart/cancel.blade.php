@extends('layouts.frontLayout.front-layout')
@section('content')
<div class="main-section-container">
	<div class="breacrumb-section">
	    <div class="container">
	        <div class="row">
	            <div class="col-lg-12">
	                <div class="breadcrumb-text product-more">
	                    <a href="{{url('/')}}">Home</a>
	                    <span>Cancel</span>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="container mt-4">
		<div class="row">
			<div class="col-12 text-center pt-5 pb-5">
	            <i class="fa fa-times-circle  fa-4x orange"></i>
	            <h3 class="mt-4 mb-4">Payment has not been made and payment is pending.</h3>
	        </div>
		</div>
	</div>
</div>
@stop