@if(!empty($cartitems))
<div class="row">
    <div class="col-lg-8">
        <div class="shopping-cart-head">
            <div id="PrintMessages"></div>
            @if(Session::has('flash_message_error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Error!</strong> {!! session('flash_message_error') !!}
            </div>
            @endif
            <h2 class="hd-20">My Cart <span>
            ({{$totalItems}} ITEMS)</span>
            </h2>
            <a href="{{url('/')}}" class="button_1 shop-cart-head-btn"><span>Continue Shopping</span></a>
        </div>
        <div class="cart-table">
            <table>
                <tbody>
                    @foreach($cartitems as $cartitem)
                    <?php $priceDetails = \App\Models\Cart::calProPricing($cartitem);
                    ?>
                    <tr class="shopping-cart-item-row">
                        <td class="cart-pic">
                            @if(!empty($cartitem['product']['product_image']))
                            <img src="{{$cartitem['product']['product_image']['medium_image_url']}}">
                            @endif
                        </td>
                        <td width="45%" class="cart-title">
                            <div class="row cart-title1">
                                <div class="shopping-cart-item-cols col-lg-7 col-md-7 col-sm-12">
                                    <h4 class="hd-20">
                                    <a href="{{url('/product/'.$cartitem['product']['id'].'/'.str_slug($cartitem['product']['product_name']))}}">{{$cartitem['product_name']}}</a>
                                    </h4>
                                    <p>Size: {{$cartitem['size']}}</p>
                                </div>
                                <div class="shopping-cart-item-cols col-lg-5 col-md-5 col-sm-12">
                                    <div class="shopping-cart-item-subhead price-cart">
                                        <h5 class="hd-24">₹ {{$priceDetails['prosubtotal']}}</h5>
                                        @if($priceDetails['discount'] >0)
                                        <h5 class="hd-24"><del>₹ {{$priceDetails['strikeprice'] * $cartitem['qty']}}</del></h5>
                                        @endif
                                    </div>
                                    @if($priceDetails['discount'] >0)
                                    <?php $saveAmt = $priceDetails['strikeprice'] * $cartitem['qty'] - $priceDetails['prosubtotal']; ?>
                                    <div class="shopping-cart-item-subhead quantity-cart">
                                        <h5 class="hd-13">You save
                                        ₹ {{$saveAmt}} <span class="discount-percentage">({{$priceDetails['discount']}}% OFF)</span>
                                        </h5>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="row cart-title2">
                                <div class="col-lg-5 col-md-5 col-sm-4 shopping-cart-item-cols1 moq-units">
                                    <div class="quantity buttons_added">
                                        <h5 class="hd-13">
                                        <span>Case Qty&nbsp;</span>
                                        <form class="plus-minus-input">
                                            <input type="button" value="+" class="plus"  data-quantity="plus" data-cartid="{{$cartitem['id']}}">
                                            <input type="number" step="1" min="1" max="" name="quantity" title="Qty" class="input-text qty text" size="4" id="Cart-{{$cartitem['id']}}" value="{{$cartitem['qty']}}">
                                            <input type="button" data-quantity="minus" data-field="quantity" value="-" class="minus" data-cartid="{{$cartitem['id']}}">
                                            
                                        </form>
                                        <a class="refresh-btn updateQty" data-cartid="{{$cartitem['id']}}">
                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                        </a>
                                        </h5>
                                    </div>
                                    @if($cartitem['moq'] >0)
                                    <div class="case-count">
                                        = {{$cartitem['qty'] * $cartitem['moq']}} units
                                    </div>
                                    @endif
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-8 shopping-cart-item-cols2">
                                    <ul class="shopping-quantity-list hd-13">
                                        <li class="shopping-quantity-list-inner">
                                            <a data-cart="{{$cartitem['id']}}" href="javascript:;" data-type="remove" class="removeCartProduct">REMOVE</a>
                                        </li>
                                        <li class="shopping-quantity-list-inner">
                                            <a class="removeCartProduct" href="javascript:;" data-type="savelater" data-cart="{{$cartitem['id']}}">SAVE FOR LATER</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="shopping-cart-head">
            <h2 class="hd-20">ORDER SUMMARY</h2>
        </div>
        <div class="addcart-col2">
            <div class="shopping-cart-discount">
                <form id="ApplyCoupon" method="post" autocomplete="off" action="javascript:;">
                    @csrf
                    <div class="row shopping-cart-discount_row">
                        <div class="shopping-cart-discount-icon col-md-2"> <img src="{{asset('assets/images/discount-icon.svg')}}" alt="discount-icon"> </div>
                        <div class="shopping-cart-discount-text col-md-10">
                            <h4 class="shopping-cart-discount-head hd-18">Enter your coupon code if you have one.</h4>
                        </div>
                    </div>
                    <div class="row shopping-cart-discount-input_row">
                        <div class="col-md-8 shopping-cart-discount-input"> <input type="text" name="code" class="discount-coupon-field" required placeholder="Enter Coupon Code" value="@if(Session::has('couponinfo') && Session::get('couponinfo')->code){{Session::get('couponinfo')->code}}@endif" id="couponInput"> </div>
                        <div class="col-md-2 shopping-cart-discount-btn">
                            <button type="submit" class="shopping-cart-btn">APPLY</button>
                        </div>
                    </div>
                </form>
                @if(Auth::check())
                <?php $coupons = \App\Models\CouponCode::availableCoupons($cartitems); ?>
                @if($coupons)
                <p class="mt-2 mb-2 orange">Available Coupons</p>
                <ul class="filter listeeTarget radio coupons-list unstyled">
                    @foreach($coupons as $coupon)
                    <?php
                        $datetime1 = new \DateTime(date('Y-m-d H:i:s'));
                        $datetime2 = new \DateTime($coupon['expiry_date'].' 23:59:00');
                        $interval = $datetime1->diff($datetime2);
                        if($interval->format('%d') != 0){
                            $couponTimeDiff = $interval->format('%d') ." Days ".$interval->format('%h')." hrs ".$interval->format('%i')." mints left"; 
                        }elseif($interval->format('%h') !=0){
                            $couponTimeDiff = $interval->format('%h')." hrs ".$interval->format('%i')." mints left";
                        }else{
                            $couponTimeDiff =$interval->format('%i')." mints left";
                        }
                    ?>
                    <li>
                        <input type="radio" value="{{$coupon['code']}}" name="coupon" id="{{$coupon['id']}}" @if(Session::has('couponinfo') && Session::get('couponinfo')->code == $coupon['code']) checked  @endif>
                        <label for="{{$coupon['id']}}">{{$coupon['code']}} <span>
                        @if($coupon['amount_type'] =="Rupees")
                        (Rs. {{formatAmt($coupon['amount'])}} off)
                        @else
                        ({{$coupon['amount']}}% off)
                        @endif
                        </span></label>
                        <br>
                        <span>({{$couponTimeDiff}})</span>
                    </li>
                    @endforeach
                </ul>
                @endif
                @endif
            </div>
            <!-- <div class="shopping-cart-discount shopping-cart-location-check">
                <div class="row shopping-cart-discount_row">
                    <div class="shopping-cart-discount-icon col-md-2"> <img src="{{asset('assets/images/location.svg')}}" alt="location"> </div>
                    <div class="shopping-cart-discount-text col-md-10">
                        <h4 class="shopping-cart-discount-head shopping-cart-discount-text-head hd-18">Check Delivery</h4>
                    </div>
                </div>
                <div class="row shopping-cart-discount-input_row">
                    <div class="col-md-8 shopping-cart-discount-input"> <input type="text" class="discount-coupon-field" placeholder="Enter Pincode"> </div>
                    <div class="col-md-2 shopping-cart-discount-btn"> <a href="" class="shopping-cart-btn">SUBMIT</a> </div>
                </div>
            </div> -->
            <div class="proceed-checkout">
                <ul>
                    <li class="subtotal">Subtotal<span>₹{{$cartSummary['subtotal']}}</span></li>
                    @if($cartSummary['discount'] >0)
                    <li class="subtotal">Coupon Discount (-) <span>₹{{$cartSummary['discount']}}</span></li>
                    @endif
                    <!-- <li class="subtotal">Estimated Tax<span>₹165</span></li> -->
                    <!-- <li class="subtotal">Shipping<span>FREE</span></li> -->
                    <li class="cart-total">Total
                        <span>₹ {{$cartSummary['grandtotal']}}</span>
                    </li>
                </ul>
            </div>
            <div class="proceed-btn-div"><a href="{{url('/checkout')}}" class="button_1"><span>Chekout</span></a></div>
        </div>
    </div>
</div>
@else
<div class="text-center">
    <p>No items in cart</p>
    <a style="float:none" href="{{url('/')}}" class="button_1 shop-cart-head-btn"><span>Continue Shopping</span></a>
</div>
@endif