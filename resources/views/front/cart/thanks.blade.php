@extends('layouts.frontLayout.front-layout')
@section('content')
<div class="main-section-container">
	<div class="breacrumb-section">
	    <div class="container">
	        <div class="row">
	            <div class="col-lg-12">
	                <div class="breadcrumb-text product-more">
	                    <a href="{{url('/')}}">Home</a>
	                    <span>Thanks</span>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="container mt-4">
		<div class="row">
			<div class="col-12 text-center pt-5 pb-5">
	            <i class="fa fa-check-square-o fa-4x orange"></i>
	            <h3 class="mt-4 mb-4">Thank you for your Purchase!</h3>
	            <span class="grey-bg order-number">Order No. #{{$thanksorderid}}</span>	
	            <a href="{{ url('/') }}" class="btn-style"> Continue Shopping</a>
	        </div>
		</div>
	</div>
</div>
@stop