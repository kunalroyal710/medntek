@extends('layouts.frontLayout.front-layout')
@section('content')
<!-- Main Container Section -->
<div class="main-section-container">
    <div class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text product-more">
                        <a href="{{url('/')}}">Home</a>
                        <a href="{{url('/cart')}}">Cart</a>
                        <span>Checkout</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-4 cart checkout">
        @if(Session::has('flash_message_error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Error!</strong> {!! session('flash_message_error') !!}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-7 col-12">
                <div class="row save-addresses" id="deliveryAddresses">
                    @include('front.address.delivery-address')    
                </div>
                @include('front.address.address-popup')
                <hr>
                @foreach($cartitems as $cartitem)
                	<?php $priceDetails = \App\Models\Cart::calProPricing($cartitem);?>
	                <div class="row cart-details checkout-cart-details checkout-cart-details-clone">
	                    <div class="col-lg-3 col-md-3 col-12 col-sm-12 cart-pic text-center">
	                    	@if(!empty($cartitem['product']['product_image']))
	                        <img src="{{$cartitem['product']['product_image']['medium_image_url']}}" class="img-fluid" alt="{{$cartitem['product_name']}}">
	                       	@endif
	                    </div>
	                    <div class="col-lg-9 col-md-9 col-12 col-sm-12 checkout-cart-details-clone-inner">
	                        <div class="row">
	                            <div class="col-md-5 col-12 product-inner-cart-details">
	                                <a href="{{url('/product/'.$cartitem['product']['id'].'/'.str_slug($cartitem['product']['product_name']))}}"><h5 class="cart-prod-name">{{$cartitem['product_name']}}</h5></a>
	                                <div class="product-inner-cart-details-content">
	                                    <p>SKU. : {{$cartitem['sku']}}</p>
	                                    <p>Size : {{$cartitem['size']}}</p>
	                                </div>
	                            </div>
	                            <div class="col-md-3 col-6 cart-qty-style">
	                                <div class="quantity-main">
	                                    {{$cartitem['qty']}}
	                                </div>
	                            </div>
	                            <div class="col-md-4 col-6 cart-prod-price">
	                                <p>
	                                    <span class="orange hd-24">₹ {{$priceDetails['prosubtotal']}}
	                                    </span>
	                                </p>
	                            </div>
	                        </div>
	                    </div>
	                </div>
                @endforeach
            </div>
            <div class="offset-lg-1 col-lg-4 col-12">
                <div class="ro">
                    <div class="col-sm-12 col-12 cart-total cart-total-chekout">
                        <h5 class="orange">Cart Total</h5>
                        <br>
                        <p>Subtotal <span>₹ {{$cartSummary['subtotal']}}</span></p>
                        <p>Discount (If Any) <span>₹ {{$cartSummary['discount']}}</span></p>
                        <div id="PrepaidDis"></div>
                        <hr>
                        <h5>Total <span id="GrandTotal">₹ {{$cartSummary['grandtotal']}}</span></h5>
                    </div>
                </div>
                <hr>
                <form id="OrderPlace" action="{{url('/place-order')}}" autocomplete="off" method="post">@csrf
                    <div class="row">
                        <div class="col-12">
                            <button type="button" class="coupon-code">
                                Select Payment Method <!-- <i class="fa fa-angle-down"></i> -->
                            </button>
                            <div id="PaymentMethod" class="collapse show mt-2">
                                <ul class="filter listeeTarget radio">
                                    @if(in_array(Auth::user()->email,array('isha@rtpltech.com','kunal100@yopmail.com','test100@yopmail.com')))
                                         <?php $paymentArray = array('ccavenue'=>'Debit\ Credit Card\ Net Banking\ Wallets','cod'=>'Cash On Delivery (COD)'); ?>
                                    @else
                                    <?php $paymentArray = array('ccavenue'=>'Debit\ Credit Card\ Net Banking\ Wallets'); ?>
                                    @endif
                                    @foreach($paymentArray as $pkey=> $payment)
                                        <li>
                                            <input type="radio" value="{{$pkey}}" name="paymentMode" id="payment-{{$pkey}}">
                                            <label for="payment-{{$pkey}}">{{$payment}}</label>
                                        </li>
                                    @endforeach
                                </ul>
                                <span class="alert alert-danger PaymentFailureFader" style="display: none; float: left;"></span>
                                <textarea class="input-style form-control" name="comments" rows="2" placeholder="Enter Comments if any..."></textarea>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-12 cart-btn">
                            <button id="PlaceOrder" type="submit" class="btn-style button_1"><span>Place Order</span></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Main Container Section End-->
<style>
/*Check-out page CSS*/
.cart-prod-price span {
	color: #783c90;
}	
.product-inner-cart-details p {
    color: #282828;
    font-size: 15px;
}
.checkout-cart-details #item-quantity {
    width:50px;
}
.product-inner-cart-details .cart-prod-name {
    margin-bottom: 20px !important;
}
button#PlaceOrder {
    width: 200px;   
    margin: 15px 0;
}
#PaymentMethod {
    color: #282828 !important;
}
.cart-total-chekout p {
    color: #282828 !important;
}

.cart-total-chekout span {
    color: #282828 !important;
}
a.address-edit.editAddress {
    margin-left: 8px;
}
.checkout-cart-details-clone {
    box-shadow: 4px -1px 14px #0000001a;
    margin: 0px 0px 30px 0px;
    padding: 0;
    border: none;
}
.checkout-cart-details-clone-inner {
    padding: 25px;
}

/*Check-out page CSS End*/

/*Responsive Check-out*/
@media(max-width: 768px) {
	.checkout-cart-details-clone-inner #item-quantity {
	    float: left;
	    margin: 0;
	}
	.product-inner-cart-details {
	    margin-bottom: 15px;
	}
}
@media(max-width: 575px) {
	ul.filter.listeeTarget.radio {
	    margin-bottom: 10px;
	}
}
/*Responsive Check-out End*/
</style>
@stop
@section('javascript')
@parent
<script type="text/javascript">
    $('#PlaceOrder').click(function(){
        if(!$("input:radio[name='paymentMode']").is(":checked")) {
            $(".PaymentFailureFader").text('Please select Payment Method'); 
            $(".PaymentFailureFader").slideDown();
            setTimeout(function(){
                $(".PaymentFailureFader").slideUp();      
            }, 3000);
            var off = $("#PaymentMethod").offset().top - 200;
            $('html, body').animate({
                    scrollTop: off
            }, 1000, 'linear', function() {});
            return false;
        }else{
            if($('input[name="paymentMode"]:checked').val() == 'cod'){
                $('.PleaseWaitDiv').show();
            }else{
                $('#PleaseWaitText').text('Redirecting to Payment Gateway. Please wait...');
                $('.PleaseWaitDiv').show();
            }
            $('#PlaceOrder').prop('disabled', true);
            $('#OrderPlace').submit();
        }
    });
</script>
@stop