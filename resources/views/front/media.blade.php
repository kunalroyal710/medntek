@extends('layouts.frontLayout.front-layout')
@section('content')
<!-- Main Container Section -->
<div class="main-section-container">
	<section class="breacrumb-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="breadcrumb-text product-more">
						<a href="{{url('/home')}}">Home</a>
						<a href="{{url('media')}}">Media</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php $typeArray = array('corporate-events','product-launch','cme','technical-seminars'); ?>
	@foreach(banners("media-banner") as $bannerInfo)
		<section class="innerheader-banner">
			<div class="banner-wrap">
				<img src="{{asset('images/BannerImages/'.$bannerInfo['image'])}}" alt="news-banner">
			</div>
		</section>
	@endforeach
</div>
@foreach($typeArray as $typeKey=> $typeInfo)
	@if(!empty(banners($typeInfo)))
		<section class="carrers  media-list @if($typeKey==3)  bottom-purpl-corner-shape @elseif($typeKey==0) right-blue-corner-shape @endif media-page-section product-launch-section">
			<div class="container">
				<div class="career-listing-heading">
					<h2 class="hd-38">{{ucwords(str_replace('-',' ', $typeInfo))}}</h2>
				</div>
				<div class="row">
					@foreach(banners($typeInfo) as $bannerInfo)
						<div class="col-lg-4 col-md-4 col-sm-6 carrers_inner_div">
							<div class="carrers_img_section">
								<img src="{{asset('images/BannerImages/'.$bannerInfo['image'])}}" alt="media-img1.jpg">
							</div>
							<h3 class="carrers_heading">{{$bannerInfo['title']}}</h3>
							<p class="carrers_date">{{date('d M Y',strtotime($bannerInfo['created_at']))}}</p>
						</div>
					@endforeach
				</div>
			</div>
		</section>
	@endif
@endforeach
<script>
	$(document).ready(function(){
$('button').click(function(e){
    var button_classes, value = +$('.counter').val();
    button_classes = $(e.currentTarget).prop('class');        
    if(button_classes.indexOf('up_count') !== -1){
        value = (value) + 1;            
    } else {
        value = (value) - 1;            
    }
    value = value < 0 ? 0 : value;
    $('.counter').val(value);
});  
$('.counter').click(function(){
    $(this).focus().select();
});
});
</script>


<!-- Main Container Section End-->
@stop