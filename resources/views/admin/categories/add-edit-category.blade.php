@extends('layouts.adminLayout.backendLayout')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Categories Management </h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ url('admin/dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ url('admin/categories') }}">Categories </a>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12 ">
                <div class="portlet blue-hoki box ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>{{ $title }}
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form id="CategoryForm" role="form" class="form-horizontal" method="post" action="javascript:;" enctype="multipart/form-data" autocomplete="off"> 
                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Category Name <span class="asteric">*</span></label>
                                    <div class="col-md-4">
                                        <input  type="text" placeholder="Category Name" name="name" style="color:gray" class="form-control" value="{{(!empty($categorydata['name']))?$categorydata['name']: '' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Category-name"></h4>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Select Category <span class="asteric">*</span></label>
                                    <div class="col-md-4">
                                        <?php $categories = categories(); ?>
                                        <select name="category" class="selectbox"> 
                                            <option value="">Select</option>
                                            <option value="ROOT" @if( empty($categorydata['parent_id'])) selected @endif>Main Category</option>
                                            <?php foreach ($categories as $key => $category) {?>
                                            <option value="{{$category['id']}}"@if(isset($categorydata['parent_id']) && $categorydata['parent_id'] ==$category['id']) selected @endif>&#9679;&nbsp;{{$category['name']}}</option>
                                            <?php if(!empty($category['subcategories'])){
                                                foreach ($category['subcategories'] as $key => $subcat) { ?>
                                                    <option value="{{$subcat['id']}}"@if(isset($categorydata['parent_id']) && $categorydata['parent_id'] ==$subcat['id']) selected @endif>&nbsp;&nbsp;&nbsp;&nbsp;&raquo; &nbsp;{{$subcat['name']}}</option>
                                                    <?php foreach ($subcat['subcategories'] as $key => $subsubcat) { ?>
                                                    <option value="{{$subcat['id']}}">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&raquo; &raquo; &nbsp;{{$subsubcat['name']}}</option>
                                                <?php } 
                                                }
                                            }
                                        } ?>
                                        </select>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Category-category"></h4>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Seo Unique <span class="asteric">*</span></label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="Seo Unique" name="seo_unique"  style="color:gray" class="form-control " value="{{(!empty($categorydata['seo_unique']))?$categorydata['seo_unique']: '' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Category-seo_unique"></h4>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">How to add Seo Unique ?</label>
                                    <div class="col-md-8">
                                        <table class="table table-striped table-bordered table-hover">
                                            <tr>
                                                <th>Category Name</th>
                                                <th>Seo Unique</th>
                                            </tr>
                                            <tr>
                                                <td>Skin Care</td>
                                                <td>skin-care</td>
                                          </tr>
                                        </table>
                                        <b>Note :- Don't enter any Special Characters in Seo Unique execpt (-)</b>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Category Image</label>
                                    <div class="col-md-5">
                                        <div data-provides="fileinput" class="fileinput fileinput-new">
                                            <div style="" class="fileinput-new thumbnail">
                                            <?php if(!empty($categorydata['image'])){
                                                $path = "images/CategoryImages/".$categorydata['image']; 
                                            if(file_exists($path)) { ?>
                                                <img style="height:100px;widtyh:100px;" class="img-responsive"  src="{{ asset('images/CategoryImages/'.$categorydata['image'])}}">
                                            <?php }else{?>
                                                    <img style="height:100px;widtyh:100px;" class="img-responsive"  src="{{ asset('images/default.png') }}">
                                            <?php } } else { ?>
                                            <img style="height:100px;widtyh:100px;" class="img-responsive"  src="{{ asset('images/default.png') }}">
                                            <?php } ?>
                                        </div>
                                            <div style="max-width: 200px; max-height: 150px; line-height: 10px;" class="fileinput-preview fileinput-exists thumbnail">
                                            </div>
                                            <div>
                                                <div class="form-group">
                                                    <span class="btn default btn-file">
                                                    <span class="fileinput-new">
                                                    Select Image </span>
                                                    <span class="fileinput-exists">
                                                    Select Image </span>
                                                    <input type="file" id="Image" name="image">
                                                    </span>
                                                    <a data-dismiss="fileinput" class="btn default fileinput-exists" href="#">
                                                    Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Category Normal Icon Image </label>
                                    <div class="col-md-4">
                                        <input type="file" name="normal_image" class="form-control" accept="image/*">
                                        @if(!empty($categorydata['normal_image']))
                                            <p><a target="_blank" href="{{$categorydata['category_normal_image_url']}}">View Image</a></p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Category Hover Icon Image </label>
                                    <div class="col-md-4">
                                        <input type="file" name="hover_image" class="form-control" accept="image/*">
                                        @if(!empty($categorydata['hover_image']))
                                            <p><a target="_blank" href="{{$categorydata['category_hover_image_url']}}">View Image</a></p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Category Discount </label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="Category Discount" name="category_discount"  style="color:gray" class="form-control " value="{{(!empty($categorydata['category_discount']))?$categorydata['category_discount']: '' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Category-category_discount"></h4>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Sort <span class="asteric">*</span></label>
                                    <div class="col-md-4">
                                        <input type="number" placeholder="Sort" name="sort"  style="color:gray" class="form-control " value="{{(!empty($categorydata['sort']))?$categorydata['sort']: '' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Category-sort"></h4>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Description :</label>
                                    <div class="col-md-4">
                                        <textarea placeholder="Description..." name="description"  style="color:gray" class="form-control">{{(!empty($categorydata['description']))?$categorydata['description']: '' }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Meta Title </label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="Meta title" name="meta_title"  style="color:gray" class="form-control " value="{{(!empty($categorydata['meta_title']))?$categorydata['meta_title']: '' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Category-meta_title"></h4>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Meta keywords :</label>
                                    <div class="col-md-4">
                                        <textarea placeholder="Meta keywords..." name="meta_keyword"  style="color:gray" class="form-control">{{(!empty($categorydata['meta_keyword']))?$categorydata['meta_keyword']: '' }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Meta Description :</label>
                                    <div class="col-md-4">
                                        <textarea placeholder="Meta Description..." name="meta_description"  style="color:gray" class="form-control">{{(!empty($categorydata['meta_description']))?$categorydata['meta_description']: '' }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Status <span class="asteric">*</span></label>
                                    <div class="col-md-4" style="margin-top:8px;">
                                        <?php $statusArr = array('1'=>'Active','0'=>'Inactive') ?>
                                        @foreach($statusArr as $skey=> $status)
                                            <label>
                                            <input type="radio" name="status" value="{{$skey}}" @if(!empty($categorydata) && $categorydata['status'] ==$skey ) checked @endif />&nbsp;{{ucwords($status)}}&nbsp;
                                        </label>
                                        @endforeach
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Category-status"></h4>
                                    </div>
                                </div>  
                                @if(!empty($categorydata['id']))
                                    <input type="hidden" name="categoryid" value="{{$categorydata['id']}}">
                                @else
                                    <input type="hidden" name="categoryid" value="">
                                @endif           
                            </div>
                            <div class="form-actions right1 text-center">
                                <button class="btn green" type="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#CategoryForm").submit(function(e){
        $('.loadingDiv').show();
        e.preventDefault();
         var formdata = new FormData(this);
        $.ajax({
            url: "{{url('/admin/save-category')}}",
            type:'POST',
            data: formdata,
            processData: false,
            contentType: false,
            success: function(data) {
                $('.loadingDiv').hide();
                if(!data.status){
                    $.each(data.errors, function (i, error) {
                        $('#Category-'+i).addClass('error-triggered');
                        $('#Category-'+i).attr('style', '');
                        $('#Category-'+i).html(error);
                        setTimeout(function () {
                            $('#Category-'+i).css({
                                'display': 'none'
                            });
                        $('#Category-'+i).removeClass('error-triggered');
                        }, 5000);
                    });
                    $('html,body').animate({
                        scrollTop: $('.error-triggered').first().stop().offset().top - 200
                    }, 1000);
                }else{
                    window.location.href= data.url;
                }
            }
        });
    });
</script>
@endsection