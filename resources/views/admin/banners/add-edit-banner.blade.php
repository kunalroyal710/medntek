@extends('layouts.adminLayout.backendLayout')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Banner Management </h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ url('admin/dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ action('Admin\BannerController@bannerImages') }}">Banner Images</a>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12 ">
                <div class="portlet blue-hoki box ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Upload Banner Images
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form id="addEditBannerImage" role="form" class="form-horizontal" method="post" @if(empty($bannerdata)) action="{{ url('admin/add-edit-banner-image') }}" @else action="{{ url('admin/add-edit-banner-image/'.$bannerdata['id']) }}" @endif enctype="multipart/form-data" autocomplete="off">
                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Banner Type :</label>
                                     <?php $typeArray = array('home1'=>'Home Page Section 1 (Top Slider)','home2'=>'Offer Banners','corporate-events'=>'Corporate Events Listing','product-launch'=>'Product Launch Listing','cme'=>'CME Listing','technical-seminars'=>'Technical Seminars Listing','media-banner'=>'Media Banner','imaec-medntek-team'=>'Imaec Medntek Team','special-events'=>'Special Events','careers-banner'=>'Careers Banner'); ?>
                                    <div class="col-md-4">
                                        <select name="type" class="selectbox" required>
                                            <option value="">Select</option>
                                            @foreach($typeArray as $key => $type)
                                                <option value="{{$key}}" @if(!empty($bannerdata) && $bannerdata['type'] == $key) selected @endif>{{$type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Select Banner Image : </label>
                                    <div class="col-md-4">
                                        <div data-provides="fileinput" class="fileinput fileinput-new">
                                            <div style="" class="fileinput-new thumbnail">
                                                @if(!empty($bannerdata['image']))
                                                    <?php $path = "images/BannerImages/".$bannerdata['image']; ?>
                                                    @if(file_exists($path))
                                                        <img style="height:100px;" class="img-responsive"  src="{{ asset('images/BannerImages/'.$bannerdata['image'])}}">
                                                    @endif
                                                @else
                                                    <img style="height:100px;" class="img-responsive"  src="{{ asset('images/default.png') }}">
                                                @endif
                                            </div>
                                            <div style="max-width: 200px; max-height: 150px; line-height: 10px;" class="fileinput-preview fileinput-exists thumbnail"></div>
                                            <div>
                                                <div class="form-group">
                                                    <span class="btn default btn-file">
                                                    <span class="fileinput-new">
                                                    Select Image </span>
                                                    <span class="fileinput-exists">
                                                    Select Image </span>
                                                    <input type="file" name="image">
                                                    </span>
                                                    <a data-dismiss="fileinput" class="btn default fileinput-exists" href="javascript:;">
                                                    Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Title :</label>
                                    <div class="col-md-4">
                                        <input type="text" min="1" step="1" placeholder="Enter Title" name="title"  style="color:gray" class="form-control" value="@if(!empty($bannerdata['title'])){{$bannerdata['title']}}@endif" />
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Description :</label>
                                    <div class="col-md-4">
                                        <textarea type="text" placeholder="Enter Description" name="description"  style="color:gray" class="form-control">@if(!empty($bannerdata['description'])) {{$bannerdata['description']}} @endif</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Sort :</label>
                                    <div class="col-md-4">
                                        <input type="number" min="1" step="1" placeholder="Enter Sort number" name="sort"  style="color:gray" class="form-control" value="@if(!empty($bannerdata['sort'])){{$bannerdata['sort']}}@endif" required />
                                        <p>Note: Heighest Sort Order Banner will come at Top</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Title 2 :</label>
                                    <div class="col-md-4">
                                        <input type="text"  placeholder="Enter Title 2" name="title_two"  style="color:gray" class="form-control" value="@if(!empty($bannerdata['title_two'])){{$bannerdata['title_two']}}@endif" />
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Link :</label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="E.g.- https://example.com" name="link"  style="color:gray" class="form-control" value="@if(!empty($bannerdata['link'])) {{$bannerdata['link']}} @endif"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Start Date :</label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="Start Date" name="start_date"  style="color:gray" class="form-control datePicker" value="@if(!empty($bannerdata['start_date'])){{$bannerdata['start_date']}}@endif" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">End Date :</label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="End Date" name="end_date"  style="color:gray" class="form-control datePicker" value="@if(!empty($bannerdata['end_date'])){{$bannerdata['end_date']}}@endif" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions right1 text-center">
                                <button id="check" class="btn green disable" type="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection