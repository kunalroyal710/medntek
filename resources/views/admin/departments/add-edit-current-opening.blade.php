@extends('layouts.adminLayout.backendLayout')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Current Openings Management </h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ url('admin/dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ url('admin/current-openings') }}">Current Openings </a>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12 ">
                <div class="portlet blue-hoki box ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>{{ $title }}
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form id="CurrentOpeningForm" role="form" class="form-horizontal" method="post" action="javascript:;" enctype="multipart/form-data" autocomplete="off"> 
                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Seelect Department <span class="asteric">*</span></label>
                                    <div class="col-md-4">
                                        <select class="form-control" name="department_id">
                                            <option value="">Please Select</option>
                                            @foreach($depts as $deptinfo)
                                                <option value="{{$deptinfo['id']}}" @if(!empty($currentopeningdata) && $currentopeningdata['department_id']==$deptinfo['id']) selected @endif>{{$deptinfo['department']}}</option>
                                            @endforeach
                                        </select>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="CurrentOpening-department_id"></h4>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Title <span class="asteric">*</span></label>
                                    <div class="col-md-4">
                                        <input  type="text" placeholder="Title" name="title" style="color:gray" class="form-control" value="{{(!empty($currentopeningdata['title']))?$currentopeningdata['title']: '' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="CurrentOpening-title"></h4>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Description <span class="asteric">*</span></label>
                                    <div class="col-md-8">
                                        <textarea class="form-control longSummernote" name="description">{{(!empty($currentopeningdata['description']))?$currentopeningdata['description']: '' }}</textarea>
                                    </div>
                                </div>        
                            </div>
                            <div class="form-actions right1 text-center">
                                <button class="btn green" type="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var currentopeningid =""; 
    <?php if(!empty($currentopeningdata['id'])){?>
        currentopeningid = "<?php echo $currentopeningdata['id']; ?>";
    <?php } ?>
    $("#CurrentOpeningForm").submit(function(e){
        $('.loadingDiv').show();
        e.preventDefault();
        var formdata = $("#CurrentOpeningForm").serialize()+"&currentopeningid="+currentopeningid;;
        $.ajax({
            url: '/admin/save-current-opening',
            type:'POST',
            data: formdata,
            success: function(data) {
                $('.loadingDiv').hide();
                if(!data.status){
                    $.each(data.errors, function (i, error) {
                        $('#CurrentOpening-'+i).addClass('error-triggered');
                        $('#CurrentOpening-'+i).attr('style', '');
                        $('#CurrentOpening-'+i).html(error);
                        setTimeout(function () {
                            $('#CurrentOpening-'+i).css({
                                'display': 'none'
                            });
                        $('#CurrentOpening-'+i).removeClass('error-triggered');
                        }, 5000);
                    });
                    $('html,body').animate({
                        scrollTop: $('.error-triggered').first().stop().offset().top - 200
                    }, 1000);
                }else{
                    window.location.href= data.url;
                }
            }
        });
    });
</script>
@endsection