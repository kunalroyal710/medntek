<?php use App\GiftOffer; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Order No. : {{$orderDetails['id']}}</title>
        <style type="text/css">
        /*Bar Code Font Declaration */
        @font-face {
        font-family: 'Basawa 3 of 9 MHR';
        font-weight: normal;
        font-style: normal;
        font-display: swap;
        }
        /*Bar Code Font Declaration */
        .Btnsdiv__Invoice{margin-bottom: 30px; margin-top: 30px; text-align: center;}
        .Btnsdiv__Invoice input{background-color: #121212; color: #fff; opacity: 0.75;text-decoration: none;outline: none; border-width: 1px; border-color: transparent; border-style: solid; display: inline-block; padding: 8px 12px; margin-bottom: 0; margin-right: 15px; margin-left: 0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; background-clip: padding-box; float: none; cursor: pointer;}
        .Btnsdiv__Invoice input:focus,
        .Btnsdiv__Invoice input:hover,
        .Btnsdiv__Invoice input:active{color: #fff; background-color: #121212; opacity: 1; text-decoration: none;outline: none; border-width: 1px; border-color: transparent; border-style: solid; cursor: pointer;}
        .fullWidth{width: 100%; float: left; display: inline-block; position: relative;}
        .InvoiceTable p{margin-bottom: 0; line-height: 1.257em;letter-spacing: 0; color: #121212;}
        .InvoiceTable p.barcode:not(:last-of-type){margin-bottom: 10px;}
        .InvoiceTable > tbody > tr > td:only-child{width: 100%;}
        .InvoiceTable p:not(:last-of-type){margin-bottom: 3px; margin-top: 0; display: inline-block; width: 100%; float: left;text-align: left;}
        .InvoiceTable h6{margin-bottom: 0;margin-top: 0;display: inline-block; width: 100%; float: left;text-align: left;font-family: 'Oswald', sans-serif; letter-spacing: 1px; font-weight: 600; font-size: 14px; line-height: 1.1428em;}
        .InvoiceTable h6 span{display: inline-block; float: none; font-family: "Roboto", sans-serif; font-weight: normal;}
        .InvoiceTable h1{margin-bottom: 10px;margin-top: 0;display: inline-block; width: 100%; float: left;text-align: left;font-family: 'Oswald', sans-serif; font-size: 23px; letter-spacing: 1px;font-weight: 600;}
        .InvoiceTable h1.para{margin-bottom: 0;margin-top: 0;display: inline-block; width: 100%; float: left;text-align: left;font-family: 'Roboto', sans-serif; font-size: 23px; letter-spacing: 1px;font-weight: 600;}
        .InvoiceTable h6:not(:last-of-type){ margin-bottom: 6px;}
        .InvoiceTable h6:only-of-type{margin-bottom: 6px !important;}
        html{font-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-size: 14px;}
        body{margin: 0; padding: 0; font-family: 'Roboto', 'Arial', 'Helvetica', sans-serif; font-size: 12px; font-weight: normal; font-style: normal; text-align: center;}
        .barcode {font-family: 'Basawa 3 of 9 MHR';font-size:48px;}
        .InvoiceTable h6{font-size: 14px; font-weight: bold;}
        h6.para{font-family: "Roboto", sans-serif; font-size: 12px;}
        table.InvoiceTable h6.para{margin-bottom: 0 !important;}
        *,*:after,*:before{box-sizing: border-box; -webkit-box-sizing: border-box; -moz-box-sizing: border-box;}
        table.InvoiceTable{float: none; width: 100%; border: 1px solid #ddd; table-layout: fixed; text-align: left; border-collapse: collapse;vertical-align: top;font-family: 'Roboto', 'Arial', 'Helvetica', sans-serif; font-size: 12px; }
        table.InvoiceTable > tbody > tr:not(:last-of-type){border-bottom: 1px solid #ddd;}
        table.InvoiceTable > tbody > tr > td:not(:last-of-type){}
        table.InvoiceTable td,
        table.InvoiceTable th{padding: 8px;vertical-align: top;}
        table:not(.InvoiceTable){table-layout: fixed; width: 100%; float: left;border-collapse: collapse; vertical-align: top;font-family: 'Roboto', 'Arial', 'Helvetica', sans-serif; font-size: 12px; }
        table td,
        table th{padding: 3px; vertical-align: top;}
        img{max-width: 100%;}
        @media print {
            @page{
                size: landscape;
            }
        }
        </style>
        <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    </head>
    <body>
        <div class="fullWidth Btnsdiv__Invoice" id="ButtonDiv">
            <input value="Print invoice" onClick="javascrip:document.getElementById('ButtonDiv').style.display='none'; window.print();" type="button">
             <!-- <input value="Close Window" onclick="window.close();" type="button"> -->
        </div>
        <div style="max-width: 100%; text-align: center; float: none; display: inline-block; width: 100%;">
            <table width="95%" class="InvoiceTable" border="0" cellspacing="0" cellpadding="0" style="text-align: left;">
                <tr>
                    
                    <td style="width: 75px; vertical-align: middle;"><img src="{{ asset('images/medntek.png') }}"></td>
                        <td>
                            <p>{{config('constants.project_name')}} Limited</p>
                            <p>{{config('constants.return_address')}}</p>
                            <!-- <p>{{config('constants.pincode')}} </p> -->
                        </td>
                        <td>
                            <p>Supply By :</p>
                            <p>Phone No. : {{config('constants.project_mobile')}}</p>
                            <p>Email: {{config('constants.project_email')}}</p>
                            <!-- <p><strong>GST : {{config('constants.gst')}}</strong></p> -->
                        </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <h6>DELIVER TO :</h6>
                        <p>
                            {{$orderDetails['name'] }}
                        </p>
                        <p>
                            {{$orderDetails['address_line_1'] }}
                        </p>
                        <p>
                            {{$orderDetails['city'] }},{{$orderDetails['state'] }}
                        </p>
                        <p>
                            {{$orderDetails['country']}} {{$orderDetails['postcode'] }}
                        </p>
                        
                        <p><b>Ph:</b> {{$orderDetails['mobile'] }}<br>
                        </p>
                    </td>
                    <td>
                        @if($orderDetails['payment_status'] =="cod")
                            <h6>COD Order</h6>
                        @else
                            <h6>Prepaid Order</h6>
                        @endif
                        <p><strong>Order No:</strong> {{$orderDetails['id']}}</p>
                        @if(!empty($orderDetails['invoice_no']))
                            <p><strong>Invoice No:</strong> {{$orderDetails['invoice_no']}}</p>
                        @endif
                        <p><strong>Order Date:</strong> {{date('d M Y h:ia',strtotime($orderDetails['created_at']))}}</p>
                        <p><strong>Pieces:</strong> {{$orderDetails['total_items']}}</p>
                    </td>
                </tr>
                <tr>
                    <!-- <td colspan="2">
                        @if($orderDetails['payment_status'] =="cod")
                            <h1>Amount To be Collected: Rs. {{formatAmt($orderDetails['grand_total'])}}</h1>
                        @else
                            <h1>Amount To be Collected: Rs 0</h1>
                        @endif
                    </td> -->
                </tr>
                <tr>
                    <!-- <td colspan="3">
                        <h1 class="para">Grand Total in Words: {{$numberWords}} Rupees Only</h1>
                    </td> -->
                </tr>
                <tr>
                    <td colspan="3">
                        <table cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr style="background-color:#ddd;">
                                    <td>
                                        <h6 class="para">Product Name </h6>
                                    <td >
                                        <h6 class="para">Product Size</h6>
                                    </td>
                                    <td>
                                        <h6 class="para">MRP</h6>
                                    </td>
                                    <td>
                                        <h6 class="para">Pro Discount</h6>
                                    </td>
                                    <td>
                                        <h6 class="para">Other Discounts</h6>
                                    </td>
                                    <td>
                                        <h6 class="para">Unit Price</h6>
                                    </td>
                                    <td>
                                        <h6 class="para">Qty</h6>
                                    </td>
                                    
                                    <td >
                                        <h6 class="para">Total</h6>
                                    </td>
                                    <td >
                                        <h6 class="para">CGST</h6>
                                    </td>
                                    <td >
                                        <h6 class="para">SGST</h6>
                                    </td>
                                    <td >
                                        <h6 class="para">IGST</h6>
                                    </td>
                                </tr>
                                @if($orderDetails['state']=="Maharashtra")
                                    <?php
                                        $mhgst = true;
                                    ?>
                                @else
                                    <?php
                                        $mhgst = false;
                                    ?>
                                @endif
                                <?php $finalcgst =0;  $finalsgst=0;$finaligst=0; $finalTotalPrice =0;?>
                                @foreach($orderDetails['order_products'] as $product)
                                    @if($product['product_gst'] ==0)
                                        <?php $product['product_gst'] = $product['productdetail']['product_gst']; ?>
                                    @endif
                                    <?php $gst = 100 + $product['product_gst']; 
                                        $unitPrice =  (($product['unit_price'] / $gst) * 100);
                                        $totalPrice = (($product['line_amount'] / $gst) * 100);
                                        $finalTotalPrice += $totalPrice;
                                    ?>
                                    @if($mhgst)
                                        <?php 
                                        $gstPBper = $product['product_gst'] /2;
                                        $gstAmt = (($product['line_amount'] - $totalPrice)/2);
                                        $cgst = round($gstAmt,2);
                                        $sgst = round($gstAmt,2);
                                        $igst = 0;
                                        $finalcgst += $cgst;
                                        $finalsgst += $sgst;
                                        $gstIGSTper =0;
                                        ?>
                                    @else
                                        <?php 
                                        $gstPBper =0;
                                        $gstIGSTper = $product['product_gst'];
                                        $cgst = 0;
                                        $sgst = 0;
                                        $igst = $product['line_amount'] - $totalPrice;
                                        $finaligst += round($igst,2);
                                        ?>
                                    @endif
                                    <tr style="border-bottom: 1px solid #ddd;">
                                        <td>
                                            <p>{{$product['product_name']}}</p>
                                        </td>
                                        <td>
                                            <p>{{$product['product_size']}}
                                                <br><br>
                                                <b>Case Count : {{$product['productdetail']['case_count']}}</b>
                                            </p>
                                        </td>
                                        <td>
                                            <p>Rs. {{$product['mrp']}}</p>
                                        </td>
                                        <td>
                                            <p>Rs. {{$product['mrp'] - $product['product_price'] }}</p>
                                        </td>
                                        <td>
                                            <p>Rs. {{round($product['line_discount'] + $product['line_prepaid_discount']) }}</p>
                                        </td>
                                        <td>
                                            <p>Rs. {{round($unitPrice,2)}}</p>
                                        </td>
                                        <td>
                                            <p>{{$product['product_qty']}}</p>
                                        </td>
                                        <td>
                                            <p>Rs. {{round($totalPrice,2)}}</p>
                                        </td>
                                        <td>
                                            <p>({{$gstPBper}}%) Rs. {{round($cgst,2)}} </p>
                                        </td>
                                        <td>
                                            <p>({{$gstPBper}}%) Rs. {{round($sgst,2)}} </p>
                                        </td>
                                        
                                        <td>
                                            <p>({{$gstIGSTper}}%) Rs. {{round($igst,2)}}</p>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr style="border-bottom: 1px solid #ddd;">
                                   
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>{{$orderDetails['total_items']}}</td>
                                    <td></td>
                                    <td>Rs. {{round($finalcgst,2)}}</td>
                                    <td>Rs. {{round($finalsgst,2)}}</td>
                                    <td>Rs. {{round($finaligst,2)}}</td>
                                </tr>
                                <tr style="border-bottom: 1px solid #ddd;">
                                    @if($mhgst)
                                    <td colspan="8"></td>
                                    <td>Total GST Amount:</td>
                                    <td>Rs. {{round($finalcgst+ $finalsgst+ $finaligst,2)}}</td>
                                    <td></td>
                                    @else
                                        <td colspan="9"></td>
                                        <td>Total GST Amount:</td>
                                        <td>Rs. {{round($finalcgst + $finalsgst+ $finaligst,2)}}</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td colspan="9">
                                        
                                    </td>
                                </tr>
                                <tr style="border-bottom: 1px solid #ddd;">
                                    <td colspan="9"></td>
                                    <td>Total:</td>
                                    <td>Rs. {{round($orderDetails['subtotal']- ($finalcgst + $finalsgst+ $finaligst),2)}}</td>
                                </tr>
                                <tr style="border-bottom: 1px solid #ddd;">
                                    <td colspan="9"></td>
                                    <td>Coupon Discount (-):</td>
                                    <td>Rs. {{$orderDetails['coupon_discount']}}</td>
                                </tr>
                                <!-- <tr style="border-bottom: 1px solid #ddd;">
                                    <td colspan="9"></td>
                                    <td>Prepaid Discount (-):</td>
                                    <td>Rs. {{$orderDetails['prepaid_discount']}}</td>
                                </tr> -->
                                <tr style="border-bottom: 1px solid #ddd;">
                                    <td colspan="9"></td>
                                    <td>Sub Total :</td>
                                    <td>Rs. {{round($orderDetails['subtotal'] - $orderDetails['coupon_discount'] - $orderDetails['prepaid_discount']- ($finalcgst + $finalsgst+ $finaligst),2)}}</td>
                                </tr>
                                <tr style="border-bottom: 1px solid #ddd;">
                                    <td colspan="9"></td>
                                    <td>Total GST (+):</td>
                                    <td>Rs. {{round($finalcgst + $finalsgst+ $finaligst,2)}}</td>
                                </tr>
                                @if(!empty($orderDetails['charges']))
                                    <tr style="border-bottom: 1px solid #ddd;">
                                        <td colspan="9"></td>
                                        <td>Shipping Charges :</td>
                                        <td>Rs. {{formatAmt($orderDetails['charges'])}}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <td colspan="9"></td>
                                    <td><b>Grand Total:</b></td>
                                    <td><b>Rs.  {{formatAmt($orderDetails['grand_total'])}}</b></td>
                                </tr>
                                <!-- <tr style="background-color:#F5F5F5;">
                                    <td colspan="5">
                                    </td>
                                    <td style="text-align: left; vertical-align: middle;">
                                        <p style="margin-top: 0">
                                            Order Status:
                                        </p>
                                    </td>
                                    <td style="text-align: left; vertical-align: middle;">
                                        <p style="margin-top: 0">
                                        <strong>{{$orderDetails['order_status']}}</strong>
                                        </p>
                                    </td>
                                </tr> -->
                            </tbody>
                        </table>
                        @if(!empty($orderDetails['invoice_no_str']))
                            <br>
                            <p style="text-align: right;"><b>IMAEC/SL-EC/FR/001-00</b></p>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="3" >
                        <p style="text-align: center;">
                            <b>This is a computer generated invoice and does not require signature</b>
                        </p>
                        <p style="text-align: center;">
                            <b>Return Address</b> {{config('constants.return_address')}}
                        </p>
                        <p style="text-align: center;">If you receive an open or a tampered package at the time of delivery, please do not accept it.</p>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>