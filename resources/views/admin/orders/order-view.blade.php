@extends('layouts.adminLayout.backendLayout')
@section('content')
<?php use App\Models\Order; ?>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Order's Management</h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{!! url('admin/dashboard') !!}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{!! url('admin/orders') !!}">Orders</a>
            </li>
        </ul>
        @if(Session::has('flash_message_error'))
        <div role="alert" class="alert alert-danger alert-dismissible fade in"> <button aria-label="Close" data-dismiss="alert" style="text-indent: 0;" class="close" type="button"><span aria-hidden="true"></span></button> <strong>Error!</strong> {!! session('flash_message_error') !!} </div>
        @endif
        @if(Session::has('flash_message_success'))
        <div role="alert" class="alert alert-success alert-dismissible fade in"> <button aria-label="Close" data-dismiss="alert" style="text-indent: 0;" class="close" type="button"><span aria-hidden="true"></span></button> <strong>Success!</strong> {!! session('flash_message_success') !!} </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-basket font-green-sharp"></i>
                            <span class="caption-subject font-green-sharp bold uppercase">
                            Order No. : {{$orderDetails['id']}} </span>
                            <span class="caption-helper">{{ date('d F Y h:ia',strtotime($orderDetails['created_at'])) }}</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet blue-hoki box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>Order Details
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        @if(!empty($orderDetails['invoice_no']))
                                            <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    Invoice No :
                                                </div>
                                                <div class="col-md-7 value">
                                                    {{$orderDetails['invoice_no']}}
                                                </div>
                                            </div>
                                        @endif
                                            <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    Number:
                                                </div>
                                                <div class="col-md-7 value">
                                                    <span class="label label-primary">IMAEC/SL-EC/FR/001-00</span>
                                                </div>
                                            </div>
                                            <div class="row static-info">
                                                <div class="col-md-5 name">
                                                    AWB No:
                                                </div>
                                                <div class="col-md-7 value">
                                                    <a target="_blank" href="https://acplcargo.com/test.php?serch={{$orderDetails['awb_number']}}">
                                                        <span class="label label-danger">{{$orderDetails['awb_number']}}</span>
                                                    </a>
                                                </div>
                                            </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Order Date & Time:
                                            </div>
                                            <div class="col-md-7 value">
                                                {{ date('d F Y h:ia',strtotime($orderDetails['created_at'])) }}
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Order Status:
                                            </div>
                                            <div class="col-md-7 value">
                                                <span class="label label-success">
                                                {{$orderDetails['order_status']}} </span>
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Payment Method:
                                            </div>
                                            <div class="col-md-7 value">
                                                {{$orderDetails['payment_method']}}
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Grand Total:
                                            </div>
                                            <div class="col-md-7 value">
                                               {{$orderDetails['currency_symbol']}} {{formatAmt($orderDetails['grand_total'])}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet blue-hoki box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>User Information
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Name:
                                            </div>
                                            <div class="col-md-7 value">
                                                {{$orderDetails['getuser']['name']}}
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Email:
                                            </div>
                                            <div class="col-md-7 value">
                                                {{$orderDetails['getuser']['email']}}
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                State:
                                            </div>
                                            <div class="col-md-7 value">
                                                {{$orderDetails['getuser']['state']}}
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                City:
                                            </div>
                                            <div class="col-md-7 value">
                                                {{$orderDetails['getuser']['city']}}
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                Mobile Number:
                                            </div>
                                            <div class="col-md-7 value">
                                                {{$orderDetails['getuser']['mobile']}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="portlet blue-hoki box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-cogs"></i>Billing Address
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="row static-info">
                                                <div class="col-md-12 value">
                                                    {{$orderDetails['getuser']['name']}}<br>
                                                    #{{$orderDetails['getuser']['address_line_1']}}<br>
                                                    {{$orderDetails['getuser']['address_line_2']}}<br>
                                                    {{$orderDetails['getuser']['state']}}<br>
                                                    {{$orderDetails['getuser']['city']}}<br>
                                                    {{$orderDetails['getuser']['postcode']}}<br>
                                                    {{$orderDetails['getuser']['mobile']}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="portlet blue-hoki box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-cogs"></i>Shipping Address
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="row static-info">
                                                <div class="col-md-12 value">
                                                    {{$orderDetails['name']}}<br>
                                                    #{{$orderDetails['address_line_1']}}<br>
                                                    {{$orderDetails['address_line_2']}}<br>
                                                    {{$orderDetails['state']}}<br>
                                                    {{$orderDetails['postcode']}}<br>
                                                    {{$orderDetails['city']}}<br>
                                                    {{$orderDetails['mobile']}}
                                                    <br>
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ShippingAddressModal">Update Address</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                    <div class="portlet blue-hoki box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-cogs"></i>Update Order Status
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <form method="post" class="form-horizontal" role="form" autocomplete="off" action="{{url('admin/update-order-status/'.$orderDetails['id'])}}">@csrf
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Select Status</label>
                                                        <div class="col-md-9">
                                                            <select id="OrderStatus" name="status" class="form-control input-sm" required>
                                                                <option value="">Please Select</option>
                                                                @foreach($getorderstatus as $ostatus)
                                                                <option value="{{$ostatus['name']}}">{{$ostatus['name']}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                       <div class="form-group" id="AwbNo" style="display: none;">
                                                        <label class="col-md-3 control-label">AWB No.</label>
                                                        <div class="col-md-9">
                                                            <input type="text" name="awb_number" placeholder="AWB Number" class="form-control" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Subject</label>
                                                        <div class="col-md-9">
                                                            <input type="text" placeholder="Subject" name="subject" class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Comments</label>
                                                        <div class="col-md-9">
                                                            <textarea placeholder="Enter Comments here..." name="comments" class="form-control" rows="8" cols="4"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions text-center">
                                                    <button type="submit" class="btn green">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet blue-hoki box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>Order History
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <div class="col-md-12 value">
                                                @foreach($orderDetails['histories'] as $history)
                                                <div class="note note-success">
                                                    <h4>
                                                        <span class="label @if($history['order_status'] =="Cancelled" || $history['order_status'] =="Pending")  label-danger @else label-success @endif">
                                                        {{$history['order_status']}} </span>
                                                    </h4>
                                                    @if(!empty($history['awb_number']))
                                                        <h6><b>AWB Number:- &nbsp;
                                                        {{$history['awb_number']}}</b>
                                                        </h6>
                                                        <h6><b>Invoice No:- &nbsp;
                                                        <span class="label label-success">{{$history['invoice_no']}} </span></b>
                                                        </h6>
                                                    @endif
                                                    <small>
                                                        {!!$history['comments']!!}
                                                        <br>
                                                        <span style="text-align: right; float: right;">
                                                        Updated at {{date('F d, Y',strtotime($history['created_at']))}}
                                                        </span>
                                                    </small>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="portlet blue-hoki box">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-cogs"></i>Ordered Products
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Product Name
                                                            </th>
                                                            <th>
                                                                Product Size
                                                            </th>
                                                            <th>
                                                                SKU
                                                            </th>
                                                            <th>
                                                                MRP
                                                            </th>
                                                            <th>
                                                                Discount
                                                            </th>
                                                            <th>
                                                                Price(Per Unit)
                                                            </th>
                                                            <th>
                                                                Quantity
                                                            </th>
                                                            <th>
                                                                Sub Total
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $priceArr = array(); ?>
                                                        @foreach($orderDetails['order_products'] as $key => $product)
                                                        <?php $priceArr[] = $product['subtotal'] ?>
                                                        <tr>
                                                            <td>
                                                               <a target="_blank" href="{{url('/product/'.$product['product_id'].'/'.str_slug($product['product_name']))}}">{{$product['product_name']}}</a>
                                                            </td>
                                                            <td>{{$product['product_size']}}
                                                            </td>
                                                            <td>{{$product['product_sku']}}</td>
                                                            <td>
                                                                {{$orderDetails['currency_symbol']}} {{formatAmt($product['mrp'])}}
                                                            </td>
                                                            <td>
                                                                {{$product['discount']}} %
                                                            </td>
                                                            <td>
                                                                {{$orderDetails['currency_symbol']}} {{formatAmt($product['product_price'])}}
                                                            </td>
                                                            <td>
                                                                {{$product['product_qty']}}
                                                            </td>
                                                            <td>
                                                                {{$orderDetails['currency_symbol']}} {{formatAmt($product['subtotal']) }}
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-md-6">
                                @if(!empty($orderDetails['comments']))
                                <div class="well">
                                    <div class="row static-info align-reverse">
                                        <div class="col-md-3 name">
                                            Comments:
                                        </div>
                                        {{$orderDetails['comments']}}
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                                <div class="well">
                                    @if(!empty($orderDetails['coupon_code']))
                                    <div class="row static-info align-reverse">
                                        <div class="col-md-8 name">
                                            Coupon Code:
                                        </div>
                                        <div class="col-md-3 value">
                                            <span class="label label-success">
                                            {{$orderDetails['coupon_code']}}</span>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="row static-info align-reverse">
                                        <div class="col-md-8 name">
                                            Sub Total:
                                        </div>
                                        <div class="col-md-3 value">
                                            {{$orderDetails['currency_symbol']}} {{formatAmt($orderDetails['subtotal'])}}
                                        </div>
                                    </div>
                                    @if($orderDetails['coupon_discount'] >0)
                                    <div class="row static-info align-reverse">
                                        <div class="col-md-8 name">
                                            Coupon Discount:
                                        </div>
                                        <div class="col-md-3 value">
                                            - {{$orderDetails['currency_symbol']}} {{formatAmt($orderDetails['coupon_discount'])}}
                                        </div>
                                    </div>
                                    @endif
                                    @if($orderDetails['prepaid_discount'] >0)
                                        <div class="row static-info align-reverse">
                                            <div class="col-md-8 name">
                                                Prepaid Discount:
                                            </div>
                                            <div class="col-md-3 value">
                                                - {{$orderDetails['currency_symbol']}} {{formatAmt($orderDetails['prepaid_discount'])}}
                                            </div>
                                        </div>
                                    @endif
                                    @if($orderDetails['cod_charges'] >0)
                                        <div class="row static-info align-reverse">
                                            <div class="col-md-8 name">
                                                COD Charges:
                                            </div>
                                            <div class="col-md-3 value">
                                                + {{$orderDetails['currency_symbol']}} {{formatAmt($orderDetails['cod_charges'])}}
                                            </div>
                                        </div>
                                    @endif
                                    @if($orderDetails['shipping_charges'] >0)
                                        <div class="row static-info align-reverse">
                                            <div class="col-md-8 name">
                                                Shipping Charges:
                                            </div>
                                            <div class="col-md-3 value">
                                                + {{$orderDetails['currency_symbol']}} {{formatAmt($orderDetails['shipping_charges'])}}
                                            </div>
                                        </div>
                                    @endif
                                    <div class="row static-info align-reverse">
                                        <div class="col-md-8 name">
                                            Grand Total:
                                        </div>
                                        <div class="col-md-3 value">
                                            {{$orderDetails['currency_symbol']}} {{formatAmt($orderDetails['grand_total'])}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ShippingAddressModal" tabindex="-1" role="dialog" aria-labelledby="ShippingAddressLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title" id="ShippingAddressLabel">Update Shipping Address</h5>
            </div>
            <form action="{{url('admin/update-shipping-address/'.$orderDetails['id'])}}" method="post" autocomplete="off">@csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label  class="col-form-label">Name:</label>
                        <input type="text" name="name" class="form-control" value="{{$orderDetails['name']}}" required>
                    </div>
                    <div class="form-group">
                        <label  class="col-form-label">Mobile:</label>
                        <input type="number" name="mobile" class="form-control" value="{{$orderDetails['mobile']}}" required>
                    </div>
                    <div class="form-group">
                        <label  class="col-form-label">State:</label>
                        <input type="text" name="state" class="form-control" value="{{$orderDetails['state']}}" required>
                    </div>
                    <div class="form-group">
                        <label  class="col-form-label">City:</label>
                        <input type="text" name="city" class="form-control" value="{{$orderDetails['city']}}" required>
                    </div>
                    <div class="form-group">
                        <label  class="col-form-label">Postcode:</label>
                        <input type="text" name="postcode" class="form-control" value="{{$orderDetails['postcode']}}" required>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Address Line 1:</label>
                        <textarea class="form-control" name="address_line_1" required>{{$orderDetails['address_line_1']}}</textarea>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Address Line 2:</label>
                        <textarea class="form-control" name="address_line_2">{{$orderDetails['address_line_2']}}</textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- View User Details Modal Starts-->
<div class="modal fade" id="MeasurementDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title" id="exampleModalLabel">View Measurement Details</h5>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped" id="appendMeasurement">
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('change','#OrderStatus',function(){
            var value = $(this).val();
            if(value =="Successful"){
                $('[name=comments]').val('');
            }else{
                $('[name=comments]').val('');
                $('#AwbNo').hide();
                $("[name=awb_number]").prop('required',false);
                if(value=="Shipped"){
                    $('#AwbNo').show();
                    $("[name=awb_number]").prop('required',true);
                    /*$('[name=comments]').val('Your order tracking number is ');*/
                }
                $('#InvoiceNo').hide();
                $('[name=invoice_no]').val('');
                $("[name=invoice_no]").prop('required',false);
            }
        })
    })
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('form').on('submit', function() {
            $('.loadingDiv').show();
        });
    })
</script>
<style type="text/css">
    /* Track Order Style Starts Here */

/*Form Wizard*/
.bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
.bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
.bs-wizard > .bs-wizard-step + .bs-wizard-step {}
.bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #595959; font-size: 14px; margin-bottom: 5px;}
.bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px; text-align: center;}
.bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 20px; height: 20px; display: block; background: #9797c0; top: 45px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;} 
.bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 10px; height: 10px; background: #7e7ebd; border-radius: 50px; position: absolute; top: 5px; left: 5px; } 
.bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 5px; box-shadow: none; margin: 18px 0;}
.bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none; background: #9797c0;}
.bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
.bs-wizard > .bs-wizard-step.active > .progress > .progress-bar {width:50%;}
.bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
.bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
.bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
.bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
.bs-wizard > .bs-wizard-step:first-child  > .progress {left: 50%; width: 50%;}
.bs-wizard > .bs-wizard-step:last-child  > .progress {width: 50%;}
.bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }
.bs-wizard .active {
background:none;
border:0;
}

/* Track Order Style Ends Here */
</style>
@stop