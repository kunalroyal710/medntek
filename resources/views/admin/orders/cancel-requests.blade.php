@extends('layouts.adminLayout.backendLayout')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Return's Management</h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{!! url('admin/dashboard') !!}">Dashboard</a>
            </li>
        </ul>
         @if(Session::has('flash_message_error'))
            <div role="alert" class="alert alert-danger alert-dismissible fade in"> <button aria-label="Close" data-dismiss="alert" style="text-indent: 0;" class="close" type="button"><span aria-hidden="true">×</span></button> <strong>Error!</strong> {!! session('flash_message_error') !!} </div>
        @endif
        @if(Session::has('flash_message_success'))
            <div role="alert" class="alert alert-success alert-dismissible fade in"> <button aria-label="Close" data-dismiss="alert" style="text-indent: 0;" class="close" type="button"><span aria-hidden="true">×</span></button> <strong>Success!</strong> {!! session('flash_message_success') !!} </div>
        @endif
        <div id="statusMessage" style="display: none;" role="alert" class="alert alert-success alert-dismissible fade in"><strong>Success!</strong> Status Updated Successfully.</div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-green-sharp bold uppercase">Cancel Requests</span>
                            <span class="caption-helper">manage records...</span>
                        </div>
                        <div class="actions">
                            
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-container">
                            <table class="table table-striped table-bordered table-hover" id="datatable_ajax">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="5%">
                                            Order Id
                                        </th>
                                        <th width="10%">
                                            Name
                                        </th>
                                        <th >
                                            Product Name
                                        </th>
                                        <th>
                                            Size
                                        </th>
                                        <th>
                                            Price
                                        </th>
                                        <th>
                                            Reason
                                        </th>
                                        <th>
                                            Image
                                        </th>
                                        <th >
                                            Created Date
                                        </th>
                                        <th >
                                            Comments
                                        </th>
                                        <th>
                                            Actions
                                        </th>
                                    </tr>
                                    <tr role="row" class="filter">
                                        <td><input type="text" autocomplete="off" class="form-control form-filter input-sm" name="orderid" placeholder="Order Id"></td>
                                        <td>
                                            <input type="text" autocomplete="off" class="form-control form-filter input-sm" name="name" placeholder="Name">
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        
                                        <td>
                                            <label for="recipient-name" class="form-control-label">From:</label>
                                            <div class="input-group input-append date datePicker">
                                                <input type="text" class="form-control date-reset form-filter resetPicker" name="from_date" />
                                                <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">To:</label>
                                                <div class="input-group input-append date datePicker">
                                                    <input type="text" class="form-control date-reset form-filter resetPicker" name="to_date" />
                                                    <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <div class="margin-bottom-5">
                                                <button class="btn btn-sm yellow filter-submit margin-bottom"><i title="Search" class="fa fa-search"></i></button>
                                                <button class="btn btn-sm red filter-cancel"><i title="Reset" class="fa fa-refresh"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop





