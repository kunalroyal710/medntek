@extends('layouts.adminLayout.backendLayout')
@section('content')
<style type="text/css">
    .red{
        color: red !important;
    }
</style>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Products Management </h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ url('admin/dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ action('Admin\ProductController@products') }}">Products </a>
            </li>
        </ul>
        @if(Session::has('flash_message_error'))
            <div role="alert" class="alert alert-danger alert-dismissible fade in"> <button aria-label="Close" data-dismiss="alert" style="text-indent: 0;" class="close" type="button"><span aria-hidden="true">×</span></button> <strong>Error!</strong> {!! session('flash_message_error') !!} </div>
        @endif
        @if(Session::has('flash_message_success'))
            <div role="alert" class="alert alert-success alert-dismissible fade in"> <button aria-label="Close" data-dismiss="alert" style="text-indent: 0;" class="close" type="button"><span aria-hidden="true">×</span></button> <strong>Success!</strong> {!! session('flash_message_success') !!} </div>
        @endif
        <div class="row">
            <div class="col-md-12 ">
                <div class="portlet blue-hoki box ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>{{ $title }}
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form id="ProductForm" role="form" class="form-horizontal" method="post" action="javascript:;" enctype="multipart/form-data"> 
                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}"  autocomplete="off" />
                            <div class="form-body">
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Product Name:<span class="red">*</span></label>
                                    <div class="col-md-6">
                                        <input autocomplete="off" type="text" placeholder="Name" name="product_name" style="color:gray" class="form-control" value="{{(!empty($productdata['product_name']))?$productdata['product_name']: '' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Product-product_name"></h4>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Product Code:<span class="red">*</span></label>
                                    <div class="col-md-6">
                                        <input autocomplete="off" type="text" placeholder="Product Code"  name="product_code" style="color:gray" class="form-control" value="{{(!empty($productdata['product_code']))?$productdata['product_code']: '' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Product-product_code"></h4>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <?php $brands = getbrands(); ?>
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Select Brand:<span class="red">*</span></label>
                                    <div class="col-md-6">
                                        <select class="form-control" name="brand_id">
                                            <option value="">Please Select</option>
                                            @foreach($brands as $brand)
                                                <option value="{{$brand['id']}}" @if(isset($productdata['brand_id']) && $productdata['brand_id'] ==$brand['id']) selected @endif>{{$brand['name']}}</option>
                                            @endforeach
                                        </select>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Product-brand_id"></h4>
                                    </div>
                                </div>
                                <?php $getCategories = categories(); ?>
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Select Category: <span class="red">*</span></label>
                                    <div class="col-md-6">
                                        <select name="category_id" class="selectbox"> 
                                            <option value="">Select</option>
                                            <?php foreach ($getCategories as $key => $category) {?>
                                            <option value="{{$category['id']}}"@if(isset($productdata['category_id']) && $productdata['category_id'] ==$category['id']) selected @endif>&#9679;&nbsp;{{$category['name']}}</option>
                                            <?php if(!empty($category['subcategories'])){
                                                foreach ($category['subcategories'] as $key => $subcat) { ?>
                                                    <option value="{{$subcat['id']}}" @if(isset($productdata['category_id']) && $productdata['category_id'] ==$subcat['id']) selected @endif>&nbsp;&nbsp;&nbsp;&nbsp;&raquo; &nbsp;{{$subcat['name']}}</option>
                                                    <?php foreach ($subcat['subcategories'] as $key => $subsubcat) { ?>
                                                    <option value="{{$subsubcat['id']}}" @if(isset($productdata['category_id']) && $productdata['category_id'] ==$subsubcat['id']) selected @endif>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&raquo; &raquo; &nbsp;{{$subsubcat['name']}}</option>
                                                <?php } 
                                                 }
                                                }
                                             } ?>
                                        </select>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Product-category_id"></h4>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Show in Other Categories: <span class="red">*</span></label>
                                    <div class="col-md-6">
                                        <select name="cats[]" class="selectbox MultipleSelect" multiple size="6" style="height: 20%;">
                                            <?php foreach ($getCategories as $key => $category) {?>
                                            <option value="{{$category['id']}}" @if(in_array($category['id'],$productCats)) selected @endif>&#9679;&nbsp;{{$category['name']}}</option>
                                            <?php if(!empty($category['subcategories'])){
                                                foreach ($category['subcategories'] as $key => $subcat) { ?>
                                                    <option value="{{$subcat['id']}}" @if(in_array($subcat['id'],$productCats)) selected @endif>&nbsp;&nbsp;&nbsp;&nbsp;&raquo; &nbsp;{{$subcat['name']}}</option>
                                                    <?php foreach ($subcat['subcategories'] as $key => $subsubcat) { ?>
                                                    <option value="{{$subsubcat['id']}}" @if(in_array($subsubcat['id'],$productCats)) selected @endif>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&raquo; &raquo; &nbsp;{{$subsubcat['name']}}</option>
                                                <?php } 
                                                 }
                                                }
                                             } ?>
                                        </select>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Product-cats"></h4>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Is Featured? :</label>
                                    <div class="col-md-6">
                                        <select class="form-control" name="is_featured">
                                            <option value="no" {{(!empty($productdata['is_featured']) && $productdata['is_featured'] =='no')?'selected': '' }}>No</option>
                                            <option value="yes" {{(!empty($productdata['is_featured']) && $productdata['is_featured'] =='yes')?'selected': '' }}>Yes</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Contact Time:<span class="red">*</span></label>
                                    <div class="col-md-6">
                                        <input autocomplete="off" type="text" placeholder="Contact Time" name="contact_time" style="color:gray" class="form-control" value="{{(!empty($productdata['contact_time']))?$productdata['contact_time']: '' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Product-contact_time"></h4>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Frequently Bought: </label>
                                    <div class="col-md-6">
                                        <select class="form-control selectpicker" name="fbt[]" data-size="6" data-live-search="true" multiple="">
                                            @foreach($allproducts as $product)
                                                <option value="{{$product['id']}}" @if(in_array($product['id'],$frequentProducts)) selected @endif>{{$product['product_name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Best Seller: <span class="red">*</span></label>
                                    <div class="col-md-6">
                                        <select name="best_seller" class="selectbox"> 
                                            <?php 
                                                $bestSellerArr = array('no','yes');
                                            foreach ($bestSellerArr as $key => $bestSeller) {?>
                                            <option value="{{$bestSeller}}"@if(isset($productdata['best_seller']) && $productdata['best_seller'] ==$bestSeller) selected @endif>{{ucwords($bestSeller)}}</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Product Price:<span class="red">*</span></label>
                                    <div class="col-md-6">
                                        <input autocomplete="off" type="text" placeholder="Product Price" name="price" style="color:gray" class="form-control" value="{{(!empty($productdata['price']))?$productdata['price']: '' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Product-price"></h4>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Product Discount: </label>
                                    <div class="col-md-5">
                                        <input autocomplete="off" type="text" placeholder="Product Discount" name="discount" style="color:gray" class="form-control" @if(!empty($productdata) && $productdata['discount_type']=="category") value="" @else value="{{(!empty($productdata['discount']))?$productdata['discount']: '' }}" @endif/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Product-discount"></h4>
                                    </div>
                                        <p style="margin-top: 8px;">%</p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Group Code:</label>
                                    <div class="col-md-6">
                                        <input autocomplete="off" min="1"  type="text" placeholder="Group Code" name="group_code" style="color:gray" class="form-control" value="{{(!empty($productdata['group_code']))?$productdata['group_code']: '' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Product-group_code"></h4>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Weight:</label>
                                    <div class="col-md-6">
                                        <input autocomplete="off" step="0.1" min="0.5"  type="number" placeholder="Weight" name="weight" style="color:gray" class="form-control" value="{{(!empty($productdata['weight']))?$productdata['weight']: '' }}"/>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Case Count:</label>
                                    <div class="col-md-6">
                                        <input autocomplete="off"  type="number" placeholder="Case Count" name="case_count" style="color:gray" class="form-control" value="{{(!empty($productdata['case_count']))?$productdata['case_count']: '' }}"/>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Short Description :</label>
                                    <div class="col-md-9">
                                        <textarea rows="3" placeholder="Short Description..." name="short_description" style="color:gray" class="form-control">{{(!empty($productdata['short_description']))?$productdata['short_description']: '' }}</textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Technical Details</label>
                                    <div class="col-md-8">
                                        <textarea class="form-control longSummernote" name="technical_details">{{(!empty($productdata['technical_details']))?$productdata['technical_details']: '' }}</textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Description/ Product Details</label>
                                    <div class="col-md-8">
                                        <textarea class="form-control longSummernote" name="product_description">{{(!empty($productdata['product_description']))?$productdata['product_description']: '' }}</textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Salient Features</label>
                                    <div class="col-md-8">
                                        <textarea class="form-control longSummernote" name="salient_features">{{(!empty($productdata['salient_features']))?$productdata['salient_features']: '' }}</textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Direction of Use </label>
                                    <div class="col-md-8">
                                        <textarea class="form-control longSummernote" name="direction_of_use">{{(!empty($productdata['direction_of_use']))?$productdata['direction_of_use']: '' }}</textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Area of Application </label>
                                    <div class="col-md-8">
                                        <textarea class="form-control longSummernote" name="area_of_application">{{(!empty($productdata['area_of_application']))?$productdata['area_of_application']: '' }}</textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Microbial Efficacy </label>
                                    <div class="col-md-8">
                                        <textarea class="form-control longSummernote" name="microbial_efficacy">{{(!empty($productdata['microbial_efficacy']))?$productdata['microbial_efficacy']: '' }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Product Status :</label>
                                    <div class="col-md-6">
                                        <select class="form-control" name="status">
                                            <option value="0" {{(empty($productdata['status']))?'selected': '' }}>Inactive</option>
                                            <option value="1" {{(!empty($productdata['status']) && $productdata['status'] ==1)?'selected': '' }}>Active</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Is New Arrival? :</label>
                                    <div class="col-md-6">
                                        <select class="form-control" name="new_arrival">
                                            <option value="no" {{(!empty($productdata['new_arrival']) && $productdata['new_arrival'] =='no')?'selected': '' }}>No</option>
                                            <option value="yes" {{(!empty($productdata['new_arrival']) && $productdata['new_arrival'] =='yes')?'selected': '' }}>Yes</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Product GST :</label>
                                    <div class="col-md-6">
                                        <select class="form-control" name="product_gst">
                                            <?php $gstArr = array(5,12,18,28); 
                                            ?>
                                            <option value="">Please Select</option>
                                            @foreach($gstArr as $gstVal)
                                                <option value="{{$gstVal}}" @if(!empty($productdata) && $productdata['product_gst']==$gstVal) selected @endif>{{$gstVal}}%</option>
                                            @endforeach
                                        </select>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Product-product_gst"></h4>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-6 control-label">Product Sort :</label>
                                    <div class="col-md-6">
                                        <input type="number" class="form-control" name="product_sort" value="{{(!empty($productdata['product_sort']))?$productdata['product_sort']: '' }}">
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Product-product_sort"></h4>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                @if(isset($productdata['attributes']) && !empty($productdata['attributes']))
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Attributes:</label>
                                        <div class="col-md-8">
                                            <table class="table table-hover table-bordered table-striped">
                                                <tbody>
                                                    <tr>
                                                        <th width="15%" class="text-center">SKU</th>
                                                        <th width="15%" class="text-center">Size</th>
                                                        <th width="15%" class="text-center">Stock</th>
                                                        <th width="25%" class="text-center">MOQ</th>
                                                        <th width="25%" class="text-center">Price</th>
                                                        <th width="25%" class="text-center">Status</th>
                                                        <th width="25%" class="text-center">#</th>
                                                    </tr>
                                                    @foreach($productdata['attributes'] as $proAttr)
                                                        <input type="hidden" name="attr_id[]" value="{{$proAttr['id']}}">
                                                        <tr class="blockIdWrap">
                                                            <td>{{$proAttr['sku']}}</td>
                                                            <td>{{$proAttr['size']}}</td>
                                                            <td>
                                                                <input type="number" class="form-control" name="attr_stock[]" placeholder="Enter Stock" value="{{$proAttr['stock']}}" />
                                                            </td>
                                                            <td>
                                                                <input type="number" class="form-control" name="attr_moq[]" placeholder="Enter MOQ" value="{{$proAttr['moq']}}" />
                                                            </td>
                                                            <td>
                                                                <input type="number" class="form-control" name="attr_price[]" placeholder="Enter Price" value="{{$proAttr['price']}}" />
                                                            </td>
                                                            <td>
                                                                <input name="radio{{$proAttr['id']}}" data-attrid="{{$proAttr['id']}}" value="yes" class="changeStatus" type="radio" @if($proAttr['status'] ==1) checked @endif> Active
                                                                <input name="radio{{$proAttr['id']}}" data-attrid="{{$proAttr['id']}}" value="no" class="changeStatus" type="radio" @if($proAttr['status'] ==0) checked @endif> Inactive</td>
                                                            <td>
                                                                <a class="btn btn-danger" href="{{url('/admin/remove-attribute/'.$proAttr['id'])}}/{{$productdata['id']}}" onclick="return confirm('Are you sure ?')"><i class="fa fa-times"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Add Attributes Details:</label>
                                    <div class="col-md-8">
                                        <table id="dynamicTable1" class="table table-hover table-bordered table-striped">
                                            <tbody>
                                                <tr>
                                                    <th class="text-center">SKU</th>
                                                    <th class="text-center">Size</th>
                                                    <th class="text-center">Stock</th>
                                                    <th class="text-center">MOQ</th>
                                                    <th class="text-center">Price</th>
                                                    <th class="text-center">Actions</th>
                                                </tr>
                                                @for ($i=1; $i <=1 ; $i++)
                                                <tr class="blockIdWrap">
                                                    <td>
                                                        <input type="text" class="form-control" name="pro_sku[]" placeholder="Enter SKU" />
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="pro_size[]" placeholder="Enter Size" />
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control" name="pro_stock[]" placeholder="Enter Stock" />
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control" name="pro_moq[]" placeholder="Enter MOQ" />
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control" name="pro_price[]" placeholder="Enter Price" />
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                @endfor
                                            </tbody>
                                        </table>
                                        <input type="button" id="addrow" value="Add More" />
                                        <table class="table table-hover table-bordered table-striped samplerow" style="display:none;">
                                            <tbody>
                                                <tr class="appenderTr blockIdWrap">
                                                    <td>
                                                        <input type="text" class="form-control" name="pro_sku[]" placeholder="Enter SKU" />
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="pro_size[]" placeholder="Enter Size" />
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control" name="pro_stock[]" placeholder="Enter Stock" />
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control" name="pro_moq[]" placeholder="Enter MOQ" />
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control" name="pro_price[]" placeholder="Enter Price" />
                                                    </td>
                                                    <td>
                                                        <a title="Remove" class="btn btn-sm red remove" href="javascript:;"> <i class="fa fa-times"></i></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>   
                            </div>
                            @if(!empty($productdata['id']))
                                <input type="hidden" name="productid" value="{{$productdata['id']}}">
                            @else
                                <input type="hidden" name="productid" value="">
                            @endif 
                            <div class="form-actions right1 text-center">
                                <button  id="ProductSubmitBtn" class="btn green" type="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var rowid = 1;
    jQuery("#addrow").click(function() {        
        var row = jQuery('.samplerow tr').clone(true);
        row.appendTo('#dynamicTable1');        
    });
    $('.remove').on("click", function() {
        $(this).parents("tr").remove();
    });
</script>
<script type="text/javascript">
    $('.MultipleSelect option').mousedown(function(e) {
        e.preventDefault();
        $('#addEditProduct').formValidation('revalidateField', 'cats[]');
        var originalScrollTop = $(this).parent().scrollTop();
        console.log(originalScrollTop);
        $(this).prop('selected', $(this).prop('selected') ? false : true);
        var self = this;
        $(this).parent().focus();
        setTimeout(function() {
            $(self).parent().scrollTop(originalScrollTop);
        }, 0);
        
        return false;
    });
</script>
<script type="text/javascript">
    $("#ProductForm").submit(function(e){
        $('.loadingDiv').show();
        e.preventDefault();
         var formdata = new FormData(this);
        $.ajax({
            url: "{{url('/admin/save-product')}}",
            type:'POST',
            data: formdata,
            processData: false,
            contentType: false,
            success: function(data) {
                $('.loadingDiv').hide();
                if(!data.status){
                    $.each(data.errors, function (i, error) {
                        $('#Product-'+i).addClass('error-triggered');
                        $('#Product-'+i).attr('style', '');
                        $('#Product-'+i).html(error);
                        setTimeout(function () {
                            $('#Product-'+i).css({
                                'display': 'none'
                            });
                        $('#Product-'+i).removeClass('error-triggered');
                        }, 5000);
                    });
                    $('html,body').animate({
                        scrollTop: $('.error-triggered').first().stop().offset().top - 200
                    }, 1000);
                }else{
                    window.location.href= data.url;
                }
            }
        });
    });
</script>
<script>
    $(document).on('change','.changeStatus',function(){
        var attrid = $(this).data('attrid');
        var status = $(this).val();
        $.ajax({
            data : {status:status,attrid:attrid},
            url: "{{url('/admin/change-attr-status')}}",
            type : 'post',
            success:function(resp){

            },
            error:function(){
                alert('error');
            }
        })
    })
</script>
@endsection
