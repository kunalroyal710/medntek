@extends('layouts.adminLayout.backendLayout')
@section('content')
<style type="text/css">
    .red{
        color: red;
    }
</style>
<script type="text/javascript" src="{!!asset('js/backend_js/ckeditor/ckeditor.js')!!}"></script>
<script type="text/javascript" src="{!!asset('js/backend_js/ckeditor/adapters/jquery.js')!!}"></script>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Products Management </h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ url('admin/dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{url('/admin/products')}}">Products</a>
            </li>
        </ul>
        @if(Session::has('flash_message_error'))
            <div role="alert" class="alert alert-danger alert-dismissible fade in"> <button aria-label="Close" data-dismiss="alert" style="text-indent: 0;" class="close" type="button"><span aria-hidden="true">×</span></button> <strong>Error!</strong> {!! session('flash_message_error') !!} </div>
        @endif
        @if(Session::has('flash_message_success'))
            <div role="alert" class="alert alert-success alert-dismissible fade in"> <button aria-label="Close" data-dismiss="alert" style="text-indent: 0;" class="close" type="button"><span aria-hidden="true">×</span></button> <strong>Success!</strong> {!! session('flash_message_success') !!} </div>
        @endif
        <div class="row">
            <div class="col-md-12 ">
                <div class="portlet blue-hoki box ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>{{ $title }}
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form id="addEditProduct" role="form" class="form-horizontal" method="post"  action="{{ url('admin/product-images/'.$productid) }}"  enctype="multipart/form-data"> 
                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}"  autocomplete="off" />
                            <div class="form-body">
                                @if(!empty($productImages))
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Product Images:</label>
                                        <div class="col-md-8">
                                            <table  class="table table-hover table-bordered table-striped">
                                                <tbody>
                                                    <tr>
                                                        <th width="15%">Image</th>
                                                        <th width="35%">Sort</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    @foreach($productImages as $key => $image)
                                                    <tr id="delete-{{$image['id']}}">
                                                        <td>
                                                            @if(!empty($image['image']))
                                                            <img width="100px" src="{{asset('images/ProductImages/medium/'.$image['image']) }}"/>
                                                            @else
                                                                N/A
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            <input id="ImageSort-{{$image['id']}}"  type="number" class="form-control" value="{{$image['sort']}}">
                                                            <br>
                                                            <button data-imageid="{{$image['id']}}" class="btn green updateImageSort" type="button"> Update</button>
                                                        </td>
                                                        <td class="text-center">
                                                            <a   id="{{ $image['id'] }}" class="btn btn-danger pImage" href="javascript:void(0);"><i class="fa fa-times"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Add Product Images:</label>
                                    <div class="col-md-8">
                                        <table id="ImageTable" class="table table-hover table-bordered table-striped">
                                            <tbody>
                                                <tr>
                                                    <th>Image</th>
                                                    <th>Image Sort</th>
                                                    <th>Actions</th>
                                                </tr>
                                                @for ($i=1; $i <=1; $i++)
                                                <tr class="blockIdWrap">
                                                    <td>
                                                        <input type="file" class="form-control" name="images[]">
                                                    <td>
                                                        <input type="text" placeholder="Image Sort" name="sort[]" style="color:gray" autocomplete="off" class="form-control"/>
                                                    </td>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                @endfor
                                            </tbody>
                                        </table>
                                        <input type="button" id="addImageRow" value="Add More" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions right1 text-center">
                                <button  id="ProductSubmitBtn" class="btn green" type="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Append Table Rows -->
<table class="table table-hover table-bordered table-striped imagesamplerow" style="display:none;">
    <tbody>
        <tr class="appenderTr blockIdWrap">
            <td>
                <input type="file" class="form-control" name="images[]">
            </td>
            <td>
                <input type="number" placeholder="Image Sort" name="sort[]" style="color:gray" autocomplete="off" class="form-control" required/>
            </td>
            <td>
                <a title="Remove" class="btn btn-sm red imageRowRemove" href="javascript:;"> <i class="fa fa-times"></i></a>
            </td>
        </tr>
    </tbody>
</table>
<!-- Append Table Rows -->
<script type="text/javascript">
    $(document).on('click','.updateImageSort',function(){
        var imageid = $(this).data('imageid');
        var sort = $('#ImageSort-'+imageid).val();
        $.ajax({
            data : {imageid:imageid,sort:sort},
            url : "/admin/update-image-sort",
            type : "get",
            success:function(resp){
                alert('Sort updated successfully');
            },
            error:function(){

            }
        })
    })
</script>
<script type="text/javascript">
    var rowid = 1;
    jQuery("#addImageRow").click(function() {        
        var row = jQuery('.imagesamplerow tr').clone(true);
        row.appendTo('#ImageTable');        
    });
    $('.imageRowRemove').on("click", function() {
        $(this).parents("tr").remove();
    });

    $(document).on('click','.pImage',function(){
        if(confirm('Are you sure?')){
            $(".loadingDiv").show();
            var ProductImageId = $(this).attr('id');
            $.ajax({
                type: "post",
                url: "/admin/delete-product-image",
                data : {id : ProductImageId},
                success:function(resp){
                    if(resp == "success"){
                        $(".loadingDiv").hide();
                        $("#delete-"+ProductImageId).remove();
                        $(".SuccessFader").slideDown();
                        setTimeout(function(){
                          $(".SuccessFader").slideUp();      
                        }, 1500);
                    }
                },
                error:function(resp){
                }
            });
        }
        return false;
    });
</script>
@endsection
