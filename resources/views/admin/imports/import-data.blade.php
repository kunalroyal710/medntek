@extends('layouts.adminLayout.backendLayout')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Imports Management </h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{url('admin/dashboard')}}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
        </ul>
        @if(Session::has('flash_message_error'))
            <div role="alert" class="alert alert-danger alert-dismissible fade in"> <button aria-label="Close" data-dismiss="alert" style="text-indent: 0;" class="close" type="button"><span aria-hidden="true"></span></button> <strong>Error!</strong> {!! session('flash_message_error') !!} </div>
        @endif
        @if(Session::has('flash_message_success'))
            <div role="alert" class="alert alert-success alert-dismissible fade in"> <button aria-label="Close" data-dismiss="alert" style="text-indent: 0;" class="close" type="button"><span aria-hidden="true"></span></button> <strong>Success!</strong> {!! session('flash_message_success') !!} </div>
        @endif
        <div class="row">
            <div class="col-md-12 ">
                <div class="portlet blue-hoki box ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>{{ $title }}
                        </div>
                    </div>
                    <div class="portlet-body form">
                        @if(isset($_GET['type']) && !empty($_GET['type']) && isset($_GET['filename']) && !empty($_GET['filename']))
                            <form class="form-horizontal" method="post" action="{{url('/admin/import-file-data')}}">@csrf
                                <div class="form-group">
                                    <label class="col-md-3 control-label">File :</label>
                                    <div class="col-md-4">
                                        <input class="form-control" type="text" name="filename" value="{{$_GET['filename']}}" readonly>
                                    </div>
                                </div>
                                <input type="hidden" name="type" value="{{$_GET['type']}}">
                                <div class="form-actions right1 text-center">
                                    <button class="btn green" type="submit">Import Now</button>
                                </div>
                            </form>
                        @else
                            <form  role="form" class="form-horizontal" method="post" enctype="multipart/form-data" action="{{url('/admin/import-data')}}"> 
                                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                <div class="form-body"> 
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Select Import Type :</label>
                                        <div class="col-md-4">
                                            <select class="form-control" name="type" required>
                                                <option value="">Please Select</option>
                                                <option value="products">Import Products</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Choose File :</label>
                                        <div class="col-md-4">
                                            <input type="file" name="file" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Samples:</label>
                                        <div class="col-md-6">
                                            <table class="table table-striped table-bordered table-hover">
                                                <tbody>
                                                    <tr>
                                                        <th>Import</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Import Products</td>
                                                        <td><a href="{{url('/samples/products.xls')}}">Download Sample</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-9">
                                            <b>Note:- Please Upload required formats of csv. we have give option of download sample formats above.</b>
                                            <br>
                                            <b>Important Note:- Please add minimum 1 and  maximum 100 rows in one csv file.</b>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions right1 text-center">
                                    <button class="btn green" type="submit">Submit</button>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop