@extends('layouts.adminLayout.backendLayout')
@section('content')
<style>
    .form-control-feedback {
      top: 9px !important;
    }
</style>
<div class="page-content-wrapper">
    @if(Session::has('flash_message_error'))
        <div role="alert" class="alert alert-danger alert-dismissible fade in"> <button aria-label="Close" data-dismiss="alert" style="text-indent: 0;" class="close" type="button"><span aria-hidden="true">×</span></button> <strong>Error!</strong> {!! session('flash_message_error') !!} </div>
    @endif
    @if(Session::has('flash_message_success'))
        <div role="alert" class="alert alert-success alert-dismissible fade in"> <button aria-label="Close" data-dismiss="alert" style="text-indent: 0;" class="close" type="button"><span aria-hidden="true">×</span></button> <strong>Success!</strong> {!! session('flash_message_success') !!} </div>
    @endif
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Coupon's Management </h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ action('Admin\AdminController@dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ action('Admin\CouponController@coupons') }}">Coupons</a>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12 ">
                <div class="portlet blue-hoki box ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>{{ $title }}
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form id="Couponform" action="javascript:;" role="form" class="form-horizontal" method="post"> 
                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                            <div class="form-body">
                                @if(!empty($couponData))
                                    <div  class="form-group">
                                    <label class="col-md-3 control-label">Coupon Code:</label>
                                    <div class="col-md-5" style="margin-top: 8px;">
                                        <span><b>{{ $couponData['code'] }}</b></span>
                                    </div>
                                </div>
                                @else
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Coupon Code :</label>
                                    <div  class="col-md-5" style="margin-top:8px;">
                                        <label>
                                            <input id="Automatic" type="radio" value="Automatic" name="codeoption" checked />Automatic &nbsp;
                                        </label>
                                        <label>
                                            <input id="Manual" type="radio" value="Manual" name="codeoption" />Manual
                                        </label>
                                    </div>
                                </div>
                                <div id="textField" class="form-group" style="display: none;">
                                    <label class="col-md-3 control-label">Enter Code:</label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="Enter code" name="code" style="color:gray" class="form-control"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Coupon-code"></h4>
                                    </div>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Coupon Behaviour :</label>
                                    <div class="col-md-4">
                                        <select class="form-control" name="coupon_behaviour">
                                            <option value="all">All Products & Categories</option>
                                            <option value="category" @if(!empty($couponData) && $couponData['coupon_behaviour'] =="category") selected @endif>Category Wise</option>
                                            <option value="product" @if(!empty($couponData) && $couponData['coupon_behaviour'] =="product") selected  @endif>Product Wise</option>
                                        </select>
                                    </div>
                                </div>
                                <?php $categories = categories(); ?>
                                <div class="form-group" id="CouponCats" @if(!empty($couponData) && !empty($couponData['categories'])) @else style="display: none;" @endif>
                                    <label class="col-md-3 control-label">Select Categories :</label>
                                    <div class="col-md-4">
                                        <select name="categories[]" class="selectpicker" data-live-search="true" data-width="100%" data-actions-box="true" multiple> 
                                            <?php foreach ($categories as $key => $category) {?>
                                            <option value="{{$category['id']}}" @if(in_array($category['id'],$Selcats)) selected @endif>&#9679;&nbsp;{{$category['name']}}</option>
                                            <?php if(!empty($category['subcategories'])){
                                                foreach ($category['subcategories'] as $key => $subcat) { ?>
                                                    <option value="{{$subcat['id']}}" @if(in_array($subcat['id'],$Selcats)) selected @endif>&nbsp;&nbsp;&nbsp;&nbsp;&raquo; &nbsp;{{$subcat['name']}}</option>
                                                    <?php foreach ($subcat['subcategories'] as $key => $subsubcat) { ?>
                                                    <option value="{{$subsubcat['id']}}" @if(in_array($subsubcat['id'],$Selcats)) selected @endif>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&raquo; &raquo; &nbsp;{{$subsubcat['name']}}</option>
                                                <?php } 
                                                }
                                            }
                                        } ?>
                                        </select>
                                    </div>
                                </div>
                                <?php $products = products(); ?>
                                <div class="form-group" id="CouponProducts" @if(!empty($couponData) && !empty($couponData['products'])) @else style="display: none;" @endif>
                                    <label class="col-md-3 control-label">Select Product :</label>
                                    <div class="col-md-4">
                                        <select name="products[]" class="selectpicker" data-live-search="true" data-width="100%" multiple data-size="6"> 
                                            <option value="">Please Select</option>
                                            <?php foreach ($products as $key => $product) {?>
                                            <option value="{{$product['id']}}" @if(in_array($product['id'],$SelProds)) selected @endif>&#9679;&nbsp;{{$product['product_name']}} ({{$product['product_code']}})</option>
                                            
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <?php $users = users();  ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Select Users :</label>
                                    <div class="col-md-4">
                                        @if(!empty($couponData['user_emails']))
                                            <span class="form-control">{{$couponData['user_emails']}}</span>
                                        @else
                                            <select name="user_emails[]" class="selectpicker" data-live-search="true" data-width="100%" multiple="">
                                                @foreach($users as $user)
                                                    <option value="{{$user->email}}">{{$user->email}}</option>
                                                @endforeach
                                            </select>
                                            <b><span>Don't select user in case of coupon for all users</span></b>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Coupon Type :</label>
                                    <div class="col-md-5" style="margin-top:8px;">
                                        @if(!empty($couponData))    
                                            @if($couponData['coupon_type']=="Multiple Times")
                                                <?php  $Mchecked = "checked";
                                                        $Lchecked = "";
                                                        $Schecked  ="";?>
                                            @elseif($couponData['coupon_type']=="Limited Times")
                                                <?php  $Lchecked = "checked";
                                                        $Mchecked = "";
                                                        $Schecked  ="";?>
                                            @else
                                                <?php $Schecked="checked";
                                                        $Lchecked="";
                                                      $Mchecked =""; ?>
                                            @endif
                                        @else
                                            <?php $Schecked=""; 
                                                $Lchecked = "";
                                                  $Mchecked="checked";?>
                                        @endif
                                        <label>
                                            <input type="radio" name="coupon_type" value="Multiple Times" {{ $Mchecked }}/>&nbsp; Multiple Times &nbsp; 
                                        </label>
                                        <label>
                                            <input type="radio" name="coupon_type" value="Single Time" {{ $Schecked }} />&nbsp; Single Time
                                        </label>&nbsp;
                                        <label>
                                            <input type="radio" name="coupon_type" value="Limited Times" {{ $Lchecked }} />&nbsp; Limited Times
                                        </label>
                                    </div>
                                </div>
                                <div id="LimitedTime" class="form-group" @if(!empty($couponData) && !empty($couponData['limited_time'])) @else style="display: none;" @endif>
                                    <label class="col-md-3 control-label">Limited Time <span class="asteric">*</span></label>
                                    <div class="col-md-4">
                                        <input maxlength="10" type="text" placeholder="Limited Time" name="limited_time" style="color:gray" class="form-control" value="{{(!empty($couponData['limited_time']))?$couponData['limited_time']: '' }}" />
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Coupon-limited_time"></h4>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Amount Type :</label>
                                    <div class="col-md-5" style="margin-top:8px;">
                                        @if(!empty($couponData))    
                                            @if($couponData['amount_type']=="Percentage")
                                                <?php  $Perchecked = "checked";
                                                        $Rschecked  ="";?>
                                            @else
                                                <?php $Rschecked="checked";
                                                      $Perchecked =""; ?>
                                            @endif
                                        @else
                                            <?php $Rschecked="checked"; 
                                                  $Perchecked="";?>
                                        @endif
                                        <label>
                                            <input type="radio" name="amount_type" value="Rupees" {{ $Rschecked }}/>Rs. &nbsp;
                                        </label>
                                        <label>
                                            <input type="radio" name="amount_type" value="Percentage" {{ $Perchecked }} />%
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Amount :</label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="Amount" name="amount" style="color:gray" autocomplete="off"  class="form-control" value="{{(!empty($couponData['amount']))?$couponData['amount']: '' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Coupon-amount"></h4>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Coupon Applicable On :</label>
                                    <div class="col-md-4" style="margin-top:8px;">
                                        <?php $applicablesOn = array('all'=>'All','discounted'=>'Discounted','non-discounted'=>'Non Discounted'); ?>
                                        <select class="form-control" name="coupon_applicable_on">
                                            @foreach($applicablesOn as $akey=> $applicable)
                                                <option value="{{$akey}}" {{(!empty($couponData['coupon_applicable_on']) && $couponData['coupon_applicable_on'] ==$akey)?'selected':''}}>{{$applicable}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Select Quantity :</label>
                                    <div class="col-md-2">
                                        <select name="min_qty" style="color:gray" class="form-control">
                                            <option value="">Select Min Qty</option>
                                            <?php for($i=1;$i<=10;$i++) { ?>
                                            <option value="{{$i}}" <?php  if(!empty($couponData['min_qty']) && $couponData['min_qty']==$i) { echo "selected"; } ?>>{{ $i }}</option>
                                            <?php } ?>
                                        </select>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Coupon-min_qty"></h4>
                                    </div>
                                    <div class="col-md-2">
                                        <select name="max_qty" style="color:gray" class="form-control">
                                            <option value="">Select Max Qty</option>
                                            <?php for($j=1;$j<=500;$j++) { ?>
                                            <option value="{{$j}}" <?php  if(!empty($couponData['max_qty']) && $couponData['max_qty']==$j) { echo "selected"; } ?>>{{ $j }}</option>
                                            <?php } ?>
                                        </select>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Coupon-max_qty"></h4>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Enter Price Range :</label>
                                    <div class="col-md-2">
                                        <input type="text" placeholder="Enter Min Amount" name="min_amount" style="color:gray" autocomplete="off"  class="form-control" value="{{(!empty($couponData['min_amount']))?$couponData['min_amount']: '500' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Coupon-min_amount"></h4>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" placeholder="Enter Max Amount" name="max_amount" style="color:gray" autocomplete="off"  class="form-control" value="{{(!empty($couponData['max_amount']))?$couponData['max_amount']: '50000' }}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Start Date :</label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="Start Date" name="start_date" style="color:gray" class="form-control datePicker" autocomplete="off" value="{{(!empty($couponData['start_date']))?$couponData['start_date']: '' }}" />
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Coupon-start_date"></h4>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Expiry Date :</label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="Expiry Date" name="expiry_date" style="color:gray" class="form-control datePicker" autocomplete="off" value="{{(!empty($couponData['expiry_date']))?$couponData['expiry_date']: '' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Coupon-expiry_date"></h4>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Status:</label>
                                    <div class="col-md-1">
                                        <input type="checkbox" style="margin-top:10px;" name="status" value="1" <?php  if(!empty($couponData['status']) && $couponData['status']=="1") { echo "checked"; } ?> />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Visible in Cart:</label>
                                    <div class="col-md-1">
                                        <input type="checkbox" style="margin-top:10px;" name="visible" value="1" <?php  if(!empty($couponData['visible']) && $couponData['visible']=="1") { echo "checked"; } ?> />
                                    </div>
                                </div>
                                @if(!empty($couponData['id']))
                                    <input type="hidden" name="couponid" value="{{$couponData['id']}}">
                                @else
                                    <input type="hidden" name="couponid" value="">
                                @endif           
                            </div>
                            <div class="form-actions right1 text-center">
                                <button class="btn green" type="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
.form-control-feedback{
    top:8px! important;
}
.form-horizontal .form-group {
    margin-left: 0px !important;
}
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('change','[name=coupon_behaviour]',function(){
            var value = $(this).val();
            if(value=="category"){
                $('#CouponCats').show();
                $('#CouponProducts').hide();
            }else if(value=="product"){
                $('#CouponCats').hide();
                $('#CouponProducts').show();
            }else{
                $('#CouponCats').hide();
                $('#CouponProducts').hide();
            }
        })
    })
</script>
<script type="text/javascript">
    $(document).on('change','[name=coupon_type]',function(){
        var couponType = $(this).val();
        if(couponType === 'Limited Times'){
            $("#LimitedTime").show();
        }else{
            $("#LimitedTime").hide();
            $('#addCouponForm').formValidation('removeField','limited_time');
        }
    });
    $("#Couponform").submit(function(e){
        $('.loadingDiv').show();
        e.preventDefault();
         var formdata = new FormData(this);
        $.ajax({
            url: "{{url('/admin/save-coupon')}}",
            type:'POST',
            data: formdata,
            processData: false,
            contentType: false,
            success: function(data) {
                $('.loadingDiv').hide();
                if(!data.status){
                    $.each(data.errors, function (i, error) {
                        $('#Coupon-'+i).addClass('error-triggered');
                        $('#Coupon-'+i).attr('style', '');
                        $('#Coupon-'+i).html(error);
                        setTimeout(function () {
                            $('#Coupon-'+i).css({
                                'display': 'none'
                            });
                        $('#Coupon-'+i).removeClass('error-triggered');
                        }, 5000);
                    });
                    $('html,body').animate({
                        scrollTop: $('.error-triggered').first().stop().offset().top - 200
                    }, 1000);
                }else{
                    window.location.href= data.url;
                }
            }
        });
    });
</script>
@endsection