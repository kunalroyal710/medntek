@extends('layouts.adminLayout.backendLayout')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Brands Management </h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ url('admin/dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ url('admin/brands') }}">Brands </a>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12 ">
                <div class="portlet blue-hoki box ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>{{ $title }}
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form id="Brandform" role="form" class="form-horizontal" method="post" action="javascript:;" enctype="multipart/form-data" autocomplete="off"> 
                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Brand Name <span class="asteric">*</span></label>
                                    <div class="col-md-4">
                                        <input  type="text" placeholder="Brand Name" name="name" style="color:gray" class="form-control" value="{{(!empty($branddata['name']))?$branddata['name']: '' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Brand-name"></h4>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Slug <span class="asteric">*</span></label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="Slug" name="slug"  style="color:gray" class="form-control " value="{{(!empty($branddata['slug']))?$branddata['slug']: '' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Brand-slug"></h4>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">How to add Slug ?</label>
                                    <div class="col-md-8">
                                        <table class="table table-striped table-bordered table-hover">
                                            <tr>
                                                <th>Brand Name</th>
                                                <th>Slug</th>
                                            </tr>
                                            <tr>
                                                <td>Pro-Fab</td>
                                                <td>prro-fab</td>
                                          </tr>
                                        </table>
                                        <b>Note :- Don't enter any Special Characters in Slug execpt (-)</b>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Brand Logo </label>
                                    <div class="col-md-5">
                                        <div data-provides="fileinput" class="fileinput fileinput-new">
                                            <div style="" class="fileinput-new thumbnail">
                                            <?php if(!empty($branddata['brand_logo'])){
                                                $path = "images/BrandImages/".$branddata['brand_logo']; 
                                            if(file_exists($path)) { ?>
                                                <img style="height:100px;widtyh:100px;" class="img-responsive"  src="{{ asset('images/BrandImages/'.$branddata['brand_logo'])}}">
                                            <?php }else{?>
                                                    <img style="height:100px;widtyh:100px;" class="img-responsive"  src="{{ asset('images/default.png') }}">
                                            <?php } } else { ?>
                                            <img style="height:100px;widtyh:100px;" class="img-responsive"  src="{{ asset('images/default.png') }}">
                                            <?php } ?>
                                        </div>
                                            <div style="max-width: 200px; max-height: 150px; line-height: 10px;" class="fileinput-preview fileinput-exists thumbnail">
                                            </div>
                                            <div>
                                                <div class="form-group">
                                                    <span class="btn default btn-file">
                                                    <span class="fileinput-new">
                                                    Select Image </span>
                                                    <span class="fileinput-exists">
                                                    Select Image </span>
                                                    <input type="file" id="Image" name="brand_logo">
                                                    </span>
                                                    <a data-dismiss="fileinput" class="btn default fileinput-exists" href="#">
                                                    Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Sort <span class="asteric">*</span></label>
                                    <div class="col-md-4">
                                        <input type="number" placeholder="Sort" name="sort"  style="color:gray" class="form-control " value="{{(!empty($branddata['sort']))?$branddata['sort']: '' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Brand-sort"></h4>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Description :</label>
                                    <div class="col-md-4">
                                        <textarea placeholder="Description..." name="description"  style="color:gray" class="form-control">{{(!empty($branddata['description']))?$branddata['description']: '' }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Meta Title </label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="Meta title" name="meta_title"  style="color:gray" class="form-control " value="{{(!empty($branddata['meta_title']))?$branddata['meta_title']: '' }}"/>
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Brand-meta_title"></h4>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Meta keywords :</label>
                                    <div class="col-md-4">
                                        <textarea placeholder="Meta keywords..." name="meta_keyword"  style="color:gray" class="form-control">{{(!empty($branddata['meta_keyword']))?$branddata['meta_keyword']: '' }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Meta Description :</label>
                                    <div class="col-md-4">
                                        <textarea placeholder="Meta keywords..." name="meta_description"  style="color:gray" class="form-control">{{(!empty($branddata['meta_description']))?$branddata['meta_description']: '' }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Status <span class="asteric">*</span></label>
                                    <div class="col-md-4" style="margin-top:8px;">
                                        <?php $statusArr = array('1'=>'Active','0'=>'Inactive') ?>
                                        @foreach($statusArr as $skey=> $status)
                                            <label>
                                            <input type="radio" name="status" value="{{$skey}}" @if(!empty($branddata) && $branddata['status'] ==$skey ) checked @endif />&nbsp;{{ucwords($status)}}&nbsp;
                                        </label>
                                        @endforeach
                                        <h4 class="text-center text-danger pt-3" style="display: none;" id="Brand-status"></h4>
                                    </div>
                                </div>  
                                @if(!empty($branddata['id']))
                                    <input type="hidden" name="brandid" value="{{$branddata['id']}}">
                                @else
                                    <input type="hidden" name="brandid" value="">
                                @endif           
                            </div>
                            <div class="form-actions right1 text-center">
                                <button class="btn green" type="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var brandid =""; 
    <?php if(!empty($branddata['id'])){?>
        brandid = "<?php echo $branddata['id']; ?>";
    <?php } ?>
    $("#Brandform").submit(function(e){
        $('.loadingDiv').show();
        e.preventDefault();
         var formdata = new FormData(this);
        $.ajax({
            url: "{{url('/admin/save-brand')}}",
            type:'POST',
            data: formdata,
            processData: false,
            contentType: false,
            success: function(data) {
                $('.loadingDiv').hide();
                if(!data.status){
                    $.each(data.errors, function (i, error) {
                        $('#Brand-'+i).addClass('error-triggered');
                        $('#Brand-'+i).attr('style', '');
                        $('#Brand-'+i).html(error);
                        setTimeout(function () {
                            $('#Brand-'+i).css({
                                'display': 'none'
                            });
                        $('#Brand-'+i).removeClass('error-triggered');
                        }, 5000);
                    });
                    $('html,body').animate({
                        scrollTop: $('.error-triggered').first().stop().offset().top - 200
                    }, 1000);
                }else{
                    window.location.href= data.url;
                }
            }
        });
    });
</script>
@endsection