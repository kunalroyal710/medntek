<div class="page-footer">
	<div class="page-footer-inner">
		<?php echo date('Y');?> &copy;  {{config('constants.project_name')}}
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- include summernote css/js-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
<script src="{!! asset('js/backend_js/admin-script.js?v='.str_random(4)) !!}" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {    
   	Metronic.init(); // init metronic core componets
   	Layout.init(); // init layout
   	Demo.init(); // init demo features 
 	Tasks.initDashboardWidget(); // init tash dashboard widget  
});
</script>
<script type="text/javascript">
    function sendFile(file, el) {
        $('.loadingDiv').show();
        formdata = new FormData();
        formdata.append("file", file);
        $.ajax({
            data: formdata,
            type: "POST",
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) myXhr.upload.addEventListener('progress',progressHandlingFunction, false);
                return myXhr;
            },
            url: "/admin/summernote/upload",
            cache: false,
            contentType: false,
            processData: false,
            success: function(url) {
                $(el).summernote('editor.insertImage', url);
                $('.loadingDiv').hide();
            }
        });
    }

    function progressHandlingFunction(e){
        if(e.lengthComputable){
            $('progress').attr({value:e.loaded, max:e.total});
            // reset progress on complete
            if (e.loaded == e.total) {
                $('progress').attr('value','0.0');
            }
        }
    }
</script>