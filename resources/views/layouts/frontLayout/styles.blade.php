<link rel="shortcut icon" href="{{asset('assets/favicon.ico')}}" type="image/x-icon">
<link rel="icon" href="{{asset('assets/images/favicon.ico')}}" type="image/x-icon">
<link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/css/themify-icons.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/css/elegant-icons.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/css/nice-select.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/css/jquery-ui.min.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/css/slicknav.min.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/css/jquery.fancybox.min.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('assets/css/style.css?v='.rand(1000,5515))}}" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/slick.css')}}"/>
@if(isset($productResp['productdetails']))
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/product_detail_style.css?v='.rand(1000,5515))}}"/>
@endif

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-DEN040BT64"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-DEN040BT64');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MPX582X');</script>
<!-- End Google Tag Manager -->