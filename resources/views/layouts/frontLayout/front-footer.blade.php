<footer class="footer-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="footer-widget">
                    <h5>Products</h5>
                    <ul>
                        @foreach(categories() as $catinfo)
                            <li><a href="{{url($catinfo['seo_unique'])}}">{{$catinfo['name']}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-12">
                <div class="footer-widget">
                    <h5>Information</h5>
                    <ul>
                        <?php $informationArr = footerInformations(); ?>
                        @foreach($informationArr as $information)
                            @if($information['slug'] =="about-us")
                                @php $information['title'] = 'Company'; @endphp
                            @endif
                            <li><a href="{{url($information['slug'])}}">{{$information['title']}}</a></li>
                        @endforeach
                        <li><a href="{{url('media')}}">Media</a></li>
                        <li><a target="_blank" href="https://www.imaecmedntek.com/blogs">Blog</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="footer-widget">
                    <h5>Customer Care</h5>
                    <ul>
                        <li><a href="{{url('/track-order')}}">Track Order</a></li>
                        <?php $informationArr = footerCustomerCare(); ?>
                        @foreach($informationArr as $information)
                            <li><a href="{{url($information['slug'])}}">{{$information['title']}}</a></li>
                        @endforeach
                    </ul>
                    <a href="{{url('contact')}}" class="button_1 ftrsmallbtn"><span>FEEDBACK</span></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="footer-widget newslatter-item">
                    <h5>Sign Up for Newsletter</h5>
                    <span class="alert alert-success SuccessFader" style="display: none; float: left; width: 100%;"></span>
                    <span class="alert alert-danger FailureFader" style="display: none; float: left; width: 100%;"></span>
                    <form id="Subscribe" method="post" action="javascript:;" autocomplete="off" class="subscribe-form">
                        <input id="subscriber"  name="email" type="email" placeholder="Enter your email">
                        <button type="submit">
                            <img src="{{asset('assets/images/submitflyicon.svg')}}"></button>
                        <span id="thanksSubscribe" style="display: none; color: green"></span>
                    </form>
                    <h5 class="topmarg">Find Us Now</h5>
                    <div class="social-media">
                        <a href="https://twitter.com/imaecmedntekltd" target="_blank" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="https://www.facebook.com/imaecmednteklimited" target="_blank" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="https://www.youtube.com/channel/UCWLEzPQd8zql2WVebMvKuxg" target="_blank" class="youtube"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                        <a href="https://www.linkedin.com/company/imaec-medntek-limited/" target="_blank" class="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        <!-- <a href="https://www.instagram.com/" target="_blank" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a> -->
                    </div>
                    <h5 class="topmarg">Payments</h5>
                    <img src="{{asset('assets/images/payment.png')}}" alt="payment">
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-reserved">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="footer-links">
                        <ul>
                            <li><a href="{{url('/privacy-policy')}}">Privacy Policy</a></li>
                            <li><a href="{{url('/disclaimer')}}">Disclaimer Policy</a></li>
                            <li class="footer-links-last-child"><a href="javascript:;">Sitemap</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="copyright-text">© {{date('Y')}} IMAEC MEDNTEK. All Rights Reserved.</div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <!-- <div class="powered-by">
                        <a href="https://www.ikf.co.in" target="_blank" title="Web Design, SEO, Digital Marketing Company in Pune, India - IKF" rel="follow">
                            <img alt="Web Design, SEO, Digital Marketing Company in Pune, India - IKF" data-src="{{asset('assets/images/ikf-logo.png')}}" class=" ls-is-cached lazyloaded" src="{{asset('assets/images/ikf-logo.png')}}">
                            <noscript><img src="{{asset('assets/images/ikf-logo.png')}}" alt="Web Design, SEO, Digital Marketing Company in Pune, India - IKF" /></noscript>
                        </a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</footer>
<a id="back-to-top" href="#" class="backtotop" role="button">&nbsp;</a>