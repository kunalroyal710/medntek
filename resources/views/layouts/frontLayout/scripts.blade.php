<script src="{{asset('assets/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.zoom.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.dd.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.slicknav.js')}}"></script>
<script src="{{asset('assets/js/jquery.fancybox.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.elevatezoom.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
<!-- Slick -->
<script type="text/javascript" src="{{asset('assets/js/jquery-migrate-1.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/slick.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/front.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom.js?v='.rand(100,5000))}}"></script>
<script>
    jQuery('.home-banner-div .owl-dots button').append('<svg class="circle-svg" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg"><circle class="circle-svg__circle" cx="50" cy="50" r="45"></circle></svg>');
</script>
<script>
  jQuery(document).ready(function(){
    $(document).on('click','[data-quantity="plus"]',function(){
        var cartid = $(this).data('cartid');
        var currentVal = parseInt($('#Cart-'+cartid).val());
        if (!isNaN(currentVal)) {
            // Increment
            $('#Cart-'+cartid).val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $('#Cart-'+cartid).val(0);
        }
    });

    $(document).on('click','[data-quantity="minus"]',function(){
        var cartid = $(this).data('cartid');
        var currentVal = parseInt($('#Cart-'+cartid).val());
        if (!isNaN(currentVal)) {
            currentVal = currentVal - 1;
            $('#Cart-'+cartid).val(currentVal);
            if(currentVal <=0){
                $('#Cart-'+cartid).val(1);
            }
        } else {
            // Otherwise put a 0 there
            $('#Cart-'+cartid).val(0);
        }
    });
});
</script>
<script type = "text/javascript">
	$(document).ready(function () {
		var time = 1;
		var $bar,
			$slick,
			isPause,
			tick,
			percentTime;
		$slick = $('.disinfectants_carousel');
		$slick.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			fade: true,
			autoplay: false,
			slide: '.dis1_inner_ctn',
			dots: true,
			asNavFor: '.disinfectants_navigation',
			dotsClass: 'custom_paging',
			customPaging: function (slider, i) {
				console.log(slider);
				return (i + 1) + '/' + '<div>' + slider.slideCount + '</div>';
			}
		});
		$('.disinfectants_navigation').slick({
			slidesToShow: 7,
			slidesToScroll: 1,
			asNavFor: '.disinfectants_carousel',
			dots: false,
			centerMode: true,
			autoplay: false,
			focusOnSelect: true,
			infinite: false,
		});

		$bar = $('.disinfectants .slider-progress .progress');

		function startProgressbar() {
			resetProgressbar();
			percentTime = 0;
			isPause = false;
			tick = setInterval(interval, 50);
		}

		function interval() {
			if (isPause === false) {
				percentTime += 1 / (time + 0.1);
				$bar.css({
					width: percentTime + "%"
				});
				if (percentTime >= 100) {
					$slick.slick('slickNext');
					startProgressbar();
				}
			}
		}

		function resetProgressbar() {
			$bar.css({
				width: 0 + '%'
			});
			clearTimeout(tick);
		}
		startProgressbar();
	});

	// Certificate Slider
	jQuery('.certificate-slider').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		autoplay: false,
		dots: false,
		variableWidth: true
	}); 
</script>
<script>

$(document).ready(function(){
  var time = 3;
  var $bar1,
      $slick1,
      isPause1,
      tick1,
      percentTime1;
  
  $slick1 = $('.personslide');
  $slick1.slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    fade: false,
    autoplay: false,
    dots: true,
    responsive: [
      {
        breakpoint: 999,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 820,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 520,
        settings: {
          slidesToShow: 1
        }
      }
    ],
    dotsClass: 'custom_paging',
    customPaging: function (slider, i) {
        console.log(slider);
        return (i + 1) + '/' + '<div>'+ slider.slideCount +'</div>';
    } 
    
  });

  $bar1 = $('.usp-wrapper .slider-progress .progress');
  
  function startProgressbar() {
    resetProgressbar();
    percentTime1 = 0;
    isPause1 = false;
    tick1 = setInterval(interval, 17);
  }
  
  function interval() {
    if(isPause1 === false) {
      percentTime1 += 1 / (time+0.1);
      $bar1.css({
        width: percentTime1+"%"
      });
      if(percentTime1 >= 100)
        {
          $slick1.slick('slickNext');
          startProgressbar();
        }
    }
  }
    
  function resetProgressbar() {
    $bar1.css({
     width: 0+'%' 
    });
    clearTimeout(tick1);
  }
  
  startProgressbar();
  
  jQuery('.toptabs li').click(function(){   
    jQuery('.toptabli').removeClass('active');
    jQuery('.showslide').removeClass('showslide');
    var getlitab = jQuery(this).attr('id');
    var getlitabch = getlitab.replace('tab_','');
    jQuery(this).addClass('active');
    jQuery('#'+getlitabch).addClass('showslide');
  });
});
jQuery( document ).ready(function() {
    jQuery('.faq-box .faq-wrapper .item .faqtitle').on('click', function(){
        var parent = jQuery(this).parent('div.item');
        jQuery('.faq-box .faq-wrapper .item').not(parent).removeClass('current');
        if(jQuery(parent).hasClass('current')) {
          jQuery(parent).removeClass('current');  
        } else {
          jQuery(parent).addClass('current');  
        } 
    });
    jQuery(".enquire_Now").click(function() {
        jQuery('html, body').animate({
            scrollTop: jQuery("#career_form_section").offset().top
        }, 2000);
    });
  });
</script>
@if(env('APP_ENV')=="live")
<!--Start of Tidio Script-->
<script src="//code.tidio.co/hss2hpnkakxjzl2iv4v235ivolwjj3kd.js" async></script>
<!--End of Tidio Script-->
@endif

@yield('javascript')