<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>@if(isset($title)) {{$title}} @else 404 Error, Page Not Found @endif - {{config('constants.project_name')}} </title>
    @if(isset($metadescription))
        <meta name="description" content="{{$metadescription}}">
    @endif
    @if(isset($metakeywords))
        <meta name="keywords" content="{{$metakeywords}}">
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
	@include('layouts.frontLayout.styles')
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
</head>
@if(isset($productResp['productdetails']))
<body class="product-template-default single single-product postid-36119 wp-embed-responsive theme-hongo woocommerce woocommerce-page woocommerce-no-js hongo-single-product-sticky-wrap wpb-js-composer js-comp-ver-6.6.0 vc_responsive">
@else
<body>
@endif
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MPX582X"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

    <!-- End Google Tag Manager (noscript) -->
    <div class="PleaseWaitDiv" style="display:none;">
        <b><p style="color: #000;" id="PleaseWaitText">Please wait...</p></b>
    </div>
	@include('layouts.frontLayout.front-header')
    	@yield('content')
    @include('layouts.frontLayout.front-footer')
    @include('layouts.frontLayout.scripts')
    @include('layouts.frontLayout.modals')
</body>
</html>