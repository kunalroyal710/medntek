<!-- Modal -->
<div class="modal fade searchpop" data-backdrop="false" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body search-modal">
				<div class="search-modal-inner">
					<div class="container">
						<h2>What would you like to know about?</h2>
						<form role="search" method="get" id="searchform" class="searchform" action="{{url('/search-results')}}" autocomplete="off">
							<div>
								<label class="screen-reader-text" for="s">Search for:</label> 
									<input placeholder="Search products here..." type="text" name="q" id="s" onkeyup="stoppedTyping(this.value)">
								<input type="submit" id="searchsubmit" value="Search">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Coming soon modal -->
<div class="coming-modal">
<div id="comingsoonModal" class="modal  modal-popup" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Coming Soon</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>This Feature is Coming Soon</p>
      </div>
     <!--  <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div> -->
    </div>
  </div>
</div>
</div>