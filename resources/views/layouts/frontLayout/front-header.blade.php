<header class="header-section">
    <div class="header-top">
        <div class="container">
            <div class="ht-left"></div>
            <div class="ht-right">
                <a href="{{url('contact')}}" class="login-panel leftbrdr">Customer Care</a>
                <a href="{{url('track-order')}}" class="login-panel">Track Order</a>
                <div id="mobile-menu-wrap"></div>
                <ul class="nav-right">
                    @if(Auth::check())
                        <li class="heart-icon main-cart">
                            <a href="javascript:;"><img src="{{asset('assets/images/user-icon.svg')}}" alt="user-icon"></a>
                            <ul class="menu-cart-list">
                                <!-- <li><a href="javascript:;">Hello, {{Auth::user()->name}}</a></li> -->
                                <li><a href="{{url('/account/my-profile')}}">My Profile</a></li>
                                <li><a href="{{url('/account/my-orders')}}">My Orders</a></li>
                                <li><a href="{{url('/account/wishlists')}}">Wishlists</a></li>
                                <li><a href="{{url('/account/settings')}}">Settings</a></li>
                                <li><a href="{{url('/logout')}}">Logout</a></li>
                            </ul>
                        </li>
                    @else
                         <li class="heart-icon main-cart">
                            <a href="{{url('/signin')}}"><img src="{{asset('assets/images/user-icon.svg')}}" alt="user-icon"></a>
                        </li>
                        <a href="{{url('signin')}}" class="login-panel">Sign In</a>
                    @endif
                    <li class="cart-icon">
                        <a href="javascript:;" class="cartItems"><img src="{{asset('assets/images/cart-icon.svg')}}" alt="cart-icon" title="Cart"><span class="supnum totalItems"></span> </a>
                        <span  id="CartItemsAjax">
                        </span>
                        
                    </li>
                    @if(Auth::check())
                        <li class="heart-icon">
                            <a href="{{url('account/wishlists')}}"><img src="{{asset('assets/images/wishlist-icon.svg')}}" alt="wishlist-icon" title="Wishlist"></a>
                        </li>
                    @else
                        <!-- <li class="heart-icon">
                            <a href="{{url('account/wishlists')}}" data-toggle="modal" data-target="#comingsoonModal"><img src="{{asset('assets/images/wishlist-icon.svg')}}" alt="wishlist-icon" title="Wishlist"></a>
                        </li> -->
                    @endif
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="inner-header">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="logo">
                        <a href="{{action('Front\IndexController@home')}}"><img src="{{asset('assets/images/medntek.svg')}}" alt="medntek.svg')}}"></a>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 nav-item">
                    <nav class="nav-menu mobile-menu">
                        <ul>
                            @if(checkCmsActive('about-us'))
                            <li>
                                <a href="{{url('about-us')}}">Company</a>
                                <ul class="dropdown">
                                    <li><a href="{{url('about-us')}}">About Us</a></li>
                                    @if(checkCmsActive('leadership'))
                                        <li><a href="{{url('/leadership')}}">Leadership Team</a></li>
                                    @endif
                                </ul>
                            </li>
                            @endif
                            <li>
                                <a href="javascript:;">Products</a>
                                <ul class="dropdown">
                                    @foreach(categories() as $category)
                                        <li>
                                            <a href="{{url($category['seo_unique'])}}">{{$category['name']}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            @if(checkCmsActive('careers'))
                                <li><a href="{{url('careers')}}">Careers</a></li>
                            @endif
                            @if(checkCmsActive('infrastructure'))
                                <li><a href="{{url('infrastructure')}}">Infrastructure</a></li>
                            @endif
                            @if(checkCmsActive('contact'))
                                <li><a href="{{url('contact')}}">Contact</a></li>
                            @endif
                            @if(checkCmsActive('contact'))
                                <li class="mobile-view-only"><a href="javascript:;" class="login-panel leftbrdr">Customer Care</a></li>
                            @endif
                            <li class="mobile-view-only"><a href="javascript:;" data-toggle="modal" data-target="#comingsoonModal" class="login-panel">Track Order</a></li>
                        </ul>
                    </nav>
                    <div class="search-icon">
                        <a href="javascript:;" class="" data-toggle="modal" data-target="#exampleModal"><img src="{{asset('assets/images/search-icon.svg')}}" alt="search-icon"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>