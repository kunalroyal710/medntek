<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \DB::table('admins')->delete();
        $adminItems = [
            ['id'=>1,'name'=>'Super Admin','type'=>'admin','mobile'=>'9876543210','email'=>'admin@medntek.com','password'=>'$2y$10$tI4A2.vIL2SBsudgCmit2ezVE0E9aHooko3ukOFIBsIAhCbn5zjby','image'=>'7167.jpg','status'=>1,'remember_token'=>'']
        ];
        foreach ($adminItems as $key => $item) {
            \App\Models\Admin::create($item);

        }
    }
}
