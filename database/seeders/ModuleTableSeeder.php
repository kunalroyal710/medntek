<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \DB::table('modules')->delete();
        \DB::select("INSERT INTO `modules` (`id`, `name`, `parent_id`, `view_route`, `edit_route`, `delete_route`, `icon`, `session_value`, `status`, `sortorder`, `shown_in_roles`, `table_name`, `created_at`, `updated_at`) VALUES
        (1, 'Users Management', 'ROOT', '', '', '', 'fa fa-users', 'subadmins,users', 1, 1, '0', '', '2021-04-07 23:24:12', '2021-04-07 23:24:12'),
        (2, 'Subadmins', '1', 'admin/subadmins', 'admin/add-edit-subadmin/{id?}', '', '', 'subadmins', 1, 1, '1', 'admins', '2021-04-07 23:24:12', '2021-04-07 23:24:12'),
        (3, 'Users', '1', 'admin/users', 'admin/add-edit-user/{id?}', '', '', 'users', 1, 2, '1', 'users', '2021-04-07 23:24:12', '2021-04-07 23:24:12'),
        (4, 'Catalogue Management', 'ROOT', '', '', '', 'fa fa-shopping-cart', 'brands,products,categories,productreviews', 1, 3, '0', '', '2021-04-07 23:24:12', '2021-04-07 23:24:12'),
        (5, 'Brands', '4', 'admin/brands', 'admin/add-edit-brand/{id?}', '', 'fa fa-list', 'brands', 1, 1, '1', 'brands', '2021-04-07 23:24:12', '2021-04-07 23:24:12'),
        (6, 'Categories', '4', 'admin/categories', 'admin/add-edit-category/{id?}', '', 'fa fa-list', 'categories', 1, 2, '1', 'categories', '2021-04-07 23:24:12', '2021-04-07 23:24:12'),
        (7, 'Products', '4', 'admin/products', 'admin/add-edit-product/{id?}', '', 'fa fa-list', 'products', 1, 3, '1', 'products', '2021-04-07 23:24:12', '2021-04-07 23:24:12'),
        (8, 'Offers Management', 'ROOT', '', '', '', 'fa fa-book', 'coupons', 1, 4, '0', '', '2021-04-07 23:24:12', '2021-04-07 23:24:12'),
        (9, 'Coupon Codes', '8', 'admin/coupons', 'admin/add-edit-coupon/{id?}', '', 'fa fa-list', 'coupons', 1, 1, '1', 'coupon_codes', '2021-04-07 23:24:12', '2021-04-07 23:24:12'),
        (10, 'Orders Management', 'ROOT', '', '', '', 'fa fa-book', 'orders', 1, 5, '0', '', '2021-04-07 23:24:12', '2021-04-07 23:24:12'),
        (11, 'Orders', '10', 'admin/orders', '', '', '', 'orders', 1, 1, '1', 'orders', '2021-04-07 23:24:12', '2021-04-07 23:24:12'),
        (12, 'Banner Images', '13', 'admin/banner-images', 'admin/add-edit-banner-image/{id?}', '', 'fa fa-list', 'banners', 1, 1, '1', 'banner_images', '2021-04-07 23:24:12', '2021-04-07 23:24:12'),
        (13, 'Data Management', 'ROOT', '', '', '', 'fa fa-book', 'banners,cmspages,subscribers', 1, 6, '0', '', '2021-04-07 23:24:12', '2021-04-07 23:24:12'),
        (14, 'CMS Pages', '13', 'admin/cms-pages', 'admin/add-edit-cms-page/{id}', '', 'fa fa-list', 'cmspages', 1, 1, '1', 'cms_pages', '2021-04-07 17:54:12', '2021-04-07 17:54:12'),
        (15, 'Product Reviews', '4', 'admin/product-reviews', '', '', 'fa fa-list', 'productreviews', 1, 3, '1', 'product_reviews', '2021-04-07 23:24:12', '2021-04-07 23:24:12'),
        (16, 'Subscribers', '13', 'admin/subscribers', '', '', 'fa fa-list', 'subscribers', 1, 1, '1', 'subscribers', '2021-04-07 17:54:12', '2021-04-07 17:54:12')");
        /*$moduleItems = [
        	['id'=>1,'name'=>'Users Management','parent_id'=>'ROOT','view_route'=>'','edit_route'=>'','delete_route'=>'','icon'=>'fa fa-users','session_value'=>'subadmins,users','shown_in_roles'=>0,'table_name'=>'','sortorder'=>1,'status'=>1],
        	['id'=>2,'name'=>'Subadmins','parent_id'=>'1','view_route'=>'admin/subadmins','edit_route'=>'admin/add-edit-subadmin/{id?}','delete_route'=>'','icon'=>'','session_value'=>'subadmins','shown_in_roles'=>1,'table_name'=>'admins','sortorder'=>1,'status'=>1],
        	['id'=>3,'name'=>'Users','parent_id'=>'1','view_route'=>'admin/users','edit_route'=>'admin/add-edit-user/{id?}','delete_route'=>'','icon'=>'','session_value'=>'users','shown_in_roles'=>1,'table_name'=>'users','sortorder'=>2,'status'=>1],
        	['id'=>4,'name'=>'Catalogue Management','parent_id'=>'ROOT','view_route'=>'','edit_route'=>'','delete_route'=>'','icon'=>'fa fa-shopping-cart','session_value'=>'brands,products,categories','shown_in_roles'=>0,'table_name'=>'','sortorder'=>3,'status'=>1],
            ['id'=>5,'name'=>'Brands','parent_id'=>'4','view_route'=>'admin/brands','edit_route'=>'admin/add-edit-brand/{id?}','delete_route'=>'','icon'=>'fa fa-list','session_value'=>'brands','shown_in_roles'=>1,'table_name'=>'brands','sortorder'=>1,'status'=>1],
        	['id'=>6,'name'=>'Categories','parent_id'=>'4','view_route'=>'admin/categories','edit_route'=>'admin/add-edit-category/{id?}','delete_route'=>'','icon'=>'fa fa-list','session_value'=>'categories','shown_in_roles'=>1,'table_name'=>'categories','sortorder'=>2,'status'=>1],
        	['id'=>7,'name'=>'Products','parent_id'=>'4','view_route'=>'admin/products','edit_route'=>'admin/add-edit-product/{id?}','delete_route'=>'','icon'=>'fa fa-list','session_value'=>'products','shown_in_roles'=>1,'table_name'=>'products','sortorder'=>3,'status'=>1],
        	['id'=>8,'name'=>'Offers Management','parent_id'=>'ROOT','view_route'=>'','edit_route'=>'','delete_route'=>'','icon'=>'fa fa-book','session_value'=>'coupons,offers','shown_in_roles'=>0,'table_name'=>'','sortorder'=>4,'status'=>1],
            ['id'=>9,'name'=>'Coupon Codes','parent_id'=>'8','view_route'=>'admin/coupons','edit_route'=>'admin/add-edit-coupon/{id?}','delete_route'=>'','icon'=>'fa fa-list','session_value'=>'coupons','shown_in_roles'=>1,'table_name'=>'coupon_codes','sortorder'=>1,'status'=>1],
        ];

        foreach ($moduleItems as $key => $item) {
        	\App\Models\Module::create($item);

        }*/
    }
}
