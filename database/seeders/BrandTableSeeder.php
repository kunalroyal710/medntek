<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class BrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('brands')->delete();
        DB::select("INSERT INTO `brands` (`id`, `name`, `brand_logo`, `description`, `meta_title`, `meta_keyword`, `meta_description`, `sort`, `slug`, `status`, `created_at`, `updated_at`) VALUES
            (1, 'Medntek', '', 'medntek', '', '', '', 1, 'medntek', 1, '2021-03-01 09:10:12', '2021-03-01 09:10:12')");
    }
}
