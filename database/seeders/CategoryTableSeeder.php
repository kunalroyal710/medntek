<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->delete();
        DB::select("INSERT INTO `categories` (`id`, `parent_id`, `name`, `image`, `description`, `category_discount`, `meta_title`, `meta_keyword`, `meta_description`, `sort`, `seo_unique`, `status`, `created_at`, `updated_at`) VALUES
        (1, NULL, 'Disinfectants', '', 'Disinfectants', 0.00, '', '', '', 1, 'disinfectants', 1, '2021-03-01 07:18:17', '2021-03-01 07:18:17'),
        (2, NULL, 'Dialysis consumables', '', 'Dialysis consumables', 0.00, '', '', '', 1, 'dialysis-consumables', 1, '2021-03-01 07:18:17', '2021-03-01 07:18:17'),
        (3, NULL, 'Disposable Garments', '', 'Disposable Garments', 0.00, '', '', '', 1, 'disposable-garments', 1, '2021-03-01 07:18:17', '2021-03-01 07:18:17'),
        (4, NULL, 'Wound Dressing', '', 'Wound Dressing', 0.00, '', '', '', 1, 'wound-dressing', 1, '2021-03-01 07:18:17', '2021-03-01 07:18:17'),
        (5, NULL, 'Three fill Syringes', '', 'Three fill Syringes', 0.00, '', '', '', 1, 'three-fill-syringes', 1, '2021-03-01 07:18:17', '2021-03-01 07:18:17'),
        (6, NULL, 'Pharma Injectables', '', 'Pharma Injectables', 0.00, '', '', '', 1, 'pharma-injectables', 1, '2021-03-01 07:18:17', '2021-03-01 07:18:17')");
    }
}
