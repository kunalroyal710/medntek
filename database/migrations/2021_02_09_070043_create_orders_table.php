<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('txn_id');
            $table->string('mihpayid');
            $table->unsignedbigInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('mobile');
            $table->string('country');
            $table->string('state');
            $table->string('city');
            $table->string('postcode');
            $table->string('address_line_1');
            $table->string('address_line_2');
            $table->string('payment_method');
            $table->string('country_code');
            $table->string('currency');
            $table->float('currency_rate');
            $table->string('currency_symbol',50);
            $table->string('coupon_code');
            $table->float('subtotal');
            $table->float('coupon_discount');
            $table->float('prepaid_discount');
            $table->float('prepaid_discount_per');
            $table->float('cod_charges');
            $table->float('shipping_charges');
            $table->float('taxes');
            $table->float('grand_total');
            $table->string('payment_status');
            $table->string('order_status');
            $table->string('delivery_method');
            $table->string('awb_number');
            $table->string('invoice_no');
            $table->string('invoice_no_str');
            $table->string('invoice_date');
            $table->string('comments');
            $table->text('payment_response');
            $table->string('ip_address');
            $table->string('api_run',10);
            $table->text('manifest_resp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
