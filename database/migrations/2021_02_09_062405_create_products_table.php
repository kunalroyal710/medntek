<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedbigInteger('category_id')->index();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->unsignedbigInteger('brand_id')->index()->nullable();
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
            $table->string('product_name');
            $table->string('contact_time');
            $table->string('product_code');
            $table->string('weight');
            $table->integer('case_count');
            $table->text('product_description');
            $table->text('technical_details');
            $table->text('short_description');
            $table->text('salient_features');
            $table->text('direction_of_use');
            $table->text('area_of_application');
            $table->text('microbial_efficacy');
            $table->string('discount_type',20);
            $table->float('price');
            $table->float('discount');
            $table->float('final_price');
            $table->integer('stock');  
            $table->string('group_code');    
            $table->string('best_seller');  
            $table->string('new_arrival');  
            $table->integer('product_gst');  
            $table->integer('product_sort');  
            $table->string('is_featured',10)->default('no');  
            $table->tinyinteger('status');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
