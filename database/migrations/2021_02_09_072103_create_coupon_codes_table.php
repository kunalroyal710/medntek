<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_codes', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('codeoption');
            $table->string('coupon_behaviour');
            $table->string('coupon_applicable_on');
            $table->text('categories');
            $table->text('products');
            $table->text('user_emails');
            $table->text('coupon_type');
            $table->float('amount');
            $table->string('amount_type');
            $table->integer('limited_time');
            $table->integer('min_qty');
            $table->integer('max_qty');
            $table->float('min_amount');
            $table->float('max_amount');
            $table->string('start_date');
            $table->string('expiry_date');
            $table->tinyInteger('status');
            $table->string('visible');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_codes');
    }
}
