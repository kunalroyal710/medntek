<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->id();
            $table->unsignedbigInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedbigInteger('order_id')->index();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->unsignedbigInteger('product_id')->index();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->string('product_code');
            $table->string('product_sku');
            $table->string('product_name');
            $table->string('product_size');
            $table->float('mrp');
            $table->float('discount');
            $table->string('discount_type');
            $table->float('product_price');
            $table->integer('product_qty');
            $table->float('subtotal');
            $table->float('grand_total');
            $table->string('coupon_used');
            $table->float('discount_rate');
            $table->float('line_discount');
            $table->float('line_shipping_charges');
            $table->float('line_prepaid_discount');
            $table->float('unit_price');
            $table->float('line_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_products');
    }
}
