<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductAttributeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_attribute_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedbigInteger('product_attribute_id')->index();
            $table->foreign('product_attribute_id')->references('id')->on('product_attributes')->onDelete('cascade');
            $table->string('stn_inv_no');
            $table->string('stn_inv_date');
            $table->string('material_code');
            $table->string('material_name');
            $table->string('batch_no');
            $table->float('qty');
            $table->float('free_qty');
            $table->float('rate_per_unit');
            $table->float('amt_before_tax');
            $table->float('gst');
            $table->float('total_inv_amt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_attribute_details');
    }
}
