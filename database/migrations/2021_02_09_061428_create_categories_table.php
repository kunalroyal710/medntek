<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->unsignedbigInteger('parent_id')->nullable();
            $table->string('name');
            $table->string('image');
            $table->text('description');
            $table->float('category_discount');
            $table->string('meta_title');
            $table->string('meta_keyword');
            $table->string('meta_description');
            $table->integer('sort');
            $table->string('seo_unique')->unique();
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
