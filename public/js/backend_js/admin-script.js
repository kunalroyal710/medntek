jQuery(document).ready(function() { 
    $.ajaxSetup({
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });    
    TableAjax.init();
    $(document).on('click','.toogle_switch',function(){
        if($(this).hasClass('bootstrap-switch-on')){
            $(this).removeClass('bootstrap-switch-on');
            $(this).addClass('bootstrap-switch-off');
            var status=0;
            var id_sent=$(this).attr('id');
        }
        else{
            $(this).removeClass('bootstrap-switch-off');
            $(this).addClass('bootstrap-switch-on');
            var status=1;
            var id_sent=$(this).attr('id');
        }
        var table = $(this).attr('rel');
        var ajax_url='status';
        $.ajax({
            url:ajax_url,
            type:'POST',
            data:{
                'id':id_sent,'status':status, 'table':table
            },
            success:function(msg) {
            }
        })
    });

    $('#change_pass').formValidation({
        framework: 'bootstrap',
        message: 'This value is not valid',
        icon:{
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        err:{
            container: 'popover'
        },
        fields:{
            "password":{
                validators:{
                    notEmpty:{
                        message: 'Current password is required'
                    },
                    remote:{
                        message: 'Current password is incorrect',
                        url: '/admin/checkAdminPassword',
                        type: 'POST',
                        delay: 1000     // Send Ajax request every 2 seconds
                    }
                }
            },
            "new_password":{
                validators:{
                    notEmpty:{
                        message: 'New password is required'
                    }
                }
            },
            "re_password":{
                validators:{
                    notEmpty:{
                        message: 'Confirm Password  is required'
                    },
                    identical:{
                        field: "new_password",
                        message: 'Confirm Password is not match with New Password'
                    }
                }
            }
        }
    });

    // Coupon Validation Starts
    $("#Manual").click(function(){
        $("#textField").show();
    });

    $("#Automatic").click(function(){
        $("#textField").hide();
        $('#addCouponForm').formValidation('removeField','code');
    });

    /*SubAdmin Roles Scripts starts*/
    $(document).on('change','.getModuleid',function(){
        var roleType = $(this).attr('data-attr');
        var id = $(this).attr('rel');
        if(roleType === "View"){
            $('#edit-'+id).prop('checked',false);
            $('#delete-'+id).prop('checked',false);
        }else if(roleType==="Edit"){
            $('#view-'+id).prop('checked',true);
            $('#delete-'+id).prop('checked',false);
        }else if(roleType==="Delete"){
            $('#view-'+id).prop('checked',true);
            $('#edit-'+id).prop('checked',true);
        }
    });
    /*SubAdmin Roles Scripts ends*/
    
    //SubAdmin Validations
    $('#addEditSubadmin').formValidation({
        framework: 'bootstrap',
        excluded: [':disabled'],
        message: 'This value is not valid',
        icon:{
            /*valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',*/
            validating: 'glyphicon glyphicon-refresh'
        },
        err:{
            container: 'popover'
        },
        fields:{
            "name":{
                validators:{
                    notEmpty:{
                        message: 'This field is required'
                    },
                }
            },
            "username":{
                validators:{
                    notEmpty:{
                        message: 'This field is required'
                    },
                    remote:{
                        message: 'This username already exists.',
                        url: '/admin/checkAdminUsername',
                        type: 'POST',
                        delay: 2000     // Send Ajax request every 2 seconds
                    }
                }
            },
            "email":{
                validators:{
                    notEmpty:{
                        message: 'This field is required'
                    },
                    emailAddress:{
                        message: 'This Email is not a valid email address'
                    },
                }
            },
            "password":{
                validators:{
                    notEmpty:{
                        message: 'This field is required'
                    },
                    stringLength:{
                        min: 6,
                        max: 10,
                        message: 'The password must be more than 5 letters.'
                    },
                }
            },
        }
    })
    .on('err.field.fv', function(e, data) {
        data.fv.disableSubmitButtons(false);
    })
    .on('success.field.fv', function(e, data) {
        data.fv.disableSubmitButtons(false);
    });

    $('.datePicker')
        .datepicker({
        format: 'yyyy-mm-dd'/*,
        startDate: new Date()*/
    }).on('changeDate', function(e) {
        $(this).datepicker('hide');
    });

    $(".shortSummernote").summernote({
        placeholder: 'Enter description...',
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
        ],
        height: 200,
        callbacks: {
            onImageUpload : function(files, editor, welEditable) {
                for(var i = files.length - 1; i >= 0; i--) {
                    sendFile(files[i], this);
                }       
            }
        }
    })

    $(".longSummernote").summernote({
        placeholder: 'Enter description...',
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            [ 'insert', [ 'link','picture'] ],
            ['view', ['fullscreen', 'codeview', 'help']]
        ],
        fontNames: ['Open Sans','Courier New', 'Helvetica','Roboto Slab'],
        fontNamesIgnoreCheck: ['Open Sans','Courier New', 'Helvetica','Roboto Slab'],
        height: 200,
        callbacks: {
            onImageUpload : function(files, editor, welEditable) {
                for(var i = files.length - 1; i >= 0; i--) {
                    sendFile(files[i], this);
                }       
            }
        }
    });
    
});