jQuery(document).ready(function() { 
    $.ajaxSetup({
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }); 
});
//Newsletter Subscribe
$("#Subscribe").on("submit",function(){
    var email = $("#subscriber").val();
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var resp= regex.test(email);
    if(resp ==false){
        $(".FailureFader").text('Please enter vaild Email address'); 
        $(".FailureFader").slideDown();
        setTimeout(function(){
            $(".FailureFader").slideUp();      
        }, 1500);
        return false;
    }
    $("#Subscribebtn").prop('disabled', true);
    var formdata = $(this).serialize();
    $.ajax({
        url:'/add-subscriber',
        data : formdata,
        type : 'post',
        dataType : 'json',
        success:function(resp){
            $("#subscriber").val('');
            if(resp.status =="ok"){
                $(".SuccessFader").text(resp.message); 
                $(".SuccessFader").slideDown();
                setTimeout(function(){
                    $(".SuccessFader").slideUp();      
                }, 1500);
            }else{
                $(".FailureFader").text(resp.message); 
                $(".FailureFader").slideDown();
                setTimeout(function(){
                    $(".FailureFader").slideUp();      
                }, 1500);
            }
            $("#Subscribebtn").prop('disabled', false);
        },
        error:function(){}
    })
});