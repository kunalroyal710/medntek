$(document).ready(function(){
    $.ajaxSetup({
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }); 
    //Login
    $("#SignInForm").submit(function(e){
        e.preventDefault();
        $('.PleaseWaitDiv').show();
        var formdata = $("#SignInForm").serialize();
        $.ajax({
            url: "/signin",
            type:'POST',
            data: formdata,
            success: function(data) {
                $('.PleaseWaitDiv').hide();
                if(!data.status){
                    $.each(data.errors, function (i, error) {
                        //$('#Login-'+i).find('.tooltip-inner').html(error);
                        $('#Login-'+i).attr('style', '');
                        $('#Login-'+i).html(error);
                        setTimeout(function () {
                            $('#Login-'+i).css({
                                'display': 'none'
                            });
                        }, 3000);
                    });
                }else{
                    window.location.href= data.url;
                }
            }
        });
    });
    //Register 
    $("#RegisterForm").submit(function(e){
        e.preventDefault();
        $('.PleaseWaitDiv').show();
        var formdata = $("#RegisterForm").serialize();
        $.ajax({
            url: "/signup",
            type:'POST',
            data: formdata,
            success: function(data) {
                $('.PleaseWaitDiv').hide();
                if(!data.status){
            		if(data.type=="validation"){
            			$.each(data.errors, function (i, error) {
            			 	$('#Register-'+i).attr('style', '');
            			 	$('#Register-'+i).html(error);
                            $('#Register-'+i).addClass('error-triggered');
            			 	setTimeout(function () {
            			 		$('#Register-'+i).css({
            			 			'display': 'none'
            			 		});
            			 	}, 3000);
    		            });
                        $('html,body').animate({
                            scrollTop: $('.error-triggered').first().stop().offset().top - 200
                        }, 1000);
            		}else{
            			var msg = [];
                        msg[0] = data.errors;
            			printErrorMsg(msg);
                    	$('.print-error-msg').delay(3000).fadeOut('slow');
            		}
            	}else{
            		window.location.href= data.url;
            	}
            }
        });
    });

     //Forgot Password
    $("#ForgotPwdForm").submit(function(e){
        e.preventDefault();
        $('.PleaseWaitDiv').show();
        var formdata = $("#ForgotPwdForm").serialize();
        $.ajax({
            url: "/forgot-password",
            type:'POST',
            data: formdata,
            success: function(data) {
                $('.PleaseWaitDiv').hide();
                if(!data.status){
                    if(data.type=="validation"){
                        $.each(data.errors, function (i, error) {
                            $('#forgot-'+i).attr('style', '');
                            $('#forgot-'+i).html(error);
                            setTimeout(function () {
                                $('#forgot-'+i).css({
                                    'display': 'none'
                                });
                            }, 3000);
                        });
                    }
                }else{
                    $('.PleaseWaitDiv').hide();
                    printSuccessMsg(data.message);
                    $('.print-success-msg').delay(2000).fadeOut('slow');
                    $("#ForgotPwdForm").trigger("reset");
                    setTimeout(function () {
                        $('#popupforgot').hide();
                        $('#overlay-back-forgot').fadeOut(500);
                    }, 2500)
                }
            }
        });
    });

    $(document).on('click','.addTocartListing',function(){
        $('.PleaseWaitDiv').show();
        var proid = $(this).data('proid');
        var size = $(this).data('size');
        $.ajax({
            url: '/add-to-cart',
            type:'POST',
            data: {proid:proid,size:size,qty:1},
            success: function(data) {
                $('.PleaseWaitDiv').hide();
                if(!data.status){
                    alert(data.message);
                }else{
                    $('.totalItems').html(data.totalItems);
                    $(".cartItems").trigger("click");
                    window.scrollTo(0, 0);
                }
            }
        });
    })

    $(document).on('click','.addWishList',function(){
        $('.PleaseWaitDiv').show();
        var proid = $(this).data('productid');
        $.ajax({
            data : {
                "proid":proid
            },
            type : 'post',
            url : '/add-to-wishlist',
            success:function(resp){ 
                if(resp.status){
                    if(resp.message ==='set'){
                        $("#WishPro-"+proid).attr("src",'/assets/images/wishlist-fill.jpg');
                    }else if(resp.message ==='unset'){
                        $("#WishPro-"+proid).attr("src",'/assets/images/wishlist.png');
                    }
                }else{
                    alert(resp.message);
                }
                $('.PleaseWaitDiv').hide();
            },
            error:function(){
            }
        }); 
    });

    $("#proReview").submit(function(e){
        e.preventDefault();
        $('.PleaseWaitDiv').show();
        var formdata = $("#proReview").serialize();
        $.ajax({
            url: "/submit-product-review",
            type:'POST',
            data: formdata,
            success: function(data) {
                $('.PleaseWaitDiv').hide();
                if(!data.status){
                    alert(data.message);
                }else{
                    alert(data.message);
                    $('#proReview')[0].reset();
                }
            }
        });
    });

    $('#ship_pincode').on('keyup', function() {
        if (this.value.length >= 4) {
            var pincode = $(this).val();
            $.ajax({
                type : 'get',
                data : {pincode:pincode},
                url : '/get-state-city',
                success:function(resp){
                    $('#ship_state').val(resp.state);
                    $('#ship_city').val(resp.city);
                },
                error:function(){
                    //nothing to do
                }
            })
        }
    });

    $(document).on('click','.cartItems',function(){
        cartItemsAjax();
    })

    $(document).on('click','#closeCart',function(){
        $('#CartItemsAjax').css("display","none");
    })

    $(document).on('click','.removeCart',function(){
        $('.PleaseWaitDiv').show();
        var cartid = $(this).data('cartid');
        $.ajax({
            url: "/remove-cart-item",
            type:'POST',
            data : {cartid:cartid},
            success: function(data) {
                $('.PleaseWaitDiv').hide();
                $('#CartItemsAjax').css("display","block");
                $('#CartItemsAjax').html(data.view);
                $('.totalItems').text(data.totalItems);
            }
        });
    })

    $('body').click(function(){    
        $('#CartItemsAjax').css("display","none");
    });
});


function cartItemsAjax(){
    $.ajax({
        url: "/cart-items-ajax",
        type:'GET',
        success: function(data) {
            $('#CartItemsAjax').css("display","block");
            $('#CartItemsAjax').html(data.view);
            $('.totalItems').text(data.totalItems);
        }
    });
}
totalcartitems();
function totalcartitems(){
    $.ajax({
        url: "/cart-items-ajax",
        type:'GET',
        success: function(data) {
            $('.totalItems').text(data.totalItems);
        }
    });
}

function printErrorMsg(msg){
    $(".print-error-msg").find("ul").html('');
    $(".print-error-msg").css('display','block');
    $.each( msg, function( key, value ) {
        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
    });
}

function printSuccessMsg(msg){
    $(".print-success-msg").find("ul").html('');
    $(".print-success-msg").css('display','block');
    $.each( msg, function( key, value ) {
        $(".print-success-msg").find("ul").append('<li>'+value+'</li>');
    });
}


 $(document).on('click', '.toggle-password', function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $("#pass_log_id");
    input.attr('type') === 'password' ? input.attr('type','text') : input.attr('type','password')
});
 $(document).on('click', '.toggle-password1', function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $("#account-password");
    input.attr('type') === 'password' ? input.attr('type','text') : input.attr('type','password')
});
  $(document).on('click', '.toggle-password2', function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $("#pass-3");
    input.attr('type') === 'password' ? input.attr('type','text') : input.attr('type','password')
});
   $(document).on('click', '.toggle-password3', function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $("#pass-4");
    input.attr('type') === 'password' ? input.attr('type','text') : input.attr('type','password')
});
    $(document).on('click', '.toggle-password4', function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $("#pass-5");
    input.attr('type') === 'password' ? input.attr('type','text') : input.attr('type','password')
});