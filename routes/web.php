<?php
use Illuminate\Support\Facades\Schema;
use App\Models\Category;
use Illuminate\Support\Facades\Route;
use App\Models\CmsPage;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/
Auth::routes();

/*Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');*/

Auth::routes();

/*Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');*/

Route::prefix('/admin')->namespace('App\Http\Controllers\Admin')->group(function(){
	Route::match(['get','post'],'/','AdminController@login');
	Route::get('logout','AdminController@logout');
	Route::group(['middleware' => ['admin']], function () {
		Route::match(['get', 'post'], '/dashboard', 'AdminController@dashboard');
		Route::match(['get', 'post'], '/profile', 'AdminController@profile');
		Route::match(['get', 'post'], '/settings', 'AdminController@settings');
		Route::match(['get', 'post'], '/change-picture', 'AdminController@changeAdminLogo');
		Route::match(['get', 'post'], '/update-password', 'AdminController@changeAdminPassword');
		Route::match(['get', 'post'], '/checkAdminPassword', 'AdminController@checkAdminPassword');
		Route::match(['get', 'post'], '/status', 'AdminController@status');

		//Subadmin Routes
		Route::match(['get', 'post'], '/subadmins', 'AdminController@subadmins');
		Route::match(['get', 'post'], '/add-edit-subadmin/{id?}', 'AdminController@addeditSubadmin');
		Route::match(['get', 'post'], '/change-subadmin-password', 'AdminController@changeSudadminPassword');
		Route::match(['get', 'post'], '/update-role/{id}', 'AdminController@updateRole');
		Route::match(['get', 'post'], '/checkAdminUsername', 'AdminController@checkAdminUsername');
		//User Routes
		Route::match(['get', 'post'], '/users', 'UsersController@users');
		Route::match(['get', 'post'], '/export-users', 'ReportsController@exportUsers');
		//Brand Routes
		Route::match(['get', 'post'], '/brands', 'BrandController@brands');
		Route::match(['get', 'post'], '/add-edit-brand/{id?}', 'BrandController@addEditBrand');
		Route::match(['get', 'post'], '/save-brand', 'BrandController@saveBrand');
		//Category Routes
		Route::match(['get', 'post'], '/categories', 'CategoryController@categories');
		Route::match(['get', 'post'], '/add-edit-category/{id?}', 'CategoryController@addEditCategory');
		Route::match(['get', 'post'], '/save-category', 'CategoryController@saveCategory');

		//Coupon Routes
		Route::match(['get', 'post'], '/coupons', 'CouponController@coupons');
		Route::match(['get', 'post'], '/add-edit-coupon/{id?}', 'CouponController@addEditCoupon');
		Route::match(['get', 'post'], '/save-coupon', 'CouponController@saveCoupon');
		Route::match(['get', 'post'], '/delete-coupon/{id}', 'CouponController@deleteCoupon');

		//Product Routes
		Route::match(['get', 'post'], '/products', 'ProductController@products');
		Route::match(['get', 'post'], '/add-edit-product/{id?}', 'ProductController@addEditProduct');
		Route::match(['get', 'post'], '/save-product', 'ProductController@saveProduct');
		Route::match(['get', 'post'], '/change-attr-status', 'ProductController@ChangeAttrStatus');
		Route::match(['get', 'post'], '/remove-attribute/{attrid}/{productid}', 'ProductController@removeAttribute');
		Route::match(['get', 'post'], '/product-images/{id}', 'ProductController@productImages');
		Route::match(['get', 'post'], '/update-image-sort', 'ProductController@updateImageSort');
		Route::match(['get', 'post'], '/delete-product-image', 'ProductController@deleteProductImage');

		//Import Mangement
		Route::match(['get', 'post'], '/import-data', 'ImportController@importData');
		Route::match(['get', 'post'], '/import-file-data', 'ImportController@importFileData');

		//Orders Routes
		Route::match(['get', 'post'], '/orders', 'OrdersController@orders');
		Route::get('/order-view/{id}', 'OrdersController@orderview');
		Route::match(['get', 'post'], '/order-invoice/{id}', 'OrdersController@vieworderInvoice');
		Route::match(['get', 'post'], '/invoice/{id}', 'OrdersController@invoice');
		Route::post('/update-shipping-address/{id}', 'OrdersController@updateShippingAddress');
		Route::match(['get', 'post'], '/update-order-status/{id}', 'OrdersController@updateOrderStatus');
		Route::match(['get', 'post'], '/export-orders', 'ReportsController@exportorders');

		Route::match(['get', 'post'], '/cancel-requests', 'ReturnController@cancelRequests');
		Route::match(['get', 'post'], '/return-requests', 'ReturnController@returnRequests');
		Route::match(['get', 'post'], '/update-return-request', 'ReturnController@updateReturnRequest');

		//Banner Routes
		Route::match(['get', 'post'], '/banner-images', 'BannerController@bannerImages');
		Route::match(['get', 'post'], '/add-edit-banner-image/{id?}', 'BannerController@addEditBannerImage');
		Route::match(['get', 'post'], '/delete-banner-image/{id}', 'BannerController@deleteBannerImage');

		Route::match(['get', 'post'], '/cms-pages', 'AdminController@cmspages');
		Route::match(['get', 'post'], '/edit-cms-page/{id}', 'AdminController@editCmsPage');
		Route::match(['get','post'],'/product-reviews','ProductController@productReviews');

		//Subscriber Routes
		Route::match(['get', 'post'], '/subscribers', 'AdminController@subscribers');
		Route::match(['get', 'post'], '/export-subscribers', 'ReportsController@exportSubscribers');

		Route::post('summernote/upload', 'SummernoteController@upload');

		Route::match(['get', 'post'], '/cms-pages', 'AdminController@cmspages');
		Route::match(['get', 'post'], '/edit-cms-page/{id}', 'AdminController@editCmsPage');

		/*Department Routes Starts*/
		Route::match(['get', 'post'], '/departments', 'DepartmentController@departments');
		Route::match(['get', 'post'], '/add-edit-department/{id?}', 'DepartmentController@addEditDepartment');
		Route::match(['get', 'post'], '/save-department', 'DepartmentController@saveDepartment');
		Route::match(['get', 'post'], '/current-openings', 'DepartmentController@currentOpenings');
		Route::match(['get', 'post'], '/add-edit-current-opening/{id?}', 'DepartmentController@addEditCurrentOpening');
		Route::match(['get', 'post'], '/save-current-opening', 'DepartmentController@saveCurrentOpening');
		/*Department Routes Ends*/
	});
});


Route::namespace('App\Http\Controllers\Front')->group(function(){
	//Route::get('/', 'IndexController@coming_soon');
	Route::get('/', 'IndexController@home');
	Route::get('/login', function () {
	    return redirect('/signin');
	});
	Route::post('login', [ 'as' => 'login', 'uses' => 'UserController@signin']);
	Route::get('/register', function () {
	    return redirect('/signin');
	});
	Route::match(['get','post'],'/forgot-password','UserController@forgotPassword');
	Route::match(['get','post'],'/signup','UserController@signup');
	Route::match(['get','post'],'/signin','UserController@signin');
	Route::match(['get','post'],'/logout','UserController@logout');
	Route::match(['post'],'/add-to-cart','CartController@addtoCart');
	Route::match(['post'],'/update-cart','CartController@updateCart');
	Route::match(['get'],'/cart-items-ajax','CartController@cartItemsAjax');
	Route::match(['get'],'/cart','CartController@cart');
	Route::match(['post'],'/remove-cart-item','CartController@removeCartItem');
	Route::match(['post'],'/apply-coupon','CartController@applyCoupon');
	/*Route::match(['get','post'],'/careers','IndexController@careers');*/
	Route::match(['get','post'],'/save-career','IndexController@saveCareer');
	Route::match(['get','post'],'/media','IndexController@media');

	Route::get('/product/{id}/{slug}', 'ProductsController@product');
	Route::post('/get-product-pricing', 'ProductsController@getProductPricing');
	Route::match(['get','post'],'/search-results','ProductsController@searchProducts');
	if (Schema::hasTable('categories')) {
		//Category Route
		$catSlugs = categories();
		foreach($catSlugs as $cat){
			Route::get('/'.$cat['seo_unique'],'ProductsController@products');
		}
	}
	Route::match(['get','post'],'/add-to-wishlist','ProductsController@addtoWishlist');
	$cmsArray = array('about-us','leadership','infrastructure','quality','privacy-policy','disclaimer','returns-replacements','cancellation','payments-refunds','shipping-delivery','account-settings','careers','contact');
	foreach($cmsArray as $cmsinfo){
		Route::get('/'.$cmsinfo,'IndexController@cms');
	}

	/*Route::get('/contact','IndexController@contact');*/
	Route::post('/save-contact','IndexController@saveContact');
	Route::match(['get','post'],'/add-subscriber','IndexController@addSubscriber');
	Route::match(['get','post'],'/track-order','UserController@trackOrder');

	Route::group(['middleware' => ['auth']], function () {
		Route::get('account/{slug}','UserController@account');
		Route::post('submit-account-details','UserController@submitAcountDetails');
		Route::post('change-password','UserController@changePassword');
		Route::get('remove-wishlist/{id}','UserController@removeWishlist');
		Route::post('return-order-item','UserController@returnOrderItem');
		Route::get('/checkout','CartController@checkout');

		//address Routes
		Route::match(['get','post'],'/save-address','UserController@saveAddress');
		Route::get('/get-delivery-address','UserController@getDeliveryAddress');
		Route::get('/set-default-address','UserController@setDefaultAddress');
		Route::get('/get-state-city','UserController@getStateCity');
		Route::get('/remove-delivery-address','UserController@removeDeliveryAddress');
		Route::post('/place-order','CartController@placeOrder');
	});
	Route::post('/submit-product-review','ProductsController@submitProductReview');
	Route::get('/get-state-city','UserController@getStateCity');
	Route::get('/thanks','CartController@thanks');
	Route::get('/cancel','CartController@cancel');

	Route::match(['get','post'],'/ccavenue/payment','CcavenueController@ccavenuePayment');
	Route::match(['get','post'],'/ccavenue/response','CcavenueController@ccavenueresponse');
	Route::match(['get','post'],'/ccavenue/cancel','CcavenueController@ccavenueCancel');

	Route::get('/google/redirect', 'SocialAuthController@googleredirect');
	Route::get('/google/callback', 'SocialAuthController@googlecallback');
});